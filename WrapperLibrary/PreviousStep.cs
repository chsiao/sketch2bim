﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Petzold.Media3D;
using System.Windows.Media.Media3D;
using WrapperLibrary.RDF;

namespace WrapperLibrary
{
    public enum StepTypeEnum
    {
        Add,
        Delete,
        AxisTransform,
        ObjectTransform,
        modifypath,
        modifysolid
    }
    public enum StepObjectTypeEnum
    {
        None = 0,
        Line = 1,
        MeshModel = 2,
        Transform = 4
    }
    public class Step
    {
        public Step()
        {
            ModifiedObjs = new List<object>();
        }
        public StepTypeEnum PStep { get; set; }
        public StepObjectTypeEnum ObjectType { get; set; }
        //public WireBase PreLine { get; set; }
        //public GeometryModel3D PreMeshModel { get; set; }
        public RDF_GeometricItem PreModel { get; set; }
        public Transform3D AxisTransform { get; set; }
        public Point3D ObjectTranslation { get; set; }
        public List<object> ModifiedObjs { get; set; }
    }
}
