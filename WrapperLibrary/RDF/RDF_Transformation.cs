﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Media;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Xml.Serialization;
using GeomLib.Constraint;

namespace WrapperLibrary.RDF
{
    public class RDF_Transformation : RDF_Abstract, GeometryProduct
    {
        public RDF_GeometricItem TransformGeom { set; get; }
        public RDF_MatrixAbstract Matrix { set; get; }
        [XmlIgnore]
        public Viewport3D viewport;
        [XmlIgnore]
        public Model3DGroup group;
        public TransformationConstraint Constraint { set; get; }
        public GeometryModel3D Geometry { set; get; }
        public DependencyObject GeometryObject { get; set; }
        public RDF_Transformation()
            :base()
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Transformation");
            this.Constraint = new TransformationConstraint(this);
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }
        public RDF_Transformation(string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Transformation");
            this.Constraint = new TransformationConstraint(this);
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }

        internal RDF_Transformation(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.Constraint = new TransformationConstraint(this);
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }
        internal RDF_Transformation(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.Constraint = new TransformationConstraint(this);
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }

        void Constraint_onSolvingConstraint(Constraint constrainObj, object curvalue)
        {
            // move the subject according to the different position
            RDF_Node3D locNode = null;
            Vector3D direction = new Vector3D();
            switch (((GeomLib.Constraint.TransformationConstraint)constrainObj).ConstraintName)
            {
                case GeomLib.TransformationConstraintEnum.TopSurface:
                    locNode = (RDF_Node3D)curvalue;
                    // some where on the surface
                    // the line segment (RDF_Line3D) decide the orientation of the surface
                    // the transformation contains 2 RDF_Matrix, but we only wants to modify the first one
                    if (locNode.PreEdge != null)
                        direction = locNode.PreEdge.GetVector3D();
                    break;
                case GeomLib.TransformationConstraintEnum.BottomSurface:
                    locNode = (RDF_Node3D)curvalue;
                    if (locNode.PreEdge != null)
                        direction = -1 * locNode.PreEdge.GetVector3D();
                    break;
                case GeomLib.TransformationConstraintEnum.EdgeSurface:
                    break;
            }
            if (locNode != null)
            {
                if (this.Matrix is RDF_MatrixMultiplication)
                {
                    var fstMtx = ((RDF_MatrixMultiplication)this.Matrix).FirstMatrix;
                    var fstMtx3D = fstMtx.Matrix3D;
                    var newLoc = locNode.GetPoint3D();
                    fstMtx3D.Invert();
                    newLoc = fstMtx3D.Transform(newLoc);
                    fstMtx3D.Invert();
                    Vector3D translation = new Vector3D(direction.X * newLoc.X, direction.Y * newLoc.Y, direction.Z * newLoc.Z);
                    this.Matrix.MoveRDFGeoms(translation);
                }
                // call RDF_MatrixAbstract.TransformRDFGeoms() to populate the changes
            }
        }

        public void SetTransformGeom(RDF_GeometricItem geom, bool isSetBackPointer = true)
        {
            this.TransformGeom = geom;
            if (isSetBackPointer)
                geom.Transform = this; // back pointer
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "object", geom.PtrToConcept);
        }
        public void SetMatrix(RDF_MatrixAbstract matrix)
        {
            this.Matrix = matrix;
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "matrix", matrix.PtrToConcept);
            // store the back pointer in matrix
            if (this.Matrix != null)
                this.Matrix.RelatedTransforms.Add(this);
        }
        public Matrix3D GetMatrix3D()
        {
            if (this.Matrix != null)
                return this.Matrix.Matrix3D;
            else
                return new Matrix3D();
        }
        internal void Remove()
        {
            if (this.viewport != null)
            {
                if (this.Geometry != null)
                {
                    
                }
                else if (this.GeometryObject != null && this.GeometryObject is Petzold.Media3D.WireBase)
                {
                    // remove the line geometry
                    this.viewport.Children.Remove((Petzold.Media3D.WireBase)this.GeometryObject);
                }
               
            }

            if (this.Matrix is RDF_MatrixMultiplication)
            {

            }
            bool isRemoved = this.Matrix != null ? this.Matrix.RelatedTransforms.Remove(this) : false;
            // Todo: need to remove the objects in its hiearachy

            // remove the objects in the RDF container
            if(RDF.RDFWrapper.All_RDF_Objects.ContainsKey(this.GUID))
                RDF.RDFWrapper.All_RDF_Objects.Remove(this.GUID);
        }
    }
}
