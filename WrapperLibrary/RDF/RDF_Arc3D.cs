﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public class RDF_Arc3D : RDF_Segment3D
    {
        public int SegParts { internal set; get; }
        public double Radius { internal set; get; }
        public double Size { internal set; get; }
        public double StartAngle { internal set; get; }
        public double EndAngle
        {
            get
            {
                return this.StartAngle + this.Size;
            }
        }

        public RDF_Arc3D(string guid = "", int segpart = 36)
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Arc3D");
            this.SetSegNums(segpart);
        }

        internal RDF_Arc3D(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            assignProperties();
            // overwrite the data from the rule
            assignRules();
        }

        internal RDF_Arc3D(SELECTED__CONCEPT__STRUCT sconcept, string guid)
            : base(sconcept, guid)
        {
            assignProperties();
            // overwrite the data from the rule
            assignRules();
        }

        private void assignProperties()
        {
            foreach (DatatypeProperty prop in this.propertylist)
            {
                if (prop.ValueData != null || prop.FormulaData.Value != null)
                {
                    var value = prop.ValueData != null ? prop.ValueData.DoubleValue : prop.FormulaData.Value.DoubleValue;
                    switch (prop.Name)
                    {
                        case "start":
                            this.StartAngle = value;
                            break;
                        case "size":
                            this.Size = value;
                            break;
                        case "segmentationParts":
                            this.SegParts = (int)value;
                            break;
                        case "radius":
                            this.Radius = value;
                            break;
                    }
                }
            }
        }

        private void assignRules()
        {
            foreach (Rule rule in this.rulelist)
            {
                switch (rule.LHS.Name)
                {
                    case "start":
                        this.StartAngle = rule.RHS.Value.DoubleValue;
                        break;
                    case "size":
                        this.Size = rule.RHS.Value.DoubleValue;
                        break;
                    case "segmentationParts":
                        this.SegParts = (int)rule.RHS.Value.DoubleValue;
                        break;
                    case "radius":
                        this.Radius = rule.RHS.Value.DoubleValue;
                        break;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ang">angle in radians</param>
        public void SetStartAngle(double ang)
        {
            this.StartAngle = ang;
            RDFWrapper.ChangeRule(RDFWrapper.FilePtr, this.PtrToConcept, "start", this.StartAngle.ToString());
        }

        public void SetRadius(double r)
        {
            this.Radius = r;
            RDFWrapper.ChangeRule(RDFWrapper.FilePtr, this.PtrToConcept, "radius", this.Radius.ToString());
        }

        public void SetSegNums(int segnum)
        {
            this.SegParts = segnum;
            RDFWrapper.ChangeRule(RDFWrapper.FilePtr, this.PtrToConcept, "segmentationParts", this.SegParts.ToString());
        }

        public void SetSize(double size)
        {
            this.Size = size;
            RDFWrapper.ChangeRule(RDFWrapper.FilePtr, this.PtrToConcept, "size", this.Size.ToString());
        }

        public Vector3D GetStartVector3D()
        {
            Vector3D v = new Vector3D(Math.Cos(this.StartAngle), Math.Sin(this.StartAngle), 0);
            Vector3D z = new Vector3D(0, 0, -1);
            Vector3D s = Vector3D.CrossProduct(v, z);
            if (this.StartAngle < 0)
                s = -s;

            return s;
        }
        public Vector3D GetEndVector3D()
        {
            Vector3D v = new Vector3D(Math.Cos(this.EndAngle), Math.Sin(this.EndAngle), 0);
            Vector3D z = new Vector3D(0, 0, 1);
            Vector3D s = Vector3D.CrossProduct(v, z);
            if (this.StartAngle < 0)
                s = -s;
            return s;
        }

        public double? GetLength()
        {
            return this.Radius * this.Size;
        }
        public Point3D GetCenter()
        {
            Point3D startPt = this.StartNode.GetPoint3D();
            Vector3D v = new Vector3D(Math.Cos(this.StartAngle), Math.Sin(this.StartAngle), 0);
            v = -v * this.Radius;

            return startPt + v;
        }

        public void ChangeStartingPoint(Point3D startPt)
        {
            Point3D endPt = this.EndNode.GetPoint3D();
            Point3D segCenter = new Point3D((startPt.X + endPt.X) / 2, (startPt.Y + endPt.Y) / 2, (startPt.Z + endPt.Z) / 2);
            Vector3D newVector = endPt - startPt;
            Vector3D z = new Vector3D(0, 0, 1);

            Vector3D newRadiusVector = Vector3D.CrossProduct(newVector, z);
            Vector3D startVector = new Vector3D(Math.Cos(this.StartAngle), Math.Sin(this.StartAngle), 0);
            startVector = -startVector;
            
            Point3D[] seg1 = { segCenter, segCenter + newRadiusVector };
            Point3D[] seg2 = { startPt, startPt + startVector};

            System.Drawing.PointF newCenter;
            if (BasicGeomLib.IsTwoLineIntersected(seg1, seg2, out newCenter))
            {
                double newR = newCenter.distanceTo(new System.Drawing.PointF((float)startPt.X, (float)startPt.Y));
                this.SetRadius(newR);

                Vector3D endVector = endPt - new Point3D(newCenter.X, newCenter.Y, endPt.Z);
                double newSize = Vector3D.AngleBetween(endVector, startVector) * Math.PI / 180;
                this.SetSize(newSize);
            }
        }

        public void ChangeEndingPoint(Point3D endPt)
        {
            Point3D startPt = this.StartNode.GetPoint3D();
            Point3D segCenter = new Point3D((startPt.X + endPt.X) / 2, (startPt.Y + endPt.Y) / 2, (startPt.Z + endPt.Z) / 2);
            Vector3D newVector = endPt - startPt;
            Vector3D z = new Vector3D(0, 0, 1);

            Vector3D newRadiusVector = Vector3D.CrossProduct(newVector, z);
            Vector3D endVector = new Vector3D(Math.Cos(this.EndAngle), Math.Sin(this.EndAngle), 0);
            endVector = -endVector;

            Point3D[] seg1 = { segCenter, segCenter + newRadiusVector };
            Point3D[] seg2 = { endPt, endPt + endVector };

            System.Drawing.PointF newCenter;
            if (BasicGeomLib.IsTwoLineIntersected(seg1, seg2, out newCenter))
            {
                double newR = newCenter.distanceTo(new System.Drawing.PointF((float)endPt.X, (float)endPt.Y));
                this.SetRadius(newR);

                Vector3D startVector = startPt - new Point3D(newCenter.X, newCenter.Y, startPt.Z);
                double newSize = Vector3D.AngleBetween(endVector, startVector);
                this.SetSize(newSize);
            }
        }

#if DEBUG
        public Point3D GetCalculatedEndPt()
        {
            Point3D ctPt = this.GetCenter();
            double endAngle = this.Size + this.StartAngle;
            double x = ctPt.X + this.Radius * Math.Cos(endAngle);
            double y = ctPt.Y + this.Radius * Math.Sign(endAngle);
            return new Point3D(x, y, 0);
        }
#endif
    }
}
