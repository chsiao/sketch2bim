﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Controls;

using Petzold.Media3D;
using GeomLib;
namespace WrapperLibrary.RDF
{
    public class RDF_Abstract
    {
        public string GUID { get; set; }
        [XmlIgnore]
        public IntPtr PtrToConcept { get; set; }
        public bool IsInstanciated { get; set; }

        protected List<Relation> relationlist;
        protected List<DatatypeProperty> propertylist;
        protected List<Rule> rulelist;

        public RDF_Abstract()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.relationlist = new List<Relation>();
            this.propertylist = new List<DatatypeProperty>();
            this.rulelist = new List<Rule>();
            this.IsInstanciated = true; // default value
            if (!RDFWrapper.All_RDF_Objects.ContainsKey(this.GUID))
                RDFWrapper.All_RDF_Objects.Add(this.GUID, this);
        }

        public RDF_Abstract(string guid = "")
        {
            this.GUID = guid == "" ? Guid.NewGuid().ToString() : guid;
            this.relationlist = new List<Relation>();
            this.propertylist = new List<DatatypeProperty>();
            this.rulelist = new List<Rule>();
            this.IsInstanciated = true; // default value
            if (!RDFWrapper.All_RDF_Objects.ContainsKey(this.GUID))
                RDFWrapper.All_RDF_Objects.Add(this.GUID, this);
        }

        internal RDF_Abstract(IntPtr ptrToRDF, string guid = "") : this(guid)
        {
            this.PtrToConcept = ptrToRDF;
            this.relationlist = RDFLibrary.GetRelationListFromConcept(ptrToRDF);
            this.propertylist = RDFLibrary.GetPropertyListFromConcept(ptrToRDF);
            this.rulelist = RDFLibrary.GetRuleList(ptrToRDF);
        }

        internal RDF_Abstract(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : this(guid)
        {
            this.PtrToConcept = sconcept.concept;
            this.propertylist = RDFLibrary.GetSelectedPropertyList(sconcept.datatypePropertyList);
            this.relationlist = RDFLibrary.GetSelectedRelationList(sconcept.objecttypePropertyList);
            this.rulelist = RDFLibrary.GetRuleList(sconcept.concept);
            if (!RDFWrapper.All_RDF_Objects.ContainsKey(this.GUID))
                RDFWrapper.All_RDF_Objects.Add(this.GUID, this);
        }

        public void ChangeDataProperty(IntPtr concept, string[] formula, int[] formulaIndex)
        {
            throw new System.NotImplementedException();
        }

        public void AddCustomRule(string rule)
        {
            RDFWrapper.AddRule(RDFWrapper.FilePtr, this.PtrToConcept, rule);
        }

        public List<IntPtr> GetPtrProperties()
        {
            if ((int)this.PtrToConcept > 0)
            {
                IntPtr[] ptrPropertyList = new IntPtr[1000];
                RDFWrapper.GetConceptProperties(this.PtrToConcept, ptrPropertyList);
                return ptrPropertyList.ToList().FindAll(ptr => (int)ptr > 0);
            }
            return null;
        }
        public List<IntPtr> GetPtrRules()
        {
            if ((int)this.PtrToConcept > 0)
            {
                IntPtr[] ptrrules = new IntPtr[1000];
                RDFWrapper.GetConceptRules(this.PtrToConcept, ptrrules);
                return ptrrules.ToList().FindAll(ptr => (int)ptr > 0);
            }
            return null;
        }
        public List<IntPtr> GetPtrRelations()
        {
            if ((int)this.PtrToConcept > 0)
            {
                IntPtr[] ptrRelations = new IntPtr[1000];
                RDFWrapper.GetConceptRelations(this.PtrToConcept, ptrRelations);
                return ptrRelations.ToList().FindAll(ptr => (int)ptr > 0);
            }
            return null;
        }
        protected void CleanRelations(string relationname, bool deepclean = false)
        {
            RDFWrapper.CleanRelations(RDFWrapper.FilePtr, this.PtrToConcept, relationname, deepclean);
        }

        public static RDF_Abstract GetRDFObject(IntPtr ptrToRDFObj)
        {
            RDF_Abstract rdfobj;

            BasicProperties bp = RDFLibrary.GetBasicProperties(ptrToRDFObj);
            switch (bp.parentName)
            {
                case "BooleanOperation":
                    rdfobj = new RDF_Boolean3D(ptrToRDFObj, bp.name);
                    break;
                case "Arc3D":
                    rdfobj = new RDF_Arc3D(ptrToRDFObj, bp.name);
                    break;
                case "SweptAreaSolid":
                    rdfobj = new RDF_SweptAreaSolid(ptrToRDFObj, bp.name);
                    break;
                case "Polygon3D":
                    rdfobj = new RDF_Polygon3D(ptrToRDFObj, bp.name);
                    break;
                case "Line3Dn":
                    rdfobj = new RDF_Line3D(ptrToRDFObj, bp.name);
                    break;
                case "Transformation":
                    rdfobj = new RDF_Transformation(ptrToRDFObj, bp.name);
                    break;
                case "Matrix":
                    rdfobj = new RDF_Abstract(ptrToRDFObj, bp.name);
                    break;
                default:
                    rdfobj = new RDF_Abstract(ptrToRDFObj, bp.name);
                    if (bp.name != "Concept")
                    {
                        // the RDF object is not instanciated, 
                        // but just add the properties into the RDF object
                        rdfobj.IsInstanciated = false;
                    }
                    break;
            }

            return rdfobj;
        }

        public static RDF_Abstract GetRDFObjectFromRelation(IntPtr ptrSConcept)
        {
            RDF_Abstract rdfobj;

            SELECTED__CONCEPT__STRUCT umSConcept = ptrSConcept.ElementAt<SELECTED__CONCEPT__STRUCT>(0);

            BasicProperties bp = RDFLibrary.GetBasicProperties(umSConcept.concept);
            switch (bp.parentName)
            {
                case "BooleanOperation":
                    rdfobj = new RDF_Boolean3D(umSConcept.concept, bp.name);
                    break;
                case "Arc3D":
                    rdfobj = new RDF_Arc3D(umSConcept.concept, bp.name);
                    break;
                case "SweptAreaSolid":
                    rdfobj = new RDF_SweptAreaSolid(umSConcept.concept, bp.name);
                    break;
                case "Polygon3D":
                    rdfobj = new RDF_Polygon3D(umSConcept.concept, bp.name);
                    break;
                case "Line3Dn":
                    rdfobj = new RDF_Line3D(umSConcept.concept, bp.name);
                    break;
                case "Transformation":
                    rdfobj = new RDF_Transformation(umSConcept.concept, bp.name);
                    break;
                case "Matrix":
                    rdfobj = new RDF_Abstract(umSConcept.concept, bp.name);
                    break;
                default:
                    rdfobj = new RDF_Abstract(umSConcept.concept, bp.name);
                    if (bp.name != "Concept")
                    {
                        // the RDF object is not instanciated, 
                        // but just add the properties into the RDF object
                        rdfobj.IsInstanciated = false;
                        switch (bp.name)
                        {
                            case "BooleanOperation":
                                rdfobj = new RDF_Boolean3D(umSConcept, bp.name);
                                break;
                            case "Arc3D":
                                rdfobj = new RDF_Arc3D(umSConcept, bp.name);
                                break;
                            case "SweptAreaSolid":
                                rdfobj = new RDF_SweptAreaSolid(umSConcept, bp.name);
                                break;
                            case "Polygon3D":
                                rdfobj = new RDF_Polygon3D(umSConcept, bp.name);
                                break;
                            case "Line3Dn":
                                rdfobj = new RDF_Line3D(umSConcept, bp.name);
                                break;
                            case "Transformation":
                                rdfobj = new RDF_Transformation(umSConcept, bp.name);
                                break;
                            case "Matrix":
                                rdfobj = new RDF_Abstract(umSConcept, bp.name);
                                break;
                        }
                    }
                    break;
            }

            return rdfobj;
        }
    }
}
