﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeomLib;
using System.Windows.Media.Media3D;
namespace WrapperLibrary.RDF
{
    public abstract class RDF_MatrixAbstract : RDF_Abstract
    {
        protected Matrix3D matrix3D;
        protected Point3D translate;
        protected RDF_MatrixInverse inverseMatrix;
        public RDF_MatrixInverse InverseMatrix
        {
            get
            {
                if (this.inverseMatrix != null) return inverseMatrix;
                this.inverseMatrix = new RDF_MatrixInverse(this);
                return this.inverseMatrix;
            }
        }
        public abstract Matrix3D Matrix3D { get; }
        public abstract Point3D Translate { get; }
        public List<RDF_Transformation> RelatedTransforms { protected set; get; }
        public List<RDF_MatrixAbstract> ChildrenMatrix { set; get; }
        
        public RDF_MatrixAbstract(string guid = "")
            : base(guid)
        {
            this.matrix3D = new Matrix3D();
            this.translate = new Point3D();
            this.RelatedTransforms = new List<RDF_Transformation>();
            this.ChildrenMatrix = new List<RDF_MatrixAbstract>();
        }
        internal RDF_MatrixAbstract(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.matrix3D = new Matrix3D();
            this.translate = new Point3D();
            this.RelatedTransforms = new List<RDF_Transformation>();
            this.ChildrenMatrix = new List<RDF_MatrixAbstract>();
        }
        internal RDF_MatrixAbstract(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.matrix3D = new Matrix3D();
            this.translate = new Point3D();
            this.RelatedTransforms = new List<RDF_Transformation>();
            this.ChildrenMatrix = new List<RDF_MatrixAbstract>();
        }
        /// <summary>
        /// alwasy return a non inversed matrix of its children
        /// </summary>
        /// <returns></returns>
        public RDF_Matrix GetLeafMatrix()
        {
            if (this is RDF_Matrix) return (RDF_Matrix)this;
            else if(this is RDF_MatrixMultiplication)
            {
                RDF_MatrixMultiplication mulmtx = (RDF_MatrixMultiplication)this;
                return mulmtx.FirstMatrix.GetLeafMatrix();
            }
            else if (this is RDF_MatrixInverse)
                return ((RDF_MatrixInverse)this).OriMatrix.GetLeafMatrix();
            return new RDF_Matrix();
        }
        public void TransformRDFGeoms(Matrix3D applyingMtx, Action<RDF_GeometricItem, Matrix3D> transformFunc = null, bool isInherited = false)
        {
            if (transformFunc == null)
                transformFunc = RDFLibrary.TransformFace2D;
            // try to Find the Face2D to it's parent
            bool isFound = false;
            foreach (var transform in this.RelatedTransforms)
            {
                if (transform.TransformGeom is RDF_Face2D) // found it!!
                {
                    transformRDFMatrix(applyingMtx, isInherited);
                    isFound = true;
                    break;
                }
            }

            // Render the children if this connects Face2D
            if (isFound)
            {
                RenderChildRelatedTransform(transformFunc);
            }
            else if (this is RDF_MatrixMultiplication)
            {
                // find the face2D from its ancestor
                ((RDF_MatrixMultiplication)this).FirstMatrix.TransformRDFGeoms(applyingMtx, transformFunc);
            }
            // couldn't find any geometry
            // try to translate it anyway
            else
            {
                transformRDFMatrix(applyingMtx, isInherited);
                RenderChildRelatedTransform(transformFunc);
            }
        }
        public Tuple<int, Point3D> MoveRDFGeoms(Vector3D offset, Action<RDF_GeometricItem, Matrix3D> translateFunc = null, bool isInherited = false, List<Point3D> followingPath = null)
        {
            if (translateFunc == null)
                translateFunc = RDFLibrary.TransformFace2D;
            double newx = 0;
            double newy = 0;
            double newz = 0;
            if (this is RDF_MatrixMultiplication)
            {
                var multiMtx = (RDF_MatrixMultiplication)this;
                if (multiMtx.FirstMatrix is RDF_MatrixMultiplication)
                {
                    return multiMtx.FirstMatrix.MoveRDFGeoms(offset, translateFunc, isInherited);
                }
                newx = ((RDF_MatrixMultiplication)this).FirstMatrix.Matrix3D.OffsetX + offset.X;
                newy = ((RDF_MatrixMultiplication)this).FirstMatrix.Matrix3D.OffsetY + offset.Y;
                newz = ((RDF_MatrixMultiplication)this).FirstMatrix.Matrix3D.OffsetZ + offset.Z;
            }
            else
            {
                newx = this.Matrix3D.OffsetX + offset.X;
                newy = this.Matrix3D.OffsetY + offset.Y;
                newz = this.Matrix3D.OffsetZ + offset.Z;
            }
            var translateTo = new Point3D(newx, newy, newz);
            var movingIndex = -1;
            if (followingPath != null)
            {
                var result = followingPath.FindClosetPointOnPolyline(translateTo);
                if (result.Item1 > -1)
                    translateTo = result.Item2;
                movingIndex = result.Item1;
            }
            
            MoveRDFGeoms(translateTo, translateFunc, offset, isInherited);

            return new Tuple<int, Point3D>(movingIndex, translateTo);    
        }
        /// <summary>
        /// We are using face2D as a major object for transformation
        /// When moving an object, it should find its first ancestors which is a RDF_Face2D
        /// </summary>
        /// <param name="translateTo"></param>
        public void MoveRDFGeoms(Point3D translateTo, Action<RDF_GeometricItem, Matrix3D> translateFunc, Vector3D? offset = null, bool isInherited = false)
        {
            // try to Find the Face2D to it's parent
            bool isFound = false;
            foreach (var transform in this.RelatedTransforms)
            {
                if (transform.TransformGeom is RDF_Face2D) // found it!!
                {
                    translateRDFMatrix(translateTo, isInherited);
                    isFound = true;
                    break;
                }
            }

            // Render the children if this connects Face2D
            if (isFound)
            {
                RenderChildRelatedTransform(translateFunc);
            }
            else if(this is RDF_MatrixMultiplication)
            {
                // find the face2D from its ancestor
                ((RDF_MatrixMultiplication)this).FirstMatrix.MoveRDFGeoms(translateTo, translateFunc);
            }
            // couldn't find any geometry
            // try to translate it anyway
            else
            {
                translateRDFMatrix(translateTo);
                RenderChildRelatedTransform(translateFunc);
            }
        }

        private static void moveSweptSolid(Action<RDF_GeometricItem, Matrix3D> translateFunc, Vector3D? offset, RDF_SweptAreaSolid first, bool isInherited = false)
        {
            if (first.SweptProfile.BackRelations.Count > 0)
            {
                var firstFace = first.SweptProfile.BackRelations.First().RelatedTo;
                if (firstFace is RDF_Face2D && offset != null)
                {
                    RDF_Face2D fstFace2D = (RDF_Face2D)firstFace;
                    fstFace2D.Transform.Matrix.MoveRDFGeoms((Vector3D)offset, translateFunc, isInherited);
                }
            }
        }

        private void transformRDFMatrix(Matrix3D applyingMtx, bool isInherited = false)
        {
            // move this matrix if it is a RDF_Matrix
            if (this is RDF_Matrix)
            {
                var newMtx = applyingMtx * this.Matrix3D;
                ((RDF_Matrix)this).SetMatrix(newMtx);
            }
            else if (this is RDF_MatrixInverse && ((RDF_MatrixInverse)this).OriMatrix is RDF_Matrix)
            {
                Matrix3D oriMtx = ((RDF_MatrixInverse)this).OriMatrix.Matrix3D;
                var newMtx = applyingMtx * oriMtx;
                ((RDF_Matrix)((RDF_MatrixInverse)this).OriMatrix).SetMatrix(newMtx);
            }
            else if (this is RDF_MatrixInverse && ((RDF_MatrixInverse)this).OriMatrix is RDF_MatrixMultiplication && !isInherited)
            {
                throw new NotImplementedException();
                RDF_MatrixMultiplication mulmtx = (RDF_MatrixMultiplication)((RDF_MatrixInverse)this).OriMatrix;
                Matrix3D parentTransform = mulmtx.FirstMatrix.Matrix3D;
                //Point3D newtranslateTo = parentTransform.Transform(translateTo);
                //((RDF_Matrix)mulmtx.SecondMatrix).SetTranslate(newtranslateTo);
            }
            else if (this is RDF_MatrixMultiplication && ((RDF_MatrixMultiplication)this).FirstMatrix is RDF_Matrix)
            {
                var childMtx = (RDF_Matrix)((RDF_MatrixMultiplication)this).FirstMatrix;
                var newMtx = applyingMtx * childMtx.matrix3D;
                childMtx.SetMatrix(newMtx);
            }
        }

        public void translateRDFMatrix(Point3D translateTo, bool isInherited = false)
        {
            // move this matrix if it is a RDF_Matrix
            if (this is RDF_Matrix)
            {
                ((RDF_Matrix)this).SetTranslate(translateTo);
            }
            else if (this is RDF_MatrixInverse && ((RDF_MatrixInverse)this).OriMatrix is RDF_Matrix)
            {
                ((RDF_Matrix)((RDF_MatrixInverse)this).OriMatrix).SetTranslate(translateTo);
            }
            else if (this is RDF_MatrixInverse && ((RDF_MatrixInverse)this).OriMatrix is RDF_MatrixMultiplication && !isInherited)
            {
                RDF_MatrixMultiplication mulmtx = (RDF_MatrixMultiplication)((RDF_MatrixInverse)this).OriMatrix;
                Matrix3D parentTransform = mulmtx.FirstMatrix.Matrix3D;
                Point3D newtranslateTo = parentTransform.Transform(translateTo);
                ((RDF_Matrix)mulmtx.SecondMatrix).SetTranslate(newtranslateTo);
            }
            #region This is deprecated
            else if (this is RDF_MatrixMultiplication)
            {
                RDF_MatrixMultiplication mulmtx = (RDF_MatrixMultiplication)this;
                // transform the "traslateTo" point to it's coordinate system
                mulmtx.FirstMatrix.translateRDFMatrix(translateTo);
                //Matrix3D parentTransform = mulmtx.FirstMatrix.Matrix3D;
                //parentTransform.Invert();
                //Point3D newtranslateTo = parentTransform.Transform(translateTo);
                //// translate this matrix
                //((RDF_Matrix)((RDF_MatrixMultiplication)this).SecondMatrix.OriMatrix).SetTranslate(newtranslateTo);
            }
            #endregion
        }

        public void RenderChildRelatedTransform(Action<RDF_GeometricItem, Matrix3D> translateFunc)
        {
            // iterate through the whole transform again
            foreach (var transform in this.RelatedTransforms)
            {
                if (transform.TransformGeom is RDF_Face2D)
                {
                    translateFunc((RDF_Face2D)transform.TransformGeom, this.Matrix3D);//, ptcount == 2);
                }
                else if (transform.TransformGeom is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid rdfSolid = (RDF_SweptAreaSolid)transform.TransformGeom;
                    rdfSolid.MoveConceptMesh();
                    //translateFunc(rdfSolid.SweptProfile.Face, this.Matrix3D);//, ptcount == 2);
                }
                else if (transform.TransformGeom is RDF_Boolean3D)
                {
                    RDF_Boolean3D rdfBoolean = (RDF_Boolean3D)transform.TransformGeom;
                    rdfBoolean.MoveConceptMesh();
                    //rdfBoolean.RenderConceptMesh();
                }
                else if (transform.TransformGeom is RDF_Polygon3D)
                {
                    RDF_Polygon3D rdfPolygon = (RDF_Polygon3D)transform.TransformGeom;
                    translateFunc(rdfPolygon, this.matrix3D);
                }
                else if (transform.TransformGeom is RDF_ExtrudedPolygon)
                {
                    RDF_ExtrudedPolygon rdfExPolygon = (RDF_ExtrudedPolygon)transform.TransformGeom;
                    rdfExPolygon.MoveConceptMesh();
                }
            }
            // find its children and render the geometry if they have any
            foreach (var childMatrix in this.ChildrenMatrix)
                childMatrix.RenderChildRelatedTransform(translateFunc);

            if(this.inverseMatrix != null)
            {
                this.inverseMatrix.RenderChildRelatedTransform(translateFunc);
            }
        }

        public abstract RDF_MatrixAbstract Clone();
    }
}
