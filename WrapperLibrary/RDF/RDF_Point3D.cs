﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using GeomLib.Constraint;
using GeomLib;

namespace WrapperLibrary.RDF
{
    // this is not implemented as 
    public class RDF_Point3D : RDF_Dim1
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public RDF_Point3D(double x, double y, double z)
            : base()
        {
            this.X = x;
            this.Y = y;
            this.Z = z;

            // Todo: should create a RDF point object
        }

        public void SetPoint(Point3D newpt)
        {
            this.X = newpt.X;
            this.Y = newpt.Y;
            this.Z = newpt.Z;
            foreach (var ctlcst in this.Controlling)
                if (ctlcst is GeomLib.Constraint.TransformationConstraint)
                    ((GeomLib.Constraint.TransformationConstraint)ctlcst).SolveConstraint();
        }

        #region code adopted from CSOpenGL
        /// <summary>
        /// Subtraction of two Vectors
        /// </summary>
        /// <param name="v1">Point3D to be subtracted from </param>
        /// <param name="v2">Point3D to be subtracted</param>
        /// <returns>Point3D representing the difference of two Vectors</returns>
        /// <Acknowledgement>This code is adapted from CSOpenGL - Lucas Viñas Livschitz </Acknowledgement>
        public static RDF_Point3D operator -(RDF_Point3D v1, RDF_Point3D v2)
        {
            return
            (
                new RDF_Point3D
                    (
                        v1.X - v2.X,
                        v1.Y - v2.Y,
                        v1.Z - v2.Z
                    )
            );
        }

        /// <summary>
        /// Product of a Point3D and a scalar value
        /// </summary>
        /// <param name="v1">Point3D to be multiplied </param>
        /// <param name="s2">Scalar value to be multiplied by </param>
        /// <returns>Point3D representing the product of the vector and scalar</returns>
        /// <Acknowledgement>This code is adapted from CSOpenGL - Lucas Viñas Livschitz </Acknowledgement>
        public static RDF_Point3D operator *(RDF_Point3D v1, double s2)
        {
            return
            (
                new RDF_Point3D
                (
                    v1.X * s2,
                    v1.Y * s2,
                    v1.Z * s2
                )
            );
        }

        /// <summary>
        /// Product of a scalar value and a Point3D
        /// </summary>
        /// <param name="s1">Scalar value to be multiplied </param>
        /// <param name="v2">Point3D to be multiplied by </param>
        /// <returns>Point3D representing the product of the scalar and Point3D</returns>
        /// <Acknowledgement>This code is adapted from CSOpenGL - Lucas Viñas Livschitz </Acknowledgement>
        /// <Implementation>
        /// Using the commutative law 'scalar x vector'='vector x scalar'.
        /// Thus, this function calls 'operator*(Point3D v1, double s2)'.
        /// This avoids repetition of code.
        /// </Implementation>
        public static RDF_Point3D operator *(double s1, RDF_Point3D v2)
        {
            return v2 * s1;
        }

        /// <summary>
        /// Division of a Point3D and a scalar value
        /// </summary>
        /// <param name="v1">Point3D to be divided </param>
        /// <param name="s2">Scalar value to be divided by </param>
        /// <returns>Point3D representing the division of the vector and scalar</returns>
        /// <Acknowledgement>This code is adapted from CSOpenGL - Lucas Viñas Livschitz </Acknowledgement>
        public static RDF_Point3D operator /(RDF_Point3D v1, double s2)
        {
            return
            (
                new RDF_Point3D
                    (
                        v1.X / s2,
                        v1.Y / s2,
                        v1.Z / s2
                    )
            );
        }

        /// <summary>
        /// Negation of a Point3D
        /// Invert the direction of the Point3D
        /// Make Point3D negative (-vector)
        /// </summary>
        /// <param name="v1">Point3D to be negated  </param>
        /// <returns>Negated vector</returns>
        /// <Acknowledgement>This code is adapted from Exocortex - Ben Houston </Acknowledgement>
        public static RDF_Point3D operator -(RDF_Point3D v1)
        {
            return
            (
                new RDF_Point3D
                    (
                        -v1.X,
                        -v1.Y,
                        -v1.Z
                    )
            );
        }

        /// <summary>
        /// Reinforcement of a Point3D
        /// Make Point3D positive (+vector)
        /// </summary>
        /// <param name="v1">Point3D to be reinforced </param>
        /// <returns>Reinforced vector</returns>
        /// <Acknowledgement>This code is adapted from Exocortex - Ben Houston </Acknowledgement>
        /// <Implementation>
        /// Using the rules of Addition (i.e. '+-x' = '-x' and '++x' = '+x')
        /// This function actually  does nothing but return the argument as given
        /// </Implementation>
        public static RDF_Point3D operator +(RDF_Point3D v1)
        {
            return
            (
                new RDF_Point3D
                    (
                        +v1.X,
                        +v1.Y,
                        +v1.Z
                    )
            );
        }

        #endregion


        /// <summary>
        /// Compare if two points are at the same location
        /// </summary>
        /// <param name="comparePoint">the point to be compared</param>
        /// <returns></returns>
        public bool EqualsPoint(Point3D comparePoint)
        {
            if (this.X == comparePoint.X && this.Y == comparePoint.Y && this.Z == comparePoint.Z)
                return true;

            return false;
        }

        /// <summary>
        /// To check whether the point is in a line segment
        /// </summary>
        /// <param name="lineSegment"></param>
        /// <returns></returns>
        public bool InLine(RDF_Line3D lineSegment)
        {
            bool bInline = false;
            double Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz;
            Bx = lineSegment.EndNode.X;
            By = lineSegment.EndNode.Y;
            Bz = lineSegment.EndNode.Z;
            Ax = lineSegment.StartNode.X;
            Ay = lineSegment.StartNode.Y;
            Az = lineSegment.StartNode.Z;
            Cx = this.X;
            Cy = this.Y;
            Cz = this.Z;

            double L = lineSegment.GetLength();
            double diff = L - Math.Sqrt((Math.Pow(Cx - Bx, 2) + Math.Pow(Cy - By, 2) + Math.Pow(Cz - Bz, 2)))
                - Math.Sqrt((Math.Pow(Cx - Ax, 2) + Math.Pow(Cy - Ay, 2) + Math.Pow(Cz - Az, 2)));
            if (Math.Abs(diff) < 0.01) bInline = true;
            return bInline;
        }

        public override string ToString()
        {
            return "(" + this.X + ", " + this.Y + ", " + this.Z + ")";
        }
        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            throw new NotImplementedException();
        }
    }

    public class RDF_Node3D : RDF_Point3D
    {
        public Point3D GetPoint3D()
        {
            return new Point3D(this.X, this.Y, this.Z);
        }
        public RDF_Node3D(double x, double y, double z):base(x, y, z)
        {
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }

        void Constraint_onSolvingConstraint(Constraint constrainObj, object curvalue)
        {
            changeNodeAngle((double)curvalue);
        }
        public RDF_Node3D(Point3D pt):base(pt.X, pt.Y, pt.Z)
        {
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }

        public RDF_Segment3D NextEdge { set; get; }
        public RDF_Segment3D PreEdge { set; get; }
        /// <summary>
        /// Reset the location of the edge and change the locations of the edge
        /// We are not dealing with the 
        /// </summary>
        /// <param name="newpt"></param>
        public void SetNode(Point3D newpt)
        {
            RDF_Segment3D previousSeg = this.PreEdge;

            if (this.NextEdge is RDF_Line3D)
            {
                RDF_Line3D nextSeg = (RDF_Line3D)this.NextEdge;
                nextSeg.ChangePoint(newpt, 0);
            }
            else if (this.NextEdge is RDF_Arc3D)
            {
                //RDF_Arc3D nextArc = (RDF_Arc3D)this.NextEdge;
                //nextArc.ChangeStartingPoint(newpt);
            }
            if (this.PreEdge is RDF_Line3D)
            {
                RDF_Line3D preSeg = (RDF_Line3D)this.PreEdge;
                preSeg.ChangePoint(newpt, 1);
            }
            else if (this.PreEdge is RDF_Arc3D)
            {
                //RDF_Arc3D preArc = (RDF_Arc3D)this.PreEdge;
                //preArc.ChangeEndingPoint(newpt);
            }
        }
        public double? Angle
        {
            get
            {
                if (NextEdge != null && PreEdge != null)
                {
                    Vector3D vector1 = new Vector3D();
                    Vector3D vector2 = new Vector3D();
                    if (this.NextEdge is RDF_Line3D)
                        vector1 = ((RDF_Line3D)this.NextEdge).GetVector3D();
                    else
                        vector1 = ((RDF_Arc3D)this.NextEdge).GetStartVector3D();

                    if (this.PreEdge is RDF_Line3D)
                        vector2 = -((RDF_Line3D)this.PreEdge).GetVector3D();
                    else
                        vector2 = -((RDF_Arc3D)this.PreEdge).GetEndVector3D();

                    return Vector3D.AngleBetween(vector1, vector2);
                }
                else return null;
            }
        }

        public bool isConcaveNode()
        {
            Vector3D preVec = this.PreEdge.GetVector3D();
            Vector3D nextVec = this.NextEdge.GetVector3D();
            preVec = -preVec;
            Vector3D sumVec = preVec + nextVec;
            sumVec.Normalize();

            var testPoint = this.GetPoint3D();
            testPoint = new Point3D(testPoint.X + sumVec.X, testPoint.Y + sumVec.Y, testPoint.Z + sumVec.Z);

            List<Point3D> ptList = new List<Point3D>();
            ptList.Add(testPoint);

            try
            {
                var nextnode = this.NextEdge.EndNode;
                while (nextnode != null && nextnode.GUID != this.GUID)
                {
                    ptList.Add(nextnode.GetPoint3D());
                    if (nextnode.NextEdge == null) break;
                    nextnode = nextnode.NextEdge.EndNode;
                }

                if (ptList.Count > 3)
                {
                    var testarea = GeomLib.BasicGeomLib.PolygonArea(ptList.ToArray());
                    ptList[0] = this.GetPoint3D();
                    var oriarea = GeomLib.BasicGeomLib.PolygonArea(ptList.ToArray());

                    if (testarea > oriarea)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception e)
            {

            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="targetAngle"></param>
        /// <returns>true if constraint is solved</returns>
        private bool changeNodeAngle(double targetAngle)
        {
            RDF_Segment3D ppseg = this.PreEdge.StartNode.PreEdge;
            RDF_Segment3D nnseg = this.NextEdge.EndNode.NextEdge;
            double? angle = this.Angle;

            if (angle.HasValue)
            {
                double ang = this.Angle.Value;
                double deltaAng = (targetAngle - ang) * Math.PI / 360;

                //adjustPreEdges(this.PreEdge, deltaAng);
                //adjustNextEdges(this.NextEdge, -deltaAng);
                adjustEdges(deltaAng);


                // render the polygons
                if (this.PreEdge.InPartOf != null)
                {
                    RDF_Dim1 polyline = this.PreEdge.InPartOf;
                    if (polyline.HasTransform)
                    {
                        if (polyline is RDF_Polygon3D)
                        {
                            RDF_Polygon3D polygon = (RDF_Polygon3D)polyline;
                            
                            foreach (var backR in polygon.BackRelations)
                            {
                                // cause rendering problems, not sure what's this for??
                                //if (backR.Name == BackRelationName.Face_Outer)
                                //    ((RDF_Face2D)backR.RelatedTo).RenderConceptMesh(null);
                                //else 
                                if (backR.Name == BackRelationName.Dim1_SweptSolid_Profile_Outer)
                                    ((RDF_SweptAreaSolid)backR.RelatedTo).RenderConceptMesh(null);
                            }
                            if (polygon.Face != null)
                            {
                                polygon.Face.RenderShapeMesh(null);
                            }
                            // cause the polygon shifting back to original place. not sure what's this for??
                            //polygon.Transform.Matrix.GetLeafMatrix().SetTranslate(new Point3D(polygon.StartPt.X, polygon.StartPt.Y, polygon.StartPt.Z));
                        }
                        var newpath = RDF_Dim1.RenderConceptLine(polyline.Transform);

                    }
                    else
                        polyline.RenderConceptLine();
                }

                return true;
            }

            return false;
        }

        private void adjustEdges(double angle)
        {
            angle = this.isConcaveNode() ? -angle : angle;
            bool isPreNodeCnstSet = this.PreEdge.StartNode.Constraint.Type != ConstraintType.NotSet;
            bool isNextNodeCnstSet = this.NextEdge.EndNode.Constraint.Type != ConstraintType.NotSet;

            Vector3D preVec = this.PreEdge.GetVector3D();
            Vector3D nextVec = this.NextEdge.GetVector3D();

            if (!isPreNodeCnstSet && !isNextNodeCnstSet)
            {
                preVec = preVec.Roll(angle);
                nextVec = nextVec.Roll(-angle);
            }
            else if (isPreNodeCnstSet && !isNextNodeCnstSet)
            {
                nextVec = nextVec.Roll(-2 * angle);
            }
            else if (!isPreNodeCnstSet && isNextNodeCnstSet)
            {
                preVec = preVec.Roll(2 * angle);
            }


            // only deal with the constrains between two straight lines for now
            if (this.PreEdge is RDF_Line3D && this.NextEdge is RDF_Line3D)
            {
                RDF_Line3D tmpPreLine = new RDF_Line3D(this.PreEdge.StartNode.GetPoint3D(),
                    this.PreEdge.StartNode.GetPoint3D() + preVec);
                RDF_Line3D tmpNextLine = new RDF_Line3D(this.NextEdge.EndNode.GetPoint3D(),
                    this.NextEdge.EndNode.GetPoint3D() + nextVec);
                System.Drawing.PointF intersectPt;
                if (BasicGeomLib.IsTwoLineIntersected(tmpPreLine.Pts.ToArray(), tmpNextLine.Pts.ToArray(), out intersectPt))
                {
                    this.SetNode(new Point3D(intersectPt.X, intersectPt.Y, this.Z));
                }
            }
        }

        private void adjustPreEdges(RDF_Segment3D preEdge, double angle)
        {
            RDF_Segment3D ppEdge = PreEdge.StartNode.PreEdge;

            Vector3D preVec = this.PreEdge is RDF_Line3D ?
                    -((RDF_Line3D)this.PreEdge).GetVector3D() : -((RDF_Arc3D)this.PreEdge).GetEndVector3D();
            preVec.Normalize();
            preVec = preVec.Roll(angle);

            if (preEdge is RDF_Line3D && ppEdge is RDF_Line3D)
                adjustLineLineEdges_1((RDF_Line3D)preEdge, (RDF_Line3D)ppEdge, preVec, preEdge.StartNode);
            else if (preEdge is RDF_Arc3D && ppEdge is RDF_Arc3D)
                adjustArcArcEdges((RDF_Arc3D)preEdge, (RDF_Arc3D)ppEdge, preEdge.StartNode, angle);
        }

        private void adjustNextEdges(RDF_Segment3D nextEdge, double angle)
        {
            RDF_Segment3D nnEdge = nextEdge.EndNode.NextEdge;

            Vector3D nextVec = this.NextEdge is RDF_Line3D ?
                   ((RDF_Line3D)this.NextEdge).GetVector3D() : ((RDF_Arc3D)this.NextEdge).GetStartVector3D();
            nextVec.Normalize();
            nextVec = nextVec.Roll(angle);

            if (nextEdge is RDF_Line3D && nnEdge is RDF_Line3D)
                adjustLineLineEdges_1((RDF_Line3D)nextEdge, (RDF_Line3D)nnEdge, nextVec, nextEdge.EndNode);
            else if (nextEdge is RDF_Arc3D && nnEdge is RDF_Arc3D)
                adjustArcArcEdges((RDF_Arc3D)nextEdge, (RDF_Arc3D)nnEdge, nextEdge.StartNode, angle);
        }

        private void adjustLineLineEdges_1(RDF_Line3D adjustingLine, RDF_Line3D adjustedLine, Vector3D adjustingVector, RDF_Node3D adjustingNode)
        {
            RDF_Line3D tmpPreLine = new RDF_Line3D(new Point3D(this.X, this.Y, this.Z),
               new Point3D(this.X + adjustingVector.X, this.Y + adjustingVector.Y, this.Z + adjustingVector.Z));
            
            System.Drawing.PointF intersectPt;
            if (GeomLib.BasicGeomLib.IsTwoLineIntersected(tmpPreLine.Pts.ToArray(), adjustedLine.Pts.ToArray(), out intersectPt))
            {
                adjustingNode.SetNode(new Point3D(intersectPt.X, intersectPt.Y, this.Z));
            }
        }

        private void adjustArcArcEdges(RDF_Arc3D arc1, RDF_Arc3D arc2, RDF_Node3D node, double angle)
        {

        }

        private void adjustArcEdges(RDF_Arc3D arc, Vector3D newVector, bool isPreEdge)
        {
            Point3D arcCenter = arc.GetCenter();
            Point3D segStart = arc.StartNode.GetPoint3D();
            Point3D segEnd = arc.EndNode.GetPoint3D();
            Point3D segCenter = new Point3D((segStart.X + segEnd.X) / 2, (segStart.Y + segEnd.Y) / 2, (segStart.Z + segEnd.Z) / 2);

            Vector3D z = new Vector3D(0, 0, 1);
            Vector3D newRadiusVector = Vector3D.CrossProduct(newVector, z);
            Point3D tmppt = isPreEdge ? segEnd + newRadiusVector : segStart + newRadiusVector;

            Point3D[] seg1 = { arcCenter, segCenter };
            Point3D[] seg2 = isPreEdge ? new Point3D[] { segEnd, tmppt } : new Point3D[] { segStart, tmppt };
            
            System.Drawing.PointF intersectPt;
            if (BasicGeomLib.IsTwoLineIntersected(seg1, seg2, out intersectPt))
            {
                Point3D newCenter = new Point3D(intersectPt.X, intersectPt.Y, segStart.Z);
                Vector3D vnewStart = segStart - newCenter;
                Vector3D vnewEnd = segEnd - newCenter;

                Vector3D x = new Vector3D(1, 0, 0);
                double r = vnewStart.Length;
                double angle = Vector3D.AngleBetween(vnewStart, x); // this angle will always be between 0 - 180
                if (vnewStart.Y < 0 || (vnewStart.X < 0 && vnewStart.Y < 0))
                    angle = -angle;
                double size = Vector3D.AngleBetween(vnewStart, vnewEnd);

                //System.Diagnostics.Debug.WriteLine("angle: " + angle + "size: " + size);

                angle = angle * Math.PI / 180;
                size = size * Math.PI / 180;
                arc.SetRadius(r);
                arc.SetStartAngle(angle);
                arc.SetSize(size);
            }
            else // two lines are parallel
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
