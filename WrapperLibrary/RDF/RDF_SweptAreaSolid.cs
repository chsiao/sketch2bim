﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Threading.Tasks;

namespace WrapperLibrary.RDF
{
    public class RDF_SweptAreaSolid : RDF_Dim3
    {
        public RDF_Polygon3D SweptProfile { get; private set; }
        public RDF_Abstract Direction { get; set; }

        public List<RDF_Polygon3D> Openings { get; private set; }
        public RDF_SweptAreaSolid(string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "SweptAreaSolid");
            this.Openings = new List<RDF_Polygon3D>();
        }
        internal RDF_SweptAreaSolid(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            assignRelations();
        }
        internal RDF_SweptAreaSolid(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            assignRelations();
        }
        private void assignRelations()
        {
            foreach (Relation r in this.relationlist)
            {
                if (r.Relations.Count > 0)
                {
                    foreach(IntPtr ptr in r.Relations){
                        RDF_Abstract rdfDim1 = GetRDFObjectFromRelation(ptr);
                        if (rdfDim1 is RDF_Dim1)
                        {
                            switch (r.Name)
                            {
                                case "sweptArea":
                                    this.SweptProfile = (RDF_Polygon3D)rdfDim1;
                                    break;
                                case "direction":
                                    this.Direction = (RDF_Dim1)rdfDim1;
                                    break;
                                case "sweptAreaOpening":
                                    this.Openings.Add((RDF_Polygon3D)rdfDim1);
                                    break;
                            }
                        }
                    }
                }
            }
        }
        public void SetSweptProfile(RDF_Polygon3D prof, bool setBackRelation = true)
        {
            this.SweptProfile = prof;
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "sweptArea", prof.PtrToConcept);

            if (setBackRelation)
            {
                BackRelation br = new BackRelation(prof);
                br.Name = BackRelationName.Dim1_SweptSolid_Profile_Outer;
                br.RelatedTo = this;
                prof.BackRelations.Add(br);
            }
        }
        public void AddOpening(RDF_Polygon3D opening, bool setBackRelation = true)
        {
            Openings.Add(opening);
            RDF_Transformation openTransform = null;
            if (opening.HasTransform)
            {
                Matrix3D openMtx3D = Matrix3D.Identity; // opening.Transform.GetMatrix3D();
                openMtx3D.OffsetX = opening.Segments.First().StartNode.X;
                openMtx3D.OffsetY = opening.Segments.First().StartNode.Y;
                openMtx3D.OffsetZ = opening.Segments.First().StartNode.Z;

                RDF_Matrix openMtx = new RDF_Matrix(openMtx3D);
                openTransform = new RDF_Transformation();
                openTransform.SetMatrix(openMtx);
                openTransform.SetTransformGeom(opening, false);
            }
            IntPtr addingPtr = openTransform != null ? openTransform.PtrToConcept : opening.PtrToConcept;

            if (this.Openings.Count == 1)
                RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "sweptAreaOpenings", addingPtr);
            else
                RDFWrapper.AddRelation(RDFWrapper.FilePtr, this.PtrToConcept, "sweptAreaOpening", addingPtr);

            if (setBackRelation)
            {
                BackRelation br = new BackRelation(opening);
                br.Name = BackRelationName.Dim1_SweptSolid_Profile_Inner;
                br.RelatedTo = this;
                opening.BackRelations.Add(br);
            }
        }
        public void SetDirection(RDF_Abstract dir, bool setBackRelation = true)
        {
            this.Direction = dir;
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "direction", dir.PtrToConcept);

            if (dir is RDF_Dim1 && setBackRelation)
                setRealtedDirection((RDF_Dim1)dir);
            else if (dir is RDF_Transformation && setBackRelation)
            {
                RDF_Transformation transform = (RDF_Transformation)dir;
                if (transform.TransformGeom is RDF_Polygon3D)
                    setRealtedDirection((RDF_Polygon3D)transform.TransformGeom);
            }
        }
        private void setRealtedDirection(RDF_Dim1 line)
        {
            if (!line.BackRelations.Exists(brtmp => brtmp.RelatedTo == this))
            {
                BackRelation br = new BackRelation(line);
                br.Name = BackRelationName.Dim1_SweptSolid_Direction;
                br.RelatedTo = this;
                line.BackRelations.Add(br);
            }
        }
        /// <summary>
        /// remove the connections to the swept solid so that we will not keap this in the memory
        /// </summary>
        internal void Remove()
        {
            BackRelation removingRelation = null;
            foreach (var br in this.SweptProfile.BackRelations)
            {
                if (br.Name == BackRelationName.Dim1_SweptSolid_Profile_Outer)
                {
                    if (br.RelatedTo == this)
                        removingRelation = br;
                }
            }
            if (removingRelation != null)
                 this.SweptProfile.BackRelations.Remove(removingRelation);

            // remove directions from view port
            if (this.Direction is RDF_Transformation)
            {
                ((RDF_Transformation)this.Direction).Remove();
                if (((RDF_Transformation)this.Direction).TransformGeom is RDF_Polygon3D)
                {
                    var polygon3D = (((RDF_Transformation)this.Direction).TransformGeom as RDF_Polygon3D);
                    if(RDFWrapper.All_RDF_Objects.ContainsKey(polygon3D.GUID))
                        removingQueue.Enqueue(polygon3D);
                }
            }
        }
        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            RDF_SweptAreaSolid cloneObj = new RDF_SweptAreaSolid();
            cloneObj.InstantiatedFrom = this;
            if (this.SweptProfile != null)
                cloneObj.SetSweptProfile(this.SweptProfile);
            if (this.Direction != null)
                cloneObj.SetDirection(this.Direction);

            var oriMatrix3D = this.Transform.GetMatrix3D();

            RDF_Matrix newmtx = new RDF_Matrix(oriMatrix3D);

            RDF_Transformation translate = new RDF_Transformation();
            translate.SetMatrix(newmtx);
            translate.SetTransformGeom(cloneObj);
            return cloneObj;
        }
    }
}
