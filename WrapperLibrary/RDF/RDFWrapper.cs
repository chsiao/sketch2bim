﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using WrapperLibrary;

namespace WrapperLibrary.RDF
{
    public static class RDFWrapper
    {
        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int InitializeBaseSet();

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetBasicConcepts([Out] IntPtr[] baseConcepts);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr LoadFile(string str);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr AddConcept(IntPtr owlfile, string conceptName, string instanceOf);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AddDataProperty(IntPtr owlFile, IntPtr concept, string left, string right);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ChangeDataProperties(IntPtr owlfile, IntPtr concept, string[] formulas, int[] formulaindex, int size);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ChangeDataProperty(IntPtr owlFile, IntPtr concept, string left, string right);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AddRule(IntPtr owlfile, IntPtr concept, string rule);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ChangeRule(IntPtr owlFile, IntPtr concept, string left, string right);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ChangeRelations(IntPtr owlfile, IntPtr concept, string relationName, IntPtr relationConcept);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AddRelation(IntPtr owlfile, IntPtr concept, string relationName, IntPtr relationConcept);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CleanRelations(IntPtr owlfile, IntPtr concept, string relationName, bool deepclean = false);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetConceptByName(string conceptname);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetConceptProperties(IntPtr concept, IntPtr[] datatypeProperties);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetConceptRelations(IntPtr concept, IntPtr[] relations);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetConceptRules(IntPtr concept, IntPtr[] rulelist);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetBasicConceptProperties(IntPtr concept);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void PrepareConcept([In] IntPtr concept, ref int vBuffSize, ref int iBuffSize);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetConceptMesh(IntPtr conceptReference, [Out] float[] vertexBuffer, [Out] int[] indexBuffer, int vBuffSize, int iBuffSize);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetConceptLines(IntPtr conceptReference, float[] vertexBuffer, int[] indexBuffer);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetConceptWireFrames(IntPtr conceptReference, float[] vertexBuffer, int[] indexBuffer);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SaveToFile(IntPtr owlFile, string filepath);

        [DllImport("RDFConceptBuilder.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CloseEngineModel();

        public static BaseConcept[] BaseConcepProperties;
        public static IntPtr FilePtr;
        public static void InitializeRDF_Engine(string baseFilePath)
        {
            // try to get all the basic concept names and concept pointers 
            // so that we can instantiate it when creating the new concepts
            int baseconceptsize = RDFWrapper.InitializeBaseSet();
            // load the files that contains the concepts we are going to use.. 
            // otherwise, we need to create a new file
            FilePtr = RDFWrapper.LoadFile(baseFilePath);
            // get the base concept and its properties
            IntPtr[] ptrsBC = new IntPtr[baseconceptsize];
            BaseConcepProperties = new BaseConcept[baseconceptsize];

            RDFWrapper.GetBasicConcepts(ptrsBC);
            for (int i = 0; i < baseconceptsize; i++)
            {
                // basic marshall
                BaseConceptUM umBC = ptrsBC[i].ElementAt<BaseConceptUM>(0);
                // find the pointer for each fields and marshall it again
                BaseConcept tmpconcept = new BaseConcept();
                tmpconcept.noOfDataProperties = umBC.noOfDataProperties;
                tmpconcept.noOfObjectProperties = umBC.noOfObjectProperties;

                tmpconcept.conceptName = Marshal.PtrToStringAnsi(umBC.conceptName);
                tmpconcept.dataPropertyNames = new string[tmpconcept.noOfDataProperties];
                IntPtr firstDataPropertyName = umBC.dataPropertyNames;
                for (int j = 0; j < tmpconcept.noOfDataProperties; j++)
                {
                    //tmpconcept.dataPropertyNames[j] = Marshal.PtrToStringAnsi(firstPropertyName);
                    IntPtr element = firstDataPropertyName.ElementAt<IntPtr>(j);
                    tmpconcept.dataPropertyNames[j] = Marshal.PtrToStringAnsi(element);
                }

                IntPtr firstObjectPropertyName = umBC.objectPropertyNames;
                tmpconcept.objectPropertyNames = new string[tmpconcept.noOfObjectProperties];
                for (int j = 0; j < tmpconcept.noOfObjectProperties; j++)
                {
                    //tmpconcept.dataPropertyNames[j] = Marshal.PtrToStringAnsi(firstPropertyName);
                    IntPtr element = firstObjectPropertyName.ElementAt<IntPtr>(j);
                    tmpconcept.objectPropertyNames[j] = Marshal.PtrToStringAnsi(element);
                }

                BaseConcepProperties[i] = tmpconcept;
            }
        }
        public static void SaveAsFile(string path)
        {
            SaveToFile(FilePtr, path);
        }
        
        public static Dictionary<string, RDF_Abstract> All_RDF_Objects;
    }


    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class BaseConcept
    {
        public int noOfDataProperties;
        public int noOfObjectProperties;
        public string conceptName;
        public string[] dataPropertyNames;
        public string[] objectPropertyNames;
        public override string ToString()
        {
            if (this.conceptName != null)
                return conceptName;
            else
                return base.ToString();
        }
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct BaseConceptUM
    {
        public int noOfDataProperties;
        public int noOfObjectProperties;
        public IntPtr conceptName;
        public IntPtr dataPropertyNames;
        public IntPtr objectPropertyNames;
    };

    [StructLayout(LayoutKind.Sequential)]
    public class Triangles
    {
        public int noOfIndex;
        public int noOfVertex;
        public int[] indexBuffer;
        public float[] vertexBuffer;

        public Triangles(TrianglesUM triUM)
        {
            IntPtr ptrIndexBuffer = triUM.indexBuffer;
            IntPtr ptrVertexBuffer = triUM.vertexBuffer;
            this.indexBuffer = new int[triUM.noOfIndex];
            this.vertexBuffer = new float[triUM.noOfVertex];


            for (int i = 0; i < triUM.noOfIndex; i++)
            {
                this.indexBuffer[i] = ptrIndexBuffer.ElementAt<int>(i);
            }
            for (int i = 0; i < triUM.noOfVertex; i++)
            {
                this.vertexBuffer[i] = ptrVertexBuffer.ElementAt<float>(i);
            }
        }
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct TrianglesUM
    {
        public int noOfIndex;
        public int noOfVertex;
        public IntPtr indexBuffer;
        public IntPtr vertexBuffer;
    };

    public class DatatypeProperty
    {
        public string Name { get; set; }
        public int Type { set; get; }
        public int MinCard { set; get; }
        public int MaxCard { set; get; }
        public Formula FormulaData { set; get; }
        public Value ValueData { set; get; }
        internal IntPtr PtrToRelation { get; set; }
    }

    public class Formula
    {
        public int Type { get; set; }
        public Formula FirstArgument { set; get; }
        public Formula SecondArgument { set; get; }
        public string Name { get; set; }
        public Value Value { get; set; }
        internal IntPtr PtrToFormula { get; set; }
    }

    public class Value
    {
        public int Format { set; get; }
        public int IntValue { set; get; }
        public double DoubleValue { set; get; }
        public ValueSet ValueSet { set; get; }
        internal IntPtr PtrToValue { set; get; }
    }

    public class ValueSet
    {
        public List<Value> Values { get; set; }
        public List<Formula> Formulas { get; set; }
        internal IntPtr PtrToVSet { get; set; }
    }

    public class BasicProperties
    {
        public string displayName;
        public string description;
        public string name;
        public string parentName;
        internal IntPtr PtrToBS { set; get; }
    }

    public class Rule
    {
        public int Type { get; set;}
        public string Name { get; set;}
        public string DisplayName { get; set;}
        public string Description { get; set;}
        public string format { get; set;}
        public Formula LHS { get; set; }
        public Formula RHS { get; set; }
        internal IntPtr PtrToRule { get; set; }
    }

    public class Relation
    {
        public string Name { get; set; }
        public List<IntPtr> Relations { get; set; }
        public int minCard;
        public int maxCard;
        internal IntPtr PtrToRelation { get; set; }
    }

    internal struct DATATYPE_PROPERTIES_STRUCT
    {
        public IntPtr datatypeProperty; //DATATYPE_PROPERTY_STRUCT
        public int type;

        public int minCard;
        public int maxCard;

        public IntPtr formula; //FORMULA_STRUCT
        public IntPtr value;   //VALUE_STRUCT
        public IntPtr hTreeItem; //HTREEITEM
    }
    internal struct DATATYPE_PROPERTY_STRUCT
    {
        public IntPtr nameStr; //NAMES_STRUCT
        public string displayName;

        public string description;

        public int format;

        public bool isfixed;
        public ulong enginePropertyByName;
    }
    internal struct SELECTED_DATATYPE_PROPERTIES_STRUCT
    {
        public IntPtr datatypeProperty; //DATATYPE_PROPERTY_STRUCT*
        public int type;
        public int minCard;
        public int maxCard;
        public string displayName;
        public IntPtr formula; //FORMULA_STRUCT*
        public IntPtr value; //VALUE_STRUCT*
        public IntPtr hTreeItem;
    }
    internal struct NAMES_STRUCT
    {
        public string name;
        public int nameSize;

        public int type;

        public IntPtr concept; //CONCEPT__STRUCT
        public IntPtr datatypeProperty; //DATATYPE_PROPERTY_STRUCT
        public IntPtr objecttypeProperty; //OBJECTTYPE_PROPERTY_STRUCT

        public IntPtr owlFile; //OWL__FILE__STRUCT

        public IntPtr next; //NAMES_STRUCT
    }
    internal struct FORMULA_STRUCT
    {
        public int type;

        public IntPtr firstArgument; //FORMULA_STRUCT
        public IntPtr secondArgument; //FORMULA_STRUCT

        public IntPtr datatypeProperty; //DATATYPE_PROPERTY_STRUCT
        public IntPtr value; //VALUE_STRUCT
    }
    internal struct VALUE_STRUCT
    {
        public int format;

        public int intValue;
        public double doubleValue;
        public IntPtr set; //VALUE_SET_STRUCT
    }
    internal struct VALUE_SET_STRUCT
    {
        public IntPtr values; //VALUE_STRUCT[]
        public IntPtr formulas; //FORMULA_STRUC[]
    }
    internal struct BASIC_PROPERTIES
    {
        public string displayName;
        public string description;
        public IntPtr nameStr;
        public IntPtr parentNameStr;
    }
    internal struct RULE_STRUCT
    {
        internal int type;
        internal IntPtr datatypeProperty; //DATATYPE_PROPERTY_STRUCT*
        internal IntPtr lhs; //FORMULA_STRUCT*
        internal IntPtr rhs; //FORMULA_STRUCT*
        internal string displayName;
    }
    internal struct OBJECTTYPE_PROPERTIES_STRUCT
    {
        public IntPtr objecttypeProperty; //OBJECTTYPE_PROPERTY_STRUCT*
        public int minCard;
        public int maxCard;
        public IntPtr selectedConcepts; //SELECTED__CONCEPT__STRUCT**
        public IntPtr hTreeItem; //HTREEITEM
    }
    internal struct SELECTED_OBJECTTYPE_PROPERTIES_STRUCT
    {
        public IntPtr objecttypeProperty; //OBJECTTYPE_PROPERTY_STRUCT*
        public int minCard;
        public int maxCard;

        public IntPtr hTreeItem;
        public IntPtr selectedConcepts;	//SELECTED__CONCEPT__STRUCT**
    }
    internal struct SELECTED__CONCEPT__STRUCT
    {
        public IntPtr concept; //CONCEPT__STRUCT*
        public IntPtr datatypePropertyList;	//SELECTED_DATATYPE_PROPERTIES_STRUCT**
        public IntPtr objecttypePropertyList;	//SELECTED_OBJECTTYPE_PROPERTIES_STRUCT**
        public IntPtr hTreeItem; //HTREEITEM
        public IntPtr hTreeItem_properties; //HTREEITEM
        public IntPtr hTreeItem_relations; //HTREEITEM
        public IntPtr referedByProperty; //OBJECTTYPE_PROPERTY_STRUCT*
    }
    internal struct OBJECTTYPE_PROPERTY_STRUCT
    {
        public IntPtr nameStr; //NAMES_STRUCT*
        public string displayName;
        public bool isfixed;
        public ulong enginePropertyByName;
        public IntPtr parent; //NAMES_STRUCT*
    }
}