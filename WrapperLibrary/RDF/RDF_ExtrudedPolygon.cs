﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace WrapperLibrary.RDF
{
    public class RDF_ExtrudedPolygon : RDF_Dim3
    {
        public double ExtrusionLength { private set; get; }
        public List<Point> Profile { private set; get; }

        public RDF_ExtrudedPolygon(string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "ExtrudedPolygon");
            this.Profile = new List<Point>();
            //this.Openings = new List<RDF_Polygon3D>();
        }
        internal RDF_ExtrudedPolygon(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            //assignRelations();
        }
        internal RDF_ExtrudedPolygon(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            //assignRelations();
        }

        public void SetProfile(List<Point> pts)
        {
            bool ischangingProfile = this.Profile.Count > 0 ? true : false;

            int j = 1;
            while (pts.Count > 52)
            {
                if (j >= pts.Count) j = 1;
                pts.RemoveAt(j);
                j = j + 2;
            }

            this.Profile = new List<Point>(pts);
            string strpts = "{ ";

            for (int i = 0; i < pts.Count; i++)
            {
                if (i > 0)
                {
                    strpts += "., ";
                }
                strpts += Math.Round(pts[i].X, 3) + "., " + Math.Round(pts[i].Y, 2);
                if (i == pts.Count - 1)
                {
                    strpts += ".}";
                }
            }

            //System.Diagnostics.Debug.WriteLine("point count: " + pts.Count);
            RDFWrapper.ChangeDataProperty(RDFWrapper.FilePtr, this.PtrToConcept, "points", strpts);    
        }

        public void SetExtrusionLength(double extrusion)
        {
            this.ExtrusionLength = Math.Round(extrusion, 4);
            RDFWrapper.ChangeDataProperty(RDFWrapper.FilePtr, this.PtrToConcept, "extrusionLength", extrusion.ToString());
        }

        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            RDF_ExtrudedPolygon cloneObj = new RDF_ExtrudedPolygon();
            cloneObj.InstantiatedFrom = this;
            cloneObj.SetExtrusionLength(this.ExtrusionLength);
            if (this.Profile.Count > 0)
                cloneObj.SetProfile(this.Profile);

            var oriMatrix3D = this.Transform.GetMatrix3D();

            RDF_Matrix newmtx = new RDF_Matrix(oriMatrix3D);

            RDF_Transformation translate = new RDF_Transformation();
            translate.SetMatrix(newmtx);
            translate.SetTransformGeom(cloneObj);
            return cloneObj;
        }
    }
}
