﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public class RDF_Boolean3D : RDF_Dim3
    {
        public enum BooleanType
        {
            UNION = 0,
            SUBSTRACT_1 = 1,
            SUBSTRACT_2 = 2,
            INTERSECTION = 3
        };
        public BooleanType Type { get; internal set; }
        public double Setting{ get; internal set; }
        public RDF_Dim3 First { get; internal set; }
        public RDF_Dim3 Second { get; internal set; }
        public RDF_Boolean3D(BooleanType booltype, string guid = "")
            : base(guid)
        {
            this.Type = booltype;
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "BooleanOperation");
            string strtype = "type = " + (int)this.Type;
            RDFWrapper.AddRule(RDFWrapper.FilePtr, this.PtrToConcept, strtype);
            this.Transform = new RDF_Transformation();
            this.Transform.SetTransformGeom(this);
        }

        internal RDF_Boolean3D(IntPtr ptrToRdf, string guid)
            : base(guid)
        {
            assignRelations();
            assignProperties();
            assignRules();

            this.Transform = new RDF_Transformation();
            this.Transform.SetTransformGeom(this);
        }

        internal RDF_Boolean3D(SELECTED__CONCEPT__STRUCT sconcept, string guid)
            : base(sconcept, guid)
        {
            assignRelations();
            assignProperties();
            assignRules();

            //this.Transform = new RDF_Transformation();
            //this.Transform.SetTransformGeom(this);
        }

        private void assignRules()
        {
            foreach (Rule r in this.rulelist)
            {
                if (r.RHS.Value != null)
                {
                    switch (r.LHS.Name)
                    {
                        case "type":
                            this.Type = (RDF_Boolean3D.BooleanType)r.RHS.Value.DoubleValue;
                            break;
                        case "setting":
                            this.Setting = r.RHS.Value.DoubleValue;
                            break;
                    }
                }
            }
        }

        private void assignProperties()
        {
            foreach (DatatypeProperty prop in this.propertylist)
            {
                if (prop.ValueData != null)
                {
                    switch (prop.Name)
                    {
                        case "type":
                            this.Type = (RDF_Boolean3D.BooleanType)((int)prop.ValueData.DoubleValue);
                            break;
                        case "setting":
                            this.Setting = prop.ValueData.DoubleValue;
                            break;
                    }
                }
            }
        }

        private void assignRelations()
        {
            foreach (Relation r in this.relationlist)
            {
                if (r.Relations.Count > 0)
                {
                    foreach (IntPtr ptr in r.Relations)
                    {
                        RDF_Abstract rdfDim3 = GetRDFObjectFromRelation(ptr);
                        if (rdfDim3 is RDF_Dim3)
                        {
                            switch (r.Name)
                            {
                                case "firstObject":
                                    this.First = (RDF_Dim3)rdfDim3;
                                    break;
                                case "secondObject":
                                    this.Second = (RDF_Dim3)rdfDim3;
                                    break;
                            }
                        }
                    }
                }
            }
        }

        public void SetFirstObject(RDF_Dim3 first)
        {
            this.First = first;
            Matrix3D mtx3D = Matrix3D.Identity;
            RDF_Matrix mtx = new RDF_Matrix(mtx3D);
            this.Transform.SetMatrix(mtx);

            //this.Transform.SetMatrix(first.Transform.Matrix);
            IntPtr fstPtr = this.First.HasTransform ? this.First.Transform.PtrToConcept : this.First.PtrToConcept;
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "firstObject", fstPtr);
        }

        public void SetSecondObject(RDF_Dim3 second)
        {
            this.Second = second;
            IntPtr sndPtr = this.Second.HasTransform ? this.Second.Transform.PtrToConcept : this.Second.PtrToConcept;
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "secondObject", sndPtr);
        }

        public GeometryModel3D MoveConceptMesh()
        {
            if (this.Geometry != null && this is RDF_Boolean3D)
            {
                var newMtx = this.Transform.GetMatrix3D().Clone();
                var solveMtx = this.GeometryTransform.Solve(newMtx);
                this.Geometry.Transform = new MatrixTransform3D(solveMtx);
                //this.Transform
                //this.Geometry.Transform
            }
            else
                return this.RenderConceptMesh(group);
            return this.Geometry;
        }

        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            RDF_Boolean3D cloneObj = new RDF_Boolean3D(BooleanType.SUBSTRACT_1);
            cloneObj.InstantiatedFrom = this;
            if (this.First != null)
                cloneObj.SetFirstObject(this.First);
            if (this.Second != null)
                cloneObj.SetSecondObject(this.Second);

            var oriMatrix3D = this.Transform.GetMatrix3D();

            RDF_Matrix newmtx = new RDF_Matrix(oriMatrix3D);

            RDF_Transformation translate = new RDF_Transformation();
            translate.SetMatrix(newmtx);
            translate.SetTransformGeom(cloneObj);
            return cloneObj;
        }

        /// <summary>
        /// remove the connections to the swept solid so that we will not keap this in the memory
        /// </summary>
        internal void Remove()
        {
            this.First.Remove(true);
            this.Second.Remove(true);
            //BackRelation removingRelation = null;
            //foreach (var br in this.SweptProfile.Relations)
            //{
            //    if (br.Name == BackRelationName.SweptSolid_Profile_Outer)
            //    {
            //        if (br.RelatedTo == this)
            //            removingRelation = br;
            //    }
            //}
            //if (removingRelation != null)
            //    this.SweptProfile.Relations.Remove(removingRelation);

            //// remove directions from view port
            //if (this.Direction is RDF_Transformation)
            //{
            //    ((RDF_Transformation)this.Direction).Remove();
            //    if (((RDF_Transformation)this.Direction).TransformGeom is RDF_Polygon3D)
            //    {
            //        var polygon3D = (((RDF_Transformation)this.Direction).TransformGeom as RDF_Polygon3D);
            //        if (RDFWrapper.All_RDF_Objects.ContainsKey(polygon3D.GUID))
            //            removingQueue.Enqueue(polygon3D);
            //    }
            //}
        }
    }
}
