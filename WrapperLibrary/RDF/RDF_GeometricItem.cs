﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using GeomLib.Constraint;

namespace WrapperLibrary.RDF
{
    public abstract class RDF_GeometricItem : RDF_Abstract, GeometryProduct
    {
        public bool IsSelected { set; get; }
        public bool IsVirtual { get; set; }
        public bool IsInArray { get; set; }
        public List<BackRelation> BackRelations { get; set; }
        public RDF_GeometricItem InstantiatedFrom { set; get; }
        public List<Constraint> Controlling { set; get; }
        public RDF_Transformation Transform { set; get; }
        public GeometryModel3D Geometry { internal set; get; }
        public GeometricConstraint Constraint { get; set; }
        public string ShapeSemantics { set; get; }
        public bool HasTransform
        {
            get
            {
                if (this.Transform != null)
                    return true;
                else
                    return false;
            }
        }
        public RDF_GeometricItem(string guid = "")
            : base(guid)
        {
            this.Constraint = new GeometricConstraint(this);
            this.IsVirtual = false;
            this.IsInArray = false;
            this.BackRelations = new List<BackRelation>();
            this.Controlling = new List<Constraint>();
        }

        internal RDF_GeometricItem(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.Constraint = new GeometricConstraint(this);
            this.IsVirtual = false;
            this.IsInArray = false;
            this.BackRelations = new List<BackRelation>();
            this.Controlling = new List<Constraint>();
        }
        internal RDF_GeometricItem(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.Constraint = new GeometricConstraint(this);
            this.IsInArray = false;
            this.IsVirtual = false;
            this.BackRelations = new List<BackRelation>();
            this.Controlling = new List<Constraint>();
        }
        public void Remove(bool isShallowRemove = false, bool resettingArray = true, bool resettingControllingObject = true)
        {
            removingQueue = new Queue<RDF_GeometricItem>();
            removingQueue.Enqueue(this);

            while (removingQueue.Count > 0)
            {
                RDF_GeometricItem curobj = removingQueue.Dequeue();

                if (curobj is RDF_Polygon3D)
                {
                    if (((RDF_Polygon3D)curobj).pathgeom != null)
                    {
                        var wire = ((RDF_Polygon3D)curobj).pathgeom;
                        if (wire.WireType == Petzold.Media3D.WireTypeEnum.Pivot)
                            continue;
                    }
                }

                if (curobj.HasTransform)
                    curobj.Transform.Remove();

                // remove the reference from constraint obj
                // and reset the constraint
                if (curobj.IsInArray && curobj.InstantiatedFrom != null)
                {
                    bool isremoved = curobj.InstantiatedFrom.Constraint.ConstraintObjects.Remove(this);

                    if (curobj.InstantiatedFrom.Constraint.ConstraintObjects.Count > 0 && resettingArray)
                        curobj.InstantiatedFrom.Constraint.Reset();
                }

                // remove the controlling obj will remove all the constraint objs
                if (resettingControllingObject)
                {
                    foreach (var constraint in curobj.Controlling)
                    {
                        //var constraint = curobj.Controlling;
                        var cstObjs = new List<GeometryProduct>(constraint.ConstraintObjects);
                        var subject = constraint.Subject;

                        foreach (var cstobj in cstObjs)
                        {
                            if (cstobj is RDF_GeometricItem)
                            {
                                //((RDF_GeometricItem)cstobj).InstantiatedFrom = null;
                                ((RDF_GeometricItem)cstobj).IsInArray = false;
                                if (cstobj is RDF_Face2D)
                                {
                                    var relations = ((RDF_Face2D)cstobj).Outer.BackRelations;
                                    var relation = relations.Find(r => r.RelatedTo == cstobj);
                                    relations.Remove(relation);
                                    // Todo: Need to clone this polygon and create a new instance for the face outer
                                }
                            }
                        }
                        if (constraint.Subject != null)
                        {
                            ((RDF_GeometricItem)constraint.Subject).Constraint.Reset();
                            // need to also remove the back relations for the polygon attach to the face
                        }
                        //if (RDFWrapper.All_RDF_Objects.ContainsKey(((RDF_GeometricItem)cstobj).GUID))
                        //    removingQueue.Enqueue((RDF_GeometricItem)cstobj);
                    }
                    curobj.Controlling = new List<Constraint>();
                }

                if (!isShallowRemove)
                    foreach (var rel in curobj.BackRelations)
                        if (rel.RelatedTo != null && rel.RelatedTo is RDF_GeometricItem && 
                            RDFWrapper.All_RDF_Objects.ContainsKey(rel.RelatedTo.GUID) && 
                            rel.Name != BackRelationName.Dim1_Polygon_Width && rel.Name != BackRelationName.Dim2_RelatedSolid
                            && rel.Name != BackRelationName.Dim1_SweptSolid_Profile_Outer)
                            removingQueue.Enqueue((RDF_GeometricItem)rel.RelatedTo);

                // the real part for removing the geometry that associate to the RDF_Object
                if (curobj.Geometry != null)
                {
                    if (curobj is RDF_Dim3 && ((RDF_Dim3)curobj).group != null)
                    {
                        RDF_Dim3 objDim3 = (RDF_Dim3)curobj;
                        objDim3.group.Children.Remove(curobj.Geometry);
                        if (objDim3 is RDF_SweptAreaSolid)
                            ((RDF_SweptAreaSolid)objDim3).Remove();
                        else if (objDim3 is RDF_Boolean3D)
                            ((RDF_Boolean3D)objDim3).Remove();
                    }
                    else if (curobj is RDF_Dim2)
                    {
                        RDF_Dim2 objDim2 = (RDF_Dim2)curobj;
                        objDim2.group.Children.Remove(curobj.Geometry);
                        if (objDim2 is RDF_Face2D && objDim2.InstantiatedFrom == null)
                            ((RDF_Face2D)objDim2).Remove();
                    }
                }
                else if (curobj is RDF_Dim1)
                {
                    RDF_Dim1 objDim1 = (RDF_Dim1)curobj;
                    if (objDim1.pathgeom != null && objDim1.viewport != null)
                        objDim1.viewport.Children.Remove(objDim1.pathgeom);
                    if (objDim1 is RDF_Polygon3D && ((RDF_Polygon3D)objDim1).DirectionFrom != null)
                    {
                        var removingPl = (RDF_Polygon3D)objDim1;
                        if (RDFWrapper.All_RDF_Objects.ContainsKey(removingPl.DirectionFrom.GUID))
                            removingQueue.Enqueue(removingPl.DirectionFrom);
                    }       
                }
                if (RDFWrapper.All_RDF_Objects.ContainsKey(curobj.GUID))
                    RDFWrapper.All_RDF_Objects.Remove(curobj.GUID);
            }
        }

        internal static Queue<RDF_GeometricItem> removingQueue;
        public abstract RDF_GeometricItem Clone(bool isCopyingEdge = false);
    }
}
