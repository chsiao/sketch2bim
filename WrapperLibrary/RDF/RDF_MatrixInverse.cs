﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public class RDF_MatrixInverse : RDF_MatrixAbstract
    {
        public override Matrix3D Matrix3D { 
            get 
            {
                Matrix3D inverseMatrix = OriMatrix.Matrix3D;
                inverseMatrix.Invert();
                return inverseMatrix; 
            } 
        }

        public override Point3D Translate
        {
            get
            {
                var mtx = this.Matrix3D;
                Point3D translate = new Point3D(mtx.OffsetX, mtx.OffsetY, mtx.OffsetZ);
                return translate;
            }
        }
        public RDF_MatrixAbstract OriMatrix { private set; get; }

        public RDF_MatrixInverse(RDF_MatrixAbstract oriMatrix, string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "InverseMatrix");
            this.SetOriMatrix(oriMatrix);
        }
        public void SetOriMatrix(RDF_MatrixAbstract oriMatrix)
        {
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "matrix", oriMatrix.PtrToConcept);
            this.OriMatrix = oriMatrix;
        }
        public override RDF_MatrixAbstract Clone()
        {
            RDF_MatrixInverse invMtx = new RDF_MatrixInverse(this.OriMatrix.Clone());
            return invMtx;
        }
    }
}
