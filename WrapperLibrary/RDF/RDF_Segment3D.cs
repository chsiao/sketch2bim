﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace WrapperLibrary.RDF
{
    public class RDF_Segment3D : RDF_Dim1
    {
        public RDF_Node3D StartNode { set; get; }
        public RDF_Node3D EndNode { set; get; }
        public RDF_Dim1 InPartOf { set; get; }
        public RDF_Segment3D(string guid = "")
            : base(guid)
        {}

        internal RDF_Segment3D(IntPtr ptrToRDF, string guid)
            : base(ptrToRDF, guid)
        {}

        internal RDF_Segment3D(SELECTED__CONCEPT__STRUCT sconcept, string guid)
            : base(sconcept, guid)
        {}

        public Vector3D GetVector3D()
        {
            if (this.StartNode != null && this.EndNode != null)
            {
                Vector3D v = Point3D.Subtract(this.EndNode.GetPoint3D(), this.StartNode.GetPoint3D());
                v.Normalize();
                return v;
            }
            return new Vector3D();
        }
        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            throw new NotImplementedException();
        }
    }
}
