﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public class RDF_Matrix : RDF_MatrixAbstract
    {
        public override Matrix3D Matrix3D { get { return this.matrix3D.Clone();} }
        public override Point3D Translate { get { return this.translate;} }
        public RDF_Matrix(Matrix3D mtx, string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Matrix");
            this.SetMatrix(mtx);
        }

        internal RDF_Matrix(string guid ="")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Matrix");
        }

        internal RDF_Matrix(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
        }

        internal RDF_Matrix(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {

        }

        public void SetMatrix(Matrix3D mtx)
        {
            this.matrix3D = mtx;

            string[] formula = {   "_43 = " + Math.Round(mtx.OffsetZ, 4), "_42 = " + Math.Round(mtx.OffsetY, 4), "_41 = " + Math.Round(mtx.OffsetX, 4),
                                   "_33 = " + Math.Round(mtx.M33, 4), "_32 = " + Math.Round(mtx.M32, 4), "_31 = " + Math.Round(mtx.M31, 4),
                                   "_23 = " + Math.Round(mtx.M23, 4), "_22 = " + Math.Round(mtx.M22, 4), "_21 = " + Math.Round(mtx.M21, 4),
                                   "_13 = " + Math.Round(mtx.M13, 4), "_12 = " + Math.Round(mtx.M12, 4), "_11 = " + Math.Round(mtx.M11, 4) };

            int[] formulaIndex = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            RDFWrapper.ChangeDataProperties(RDFWrapper.FilePtr, this.PtrToConcept, formula, formulaIndex, formula.Length);
            this.translate = new Point3D(mtx.OffsetX, mtx.OffsetY, mtx.OffsetZ);
        }

        public void SetTranslate(Point3D pt)
        {
            this.translate = new Point3D(Math.Round(pt.X, 4), Math.Round(pt.Y, 4), Math.Round(pt.Z, 4));
            Matrix3D mtx = this.Matrix3D;
            mtx.OffsetX = this.Translate.X;
            mtx.OffsetY = this.Translate.Y;
            mtx.OffsetZ = this.Translate.Z;
            this.matrix3D = mtx;
            string[] formula = { "_43 = " + Math.Round(pt.Z, 4), "_42 = " + Math.Round(pt.Y, 4), "_41 = " + Math.Round(pt.X, 4) };
            int[] formulaIndex = { 0, 1, 2 };
            RDFWrapper.ChangeDataProperties(RDFWrapper.FilePtr, this.PtrToConcept, formula, formulaIndex, formula.Length);
        }

        public override RDF_MatrixAbstract Clone()
        {
            RDF_Matrix clonedMtx = new RDF_Matrix();
            clonedMtx.SetMatrix(this.Matrix3D);
            return clonedMtx;
        }
    }
}
