﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using GeomLib.Constraint;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public class RDF_Line3D : RDF_Segment3D
    {
        public List<Point3D> Pts { set; get; }

        public RDF_Line3D(Point3D pt1, Point3D pt2, string guid = "") 
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Line3Dn");
            Pts = new List<Point3D>();
            Pts.Add(pt1);
            Pts.Add(pt2);
            this.SetPoints(Pts);
            this.Constraint.onSolvingConstraint  += Constraint_onSolvingConstraint;
        }


        internal RDF_Line3D(IntPtr ptrToRDF, string guid)
            : base(ptrToRDF, guid)
        {
            Pts = new List<Point3D>();
            assignProperties();
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }

        internal RDF_Line3D(SELECTED__CONCEPT__STRUCT sconcept, string guid)
            : base(sconcept, guid)
        {
            Pts = new List<Point3D>();
            assignProperties();
            this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
        }

        private void assignProperties()
        {
            foreach (DatatypeProperty prop in this.propertylist)
            {
                if (prop.FormulaData != null && prop.FormulaData.Value.ValueSet != null)
                {
                    int i = 1;
                    double x = 0, y = 0, z = 0;
                    foreach (Formula f in prop.FormulaData.Value.ValueSet.Formulas)
                    {
                        if (f.Value != null)
                        {
                            switch (i % 3)
                            {
                                case 0:
                                    Point3D newpt = new Point3D(x, y, z);
                                    break;
                                case 1:
                                    x = f.Value.DoubleValue;
                                    break;
                                case 2:
                                    y = f.Value.DoubleValue;
                                    break;
                                case 3:
                                    z = f.Value.DoubleValue;
                                    break;
                            }
                        }

                        i++;
                    }
                }
            }
        }

        public void SetPoints(List<Point3D> pts)
        {
            string strpts = "points = { ";
            this.Pts = new List<Point3D>(pts);

            for (int i = 0; i < pts.Count; i++)
            {
                if(i > 0)
                {
                    strpts += "., ";
                }
                strpts += Math.Round(pts[i].X, 4) + "., " + Math.Round(pts[i].Y, 4) + "., " + Math.Round(pts[i].Z, 4);
                if (i == pts.Count - 1)
                {
                    strpts += ".}";
                }
            }

            RDFWrapper.AddRule(RDFWrapper.FilePtr, this.PtrToConcept, strpts);
            this.StartNode = new RDF_Node3D(pts.First());
            this.EndNode = new RDF_Node3D(pts.Last());
            this.StartNode.NextEdge = this;
            this.EndNode.PreEdge = this;
        }

        public void ChangePoints(List<Point3D> pts)
        {
            this.Pts = new List<Point3D>(pts);
            string strpts = "{ ";

            for (int i = 0; i < pts.Count; i++)
            {
                if (i > 0)
                {
                    strpts += "., ";
                }
                strpts += Math.Round(pts[i].X, 4) + "., " + Math.Round(pts[i].Y, 4) + "., " + Math.Round(pts[i].Z, 4);
                if (i == pts.Count - 1)
                {
                    strpts += ".}";
                }
            }

            RDFWrapper.ChangeRule(RDFWrapper.FilePtr, this.PtrToConcept, "points", strpts);
            this.StartNode.SetPoint(pts.First());
            this.EndNode.SetPoint(pts.Last());
        }

        public void ChangePoint(Point3D pt, int index)
        {
            string strpts = "{ ";

            for (int i = 0; i < this.Pts.Count; i++)
            {
                if (i > 0)
                {
                    strpts += "., ";
                }
                if (i == index) this.Pts[i] = pt;
                strpts += Math.Round(this.Pts[i].X, 4) + "., " + Math.Round(this.Pts[i].Y, 4) + "., " + Math.Round(this.Pts[i].Z, 4);
                if (i == this.Pts.Count - 1)
                {
                    strpts += ".}";
                }
            }

            RDFWrapper.ChangeRule(RDFWrapper.FilePtr, this.PtrToConcept, "points", strpts);
            this.StartNode.SetPoint(this.Pts.First());
            this.EndNode.SetPoint(this.Pts.Last());
        }

        void Constraint_onSolvingConstraint(Constraint constrain, object curobj)
        {
            if (constrain.ConstraintObjects.Count == 2 && curobj is double)
            {
                double curvalue = (double)curobj;
                var firstCstObj = constrain.ConstraintObjects[0];
                var secCstObj = constrain.ConstraintObjects[1];

                // dealing with line for now
                if (firstCstObj is RDF_Line3D && secCstObj is RDF_Line3D)
                {
                    var firstCstLine = (RDF_Line3D)firstCstObj; // should be the foundation line
                    Vector3D thisvector = this.GetVector3D();
                    //var secCstLine = (RDF_Line3D)secCstObj;   // should be this line


                    // change only endnode 
                    if (firstCstLine.StartNode == this.StartNode || firstCstLine.EndNode == this.StartNode
                        || this.StartNode.PreEdge is RDF_Arc3D)
                    {
                        var staticPt = this.StartNode.GetPoint3D();
                        var changingNode = this.EndNode;
                        
                        if (changingNode.NextEdge is RDF_Line3D)
                        {
                            Point3D changedPt = new Point3D(staticPt.X + curvalue * thisvector.X,
                                staticPt.Y + curvalue * thisvector.Y,
                                staticPt.Z + curvalue * thisvector.Z);
                            changingNode.SetNode(changedPt);
                            renderLine3D();
                        }
                    }
                    // change only start node
                    else if (firstCstLine.StartNode == this.EndNode || firstCstLine.EndNode == this.EndNode
                        || this.EndNode.NextEdge is RDF_Arc3D)
                    {
                        var staticPt = this.EndNode.GetPoint3D();
                        var changingNode = this.StartNode;

                        if (changingNode.PreEdge is RDF_Line3D)
                        {
                            Point3D changedPt = new Point3D(staticPt.X - curvalue * thisvector.X,
                                staticPt.Y - curvalue * thisvector.Y,
                                staticPt.Z - curvalue * thisvector.Z);
                            changingNode.SetNode(changedPt);
                            renderLine3D();
                        }
                    }
                    // change both nodes
                    else if(this.EndNode.NextEdge is RDF_Line3D && this.StartNode.PreEdge is RDF_Line3D)
                    {
                        var endpt = this.EndNode.GetPoint3D();
                        var startpt = this.StartNode.GetPoint3D();
                        var midpt = new Point3D((endpt.X + startpt.X) / 2, (endpt.Y + startpt.Y) / 2, (endpt.Z + startpt.Z) / 2);
                        var newlength = curvalue / 2;

                        Point3D newEndPt = new Point3D(midpt.X + newlength * thisvector.X,
                            midpt.Y + newlength * thisvector.Y,
                            midpt.Z + newlength * thisvector.Z);
                        Point3D newStPt = new Point3D(midpt.X - newlength * thisvector.X,
                            midpt.Y - newlength * thisvector.Y,
                            midpt.Z - newlength * thisvector.Z);

                        this.EndNode.SetNode(newEndPt);
                        this.StartNode.SetNode(newStPt);
                        renderLine3D();
                    }
                }
            }
        }

        public void renderLine3D()
        {
            var polyline = this.InPartOf;
            if (polyline.HasTransform)
            {
                if (polyline is RDF_Polygon3D)
                {
                    RDF_Polygon3D polygon = (RDF_Polygon3D)polyline;

                    foreach (var backR in polygon.BackRelations)
                    {
                        // cause rendering problems, not sure what's this for??
                        //if (backR.Name == BackRelationName.Face_Outer)
                        //    ((RDF_Face2D)backR.RelatedTo).RenderConceptMesh(null);
                        //else 
                        if (backR.Name == BackRelationName.Dim1_SweptSolid_Profile_Outer)
                            ((RDF_SweptAreaSolid)backR.RelatedTo).RenderConceptMesh(null);
                    }
                    if (polygon.Face != null)
                    {
                        polygon.Face.RenderShapeMesh(null);
                    }
                    // cause the polygon shifting back to original place. not sure what's this for??
                    //polygon.Transform.Matrix.GetLeafMatrix().SetTranslate(new Point3D(polygon.StartPt.X, polygon.StartPt.Y, polygon.StartPt.Z));
                }
                var newpath = RDF_Dim1.RenderConceptLine(polyline.Transform);

            }
            else
                polyline.RenderConceptLine();
        }

        public double GetLength()
        {
            double l = 0;
            if (this.Pts.Count > 1)
            {
                Point3D prept = Pts.First();
                for (int i = 1; i< Pts.Count; i++)
                {
                    var pt = this.Pts[i];
                    l += pt.distanceTo(prept);

                    prept = pt;
                }

            }
            return l;
        }
    }
}
