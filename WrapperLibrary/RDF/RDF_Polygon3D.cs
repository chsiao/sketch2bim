﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public class RDF_Polygon3D : RDF_Dim1
    {
        public List<RDF_Segment3D> Segments { private set; get; }
        public RDF_Polygon3D DirectionFrom { private set; get; }
        public RDF_Dim1 WidthFrom { private set; get; }
        public RDF_Node3D StartPt
        {
            get
            {
                return this.Segments.First().StartNode;
            }
        }
        public RDF_Node3D EndPt
        {
            get
            {
                return this.Segments.Last().EndNode;
            }
        }
        public RDF_Face2D Face { get; set; }
        public bool IsCircle { set; get; }
        public bool IsClosedProfile
        {
            get
            {
                if (this.Segments.Count >= 2 && this.StartPt != null && this.EndPt != null)
                {
                    double diff = Math.Round(this.StartPt.X, 4) - Math.Round(this.EndPt.X, 4)
                        + Math.Round(this.StartPt.Y, 4) - Math.Round(this.EndPt.Y, 4)
                        + Math.Round(this.StartPt.Y, 4) - Math.Round(this.EndPt.Y, 4);
                    if (diff == 0)
                        return true;
                }
                return false;
            }
        }

        public RDF_Polygon3D(string guid = "") : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Polygon3D");
            this.Segments = new List<RDF_Segment3D>();
            this.IsCircle = false;
        }

        internal RDF_Polygon3D(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.Segments = new List<RDF_Segment3D>();
            assignRelations();
        }

        internal RDF_Polygon3D(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.Segments = new List<RDF_Segment3D>();
            assignRelations();
        }

        public void AddPart(RDF_Segment3D seg, bool chkClose = true)
        {
            this.Segments.Add(seg);
            seg.InPartOf = this;
            if (this.Segments.Count == 1)
            {
                RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "lineParts", seg.PtrToConcept);
            }
            else
            {
                int lastindex = this.Segments.Count - 2;
                RDF_Node3D preEndNode = this.Segments[lastindex].EndNode;
                seg.StartNode = preEndNode;
                preEndNode.NextEdge = seg;

                if(chkClose)
                    CheckIfClosed();

                RDFWrapper.AddRelation(RDFWrapper.FilePtr, this.PtrToConcept, "lineParts", seg.PtrToConcept);
            }

        }

        public void CheckIfClosed()
        {
            if (this.Segments.Count == 0)
            {
                this.IsClosed = false;
                return;
            }
            RDF_Node3D preSTNode = this.Segments[0].StartNode;
            RDF_Segment3D seg = this.Segments.Last();
            if (preSTNode.GetPoint3D().distanceTo(seg.EndNode.GetPoint3D()) < 5)
            {
                seg.EndNode = preSTNode;
                preSTNode.PreEdge = seg;
                this.IsClosed = true;
                this.SetCentroid();
            }
        }

        /// <summary>
        /// currently only used for offset generation
        /// </summary>
        /// <param name="genpl"></param>
        public void SetDirectionPolygon(RDF_Polygon3D genpl)
        {
            if (this.DirectionFrom != null)
                RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "directedFrom", genpl.PtrToConcept);
            else
                RDFWrapper.AddRelation(RDFWrapper.FilePtr, this.PtrToConcept, "directedFrom", genpl.PtrToConcept);

            this.DirectionFrom = genpl;

            BackRelation br = new BackRelation(genpl);
            br.Name = BackRelationName.Dim1_Polygon_Direction;
            br.RelatedTo = this;
            genpl.BackRelations.Add(br);
        }

        public void SetWidthLine(RDF_Dim1 widthPl)
        {
            if (this.WidthFrom != null)
                RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "widthFrom", widthPl.PtrToConcept);
            else
                RDFWrapper.AddRelation(RDFWrapper.FilePtr, this.PtrToConcept, "widthFrom", widthPl.PtrToConcept);

            this.WidthFrom = widthPl;

            BackRelation br = new BackRelation(widthPl);
            br.Name = BackRelationName.Dim1_Polygon_Width;
            br.RelatedTo = this;
            widthPl.BackRelations.Add(br);
        }

        public void CleanAllParts()
        {
            this.CleanRelations("lineParts", true);
            this.Segments = new List<RDF_Segment3D>();
        }

        private void SetCentroid()
        {
            List<Point3D> polygonPts = new List<Point3D>();
            foreach (var segment in this.Segments)
                polygonPts.Add(segment.StartNode.GetPoint3D());

            this.Centroid = BasicGeomLib.Centroid(polygonPts);
        }

        private void assignRelations()
        {
            foreach (Relation r in this.relationlist)
            {
                if (r.Relations.Count > 0)
                {
                    foreach (IntPtr ptrR in r.Relations)
                    {
                        RDF_Abstract rdfDim1 = GetRDFObjectFromRelation(ptrR);
                        if (rdfDim1 is RDF_Segment3D)
                        {
                            switch (r.Name)
                            {
                                case "lineParts":
                                    this.Segments.Add((RDF_Segment3D)rdfDim1);
                                    break;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// create inner polygons based on the offset from this polygon
        /// </summary>
        /// <returns></returns>
        public RDF_Polygon3D CreateInnerPolygon()
        {
            RDF_Polygon3D innerPolygon= new RDF_Polygon3D();
            if (this.Transform.Matrix is RDF_MatrixInverse)
            {
                var inverseMatrix = this.Transform.Matrix as RDF_MatrixInverse;
                if (inverseMatrix.OriMatrix is RDF_MatrixMultiplication)
                {
                    var multiMtx = inverseMatrix.OriMatrix as RDF_MatrixMultiplication;
                    this.Segments.innerPolygonCreateRDFSegments(innerPolygon, multiMtx.FirstMatrix.Matrix3D);
                }
            }
            //this.Segments.CreateRDFSegments(innerPolygon, );
            return innerPolygon;
        }

        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            RDF_Polygon3D clonedPl = new RDF_Polygon3D();
            clonedPl.InstantiatedFrom = this;
            var oriMatrix3D = this.Transform.GetMatrix3D();

            // add all the segments into the cloned polygon
            foreach (var seg in this.Segments)
            {
                clonedPl.AddPart(seg);
            }
            clonedPl.IsClosed = this.IsClosed;
            clonedPl.IsCircle = this.IsCircle;
            if (clonedPl.Transform != null && clonedPl.Transform.Matrix is RDF_Matrix)
            {
                // try to move the shape to the right position
                var clonedMatrix3D = (RDF_Matrix)clonedPl.Transform.Matrix;
                clonedMatrix3D.SetMatrix(oriMatrix3D);
            }
            else
            {
                RDF_Matrix newmtx = new RDF_Matrix(oriMatrix3D);
                RDF_Transformation translate = new RDF_Transformation();
                translate.SetMatrix(newmtx);
                translate.SetTransformGeom(clonedPl);
            }

            BackRelation br = new BackRelation(clonedPl);
            br.RelatedTo = this;
            br.Name = BackRelationName.Dim1_Clone;
            clonedPl.BackRelations.Add(br);

            return clonedPl;
        }
    }
}
