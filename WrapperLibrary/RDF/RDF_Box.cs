﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperLibrary.RDF
{
    public class RDF_Box : RDF_Dim3
    {
        public double Width { get; internal set; }
        public double Length { get; internal set; }
        public double Height { get; internal set; }

        public RDF_Box(string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Box");
        }

        internal RDF_Box(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {

        }

        internal RDF_Box(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {

        }

        public void SetWidth(double width)
        {
            this.Width = width;
            string strwidth = "start = " + this.Width;
            RDFWrapper.AddRule(RDFWrapper.FilePtr, this.PtrToConcept, strwidth);
        }

        public void SetHeight(double height)
        {
            this.Height = height;
            string strheight = "height = " + this.Height;
            RDFWrapper.AddRule(RDFWrapper.FilePtr, this.PtrToConcept, strheight);
        }

        public void SetLength(double length)
        {
            this.Length = length;
            string strlength = "length = " + this.Length;
            RDFWrapper.AddRule(RDFWrapper.FilePtr, this.PtrToConcept, strlength);
        }

        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            throw new NotImplementedException();
        }
    }
}
