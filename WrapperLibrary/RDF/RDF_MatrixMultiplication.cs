﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace WrapperLibrary.RDF
{
    /// <summary>
    /// This Class is mainly for supporting the Children-Parent Relationship Matrix
    /// </summary>
    public class RDF_MatrixMultiplication : RDF_MatrixAbstract
    {
        public RDF_MatrixAbstract FirstMatrix { private set; get; }
        public RDF_MatrixAbstract SecondMatrix { private set; get; }
        public override Matrix3D Matrix3D
        {
            get
            {
                if (FirstMatrix == null) return SecondMatrix.Matrix3D;
                if (SecondMatrix == null) return FirstMatrix.Matrix3D;
                if (FirstMatrix != null && SecondMatrix != null)
                {
                    Matrix3D resultMtx = Matrix3D.Multiply(FirstMatrix.Matrix3D, SecondMatrix.Matrix3D);
                    return resultMtx;
                }

                return new Matrix3D();
            }
        }
        public override Point3D Translate
        {
            get
            {
                var mtx = this.Matrix3D;
                Point3D translate = new Point3D(mtx.OffsetX, mtx.OffsetY, mtx.OffsetZ);
                return translate;
            }
        }
        public RDF_MatrixMultiplication(RDF_MatrixAbstract first, RDF_MatrixAbstract second, string guid = "")
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "MatrixMultiplication");
            this.SetFirstMatrix(first);
            this.SetSecondtMatrix(second);
        }

        public RDF_MatrixMultiplication(string guid ="")
            :base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "MatrixMultiplication");
        }

        public void SetFirstMatrix(RDF_MatrixAbstract mtx)
        {
            this.FirstMatrix = mtx;
            mtx.ChildrenMatrix.Add(this); // add backpointer
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "firstMatrix", mtx.PtrToConcept);

            this.matrix3D = this.FirstMatrix.Matrix3D;
            if (FirstMatrix != null && SecondMatrix != null)
                this.matrix3D = Matrix3D.Multiply(FirstMatrix.Matrix3D, SecondMatrix.Matrix3D);
            this.translate = new Point3D(this.matrix3D.OffsetX, this.matrix3D.OffsetY, this.matrix3D.OffsetZ);
        }

        public void SetSecondtMatrix(RDF_MatrixAbstract mtx)
        {
            this.SecondMatrix = mtx;
            mtx.ChildrenMatrix.Add(this); // add backpointer
            RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "secondMatrix", mtx.PtrToConcept);

            this.matrix3D = this.SecondMatrix.Matrix3D;
            if (FirstMatrix != null && SecondMatrix != null)
                this.matrix3D = Matrix3D.Multiply(FirstMatrix.Matrix3D, SecondMatrix.Matrix3D);
            this.translate = new Point3D(this.matrix3D.OffsetX, this.matrix3D.OffsetY, this.matrix3D.OffsetZ);
        }

        public override RDF_MatrixAbstract Clone()
        {
            RDF_MatrixMultiplication multMtx = new RDF_MatrixMultiplication();
            multMtx.SetFirstMatrix((RDF_MatrixAbstract)this.FirstMatrix.Clone());
            multMtx.SetSecondtMatrix((RDF_MatrixAbstract)this.SecondMatrix.Clone());
            return multMtx;
        }
    }
}
