﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperLibrary.RDF
{
    public class BackRelation
    {
        public BackRelation()
        {
            this.Relating = null;
        }
        public BackRelation(RDF_GeometricItem relatingItem)
        {
            this.Relating = relatingItem;
        }
        public BackRelationName Name { set; get; }
        public RDF_Abstract RelatedTo { set; get; }
        public RDF_GeometricItem Relating { set; get; }
        public void Render()
        {
            switch (this.Name)
            {
                case BackRelationName.Dim1_Face_Inner:
                case BackRelationName.Dim1_Face_Outer:
                    RDF_Face2D face = (RDF_Face2D)this.RelatedTo;
                    face.RenderConceptMesh();
                    break;
                case BackRelationName.Dim1_SweptSolid_Direction:
                    break;
                case BackRelationName.Dim1_SweptSolid_Profile_Inner:
                case BackRelationName.Dim1_SweptSolid_Profile_Outer:
                    break;
            }
        }
        public BackRelation Clone()
        {
            return (BackRelation)this.MemberwiseClone();
        }
    }
    public enum BackRelationName
    {
        None,
        Dim1_SweptSolid_Direction,
        Dim1_SweptSolid_Profile_Outer,
        Dim1_SweptSolid_Profile_Inner,
        Dim1_Clone,
        Dim1_Face_Outer,
        Dim1_Face_Inner,
        Dim1_Polygon_Direction,
        Dim1_Polygon_Width,
        Dim2_RelatedSolid,
        Dim2_Clone,
        Dim3_BooleanProduct1,
        Dim3_BooleanProduct2,
        Dim3_Clone
    }
}
