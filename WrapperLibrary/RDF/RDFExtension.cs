﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Drawing;

namespace WrapperLibrary.RDF
{
    public static class RDFExtension
    {
        public static readonly DependencyProperty RDFObjectProperty =
            DependencyProperty.RegisterAttached("RDF_Obj", typeof(RDF_Abstract), typeof(RDFExtension), new PropertyMetadata(default(RDF_Abstract)));

        public static void SetRDFObjectProperty(UIElement element, RDF_Abstract value)
        {
            element.SetValue(RDFObjectProperty, value);
        }

        public static RDF_Abstract GetRDFObjectProperty(UIElement element)
        {
            return (RDF_Abstract)element.GetValue(RDFObjectProperty);
        }

        public static List<PointF> ConvertToPointF(this List<RDF_Segment3D> segs)
        {
            List<PointF> newpts = new List<PointF>();
            foreach (RDF_Segment3D seg in segs)
            {
                newpts.Add(new PointF((float)seg.StartNode.X, (float)seg.StartNode.Y));
            }
            return newpts;
        }
    }
}
