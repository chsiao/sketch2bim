﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using TriangleNet;
using TriangleNet.Geometry;
using GeomLib;
using Petzold.Media3D;

namespace WrapperLibrary.RDF
{
    public class RDF_Face2D : RDF_Dim2
    {
        private List<GeometryModel3D> geometryMesh;
        public RDF_Polygon3D Outer { internal set; get; }
        public List<RDF_Polygon3D> Inners { internal set; get; }
        public RDF_Face2D(RDF_Polygon3D outer, string guid = "", bool isInherited = true)
            : base(guid)
        {
            this.PtrToConcept = RDFWrapper.AddConcept(RDFWrapper.FilePtr, this.GUID, "Face2D");
            this.Outer = new RDF_Polygon3D();
            this.Inners = new List<RDF_Polygon3D>();
            this.geometryMesh = new List<GeometryModel3D>();
            if (outer.IsClosed)
            {
                this.Outer = outer;
                RDFWrapper.AddRelation(RDFWrapper.FilePtr, this.PtrToConcept, "outerPolygon", outer.PtrToConcept);
                if (isInherited)
                {
                    outer.Face = this;
                }
                    addOuterBackRelation(outer);
                
                if (outer.HasTransform)
                {
                    RDF_MatrixAbstract outerMtx = outer.Transform.Matrix;
                    outerMtx = isInherited ? outerMtx : new RDF_Matrix(outerMtx.Matrix3D);
                    RDF_Transformation outerTransform = new RDF_Transformation();
                    outerTransform.SetMatrix(outerMtx);
                    outerTransform.SetTransformGeom(this);
                }

                this.Constraint.onSolvingConstraint += Constraint_onSolvingConstraint;
            }
        }

        void Constraint_onSolvingConstraint(GeomLib.Constraint.Constraint constrainObj, object curvalue)
        {
            if(constrainObj is GeomLib.Constraint.GeometricConstraint && curvalue is double)
                RotateShapeInDegree((double)curvalue - ((GeomLib.Constraint.GeometricConstraint)constrainObj).OriConstraintValue.Value);
        }

        public void RotateToConstraint()
        {
            RDF_Polygon3D ctlPl = null;
            Point3D? rotLoc = null;
            if (this.InstantiatedFrom != null && this.InstantiatedFrom.Constraint.ControllingObject is RDF_Polygon3D)
            {
                ctlPl = (RDF_Polygon3D)this.InstantiatedFrom.Constraint.ControllingObject;
                rotLoc = this.InstantiatedFrom.Constraint.Location;
            }
            else if (this.Constraint.ControllingObject is RDF_Polygon3D)
            {
                ctlPl = (RDF_Polygon3D)this.Constraint.ControllingObject;
                rotLoc = this.Constraint.Location;
            }
            if (rotLoc != null && ctlPl != null && this.Constraint.Type == ConstraintType.Angle)
            {
                var rotateOnGlobal = this.Transform.GetMatrix3D().Transform(rotLoc.Value);
                var fstEdge = this.Outer.PathGeometry;
                var sndEdge = ctlPl.pathgeom;
                var dist1 = fstEdge.LineCollection.ToList<Point3D>().FindClosetPointOnPolyline(rotateOnGlobal, this.Transform.GetMatrix3D());
                var dist2 = sndEdge.LineCollection.ToList<Point3D>().FindClosetPointOnPolyline(rotateOnGlobal, sndEdge.Transform.Value);
                var edge1 = new Point3D[2] { fstEdge.LineCollection[dist1.Item1 - 1], fstEdge.LineCollection[dist1.Item1] };
                var edge2 = new Point3D[2] { sndEdge.LineCollection[dist2.Item1 - 1], sndEdge.LineCollection[dist2.Item1] };
                this.Transform.GetMatrix3D().Transform(edge1);
                sndEdge.Transform.Value.Transform(edge2);
                var vec1 = new Vector3D(edge1[1].X - edge1[0].X, edge1[1].Y - edge1[0].Y, edge1[1].Z - edge1[0].Z);
                var vec2 = new Vector3D(edge2[1].X - edge2[0].X, edge2[1].Y - edge2[0].Y, edge2[1].Z - edge2[0].Z);

                double angle = Vector3D.AngleBetween(vec1, vec2);
                RotateShapeInDegree(angle - 90);
                System.Diagnostics.Debug.WriteLine(dist2.Item1 + ", " + angle + ", ");

            }
        }

        private void RotateShapeInDegree(double rot)
        {
            // use z axis for now
            AxisAngleRotation3D rot3D = new AxisAngleRotation3D(new Vector3D(0, 0, 1), rot);
            RotateTransform3D rotTransform = new RotateTransform3D(rot3D, this.Outer.Centroid);

            RDF_Polygon3D outer = this.Outer;
            RDFLibrary.tmpRotation = rot3D;
            // set the curRotatoin value in the RDFLibrary
            this.Transform.Matrix.TransformRDFGeoms(rotTransform.Value, RDFLibrary.TransformFace2D);
            // reset it!!
            RDFLibrary.tmpRotation = null;
        }

        public void SetOuterPolygon(RDF_Polygon3D outer, bool updatingTransform = true)
        {
            if (outer.IsClosed)
            {
                this.Outer = outer;
                outer.Face = this;
                RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "outerPolygon", outer.PtrToConcept);
                if (outer.HasTransform && updatingTransform)
                {
                    RDF_MatrixAbstract outerMtx = outer.Transform.Matrix;
                    RDF_Transformation outerTransform = new RDF_Transformation();
                    outerTransform.SetMatrix(outerMtx);
                    outerTransform.SetTransformGeom(this);
                }
                addOuterBackRelation(outer);
            }
        }

        public void AddInnerPolygon(RDF_Polygon3D inner)
        {
            if (inner.IsClosed)
            {
                this.Inners.Add(inner);
                inner.Face = this;
                IntPtr innerptr = inner.PtrToConcept;
                if (inner.HasTransform && this.Outer.HasTransform)
                {
                    //RDF_Matrix innermatrix = inner.Transform.Matrix;
                    //Matrix3D innerTranslate = innermatrix.Matrix3D;
                    //Matrix3D outerTranslate = Outer.Transform.Matrix.Matrix3D;
                    //Point3D newtranslate = new Point3D(innerTranslate.OffsetX - outerTranslate.OffsetX, innerTranslate.OffsetY - outerTranslate.OffsetY, innerTranslate.OffsetZ - outerTranslate.OffsetZ);
                    //innermatrix.setTranslate(newtranslate);
                    //innerptr = inner.Transform.PtrToConcept;
                    innerptr = inner.Transform.PtrToConcept;
                }

                if (this.Inners.Count == 1)
                    RDFWrapper.ChangeRelations(RDFWrapper.FilePtr, this.PtrToConcept, "innerPolygons", innerptr);
                else
                    RDFWrapper.AddRelation(RDFWrapper.FilePtr, this.PtrToConcept, "innerPolygons", innerptr);

                BackRelation br = new BackRelation(inner);
                br.Name = BackRelationName.Dim1_Face_Inner;
                br.RelatedTo = this;
                inner.BackRelations.Add(br);
            }
        }

        public void RenderShapeMesh(Step pstep, Model3DGroup group = null, bool usingItsTransform = false)
        {
            if (this.group == null || group != null)
                this.group = group;

            if (this.Outer.IsClosedProfile && this.group != null)
            {
                foreach (var mesh in this.geometryMesh)
                    this.group.Children.Remove(mesh);

                this.geometryMesh = new List<GeometryModel3D>();

                // run the background thread to save the processing time
                var tk = Task.Run<Tuple<List<float>, List<int>>>(() =>
                {
                    if (this.Outer.Segments.Count >= 2)
                    {
                        List<float> verticies = new List<float>();
                        List<int> indicies = new List<int>();

                        TriangulatePolygon(this.Outer.Segments, this.Inners, verticies, indicies);
                        Tuple<List<float>, List<int>> result = new Tuple<List<float>, List<int>>(verticies, indicies);
                        return result;
                    }
                    else
                        return null;
                });
                // render the result back to the canvas
                tk.ContinueWith(result =>
                {
                    if (result != null)
                    {
                        try
                        {
                            List<float> verticies = result.Result.Item1;
                            List<int> indicies = result.Result.Item2;
                            GeometryModel3D mesh = RDF_Dim3.BuildMesh(verticies: verticies.ToArray(), indicies: indicies.ToArray(),
                                group: this.group, rdfobj: this, isline: false, isSurface: true);
                            if (this.Outer.HasTransform && !usingItsTransform)
                            {
                                Matrix3D mtx = this.Outer.Transform.Matrix.Matrix3D;
                                if (mtx == Matrix3D.Identity)
                                {

                                }
                                Transform3D transform = new MatrixTransform3D(mtx);
                                mesh.Transform = transform;
                            }
                            else
                            {
                                Transform3D transform = new MatrixTransform3D(this.Transform.GetMatrix3D());
                                mesh.Transform = transform;
                            }

                            this.Geometry = mesh;
                            this.geometryMesh.Add(mesh);
                            if (pstep != null)
                                pstep.PreModel = this;
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());

            }
        }

        private void TriangulatePolygon(List<RDF_Segment3D> lineSegments, List<RDF_Polygon3D> innerPolygons, List<float> verticies, List<int> indicies)
        {
            InputGeometry input = new InputGeometry();
            int vertexCount = 0, profileCount = 1;
            Point3D translation = new Point3D();
            //addLineAndArc(input, lineSegments, ref vertexCount, profileCount, ref translation);
            //vertexCount = 0;
            addOuterToInputGeom(input, ref vertexCount, profileCount);

            // add holes
            foreach (RDF_Polygon3D pl in innerPolygons)
            {
                profileCount++;
                // the default innertranslateion

                //innerTranslation = new Point3D(-innerTranslation.X, -innerTranslation.Y, -innerTranslation.Z);
                //addLineAndArc(input, pl.Segments, ref vertexCount, profileCount, ref innerTranslation);
                addInnerToInputGeom(input, pl, ref vertexCount, profileCount);
            }

            Mesh mesh = new Mesh();
            mesh.Behavior.ConformingDelaunay = false;
            //mesh.Behavior.MinAngle = 20;
            //mesh.Behavior.MaxAngle = 180;
            mesh.Behavior.Convex = false;
            mesh.Behavior.Quality = true;
            mesh.Triangulate(input);
            mesh.Refine();

            int i = 0;
            foreach (var triangle in mesh.Triangles)
            {
                verticies.Add((float)triangle.GetVertex(0).X);
                verticies.Add((float)triangle.GetVertex(0).Y);
                verticies.Add(0);
                verticies.Add(0);
                verticies.Add(0);
                verticies.Add(1);

                verticies.Add((float)triangle.GetVertex(1).X);
                verticies.Add((float)triangle.GetVertex(1).Y);
                verticies.Add(0);
                verticies.Add(0);
                verticies.Add(0);
                verticies.Add(1);

                verticies.Add((float)triangle.GetVertex(2).X);
                verticies.Add((float)triangle.GetVertex(2).Y);
                verticies.Add(0);
                verticies.Add(0);
                verticies.Add(0);
                verticies.Add(1);

                indicies.Add(i * 3);
                indicies.Add(i * 3 + 1);
                indicies.Add(i * 3 + 2);
                i++;
            }
        }
        /// <summary>
        /// using points on existing wirepath for generating polygon mesh
        /// </summary>
        /// <param name="input"></param>
        /// <param name="curVertex"></param>
        /// <param name="pcount"></param>
        /// <param name="translatePt"></param>
        private void addOuterToInputGeom(InputGeometry input, ref int curVertex, int pcount)
        {
            if (this.Outer.PathGeometry != null && this.Outer.PathGeometry.Paths.Count > 0)
            {
                //Matrix3D inverseMtx = this.Outer.Transform.GetMatrix3D();
                //inverseMtx.Invert();
                var path = this.Outer.PathGeometry.Paths.First();

                GeomLib.BasicGeomLib.ConstructInputProfile(input, ref curVertex, pcount, path);

                //inverseMtx.Invert();
            }
        }
        private void addInnerToInputGeom(InputGeometry input, RDF_Polygon3D innerPl, ref int curVertex, int pcount)
        {
            if (innerPl.PathGeometry.Paths.Count > 0)
            {
                //Matrix3D inverseMtx = innerPl.Transform.GetMatrix3D();
                //inverseMtx.Invert();
                var path = innerPl.PathGeometry.Paths.First();

                GeomLib.BasicGeomLib.ConstructInputProfile(input, ref curVertex, pcount, path);

                List<Point3D> trsformedPts = new List<Point3D>();
                path.Reverse();
                foreach (var pt in path)
                {
                    //var newpt = inverseMtx.Transform(pt);
                    trsformedPts.Add(pt);
                }

                System.Drawing.PointF? innerpt = GeomLib.BasicGeomLib.FindPointInPolygon(trsformedPts);
                if(innerpt != null)
                    input.AddHole(innerpt.Value.X, innerpt.Value.Y);

                //inverseMtx.Invert();
            }
        }

        /// <summary>
        /// using the rdf segementation for generating polygon mesh
        /// </summary>
        /// <param name="input"></param>
        /// <param name="lineSegments"></param>
        /// <param name="j"></param>
        /// <param name="pcount"></param>
        /// <param name="translatePt"></param>
        private void addLineAndArc(InputGeometry input, List<RDF_Segment3D> lineSegments, ref int j, int pcount, ref Point3D translatePt)
        {
            int i = 0;
            int orij = j;

            if (translatePt.X == 0 && translatePt.Y == 0)
                translatePt = lineSegments.First().StartNode.GetPoint3D();

            foreach (RDF_Segment3D seg in lineSegments)
            {
                if (seg is RDF_Line3D)
                {
                    if (i == lineSegments.Count - 1)
                    {
                        input.AddPoint(Math.Round(seg.StartNode.X - translatePt.X, 4), Math.Round(seg.StartNode.Y - translatePt.Y, 4), pcount);
                        input.AddSegment(j, orij);
                    }
                    else 
                    {
                        input.AddPoint(Math.Round(seg.StartNode.X - translatePt.X, 4), Math.Round(seg.StartNode.Y - translatePt.Y, 4));
                        input.AddSegment(j, j + 1);
                    }
                    j++;
                }
                else if (seg is RDF_Arc3D)
                {
                    double prex = Math.Round(seg.StartNode.X, 4);
                    double prey = Math.Round(seg.StartNode.Y, 4);
                    double reverse = ((RDF_Arc3D)seg).Radius < 0 ? -1 : 1;
                    double ctrx = prex - reverse * Math.Cos(((RDF_Arc3D)seg).StartAngle) * ((RDF_Arc3D)seg).Radius;
                    double ctry = prey - reverse * Math.Sin(((RDF_Arc3D)seg).StartAngle) * ((RDF_Arc3D)seg).Radius;
                    double segAng = ((RDF_Arc3D)seg).Size / ((RDF_Arc3D)seg).SegParts;
                    for (int k = 0; k < ((RDF_Arc3D)seg).SegParts; k++)
                    {
                        double newx = ctrx + reverse * ((RDF_Arc3D)seg).Radius * Math.Cos(segAng * k + ((RDF_Arc3D)seg).StartAngle) - translatePt.X;
                        double newy = ctry + reverse * ((RDF_Arc3D)seg).Radius * Math.Sin(segAng * k + ((RDF_Arc3D)seg).StartAngle) - translatePt.Y;
                        

                        if (i == lineSegments.Count - 1 && k == ((RDF_Arc3D)seg).SegParts - 1)
                        {
                            input.AddPoint(Math.Round(newx, 4), Math.Round(newy, 4), pcount);
                            input.AddSegment(j, orij);
                        }
                        else
                        {
                            input.AddPoint(Math.Round(newx, 4), Math.Round(newy, 4));
                            input.AddSegment(j, j + 1);
                        }                            
                        j++;
                    }
                }
                i++;
            }
        }
    
        private void addOuterBackRelation(RDF_Polygon3D outer)
        {
            BackRelation br = new BackRelation(outer);
            br.Name = BackRelationName.Dim1_Face_Outer;
            br.RelatedTo = this;
            outer.BackRelations.Add(br);
        }

        internal void Remove()
        {
            // remove the paths.. only if the face is not instanciated from an array
            if (!this.IsInArray)
            {
                if (RDFWrapper.All_RDF_Objects.ContainsKey(this.Outer.GUID))
                    removingQueue.Enqueue(this.Outer);
                foreach (var innerpath in this.Inners)
                    if (RDFWrapper.All_RDF_Objects.ContainsKey(innerpath.GUID))
                        removingQueue.Enqueue(innerpath);
            }
        }

        private bool changeFaceRotation(double roateAngle)
        {
            

            return false;
        }


        /// <summary>
        /// clone the core properties 
        /// </summary>
        /// <returns></returns>
        public override RDF_GeometricItem Clone(bool iscopyingEdge = false)
        {
            var profile = this.Outer;
            if (iscopyingEdge)
            {
                profile = (RDF_Polygon3D)this.Outer.Clone();
                profile.BackRelations = new List<BackRelation>();
            }
            RDF_Face2D clonedFace = new RDF_Face2D(profile, "", iscopyingEdge);
            if (!iscopyingEdge)
                clonedFace.InstantiatedFrom = this;
            var oriMatrix3D = this.Transform.GetMatrix3D();

            if (clonedFace.Transform.Matrix is RDF_Matrix)
            {
                // try to move the shape to the right position
                var clonedMatrix3D = (RDF_Matrix)clonedFace.Transform.Matrix;
                clonedMatrix3D.SetMatrix(oriMatrix3D);
            }
            else
            {
                RDF_Matrix newmtx = new RDF_Matrix(oriMatrix3D);
                RDF_Transformation translate = new RDF_Transformation();
                translate.SetMatrix(newmtx);
                translate.SetTransformGeom(clonedFace);
            }
            return clonedFace;
        }
    }
}
