﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Controls;
using GeomLib;
using Petzold.Media3D;

namespace WrapperLibrary.RDF
{
    public abstract class RDF_Dim1 : RDF_GeometricItem
    {
        public WirePath pathgeom;
        public WirePath PathGeometry { 
            get 
            {
                if (this.HasTransform && this.Transform.GeometryObject is WirePath)
                    return (WirePath)this.Transform.GeometryObject;
                else
                    return this.pathgeom;
            }
            private set 
            { 
                this.pathgeom = value;
            } 
        }
        public Viewport3D viewport;
        public bool IsClosed { protected set; get; }
        public Point3D Centroid { protected set; get; }
        public RDF_Dim1(string guid = "")
            : base(guid)
        {
            this.viewport = null;
            this.pathgeom = null;
            this.IsClosed = false;
            this.Centroid = new Point3D();
        }
        internal RDF_Dim1(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.viewport = null;
            this.pathgeom = null;
            this.IsClosed = false;
            this.Centroid = new Point3D();
        }
        internal RDF_Dim1(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.viewport = null;
            this.pathgeom = null;
            this.IsClosed = false;
            this.Centroid = new Point3D();
        }
        public WirePath RenderConceptLine(Viewport3D viewport = null)
        {
            if (this.viewport == null)
                this.viewport = viewport;
            else if (this.PathGeometry != null)
                this.viewport.Children.Remove(this.PathGeometry);

            if (this.viewport == null) return null;

            //System.Diagnostics.Debug.WriteLine("render");

            IntPtr backupPtr = this.PtrToConcept; // there are some memory leak?? don't know why this pointer will be changed after passed the wrpper
            int vbuffsize = 0, ibuffsize = 0;
            // calculate the triangulations
            RDFWrapper.PrepareConcept(this.PtrToConcept, ref vbuffsize, ref ibuffsize);
            if (ibuffsize > 0 && vbuffsize > 0)
            {
                int[] indicies = new int[ibuffsize];
                float[] verticies = new float[vbuffsize * 6];
                RDFWrapper.GetConceptMesh(this.PtrToConcept, verticies, indicies, vbuffsize, ibuffsize);

                //IntPtr ptrTrinagles = RDFWrapper.GetConceptLines(concept, verticies, indicies);
                //TrianglesUM trianglesUM = ptrTrinagles.ElementAt<TrianglesUM>(0);
                //Triangles triangles = new Triangles(trianglesUM);

                PathSegment3DCollection pscollection = new PathSegment3DCollection();

                float x = verticies[0];
                float y = verticies[1];
                float z = verticies[2];
                float zero = 0;
                z = z == -1 / zero ? 0 : z;
                Point3D stpt = new Point3D(x, y, z);
                PathFigure3D pf = new PathFigure3D();
                pf.Segments = pscollection;
                pf.StartPoint = stpt;

                // add each segment until the end
                for (int i = 1; i < vbuffsize; i++)
                {
                    x = verticies[i * 6 + 0];
                    y = verticies[i * 6 + 1];
                    z = verticies[i * 6 + 2];
                    z = z == -1 / zero ? 0 : z;

                    //System.Diagnostics.Debug.WriteLine(x + ", " + y + ", " + z);

                    Point3D endpt = new Point3D(x, y, z);
                    LineSegment3D segment = new LineSegment3D();
                    segment.Point = endpt;
                    pscollection.Add(segment);
                }

                PathFigure3DCollection pfcollection = new PathFigure3DCollection();
                pfcollection.Add(pf);

                PathGeometry3D pathdata = new PathGeometry3D();
                pathdata.Figures = pfcollection;

                WirePath newpath = new WirePath();
                newpath.Rounding = 3;
                newpath.Color = Color.FromArgb(200, 200, 200, 0);
                newpath.Thickness = 2;
                newpath.Data = pathdata;
                viewport.Children.Insert(0, newpath);

                this.PathGeometry = newpath;
                return this.PathGeometry;
            }
            return null;
        }
        
        public static WirePath RenderConceptLine(RDF_Transformation transform, Viewport3D viewport = null, bool isInner = false)
        {
            if (viewport != null)// if it is rendering a conceptline, it shold be a wirepath
                transform.viewport = viewport;

            if (transform.viewport != null)
            {

                if (transform.TransformGeom is RDF_Polygon3D)
                {
                    var polygon3D = (RDF_Polygon3D)transform.TransformGeom;
                    polygon3D.viewport = viewport;
#if Debug
                    foreach (var seg in polygon3D.Segments)
                    {
                        //System.Diagnostics.Debug.WriteLine(seg.GetType());
                        //System.Diagnostics.Debug.WriteLine(seg.StartNode.ToString());
                        //System.Diagnostics.Debug.WriteLine(seg.EndNode.ToString());

                        if (seg is RDF_Arc3D)
                        {
                            var arc = (RDF_Arc3D)seg;
                            //System.Diagnostics.Debug.WriteLine("cal: " + arc.GetCalculatedEndPt());
                        }
                    }
#endif
                }
                bool isremoved = transform.viewport.Children.Remove((WirePath)transform.GeometryObject);
                WirePath newpath = ConstructConceptLine(transform, transform.viewport, transform.TransformGeom.IsVirtual, isInner);
                if (newpath != null)
                {
                    ConnectWirePathAndRDFobj(transform, newpath);

                    return (WirePath)transform.GeometryObject;
                }
            }
            return null;
        }

        public static void ConnectWirePathAndRDFobj(RDF_Transformation transform, WirePath newpath)
        {
            newpath.ID = transform.GUID;
            transform.GeometryObject = newpath;
            if (transform.TransformGeom is RDF_Polygon3D)
                ((RDF_Polygon3D)transform.TransformGeom).PathGeometry = newpath;
        }
        public static WirePath ConstructConceptLine(RDF_Transformation transformConcept, Viewport3D viewport, bool isVirtual = false, bool isInner = false)
        {
            bool isClosed = transformConcept.TransformGeom is RDF_Polygon3D ? ((RDF_Polygon3D)transformConcept.TransformGeom).IsClosed : false;
            //IntPtr rdfPtr = transformConcept.PtrToConcept; // there are some memory leak?? don't know why this pointer will be changed after passed the wrpper
            IntPtr rdfPtr = transformConcept.TransformGeom.PtrToConcept;
            var wireMtx = transformConcept.GetMatrix3D().Clone();

            Point3D firstPt = new Point3D();
            
            if (transformConcept.TransformGeom is RDF_Polygon3D && isInner)
            {
                var pl = transformConcept.TransformGeom as RDF_Polygon3D;
                firstPt = pl.Segments.First().StartNode.GetPoint3D();
                wireMtx.OffsetX = 0;
                wireMtx.OffsetY = 0;
                wireMtx.OffsetZ = 0;
                firstPt = wireMtx.Transform(firstPt);
            }
            
            int vbuffsize = 0, ibuffsize = 0;
            // calculate the triangulations
            try
            {
                RDFWrapper.PrepareConcept(rdfPtr, ref vbuffsize, ref ibuffsize);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Engine can't generate geometry");
            }

            if (ibuffsize > 0 && vbuffsize > 0)
            {
                int[] indicies = new int[ibuffsize];
                float[] verticies = new float[vbuffsize * 6];
                RDFWrapper.GetConceptMesh(rdfPtr, verticies, indicies, vbuffsize, ibuffsize);

                //IntPtr ptrTrinagles = RDFWrapper.GetConceptLines(concept, verticies, indicies);
                //TrianglesUM trianglesUM = ptrTrinagles.ElementAt<TrianglesUM>(0);
                //Triangles triangles = new Triangles(trianglesUM);

                PathSegment3DCollection pscollection = new PathSegment3DCollection();

                float x = verticies[0] + (float)firstPt.X;
                float y = verticies[1] + (float)firstPt.Y;
                float z = verticies[2] + (float)firstPt.Z;
                float zero = 0;
                z = z == -1 / zero ? 0 : z;
                Point3D stpt = new Point3D(x, y, z);
                PathFigure3D pf = new PathFigure3D();
                pf.Segments = pscollection;
                pf.StartPoint = stpt;

                Point3D prept = new Point3D();
                prept = stpt;

#if DEBUG
                //System.Diagnostics.Debug.WriteLine("new polyline");
                //System.Diagnostics.Debug.WriteLine(stpt.ToString());
#endif

                // add each segment until the end
                for (int i = 1; i < vbuffsize; i++)
                {
                    //int j = i == vbuffsize - 1 && isClosed ? 0 : i;
                    int j = i;
                    x = verticies[j * 6 + 0] + (float)firstPt.X;
                    y = verticies[j * 6 + 1] + (float)firstPt.Y;
                    z = verticies[j * 6 + 2] + (float)firstPt.Z;
                    z = z == -1 / zero ? 0 : z;

                    Point3D endpt = new Point3D(x, y, z);

                    // prevent duplicate points
                    if (prept.distanceTo(endpt) < 0.1)
                        continue;

                    prept = endpt;

                    LineSegment3D segment = new LineSegment3D();
                    segment.Point = endpt;
                    pscollection.Add(segment);
                }

                PathFigure3DCollection pfcollection = new PathFigure3DCollection();
                pfcollection.Add(pf);

                PathGeometry3D pathdata = new PathGeometry3D();
                pathdata.Figures = pfcollection;

                WirePath newpath = new WirePath();
                newpath.Rounding = 0;
                newpath.Color = isVirtual ? Color.FromArgb(20, 200, 200, 200) : Colors.Gray;
                newpath.Thickness = isVirtual ? 1 : 2;
                newpath.Data = pathdata;
                newpath.Transform = new MatrixTransform3D(transformConcept.GetMatrix3D());
                viewport.Children.Insert(0, newpath);
                return newpath;
            }
            return null;
        }
    }
    public abstract class RDF_Dim2 : RDF_GeometricItem
    {
        public Model3DGroup group;
        public RDF_Dim2(string guid = "")
            : base(guid)
        {
            this.group = null;
            this.Geometry = null;
        }
        internal RDF_Dim2(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.group = null;
            this.Geometry = null;
        }
        internal RDF_Dim2(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.group = null;
            this.Geometry = null;
        }
        public GeometryModel3D RenderConceptMesh(Model3DGroup group = null, bool drawEdges = false)
        {
            if (this.group == null)
                this.group = group;
            else if (this.Geometry != null)
                this.group.Children.Remove(this.Geometry);
            if (this.group == null) return null;

            this.Geometry = RDF_Dim3.RenderConceptMesh(this, this.group, drawEdges);
            return this.Geometry;
        }
    }
    public abstract class RDF_Dim3 : RDF_GeometricItem
    {
        public Model3DGroup group;
        /// <summary>
        /// record the matrix of the geometry when generating it
        /// </summary>
        public Matrix3D GeometryTransform;
        public RDF_Dim3(string guid = "")
            : base(guid)
        {
            this.group = null;
            this.Geometry = null;
            this.GeometryTransform = Matrix3D.Identity;
        }
        internal RDF_Dim3(IntPtr ptrToRDF, string guid = "")
            : base(ptrToRDF, guid)
        {
            this.group = null;
            this.Geometry = null;
            this.GeometryTransform = Matrix3D.Identity;
        }
        internal RDF_Dim3(SELECTED__CONCEPT__STRUCT sconcept, string guid = "")
            : base(sconcept, guid)
        {
            this.group = null;
            this.Geometry = null;
            this.GeometryTransform = Matrix3D.Identity;
        }
        public GeometryModel3D MoveConceptMesh(Model3DGroup group = null, bool drawEdges = false)
        {
            if (this.Geometry != null && this is RDF_SweptAreaSolid)
            {
                var newMtx = this.Transform.GetMatrix3D().Clone();
                var solveMtx = this.GeometryTransform.Solve(newMtx);
                this.Geometry.Transform = new MatrixTransform3D(solveMtx);
                //this.Transform
                //this.Geometry.Transform
            }
            else
                return this.RenderConceptMesh(group);
            return this.Geometry;
        }
        public GeometryModel3D RenderConceptMesh(Model3DGroup group = null, bool drawEdges = false)
        {
            if (this.group == null)
                this.group = group;
            else if (this.Geometry != null)
                this.group.Children.Remove(this.Geometry);
            if (this.group == null) return null;

            if (this.HasTransform)
            {
                this.Geometry = RDF_Dim3.RenderConceptMesh(this.Transform, this.group, drawEdges);
                this.GeometryTransform = this.Transform.GetMatrix3D().Clone();
            }
            else
                this.Geometry = RDF_Dim3.RenderConceptMesh(this, this.group, drawEdges);

            return this.Geometry;
        }
        public static GeometryModel3D RednerConceptMesh(RDF_Transformation transform, Model3DGroup group = null)
        {
            transform.Geometry = RDF_Dim3.RenderConceptMesh(transform, group);
            return transform.Geometry;
        }
        public static GeometryModel3D RenderConceptMesh(RDF_Abstract rdfobj, Model3DGroup group, bool drawEdges = false)
        {
            IntPtr backupPtr = rdfobj.PtrToConcept; // there are some memory leak?? don't know why this pointer will be changed after passed the wrpper
            int vbuffsize = 0, ibuffsize = 0;
            try
            {
                // calculate the triangulations
                RDFWrapper.PrepareConcept(rdfobj.PtrToConcept, ref vbuffsize, ref ibuffsize);
                if (ibuffsize > 0 && vbuffsize > 0)
                {
                    int[] indicies = new int[ibuffsize];
                    float[] verticies = new float[vbuffsize * 6];
                    RDFWrapper.GetConceptMesh(rdfobj.PtrToConcept, verticies, indicies, vbuffsize, ibuffsize);

                    GeometryModel3D newgeom = null;
                    if (rdfobj is RDF_Transformation)
                        newgeom = BuildMesh(verticies, indicies, group, ((RDF_Transformation)rdfobj).TransformGeom);
                    else
                        newgeom = BuildMesh(verticies, indicies, group, (RDF_GeometricItem)rdfobj);

                    if (drawEdges)
                    {
                        IntPtr ptrTrinagles = RDFWrapper.GetConceptWireFrames(rdfobj.PtrToConcept, verticies, indicies);
                        TrianglesUM triangles = ptrTrinagles.ElementAt<TrianglesUM>(0);
                        //BuildMesh(triangles.vertexBuffer, triangles.indexBuffer, group, ref mGeometry, true);
                    }

                    return newgeom;
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        internal static GeometryModel3D BuildMesh(float[] verticies, int[] indicies, Model3DGroup group, RDF_GeometricItem rdfobj,
            bool isline = false, bool isSurface = false, int noOfIndex = int.MaxValue, int noOfVertex = int.MaxValue)
        {
            // Define 3D mesh object
            MeshGeometry3D mesh = new MeshGeometry3D();
            for (int i = 0; i < verticies.Length / 6 && i < noOfVertex / 6; i++)
            {
                float x = verticies[i * 6 + 0],
                      y = verticies[i * 6 + 1],
                      z = verticies[i * 6 + 2],
                      nx = verticies[i * 6 + 3],
                      ny = verticies[i * 6 + 4],
                      nz = verticies[i * 6 + 5];
                mesh.Positions.Add(new Point3D(x, y, z));
                if (!isline)
                    mesh.Normals.Add(new Vector3D(nx, ny, nz));
            }

            for (int i = 0; i < indicies.Length / 3 && i < noOfIndex / 3; i++)
            {
                // it needs to be clockwise order in WPF 3D canvas
                mesh.TriangleIndices.Add(indicies[3 * i + 2]);
                mesh.TriangleIndices.Add(indicies[3 * i + 1]);
                mesh.TriangleIndices.Add(indicies[3 * i + 0]);
            }

            SolidColorBrush br = new SolidColorBrush(Colors.LightSlateGray);
            br.Opacity = 0.3;

            // Geometry creation
            MaterialGroup mg = new MaterialGroup();
            mg.Children.Add(new DiffuseMaterial(br));
            mg.Children.Add(new SpecularMaterial(Brushes.White, 80));
            GeometryModel3D mGeometry = new GeometryModel3D(mesh, mg);
            mGeometry.BackMaterial = new DiffuseMaterial(br);
            mGeometry.SetValue(Extensions.IDProperty, Guid.NewGuid().ToString());
            mGeometry.SetValue(RDFExtension.RDFObjectProperty, rdfobj); // set the pointer to RDF object
            mGeometry.Transform = new Transform3DGroup();
            if(!rdfobj.IsVirtual)
                group.Children.Add(mGeometry);

            rdfobj.Geometry = mGeometry; // set the pointer to Geometry Model object

            return mGeometry;
        }
    }
}
