﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Windows;
using Petzold.Media3D;
using GeomLib;

namespace WrapperLibrary.RDF
{
    public static class RDFLibrary
    {
        /// <returns>the offset of the polygon related to global locations</returns>
        public static Point3D innerPolygonCreateRDFSegments(this List<RDF_Segment3D> segments, RDF_Polygon3D polygon, Matrix3D matrix)
        {
            Point3D stpt = new Point3D();

            int i = 1;
            foreach (var pm in segments)
            {
                //System.Diagnostics.Debug.WriteLine("Start Point " + i + ": " + pm.start);
                RDF_Segment3D seg = new RDF_Segment3D();
                if (pm is RDF_Line3D)
                {
                    seg = new RDF_Line3D(new Point3D(pm.StartNode.X - matrix.OffsetX, pm.StartNode.Y - matrix.OffsetY, 0),
                        new Point3D(pm.EndNode.X - matrix.OffsetX, pm.EndNode.Y - matrix.OffsetY, 0));
                }
                else if (pm is RDF_Arc3D)
                {
                    var arc = pm as RDF_Arc3D;
                    seg = new RDF_Arc3D();
                    ((RDF_Arc3D)seg).SetStartAngle(arc.StartAngle);
                    ((RDF_Arc3D)seg).SetRadius(arc.Radius);
                    ((RDF_Arc3D)seg).SetSize(arc.Size);
                    ((RDF_Arc3D)seg).StartNode = new RDF_Node3D(arc.StartNode.X - matrix.OffsetX, arc.StartNode.Y - matrix.OffsetY, 0);
                    ((RDF_Arc3D)seg).EndNode = new RDF_Node3D(arc.EndNode.X - matrix.OffsetX, arc.EndNode.Y - matrix.OffsetY, 0);
                    ((RDF_Arc3D)seg).StartNode.NextEdge = (RDF_Arc3D)seg;
                    ((RDF_Arc3D)seg).EndNode.PreEdge = (RDF_Arc3D)seg;
                }
                polygon.AddPart(seg);
                if (i == 1)
                {
                    stpt = new Point3D(pm.StartNode.X - matrix.OffsetX, pm.StartNode.Y - matrix.OffsetY, 0);
                }

                i++;
            }

            return stpt;
        }
        public static List<DatatypeProperty> GetPropertyListFromConcept(IntPtr ptrToRDFObj)
        {
            List<DatatypeProperty> propertyList = new List<DatatypeProperty>();
            IntPtr[] ptrPropertyList = new IntPtr[1000];
            RDFWrapper.GetConceptProperties(ptrToRDFObj, ptrPropertyList);

            foreach (IntPtr ptr in ptrPropertyList)
            {
                if ((int)ptr <= 0) break;
                DatatypeProperty property = RDFLibrary.GetProperty(ptr);
                property.PtrToRelation = ptr;
                propertyList.Add(property);
            }

            return propertyList;
        }
        public static DatatypeProperty GetProperty(IntPtr ptrToProp)
        {
            DATATYPE_PROPERTIES_STRUCT umProperty = ptrToProp.ElementAt<DATATYPE_PROPERTIES_STRUCT>(0);
            DatatypeProperty property = new DatatypeProperty();
            property.MaxCard = umProperty.maxCard;
            property.MinCard = umProperty.minCard;
            property.Type = umProperty.type;

            DATATYPE_PROPERTY_STRUCT umTypeProperty = umProperty.datatypeProperty.ElementAt<DATATYPE_PROPERTY_STRUCT>(0);
            NAMES_STRUCT umNameProperty = umTypeProperty.nameStr.ElementAt<NAMES_STRUCT>(0);
            property.Name = umNameProperty.name;

            if ((int)umProperty.formula > 0)
                property.FormulaData = RDFLibrary.GetFormula(umProperty.formula);

            if ((int)umProperty.value > 0)
                property.ValueData = RDFLibrary.GetValue(umProperty.value);

            return property;
        }
        public static List<DatatypeProperty> GetSelectedPropertyList(IntPtr ptrlist)
        {
            List<DatatypeProperty> propertyList = new List<DatatypeProperty>();
            for (int i = 0; ; i++)
            {
                IntPtr ptr = ptrlist.ElementAt<IntPtr>(i);

                if ((int)ptr <= 0) break;
                DatatypeProperty property = RDFLibrary.GetSelectedProperty(ptr);
                property.PtrToRelation = ptr;
                propertyList.Add(property);
            }

            return propertyList;
        }
        public static DatatypeProperty GetSelectedProperty(IntPtr ptrToProp)
        {
            SELECTED_DATATYPE_PROPERTIES_STRUCT umProperty = ptrToProp.ElementAt<SELECTED_DATATYPE_PROPERTIES_STRUCT>(0);
            DatatypeProperty property = new DatatypeProperty();
            property.MaxCard = umProperty.maxCard;
            property.MinCard = umProperty.minCard;
            property.Type = umProperty.type;

            if ((int)umProperty.datatypeProperty > 0)
            {
                DATATYPE_PROPERTY_STRUCT umTypeProperty = umProperty.datatypeProperty.ElementAt<DATATYPE_PROPERTY_STRUCT>(0);
                NAMES_STRUCT umNameProperty = umTypeProperty.nameStr.ElementAt<NAMES_STRUCT>(0);
                property.Name = umNameProperty.name;
            }

            if ((int)umProperty.formula > 0)
                property.FormulaData = RDFLibrary.GetFormula(umProperty.formula);

            if ((int)umProperty.value > 0)
                property.ValueData = RDFLibrary.GetValue(umProperty.value);

            return property;
        }
        public static List<Relation> GetRelationListFromConcept(IntPtr ptrToRDFObj)
        {
            List<Relation> relations = new List<Relation>();
            IntPtr[] ptrRelations = new IntPtr[1000];
            RDFWrapper.GetConceptRelations(ptrToRDFObj, ptrRelations);
            foreach (IntPtr ptr in ptrRelations)
            {
                if ((int)ptr <= 0) break;
                Relation r = RDFLibrary.GetRelations(ptr);
                r.PtrToRelation = ptr;
                relations.Add(r);
            }
            return relations;
        }
        public static Relation GetRelations(IntPtr ptrToRelation)
        {
            Relation relation = new Relation();
            OBJECTTYPE_PROPERTIES_STRUCT umRelation = ptrToRelation.ElementAt<OBJECTTYPE_PROPERTIES_STRUCT>(0);

            relation.maxCard = umRelation.maxCard;
            relation.minCard = umRelation.minCard;

            OBJECTTYPE_PROPERTY_STRUCT umOProp = umRelation.objecttypeProperty.ElementAt<OBJECTTYPE_PROPERTY_STRUCT>(0);
            NAMES_STRUCT name = umOProp.nameStr.ElementAt<NAMES_STRUCT>(0);
            relation.Name = name.name;
            relation.Relations = new List<IntPtr>();

            for (int i = 0; ; i++)
            {
                IntPtr ptrSConcept = umRelation.selectedConcepts.ElementAt<IntPtr>(i);
                if ((int)ptrSConcept <= 0) break;

                //SELECTED__CONCEPT__STRUCT umSConcept = ptrSConcept.ElementAt<SELECTED__CONCEPT__STRUCT>(0);
                relation.Relations.Add(ptrSConcept);
            }

            return relation;
        }
        public static List<Relation> GetSelectedRelationList(IntPtr ptrToRDFObj)
        {
            List<Relation> relations = new List<Relation>();
            for (int i = 0; ; i++)
            {
                IntPtr ptr = ptrToRDFObj.ElementAt<IntPtr>(i);
                if ((int)ptr <= 0) break;
                Relation r = RDFLibrary.GetSelectedRelations(ptr);
                r.PtrToRelation = ptr;
                relations.Add(r);
            }
            return relations;
        }
        public static Relation GetSelectedRelations(IntPtr ptrToRelation)
        {
            Relation relation = new Relation();
            SELECTED_OBJECTTYPE_PROPERTIES_STRUCT umRelation = ptrToRelation.ElementAt<SELECTED_OBJECTTYPE_PROPERTIES_STRUCT>(0);

            relation.maxCard = umRelation.maxCard;
            relation.minCard = umRelation.minCard;

            OBJECTTYPE_PROPERTY_STRUCT umOProp = umRelation.objecttypeProperty.ElementAt<OBJECTTYPE_PROPERTY_STRUCT>(0);
            NAMES_STRUCT name = umOProp.nameStr.ElementAt<NAMES_STRUCT>(0);
            relation.Name = name.name;
            relation.Relations = new List<IntPtr>();

            for (int i = 0; ; i++)
            {
                IntPtr ptrSConcept = umRelation.selectedConcepts.ElementAt<IntPtr>(i);
                if ((int)ptrSConcept <= 0) break;

                //SELECTED__CONCEPT__STRUCT umSConcept = ptrSConcept.ElementAt<SELECTED__CONCEPT__STRUCT>(0);
                relation.Relations.Add(ptrSConcept);
            }

            return relation;
        }
        public static List<Rule> GetRuleList(IntPtr ptrToRDFObj)
        {
            List<Rule> rules = new List<Rule>();
            IntPtr[] ptrrules = new IntPtr[1000];
            RDFWrapper.GetConceptRules(ptrToRDFObj, ptrrules);
            foreach (IntPtr ptr in ptrrules)
            {
                if ((int)ptr <= 0) break;
                Rule rule = RDFLibrary.GetRule(ptr);
                rule.PtrToRule = ptr;
                rules.Add(rule);
            }
            return rules;
        }
        public static Rule GetRule(IntPtr ptrrule)
        {
            Rule rule = new Rule();

            RULE_STRUCT umrule = ptrrule.ElementAt<RULE_STRUCT>(0);
            rule.DisplayName = umrule.displayName;
            rule.Type = umrule.type;

            if ((int)umrule.lhs > 0)
                rule.LHS = GetFormula(umrule.lhs);
            if ((int)umrule.rhs > 0)
                rule.RHS = GetFormula(umrule.rhs);
            if ((int)umrule.datatypeProperty > 0)
            {
                DATATYPE_PROPERTY_STRUCT umprop = umrule.datatypeProperty.ElementAt<DATATYPE_PROPERTY_STRUCT>(0);
                rule.Description = umprop.description;

                if ((int)umprop.nameStr > 0)
                {
                    NAMES_STRUCT umNameStr = umprop.nameStr.ElementAt<NAMES_STRUCT>(0);
                    rule.Name = umNameStr.name;
                }
            }

            return rule;
        }
        public static Formula GetFormula(IntPtr ptrToF)
        {
            FORMULA_STRUCT umF = ptrToF.ElementAt<FORMULA_STRUCT>(0);

            Formula f = new Formula();
            f.Type = umF.type;
            f.PtrToFormula = ptrToF;

            if ((int)umF.firstArgument > 0)
                f.FirstArgument = GetFormula(umF.firstArgument);
            if ((int)umF.secondArgument > 0)
                f.SecondArgument = GetFormula(umF.secondArgument);
            if ((int)umF.datatypeProperty > 0)
            {
                DATATYPE_PROPERTY_STRUCT prop = umF.datatypeProperty.ElementAt<DATATYPE_PROPERTY_STRUCT>(0);
                if ((int)prop.nameStr > 0)
                {
                    NAMES_STRUCT ptrName = prop.nameStr.ElementAt<NAMES_STRUCT>(0);
                    f.Name = ptrName.name;
                }
            }
            if ((int)umF.value > 0)
            {
                f.Value = GetValue(umF.value);
            }

            return f;
        }
        public static Value GetValue(IntPtr ptrToV)
        {
            VALUE_STRUCT umValue = ptrToV.ElementAt<VALUE_STRUCT>(0);
            Value v = new Value();
            v.PtrToValue = ptrToV;
            v.Format = umValue.format;
            v.IntValue = umValue.intValue;
            v.DoubleValue = umValue.doubleValue;
            if ((int)umValue.set > 0)
            {
                v.ValueSet = GetValueSet(umValue.set);
            }
            return v;
        }
        public static ValueSet GetValueSet(IntPtr ptrToVS)
        {
            IntPtr tmpptr = ptrToVS.ElementAt<IntPtr>(0);
            VALUE_SET_STRUCT umVS = ptrToVS.ElementAt<VALUE_SET_STRUCT>(0);
            List<Value> values = new List<Value>();
            List<Formula> formulas = new List<Formula>();
            ValueSet vs = new ValueSet();
            vs.PtrToVSet = ptrToVS;

            int i = 0;
            if ((int)umVS.values > 0)
            {
                vs.Values = values;
                while (true)
                {
                    IntPtr offsetPtr = umVS.values.ElementAt<IntPtr>(i);
                    if ((int)offsetPtr <= 0) break;

                    values.Add(GetValue(offsetPtr));
                    i++;
                }
            }

            i = 0;
            if ((int)umVS.formulas > 0)
            {
                vs.Formulas = formulas;
                while (true)
                {
                    IntPtr offsetPtr = umVS.formulas.ElementAt<IntPtr>(i);
                    if ((int)offsetPtr <= 0) break;

                    formulas.Add(GetFormula(offsetPtr));
                    i++;
                }
            }
            return vs;
        }
        public static BasicProperties GetBasicProperties(IntPtr ptrToRDFObj)
        {
            BasicProperties bp = new BasicProperties();
            IntPtr ptrBP = RDFWrapper.GetBasicConceptProperties(ptrToRDFObj);
            BASIC_PROPERTIES umbp = ptrBP.ElementAt<BASIC_PROPERTIES>(0);
            bp.displayName = umbp.displayName;
            bp.description = umbp.description;
            bp.PtrToBS = ptrBP;

            if ((int)umbp.nameStr > 0)
            {
                NAMES_STRUCT umName = umbp.nameStr.ElementAt<NAMES_STRUCT>(0);
                bp.name = umName.name;
            }

            if ((int)umbp.parentNameStr > 0)
            {
                NAMES_STRUCT umParentName = umbp.parentNameStr.ElementAt<NAMES_STRUCT>(0);
                bp.parentName = umParentName.name;
            }

            return bp;
        }
        public static RDF_GeometricItem FindParentSweptSolid(RDF_SweptAreaSolid swept3D)
        {
            RDF_GeometricItem firstGeom = null;

            // find whether the swept solid has parent transform
            RDF_Transformation transform = swept3D.Transform;
            RDF_MatrixMultiplication mulmatrix = transform.Matrix is RDF_MatrixMultiplication ?
                transform.Matrix as RDF_MatrixMultiplication : null;
            if (mulmatrix == null && transform.Matrix is RDF_MatrixInverse)
            {
                RDF_MatrixAbstract orimatrix = ((RDF_MatrixInverse)transform.Matrix).OriMatrix;
                mulmatrix = orimatrix is RDF_MatrixMultiplication ?
                    orimatrix as RDF_MatrixMultiplication : null;
            }
            if (mulmatrix != null)
            {
                foreach (var fstTransform in mulmatrix.FirstMatrix.RelatedTransforms)
                {
                    if (fstTransform.TransformGeom is RDF_SweptAreaSolid)
                    {
                        firstGeom = fstTransform.TransformGeom as RDF_SweptAreaSolid;
                        break;
                    }
                }
            }
            return firstGeom;
        }
        public static void TransformFace2D(RDF_GeometricItem geom, Matrix3D newTransform)
        {
            if (!(geom is RDF_Face2D)) return;
            var face = (RDF_Face2D)geom;
            #region transform 2D paths
            var outer = face.Outer;
            // Todo: Need to relocate the stroke that associates with the 

            //cursor.DestroyCursor();
            // set the cursor to the new location
            // not use for now -- util the object can retain selected when moving
            var oldMtx = outer.Transform.GetMatrix3D();
            MatrixTransform3D mtxTransform = new MatrixTransform3D(Matrix3D.Identity);
            var centroidmtx = oldMtx.Clone();
            var centroid = outer.Centroid;
            centroid = centroidmtx.Transform(centroid);
            centroidmtx.OffsetX = centroid.X;
            centroidmtx.OffsetY = centroid.Y;
            centroidmtx.OffsetZ = centroid.Z;
            mtxTransform.Matrix = centroidmtx;
            //if(GlobalVariables.CurrentMode != Mode.MoveShape)
            //Cursor3D.SetTransform(mtxTransform);

            #region translate all the wirepaths associated to this 2D shape
            // just translate outer.Transform.GeometryObject instead of rerender the whole geometry
            var line = outer.Transform.GeometryObject;
            if (line is WirePath && ((WirePath)line).Paths.Count > 0)
                RDFLibrary.TranslateWirePath(oldMtx, line);
            else
                line = RDF_Dim1.RenderConceptLine(outer.Transform);

            foreach (var inner in face.Inners)
            {
                var innerline = inner.PathGeometry;
                if (innerline is WirePath && ((WirePath)innerline).Paths.Count > 0)
                    RDFLibrary.TranslateWirePath(oldMtx, innerline);
                else
                    innerline = RDF_Dim1.RenderConceptLine(inner.Transform);
            }

            if (outer.DirectionFrom != null)
            {
                var directionfrom = outer.DirectionFrom;
                var directionline = directionfrom.PathGeometry;
                if (directionline != null)
                    RDFLibrary.TranslateWirePath(oldMtx, directionline);
            }
            #endregion

            //PreviousStep pstep = addPstepLine(line);
            //pstep.ObjectType = pstep.ObjectType | PreStepObjectTypeEnum.MeshModel;
            //rdfFace.RenderShapeMesh(pstep, group1);
            if (face.Geometry != null)
                face.Geometry.Transform = new MatrixTransform3D(newTransform);
            //else
            //    System.Diagnostics.Debug.WriteLine("no rdfface geometry");

            #endregion

        }
        public static void TranslateWirePath(Matrix3D oldMtx, DependencyObject line)
        {
            // need to set a right matrix...
            WirePath translatingPath = (WirePath)line;
            var firstPt = translatingPath.Paths.First().First();
            Matrix3D newProfileMtx = oldMtx;

            AxisAngleRotation3D rot3D = new AxisAngleRotation3D();
            if (tmpRotation != null)
            {
                RotateTransform3D rotTransform = new RotateTransform3D(tmpRotation, firstPt);
                newProfileMtx = rotTransform.Value * newProfileMtx;
            }

            MatrixTransform3D newtransPathform = new MatrixTransform3D(newProfileMtx);
            translatingPath.Transform = newtransPathform;
        }
        public static Rotation3D tmpRotation;
        public static void ProcessRotationCommands(RDF_Face2D rotatingShape, RDF_Polygon3D rotateFrom, Point3D rotateOn)
        {
            var rotateOnGlobal = rotatingShape.Transform.GetMatrix3D().Transform(rotateOn);
            var fstEdge = rotatingShape.Outer.PathGeometry;
            var sndEdge = rotateFrom.pathgeom;
            var dist1 = fstEdge.LineCollection.ToList<Point3D>().FindClosetPointOnPolyline(rotateOnGlobal, rotatingShape.Transform.GetMatrix3D());
            var dist2 = sndEdge.LineCollection.ToList<Point3D>().FindClosetPointOnPolyline(rotateOnGlobal, sndEdge.Transform.Value);
            var edge1 = new Point3D[2] { fstEdge.LineCollection[dist1.Item1 - 1], fstEdge.LineCollection[dist1.Item1] };
            var edge2 = new Point3D[2] { sndEdge.LineCollection[dist2.Item1 - 1], sndEdge.LineCollection[dist2.Item1] };
            rotatingShape.Transform.GetMatrix3D().Transform(edge1);
            sndEdge.Transform.Value.Transform(edge2);
            var vec1 = new Vector3D(edge1[1].X - edge1[0].X, edge1[1].Y - edge1[0].Y, edge1[1].Z - edge1[0].Z);
            var vec2 = new Vector3D(edge2[1].X - edge2[0].X, edge2[1].Y - edge2[0].Y, edge2[1].Z - edge2[0].Z);

            double angle = Vector3D.AngleBetween(vec1, vec2);
            //set the constraint in the RDF engine
            //RDFWrapper.AddDataProperty(RDFWrapper.FilePtr, rdf_parent.PtrToConcept, "angle", Math.Round(angle, 4).ToString());
            // set up the constraint
            rotatingShape.Constraint.Type = ConstraintType.Angle;
            rotatingShape.Constraint.ConstraintName = rotatingShape.Constraint.ConstraintName != GeometricConstraintEnum.Array ? GeometricConstraintEnum.Rotation : rotatingShape.Constraint.ConstraintName;
            //rotatingShape.Constraint.ConstraintObjects.Add(rotateFrom);
            rotatingShape.Constraint.OriConstraintValue = angle;
            rotatingShape.Constraint.TarConstraintValue = 90.0;
            rotatingShape.Constraint.IsSolved = angle == 90.0;
            rotatingShape.Constraint.Location = rotateOn;
            
            if (!rotatingShape.Constraint.IsSolved)
            {
                // solve the constraint
                rotatingShape.Constraint.SolveConstraint();
            }

            // remain the original gesture;
        }
    }
}
