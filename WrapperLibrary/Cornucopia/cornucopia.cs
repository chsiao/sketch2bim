﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Shapes;
using System.Runtime.InteropServices;

namespace WrapperLibrary.Cornucopia
{
    public static class cornucopia
    {
        //[DllImport("cornucopia.dll", CallingConvention = CallingConvention.Cdecl)]
        //public static extern int print_line(string str);

        [DllImport("cornucopia.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getBasicPrimitives([In, Out] int[,] passin, int size, int paramtype, int arccost);

        [DllImport("cornucopia.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getBasicBezier([In, Out] int[,] passin, int size, int paramtype);
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct SPoint //basic point class
    {
        public double x;
        public double y;
        public override string ToString()
        {
            return "(" + this.x + ", " + this.y + ")";
        }
    };
    [StructLayout(LayoutKind.Sequential)]
    public class BasicPrimitive{
        public enum PrimitiveType
        {
            LINE = 0,
            ARC = 1,
            CLOTHOID = 2
        };
        public double length;
        public double startAngle;
        public double startCurvature;
        public double curvatureDerivative;
        public PrimitiveType type;
        public SPoint start;
        public void Scale(double factor)
        {
            this.start.x = this.start.x * factor;
            this.start.y = this.start.y * factor;
            this.length = this.length * factor;
            this.startCurvature = this.startCurvature / factor;

        }
    };
    [StructLayout(LayoutKind.Sequential)]
    public struct BasicBezier
    {
        public SPoint[] controlPoint;
    };
    public enum Preset
    {
        DEFAULT,
        LOOSE,
        ACCURATE,
        POLYLINE,
        LINES_AND_ARCS,
        CLOTHOID_ONLY,
        NEW_LINES_AND_ARCS,
        NUM_PRESETS //must be last
    };
    public static class PrimitiveExtensions
    {
        public static Point getEndPoint(this BasicPrimitive pm)
        {
            Point strpt = new Point(pm.start.x, pm.start.y);
            Point endpt = new Point();
            double angle = pm.startAngle;
            double length = pm.length;
            double strcurveture = pm.startCurvature;

            switch (pm.type)
            {
                case BasicPrimitive.PrimitiveType.LINE:
                    double nextX = strpt.X + length * Math.Cos(angle);
                    double nextY = strpt.Y + length * Math.Sin(angle);
                    endpt = new Point(nextX, nextY);
                    break;
                case BasicPrimitive.PrimitiveType.ARC:
                    double radius = 1 / strcurveture;
                    Size cruveSize = new Size(Math.Abs(radius), Math.Abs(radius));
                    double ctrX = strpt.X + radius * Math.Cos(angle + Math.PI / 2);
                    double ctrY = strpt.Y + radius * Math.Sin(angle + Math.PI / 2);

                    double a = (length / radius) * 180 / Math.PI;
                    System.Windows.Media.Matrix rotation = new System.Windows.Media.Matrix();
                    rotation.Rotate(a);
                    Vector v = new Vector(strpt.X - ctrX, strpt.Y - ctrY);
                    Vector resultV = rotation.Transform(v);
                    resultV.Normalize();
                    double endX = ctrX + Math.Abs(radius) * resultV.X;
                    double endY = ctrY + Math.Abs(radius) * resultV.Y;

                    endpt = new Point(endX, endY);
                    break;
                case BasicPrimitive.PrimitiveType.CLOTHOID:
                    break;
            }
            return endpt;
        }
    }
}
