﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Utility;
using GeomLib;
using GeomLib.Constraint;
using WrapperLibrary;
using WrapperLibrary.RDF;


namespace Wpf3DTest
{
    public static class CommandLibrary
    {
        private static Point3D Origin = new Point3D(0, 0, 0);
        
        private static double firstZ;
        public static RDF_SweptAreaSolid SweptSolid;
        public static void SetDefaultCommand(bool initializing = false)
        {
            firstZ = 0;
            if (GlobalVariables.CurrentMode != Mode.Standard || initializing)
            {
                SweptSolid = new RDF_SweptAreaSolid();
            }
        }
        
        public static void CreateExtrusionCommand()
        {
            Point3D hitloc = Library.CurSketch.HitPoints.First();
            Matrix3D mtxextrusion = Library.Cursor3D.CHZAxis.Transform.Value;
            // get the local coordinate system
            mtxextrusion.Invert();
            Point3D curLoc = mtxextrusion.Transform(hitloc);
            mtxextrusion.Invert();

            firstZ = Math.Round(curLoc.Z, 4);
            curLoc = new Point3D(Origin.X, Origin.Y, Math.Round(curLoc.Z, 4) - firstZ);

            RDF_Face2D area = (RDF_Face2D)GlobalVariables.SelectedItem;
            RDF_Polygon3D outer = area.Outer;
            Point3D startPt = outer.StartPt.GetPoint3D();
            RDF_Line3D dirPath = new RDF_Line3D(Origin, curLoc);

            // need to transform the points in order to fix 
            // the flipping problem when the direction is negative

            SweptSolid.SetDirection(dirPath);
            SweptSolid.SetSweptProfile(outer);

            foreach (var inner in area.Inners)
            {
                //RDF_Polygon3D newInner = inner.CreateInnerPolygon();
                SweptSolid.AddOpening(inner);
            }
            RDF_Matrix flippingMtx = new RDF_Matrix(Matrix3D.Identity);
            RDF_MatrixMultiplication sweptSolidMtx = new RDF_MatrixMultiplication(outer.Transform.Matrix, flippingMtx);
            

            RDF_Transformation translate = new RDF_Transformation();
            translate.SetMatrix(sweptSolidMtx);
            translate.SetTransformGeom(SweptSolid);

            // 1. get all the constraint objects
            foreach (var clonedArea in area.Constraint.ConstraintObjects)
            {
                // 2. get the transform from the constraint object
                var clonedTransform = ((RDF_Face2D)clonedArea).Transform;
                var clonedMtx = clonedTransform.Matrix;

                // 3. create the cloned swept solid
                RDF_SweptAreaSolid clonedSweptSolid = (RDF_SweptAreaSolid)SweptSolid.Clone();

                // 4. apply the transform to the cloned swept solid
                RDF_Transformation clonedTranslate = new RDF_Transformation();
                clonedTranslate.SetMatrix(clonedMtx);
                clonedTranslate.SetTransformGeom(clonedSweptSolid);
            }
        }

        public static void ProcessExtrusionCommand()
        {
            Point3D hitloc = Library.CurSketch.HitPoints.First();
            Matrix3D mtxextrusion = Library.Cursor3D.CHZAxis.Transform.Value;
            // get the local coordinate system
            mtxextrusion.Invert();
            Point3D curLoc = mtxextrusion.Transform(hitloc);
            mtxextrusion.Invert();

            double curZ = Math.Round(curLoc.Z, 4) - firstZ;
            curLoc = new Point3D(Origin.X, Origin.Y, Math.Abs(curZ));
            if (Math.Round(curLoc.Z, 4) - firstZ < 0)
            {
                //System.Diagnostics.Debug.WriteLine(curLoc.Z);
            }
            //System.Diagnostics.Debug.WriteLine("extrude!!");
            var dir = SweptSolid.Direction;
            RDF_Line3D dirPath;
            if (dir is RDF_Line3D)
                dirPath = (RDF_Line3D)dir;
            else
                dirPath = (RDF_Line3D)((RDF_Transformation)dir).TransformGeom;
            dirPath.ChangePoint(curLoc, 1);

            // flip => not working good
            if (curZ < 0)
            {
                var swMtxAbstract = SweptSolid.Transform.Matrix;
                if (swMtxAbstract is RDF_MatrixMultiplication)
                {
                    var swMtx = (RDF_MatrixMultiplication)swMtxAbstract;
                    var fstMtx3D = swMtx.FirstMatrix.Matrix3D.Clone();
                    fstMtx3D.OffsetX = 0;
                    fstMtx3D.OffsetY = 0;
                    fstMtx3D.OffsetZ = 0;

                    var newTraslate = new Point3D(0, 0, curZ);
                    newTraslate = fstMtx3D.Transform(newTraslate);

                    ((RDF_Matrix)swMtx.SecondMatrix).SetTranslate(newTraslate);
                }
            }
        }

        public static void ProcessArrayCommand(Constraint cst)
        {
            if (GlobalVariables.SelectedItem != null && Library.CurSketch.HitPlanePoints3D.Count > 0)
            {
                if (cst.ConstraintName == GeometricConstraintEnum.None)
                {
                    cst.ConstraintName = GeometricConstraintEnum.Array;
                    cst.ArrayOffset = Library.CurSketch.HitPlanePoints3D.Last() - GlobalVariables.SelectedItem.Transform.Matrix.Translate;
                }
                else if (cst.ConstraintName != GeometricConstraintEnum.Array)
                {
                    cst.Reset();
                    cst.ArrayOffset = Library.CurSketch.HitPlanePoints3D.Last() - GlobalVariables.SelectedItem.Transform.Matrix.Translate;
                }
                int numInArray = cst.ConstraintObjects.Count;
                double distance = Library.CurSketch.CurrentDrawingDistance;
                int tarNumInArray = (int)distance / 50 + 1;

                // insert a new element in array if necessary
                if (tarNumInArray > numInArray)
                {
                    var newObj = GlobalVariables.SelectedItem.Clone();
                    cst.ConstraintObjects.Insert(0, newObj);
                    Step pstep = new Step();
                    pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                    if (newObj is RDF_Face2D)
                        ((RDF_Face2D)newObj).RenderShapeMesh(pstep, Library.SolidModelGroup);
                    else if (newObj is RDF_SweptAreaSolid)
                        ((RDF_SweptAreaSolid)newObj).RenderConceptMesh(Library.SolidModelGroup);
                    else if(newObj is RDF_Boolean3D)
                        ((RDF_Boolean3D)newObj).RenderConceptMesh(Library.SolidModelGroup);
                }

                var ptI = 1;
                double segDist = distance / tarNumInArray;
                double accuDist = 0;
                foreach (var cloneElem in cst.ConstraintObjects)
                {
                    for (; ptI < Library.CurSketch.HitPlanePoints3D.Count; ptI++)
                    {
                        int prePtI = ptI - 1;
                        var prePt = Library.CurSketch.HitPlanePoints3D[prePtI];
                        var curPt = Library.CurSketch.HitPlanePoints3D[ptI];
                        accuDist += curPt.distanceTo(prePt);

                        if (accuDist >= segDist)
                        {
                            accuDist = 0;
                            // move the elements to the right location
                            var translateTo = Library.CurSketch.HitPlanePoints3D[ptI] - cst.ArrayOffset;
                            RDF_MatrixAbstract mtxabstract = ((RDF_GeometricItem)cloneElem).Transform.Matrix;
                            mtxabstract.MoveRDFGeoms(translateTo, Library.TransformFace2D);
                            break;
                        }                        
                    }
                }
               
            }
        }

        public static void RenderConnectedSweptSolid(this RDF_SweptAreaSolid originSolid, Model3DGroup group)
        {
            // find the face2D
            RDF_Polygon3D profile = originSolid.SweptProfile;
            RDF_Face2D face = profile.Face;

            // two routes to get to the connected swept solid
            // 1. find the swept solid in the back relation list
            // 2. find the constraint object, and its swept solid from the matrix

            #region Method 1
            foreach (var brObj in profile.BackRelations)
            {
                if (brObj.RelatedTo is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid clonedSolid = (RDF_SweptAreaSolid)brObj.RelatedTo;
                    clonedSolid.RenderConceptMesh(group);
                }
            }
            #endregion


            #region Method 2

            //foreach (var clonedArea in face.Constraint.ConstraintObjects)
            //{
            //    if (clonedArea is RDF_Face2D)
            //    {
            //        var relatedTransforms = ((RDF_Face2D)clonedArea).Transform.Matrix.RelatedTransforms;


            //    }
            //}
            #endregion
        }
    }
}
