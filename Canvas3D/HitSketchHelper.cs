﻿using System;
using System.Collections.Generic;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Petzold.Media3D;
using WrapperLibrary.RDF;
using Utility;
using GeomLib;

namespace Wpf3DTest
{  
    /// <summary>
    /// Find the objects being hit by either cursor move or sketch drawings.
    /// This takes the objects in the scene (ViewPort3D), including contextual 
    /// axes, hitplane (hidden), and actual geometry into account for sorting out 
    /// the hit priority. When doing the hit test, the actual geometry is the 
    /// most important object. 
    /// </summary>
    public class HitSketchHelper
    {
        public Viewport3D ViewPort { get; set; }

        public List<Point> SketchPoints { get; private set; }
        /// <summary>
        /// When sketch, the sketch points are the points that closest to the view camera
        /// Thus, Sketch Points could be on HitPlane or on HitGeometry.
        /// </summary>
        public double CurrentDrawingDistance { get; set; }
        public List<Point3D> SketchPoints3D { get; private set; }
        public List<Point3D> HitPlanePoints3D { get; private set; }
        private Rect bbox;
        public Rect BoundingBox { get { return bbox; } }
        private Rect3D bbox3D;
        public Rect3D BoundingBox3D { get { return bbox3D; } }

        public GeometryModel3D HitPlane {get; set;}
        public List<GeometryModel3D> CurHitGeoms { get; private set; }
        public List<GeometryModel3D> HitGeoms { get; private set; }
        /// <summary>
        /// The first hit geom is not necessary the first geom hit by ray
        /// When two meshes overlayed, it will try to find the mesh associated
        /// with RDF_Face2D.
        /// </summary>
        public GeometryModel3D FirstHitGeom { set; get; }
        public List<WireBase> HitWires { get; private set; }
        public List<WireBase> AreaHitWires { set; get; }
        public List<WireBase> HitUtilities { get; private set; }
        public List<GeometryModel3D> HitUtilPlanes { get; private set; }
        public HitResultTypeEnum LastResultType { get; private set; }

        private List<Point3D> hitPts;
        public List<Point3D> HitPoints
        {
            get
            {
                return this.hitPts;
            }
        }
        public Vector3D HitNormal { get; private set; }

        // for testing the hit utility function
        private double distanceToCursorLoc1D;
        private double preDistanceToCursorLoc1D;
        private int singleAxisHitThreshold = 0;
        private double distanceToCursorLoc2D;
        private int planeHitThreshold = 0;

        /// <summary>
        /// flags for hit tests
        /// </summary>
        private bool isPlaneHitting = true;
        private bool isFindingUtil = false;
        private bool isHovering = false;
        private bool isTouching = false;
        public int Count 
        { 
            get 
            {
                return this.SketchPoints.Count; 
            } 
        }

        private GeometryModel3D premodel = null;
        public HitSketchHelper(Viewport3D vp)
        {
            this.SketchPoints = new List<Point>();
            this.CurrentDrawingDistance = 0;
            this.SketchPoints3D = new List<Point3D>();
            this.HitPlanePoints3D = new List<Point3D>();
            this.CurHitGeoms = new List<GeometryModel3D>();
            this.HitGeoms = new List<GeometryModel3D>();
            this.HitWires = new List<WireBase>();
            this.HitUtilities = new List<WireBase>();
            this.HitUtilPlanes = new List<GeometryModel3D>();
            this.bbox = new Rect();
            this.bbox3D = new Rect3D();
            this.ViewPort = vp;
            this.AreaHitWires = new List<WireBase>();
        }

        public void EndSketch()
        {
            this.SketchPoints = new List<Point>();
            this.CurrentDrawingDistance = 0;
            this.SketchPoints3D = new List<Point3D>();
            this.HitPlanePoints3D = new List<Point3D>();
            this.bbox = new Rect();
            this.bbox3D = new Rect3D();
            //this.CurHitGeoms = new List<GeometryModel3D>();
            this.HitGeoms = new List<GeometryModel3D>();
            this.FirstHitGeom = null;
            this.HitWires = new List<WireBase>();
            this.HitUtilities = new List<WireBase>();
            this.HitUtilPlanes = new List<GeometryModel3D>();

            this.isPlaneHitting = true;
            this.isFindingUtil = false;
            this.isHovering = false;

            this.LastResultType = HitResultTypeEnum.None;

            this.singleAxisHitThreshold = 0;
            this.planeHitThreshold = 0;
            this.AreaHitWires = new List<WireBase>();
        }

        public HitResultTypeEnum AddSketchPoint(Point newpt)
        {
            if (this.SketchPoints.Count > 0)
            {
                Point lastpt = this.SketchPoints.Last();
                if (newpt.distanceTo(lastpt) == 0)
                {
                    //GlobalVariables.CurrentMode = Mode.Standard;
                    return HitResultTypeEnum.None;
                }
            }

            this.SketchPoints.Add(newpt);
            if (this.bbox.Width == 0 && this.bbox.Height == 0)
                this.bbox = new Rect(newpt.X, newpt.Y, 1, 1);
            else
                this.bbox = Rect.Union(this.bbox, newpt); // expand the bounding box
                

            // reset the hit test variables
            //this.HitPlane = null;
            this.CurHitGeoms = new List<GeometryModel3D>();
            this.HitNormal = new Vector3D(double.MinValue, double.MinValue, double.MinValue);
            this.hitPts = new List<Point3D>();

            // set the range of ray source points for hit test
            // if the boundingbox is too large, then set the range to 0
            int rangeX = 0, rangeY = 0;
            if (this.BoundingBox.Size.Width * this.BoundingBox.Size.Height < 900) // 30 * 30
            {
                rangeX = 1;
                rangeY = 1;
            }
            else
            {
                // cancel the utility hit mode
                this.HitUtilities = new List<WireBase>();
                this.HitUtilPlanes = new List<GeometryModel3D>();
            }

            #region hit test!!
            isFindingUtil = false;
            isHovering = false;
            isTouching = false;
            for (int x = 0; x <= rangeX; x++)
            {
                for (int y = 0; y <= rangeY; y++)
                {
                    if (x == 0 && y == 0)
                        isPlaneHitting = true;
                    else
                    {
                        isPlaneHitting = false;
                        if (isFindingUtil)
                            continue; // if it's already find the util object, don't try to find it again
                    }
                    Point testpt = new Point(newpt.X + x, newpt.Y + y);
                    PointHitTestParameters pointparams = new PointHitTestParameters(testpt);
                    VisualTreeHelper.HitTest(this.ViewPort, null, HTResult, pointparams);

                    if (!isPlaneHitting && !isFindingUtil)
                    {
                        Point testptMinus = new Point(newpt.X - x, newpt.Y - y);
                        PointHitTestParameters pointparamsminus = new PointHitTestParameters(testptMinus);
                        VisualTreeHelper.HitTest(this.ViewPort, null, HTResult, pointparamsminus);
                    }
                }
            }
            #endregion

            if (this.HitPoints.Count > 0)
            {
                this.SketchPoints3D.Add(this.HitPoints.First());
                if (this.SketchPoints3D.Count > 1)
                {
                    var lastPt = this.SketchPoints3D[this.SketchPoints3D.Count - 2];
                    var dist = lastPt.distanceTo(this.HitPoints.First());
                    CurrentDrawingDistance += dist;
                }
            }

            #region Recognize Commands
            double utilHitPercent = 0;
            if (GlobalVariables.SelectedItem != null && GlobalVariables.CurrentMode == Mode.Sketch)
            {
                #region Hit Utility lines
                if (this.HitUtilities.Count > 5) // the last one is to prevent 
                {
                    // calculate the utility hit percentages
                    utilHitPercent = (double)this.HitUtilities.Count / (double)this.SketchPoints.Count;
                    #region Hit Two Axes
                    if (this.HitUtilities.Count > 10 && utilHitPercent >= 1 && (this.HitUtilities.First() != this.HitUtilities.Last())) // should equals to 1, but in case...
                    {
                        this.LastResultType = HitResultTypeEnum.HitTwoAxes;
                        preDistanceToCursorLoc1D = distanceToCursorLoc1D;
                        singleAxisHitThreshold--;
                        return HitResultTypeEnum.HitTwoAxes;
                    }
                    #endregion
                    #region Hit One Axis
                    else if (distanceToCursorLoc1D - preDistanceToCursorLoc1D > 0.1 && GlobalVariables.SelectedItem != null)
                    {
                        singleAxisHitThreshold++;
                        if (singleAxisHitThreshold > 1)
                        {
                            this.LastResultType = HitResultTypeEnum.HitOneAxis;
                            preDistanceToCursorLoc1D = distanceToCursorLoc1D;
                            return HitResultTypeEnum.HitOneAxis;
                        }
                    }
                    #endregion
                    else
                        singleAxisHitThreshold--;
                }
                else
                {
                    singleAxisHitThreshold--;
                }
                singleAxisHitThreshold = singleAxisHitThreshold < 0 ? 0 : singleAxisHitThreshold;
                preDistanceToCursorLoc1D = distanceToCursorLoc1D;
                #endregion

                #region Plane close to origin
                //System.Diagnostics.Debug.WriteLine(planeHitThreshold);
                if (distanceToCursorLoc2D < 20)
                {
                    planeHitThreshold++;
                    if (planeHitThreshold > 10)
                    {
                        this.LastResultType = HitResultTypeEnum.HitPlaneOrigin;
                        return HitResultTypeEnum.HitPlaneOrigin;
                    }
                }
                else
                    planeHitThreshold = 0;
                #endregion
            }
            #endregion

            #region Create Sketch Lines
            if (this.LastResultType != HitResultTypeEnum.HitOneAxis &&
                this.LastResultType != HitResultTypeEnum.HitTwoAxes && this.HitPoints.Count > 0)
            {
                #region Hit Geometry to create Sketch Lines
                if (this.CurHitGeoms.Count > 0)
                {
                    if (this.bbox3D.SizeX == 0 && this.bbox3D.SizeY == 0 && this.bbox3D.SizeZ == 0)
                        this.bbox3D = new Rect3D(this.HitPoints.First(), new Size3D(1, 1, 1));
                    else
                        this.bbox3D = Rect3D.Union(this.bbox3D, this.HitPoints.First()); // expand the bounding box

                    //this.SketchPoints3D.Add(this.HitPoints.First());
                    this.LastResultType = HitResultTypeEnum.HitGeometry;
                    return HitResultTypeEnum.HitGeometry;
                }
                #endregion
                #region Hit Plane to create Sketch Lines
                else if (this.HitPlane != null)
                {
                    if (this.bbox3D.SizeX == 0 && this.bbox3D.SizeY == 0 && this.bbox3D.SizeZ == 0)
                        this.bbox3D = new Rect3D(this.HitPoints.First(), new Size3D(1, 1, 1));
                    else
                        this.bbox3D = Rect3D.Union(this.bbox3D, this.HitPoints.First()); // expand the bounding box

                    //this.SketchPoints3D.Add(this.HitPoints.First());
                    this.LastResultType = HitResultTypeEnum.HitPlane;
                    return HitResultTypeEnum.HitPlane;
                }
                #endregion
            }
            #endregion

            // else do nothing
            this.LastResultType = HitResultTypeEnum.None;
            return HitResultTypeEnum.None;
        }

        public HitResultTypeEnum Touch(Point newpt)
        {
            return Hover(newpt, true);
        }

        public HitResultTypeEnum Hover(Point newpt, bool isTouching = false)
        {
            // reset the hit test variables
            if (this.CurHitGeoms.Count > 0)
            {
                // reset the appearance of hit test
                this.CurHitGeoms.ForEach(md =>
                {
                    var hitObj = md.GetValue(RDFExtension.RDFObjectProperty);

                    if (hitObj is RDF_GeometricItem && ((RDF_GeometricItem)hitObj).IsSelected)
                    {

                    }
                    else
                    {
                        md.Material = GlobalConstant.UnSelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.UnSelectedMeshBrush);
                        md.BackMaterial = GlobalConstant.UnSelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.UnSelectedMeshBrush);
                    }
                });
            }

            this.CurHitGeoms = new List<GeometryModel3D>();
            this.HitNormal = new Vector3D(double.MinValue, double.MinValue, double.MinValue);
            this.hitPts = new List<Point3D>();
            this.isTouching = isTouching;
            isFindingUtil = false;
            isPlaneHitting = true;
            isHovering = true;

            PointHitTestParameters pointparams = new PointHitTestParameters(newpt);
            VisualTreeHelper.HitTest(this.ViewPort, null, HTResult, pointparams);

            if (CurHitGeoms.Count > 0)
            {
                this.LastResultType = HitResultTypeEnum.HitGeometry;
                return HitResultTypeEnum.HitGeometry;
            }
            else if (this.HitPlane != null)
            {
                this.LastResultType = HitResultTypeEnum.HitPlane;
                return HitResultTypeEnum.HitPlane;
            }
            else
            {
                this.LastResultType = HitResultTypeEnum.None;
                return HitResultTypeEnum.None;
            }
        }

        /// <summary>
        /// Take the existing Sketch Points and do the hit tests
        /// 
        /// Report: this algorithm could be mentioned in the paper
        /// </summary>
        /// <returns>tuple1: polygon wire hit; tuple2: utility hit</returns>
        public Tuple<List<WireBase>,List<WireBase>> FindAreaHitWires()
        {
            this.AreaHitWires = new List<WireBase>();
            if (this.SketchPoints.Count > 1)
            {
                int count = this.SketchPoints.Count;
                var byX = this.SketchPoints.OrderBy(pt => pt.X).ToList();
                var byY = this.SketchPoints.OrderBy(pt => pt.Y).ToList();
                var midX = (int)(byX[0].X + byX[count - 1].X) / 2;
                var midY = (int)(byY[0].Y + byY[count - 1].Y) / 2;
                var rangeX = Math.Ceiling(((midX - byX[0].X) * 0.8));
                var rangeY = Math.Ceiling(((midY - byY[0].Y) * 0.8));
                isPlaneHitting = false;

                for (int x = midX; x <= midX + rangeX; x++)
                {
                    for (int y = midY; y <= midY + rangeY; y++)
                    {
                        Point testpt = new Point(x, y);
                        PointHitTestParameters pointparams = new PointHitTestParameters(testpt);
                        VisualTreeHelper.HitTest(this.ViewPort, null, HTResult, pointparams);

                        if (!isPlaneHitting && !isFindingUtil)
                        {
                            Point testptMinus = new Point(2 * midX - x, 2 * midY - y);
                            PointHitTestParameters pointparamsminus = new PointHitTestParameters(testptMinus);
                            VisualTreeHelper.HitTest(this.ViewPort, null, HTResult, pointparamsminus);
                        }
                    }
                }
            }
            //this.AreaHitWires.RemoveAll(wire => wire is WireLine);
            return new Tuple<List<WireBase>,List<WireBase>>(this.AreaHitWires, this.HitUtilities);
        }

        private HitTestResultBehavior HTResult(HitTestResult rawresult)
        {
            RayHitTestResult rayResult = rawresult as RayHitTestResult;
            Visual3D hitobj = rayResult.VisualHit;

            #region Hit a Mesh Model
            if (rayResult != null && isPlaneHitting && !(hitobj is WireBase))
            {
                RayMeshGeometry3DHitTestResult rayMeshResult = rayResult as RayMeshGeometry3DHitTestResult;
                if (rayMeshResult != null)
                {
                    distanceToCursorLoc2D = rayMeshResult.PointHit.distanceTo(GlobalVariables.CursorPosition);
                    GeometryModel3D hitgeo = rayMeshResult.ModelHit as GeometryModel3D;

                    if ((string)hitgeo.GetValue(GeomLib.Extensions.IDProperty) == "cxyplane" || (string)hitgeo.GetValue(GeomLib.Extensions.IDProperty) == "cxzplane" || (string)hitgeo.GetValue(GeomLib.Extensions.IDProperty) == "cyzplane"
                     || (string)hitgeo.GetValue(GeomLib.Extensions.IDProperty) == "orixyplane" || (string)hitgeo.GetValue(GeomLib.Extensions.IDProperty) == "orixzplane" || (string)hitgeo.GetValue(GeomLib.Extensions.IDProperty) == "oriyzplane")                    
                    {
                        HitUtilPlanes.Add(hitgeo);
                        if(HitUtilPlanes.Count == 1)
                            hitgeo.ChangeFrontBackOpacity(255);
                    }
                    else if (hitgeo != HitPlane)
                    {
                        //if (GlobalVariables.CurrentMode != Mode.Array)
                        {
                            this.hitPts.Add(rayMeshResult.PointHit);
                            RDF_Abstract hitRDF = (RDF_Abstract)hitgeo.GetValue(WrapperLibrary.RDF.RDFExtension.RDFObjectProperty);

                            // get the normal of the hit point of the first hit object
                            if (HitNormal.X == double.MinValue) // make sure it's the first mesh being hit in this test session
                                CalculateHitNormal(rayMeshResult, hitRDF);

                            hitgeo.Material = GlobalConstant.SelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.SelectedMeshBrush);
                            hitgeo.BackMaterial = GlobalConstant.SelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.SelectedMeshBrush);

                            premodel = hitgeo;
                            this.CurHitGeoms.Add(hitgeo);
                            this.HitGeoms.Add(hitgeo);

                            // decide what is the FirstHitGeom
                            // case 1. the first hit obj
                            // case 2. this is the second hit obj, it is a RDF_Face2D and very close to the first one
                            if (hitRDF is RDF_Face2D &&
                                this.hitPts.Count == 2 && this.hitPts[0].distanceTo(this.hitPts[1]) < 1)
                                this.FirstHitGeom = hitgeo;
                            else if (this.FirstHitGeom == null)
                                this.FirstHitGeom = hitgeo;
                        }
                    }
                    else
                    {
                        this.hitPts.Add(rayMeshResult.PointHit);
                        if (HitNormal.X == double.MinValue && !isHovering) // make sure it's the first mesh being hit in this test session
                            CalculateHitNormal(rayMeshResult);

                        this.HitPlane = hitgeo;
                        this.HitPlanePoints3D.Add(rayMeshResult.PointHit);
                    }
                }
            }
            #endregion
            #region Hit a Line
            else if (hitobj is WireBase && !isHovering && !isPlaneHitting || isTouching)
            {
                WireBase wire = (WireBase)hitobj;

                if (wire is WireLines) // gridlines .... useless
                {}
                else
                {
                    this.HitWires.Add(wire);
                    if(wire is WirePath)
                        this.AreaHitWires.Add(wire);
                    if (wire is WireLine)
                    {
                        if (wire.ID == "cxaxis" || wire.ID == "cyaxis" || wire.ID == "czaxis")
                        {
                            distanceToCursorLoc1D = rayResult.PointHit.distanceTo(new Point3D(0, 0, 0)); // find the distance to origin
                            //System.Diagnostics.Debug.WriteLine(distanceToCursorLoc1D);

                            this.HitUtilities.Add(wire);
                            isFindingUtil = true;
                        }
                        else if (wire.ID == "hxaxis" || wire.ID == "hyaxis" || wire.ID == "hzaxis")
                        {
                            distanceToCursorLoc1D = rayResult.PointHit.distanceTo(new Point3D(0, 0, 0));
                            this.HitUtilities.Add(wire);
                            isFindingUtil = true;
                            //System.Diagnostics.Debug.WriteLine("hit");
                        }
                    }
                }
                //    isUtilObj = true;
            }
            #endregion
            else if(isPlaneHitting)
                premodel = null;

            return HitTestResultBehavior.Continue;
        }

        private void CalculateHitNormal(RayMeshGeometry3DHitTestResult rayMeshResult, RDF_Abstract hitRDF = null)
        {
            int index1 = rayMeshResult.VertexIndex1;
            int index2 = rayMeshResult.VertexIndex2;
            int index3 = rayMeshResult.VertexIndex3;

            if (rayMeshResult.MeshHit.Normals.Count > 0 )
            {
                HitNormal = rayMeshResult.MeshHit.Normals[index1];
                HitNormal.Normalize();
                if (hitRDF != null && hitRDF is RDF_Face2D && ((RDF_Face2D)hitRDF).HasTransform)
                {
                    Matrix3D mtx = ((RDF_Face2D)hitRDF).Transform.GetMatrix3D();
                    HitNormal = mtx.Transform(HitNormal);
                }
            }
            else
            {
                var pt1 = rayMeshResult.MeshHit.Positions[index1];
                var pt2 = rayMeshResult.MeshHit.Positions[index2];
                var pt3 = rayMeshResult.MeshHit.Positions[index3];
                Vector3D vecX = pt2 - pt1;
                vecX.Normalize();
                Vector3D vecY = pt3 - pt1;
                vecY.Normalize();
                Vector3D vecZ = Vector3D.CrossProduct(vecX, vecY);
                vecZ.Normalize();

                if (rayMeshResult.ModelHit is GeometryModel3D)
                {
                    var geomModel = (GeometryModel3D)rayMeshResult.ModelHit;
                    var mtx = geomModel.Transform.Value;
                    vecZ = mtx.Transform(vecZ);
                }
                HitNormal = vecZ;
            }
        }

    }
}
