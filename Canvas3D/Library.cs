﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Petzold.Media3D;
using Utility;
using WrapperLibrary;
using WrapperLibrary.RDF;
using WrapperLibrary.Cornucopia;
using CanvasSketch;
using GeomLib;


namespace Wpf3DTest
{
    public static class Library
    {
        public static int TouchCount = 0;
        public static int MoveCount = 0;
        public static MovingAxis TouchOn = MovingAxis.none;
        public static int TouchOnID = -1;
        private static Point3D preSecTouch = new Point3D();
        public static SketchCanvas InkCanv = null;
        public static Viewport3D ViewPort3D = null;
        public static Cursor3D Cursor3D = null;
        public static Model3DGroup SolidModelGroup = null;
        public static Model3DGroup PlaneGroup = null;
        public static WireLines Gridlines = new WireLines();
        public static WireLines TenGridLines = new WireLines();
        public static HitSketchHelper CurSketch;
        private static AxisAngleRotation3D curRotation = null;// Matrix3D.Identity;

        public static void InitializeCanvasComponents(Viewport3D viewport, SketchCanvas inkcanvas, Cursor3D cursor, Model3DGroup gp, Model3DGroup gp1, HitSketchHelper curSketch)
        {
            InkCanv = inkcanvas;
            ViewPort3D = viewport;
            Cursor3D = cursor;
            SolidModelGroup = gp;
            PlaneGroup = gp1;
            CurSketch = curSketch;

            createXYZAxes();
            createGrid();
        }

        #region Create Utility Objects
        private static void createXYZAxes()
        {
            Color transparent = Colors.White;
            transparent.A = 0;

            //createContextAxis();

            #region main axis
            WireLine hxaxis = new WireLine();
            WireLine hyaxis = new WireLine();
            WireLine hzaxis = new WireLine();
            hxaxis.ID = "hxaxis";
            hxaxis.WireType = WireTypeEnum.HitAxis;
            hxaxis.Opacity = 0;
            hxaxis.Color = Colors.Red;
            hxaxis.Thickness = 15;
            hyaxis.ID = "hyaxis";
            hyaxis.WireType = WireTypeEnum.HitAxis;
            hyaxis.Opacity = 0;
            hyaxis.Color = Colors.Green;
            hyaxis.Thickness = 15;
            hzaxis.ID = "hzaxis";
            hzaxis.WireType = WireTypeEnum.HitAxis;
            hzaxis.Opacity = 0;
            hzaxis.Color = Colors.Blue;
            hzaxis.Thickness = 15;


            hxaxis.Point1 = new Point3D(-15, 0, 0);
            hxaxis.Point2 = new Point3D(15, 0, 0);
            hyaxis.Point1 = new Point3D(0, -15, 0);
            hyaxis.Point2 = new Point3D(0, 15, 0);
            hzaxis.Point1 = new Point3D(0, 0, -15);
            hzaxis.Point2 = new Point3D(0, 0, 15);

            ViewPort3D.Children.Add(hxaxis);
            ViewPort3D.Children.Add(hyaxis);
            ViewPort3D.Children.Add(hzaxis);

            WireLine xaxis = new WireLine();
            xaxis.ID = "xaxis";
            xaxis.Color = Colors.Red;
            xaxis.WireType = WireTypeEnum.CursorAxis;
            WireLine yaxis = new WireLine();
            yaxis.ID = "yaxis";
            yaxis.Color = Colors.Green;
            yaxis.WireType = WireTypeEnum.CursorAxis;
            WireLine zaxis = new WireLine();
            zaxis.ID = "zaxis";
            zaxis.Color = Colors.Blue;
            zaxis.WireType = WireTypeEnum.CursorAxis;

            xaxis.Point1 = new Point3D(-100, 0, 0);
            xaxis.Point2 = new Point3D(100, 0, 0);
            yaxis.Point1 = new Point3D(0, -100, 0);
            yaxis.Point2 = new Point3D(0, 100, 0);
            zaxis.Point1 = new Point3D(0, 0, -100);
            zaxis.Point2 = new Point3D(0, 0, 100);

            ViewPort3D.Children.Insert(3, xaxis);
            ViewPort3D.Children.Insert(3, yaxis);
            ViewPort3D.Children.Insert(3, zaxis);
            #endregion

            #region hit planee
            SolidColorBrush plancolor = new SolidColorBrush(transparent);

            MeshGeometry3D hitplane = new MeshGeometry3D();
            hitplane.Positions.Add(new Point3D(-10000, -10000, 0));
            hitplane.Positions.Add(new Point3D(10000, -10000, 0));
            hitplane.Positions.Add(new Point3D(10000, 10000, 0));
            hitplane.Positions.Add(new Point3D(-10000, 10000, 0));

            hitplane.TriangleIndices.Add(0);
            hitplane.TriangleIndices.Add(1);
            hitplane.TriangleIndices.Add(2);
            hitplane.TriangleIndices.Add(2);
            hitplane.TriangleIndices.Add(3);
            hitplane.TriangleIndices.Add(0);

            GeometryModel3D hitPlane = new GeometryModel3D(hitplane, new DiffuseMaterial(plancolor));
            hitPlane.BackMaterial = new DiffuseMaterial(plancolor);

            Transform3DGroup tgroup = new Transform3DGroup();
            RotateTransform3D rtransform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), 0));
            TranslateTransform3D ttransform = new TranslateTransform3D(0, 0, 0);
            tgroup.Children.Add(rtransform);
            tgroup.Children.Add(ttransform);

            hitPlane.Transform = tgroup;
            hitPlane.SetValue(GeomLib.Extensions.IDProperty, "hitplan");
            hitPlane.SetValue(GeomLib.Extensions.IsInHitTestProperty, true);
            Library.PlaneGroup.Children.Add(hitPlane);

            Library.CurSketch.HitPlane = hitPlane;

            #endregion
        }

        private static void createGrid()
        {
            Transform3DGroup tgroup = new Transform3DGroup();
            RotateTransform3D rtransform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), 0));
            TranslateTransform3D ttransform = new TranslateTransform3D(0, 0, 0);
            tgroup.Children.Add(rtransform);
            tgroup.Children.Add(ttransform);
            Gridlines.Transform = tgroup;
            TenGridLines.Transform = tgroup;

            Gridlines.Lines = new Point3DCollection();
            TenGridLines.Lines = new Point3DCollection();

            Gridlines.Color = Colors.LightBlue;
            Gridlines.Thickness = 1;
            TenGridLines.Color = Colors.LightSkyBlue;
            TenGridLines.Thickness = 1;

            for (int x = -50; x <= 50; x++)
            {
                Point3D pt3d1 = new Point3D(x, -50, -0.1);
                Point3D pt3d2 = new Point3D(x, 50, -0.1);
                if (x % 10 == 0)
                {
                    TenGridLines.Lines.Add(pt3d1);
                    TenGridLines.Lines.Add(pt3d2);
                }
                else
                {
                    Gridlines.Lines.Add(pt3d1);
                    Gridlines.Lines.Add(pt3d2);
                }
            }

            for (int y = -50; y <= 50; y++)
            {
                Point3D pt3d1 = new Point3D(-50, y, -0.1);
                Point3D pt3d2 = new Point3D(50, y, -0.1);
                if (y % 10 == 0)
                {
                    TenGridLines.Lines.Add(pt3d1);
                    TenGridLines.Lines.Add(pt3d2);
                }
                else
                {
                    Gridlines.Lines.Add(pt3d1);
                    Gridlines.Lines.Add(pt3d2);
                }
            }

            ViewPort3D.Children.Insert(0, Gridlines);
            ViewPort3D.Children.Insert(0, TenGridLines);
        }
        #endregion

        /// <summary>
        /// Take the primitives and add the parts(segments) into the RDFPolygon
        /// </summary>
        /// <param name="primitives"></param>
        /// <param name="polygon"></param>
        /// <param name="mtx">for transforming the first point to the global locatoin</param>
        /// <returns>the offset of the polygon related to global locations</returns>
        public static Point3D CreateRDFSegments(this List<Primitive> primitives, RDF_Polygon3D polygon, bool resetGUID = false)
        {
            Point3D stpt = new Point3D();

            int i = 1;
            foreach (Primitive pm in primitives)
            {
                if (resetGUID)
                    pm.GUID = Guid.NewGuid().ToString();

                //System.Diagnostics.Debug.WriteLine("Start Point " + i + ": " + pm.start);
                var seg = new RDF_Segment3D(pm.GUID);
                if (pm.type == WrapperLibrary.Cornucopia.BasicPrimitive.PrimitiveType.LINE)
                {
                    seg = new RDF_Line3D(new Point3D(pm.PreviousNode.X, pm.PreviousNode.Y, 0),
                        new Point3D(pm.NextNode.X, pm.NextNode.Y, 0), pm.GUID);
                }
                else if (pm.type == WrapperLibrary.Cornucopia.BasicPrimitive.PrimitiveType.ARC)
                {
                    seg = new RDF_Arc3D(pm.GUID);
                    double rotate = pm.startCurvature < 0 ? Math.PI / 2 : -Math.PI / 2;
                    ((RDF_Arc3D)seg).SetStartAngle(pm.startAngle + rotate);
                    ((RDF_Arc3D)seg).SetRadius(1 / pm.startCurvature);
                    ((RDF_Arc3D)seg).SetSize(pm.startCurvature * pm.length);
                    ((RDF_Arc3D)seg).StartNode = new RDF_Node3D(pm.PreviousNode.X, pm.PreviousNode.Y, 0);
                    ((RDF_Arc3D)seg).EndNode = new RDF_Node3D(pm.NextNode.X, pm.NextNode.Y, 0);
                    ((RDF_Arc3D)seg).StartNode.NextEdge = (RDF_Arc3D)seg;
                    ((RDF_Arc3D)seg).EndNode.PreEdge = (RDF_Arc3D)seg;
                }
                polygon.AddPart(seg);
                if (i == 1)
                {
                    stpt = new Point3D(pm.PreviousNode.X, pm.PreviousNode.Y, 0);
                }

                i++;
            }

            return stpt;
        }
        public static CustomStroke FindCustomStrokeByID(this StrokeCollection strokes, string GUID)
        {
            foreach (var st in strokes.ToList())
            {
                if (st is CustomStroke)
                {
                    CustomStroke ct = (CustomStroke)st;
                    if (ct.GUID == GUID)
                        return ct;
                }
            }
            return null;
        }
        public static void TransformFace2D(RDF_Face2D rdfFace, Matrix3D newTransform)//, bool isFirst)
        {
            RDF_Polygon3D outer = rdfFace.Outer;

            CustomStroke customStroke = InkCanv.Strokes.FindCustomStrokeByID(outer.GUID);
            if (customStroke != null)
            {
                customStroke.matrix = newTransform.Clone();
                customStroke.matrix.OffsetX = -customStroke.matrix.OffsetX;
                customStroke.matrix.OffsetY = -customStroke.matrix.OffsetY;
                //customStroke.matrix.OffsetZ = -customStroke.matrix.OffsetY;
            }
            else
            {
                // break
            }


            //cursor.DestroyCursor();
            // set the cursor to the new location
            // not use for now -- util the object can retain selected when moving
            var oldMtx = outer.Transform.GetMatrix3D();
            MatrixTransform3D mtxTransform = new MatrixTransform3D(Matrix3D.Identity);
            var centroidmtx = oldMtx.Clone();
            var centroid = outer.Centroid;
            centroid = centroidmtx.Transform(centroid);
            centroidmtx.OffsetX = centroid.X;
            centroidmtx.OffsetY = centroid.Y;
            centroidmtx.OffsetZ = centroid.Z;
            mtxTransform.Matrix = centroidmtx;
            //if(GlobalVariables.CurrentMode != Mode.MoveShape)
                Cursor3D.SetTransform(mtxTransform);

            // just translate outer.Transform.GeometryObject instead of rerender the whole geometry
            var line = outer.Transform.GeometryObject;
            if (line is WirePath && ((WirePath)line).Paths.Count > 0)
            {
                // need to set a right matrix...
                WirePath translatingPath = (WirePath)line;
                var firstPt = translatingPath.Paths.First().First();
                Matrix3D newProfileMtx = translatingPath.Transform.Value.Clone();

                AxisAngleRotation3D rot3D = new AxisAngleRotation3D();
                if (curRotation != null)
                {
                    RotateTransform3D rotTransform = new RotateTransform3D(curRotation, firstPt);
                    newProfileMtx = rotTransform.Value * newProfileMtx;
                }
                else
                {
                    // rotate the firstPT to the real location
                    newProfileMtx.OffsetX = 0;
                    newProfileMtx.OffsetY = 0;
                    newProfileMtx.OffsetZ = 0;
                    firstPt = newProfileMtx.Transform(firstPt);

                    newProfileMtx.OffsetX = oldMtx.OffsetX - firstPt.X;
                    newProfileMtx.OffsetY = oldMtx.OffsetY - firstPt.Y;
                    newProfileMtx.OffsetZ = oldMtx.OffsetZ - firstPt.Z;
                }
                //newProfileMtx = oldMtx;
                MatrixTransform3D newtransPathform = new MatrixTransform3D(newProfileMtx);
                translatingPath.Transform = newtransPathform;
            }
            else
                line = RDF_Dim1.RenderConceptLine(outer.Transform, ViewPort3D);
            foreach (var inner in rdfFace.Inners)
            {
                WirePath innerline = RDF_Dim1.RenderConceptLine(inner.Transform, ViewPort3D);
            }

            //PreviousStep pstep = addPstepLine(line);
            //pstep.ObjectType = pstep.ObjectType | PreStepObjectTypeEnum.MeshModel;
            //rdfFace.RenderShapeMesh(pstep, group1);
            if (rdfFace.Geometry != null)
                rdfFace.Geometry.Transform = new MatrixTransform3D(newTransform);
            else
                System.Diagnostics.Debug.WriteLine("no rdfface geometry");
        }

        public static void RotateShape(TouchPointCollection tppts)
        {
            if (GlobalVariables.SelectedItem != null)
            {
                var touchpt = tppts.Last();//.First(pt => pt.TouchDevice.Id != Library.TouchOnID);
                Vector3D pivot = new Vector3D(0, 0 ,1);
                switch (TouchOn)
                {
                    case MovingAxis.xaxis:
                        RotateHitPlanes(Cursor3D.CHZAxis, Cursor3D.CHYAxis, false);
                        CurSketch.Hover(touchpt.Position);
                        pivot = new Vector3D(Cursor3D.CHXAxis.Point2.X - Cursor3D.CHXAxis.Point1.X, 
                            Cursor3D.CHXAxis.Point2.Y - Cursor3D.CHXAxis.Point1.Y, Cursor3D.CHXAxis.Point2.Z - Cursor3D.CHXAxis.Point1.Z);
                        pivot = Cursor3D.CHXAxis.Transform.Transform(pivot);
                        break;
                    case MovingAxis.yaxis:
                        RotateHitPlanes(Cursor3D.CHXAxis, Cursor3D.CHZAxis, false);
                        CurSketch.Hover(touchpt.Position);
                        pivot = new Vector3D(Cursor3D.CHYAxis.Point2.X - Cursor3D.CHYAxis.Point1.X, 
                            Cursor3D.CHYAxis.Point2.Y - Cursor3D.CHYAxis.Point1.Y, Cursor3D.CHYAxis.Point2.Z - Cursor3D.CHYAxis.Point1.Z);
                        pivot = Cursor3D.CHYAxis.Transform.Transform(pivot);
                        break;
                    case MovingAxis.zaxis:
                        RotateHitPlanes(Cursor3D.CHXAxis, Cursor3D.CHYAxis, false);
                        CurSketch.Hover(touchpt.Position);
                        pivot = new Vector3D(Cursor3D.CHZAxis.Point2.X - Cursor3D.CHZAxis.Point1.X, 
                            Cursor3D.CHZAxis.Point2.Y - Cursor3D.CHZAxis.Point1.Y, Cursor3D.CHZAxis.Point2.Z - Cursor3D.CHZAxis.Point1.Z);
                        pivot = Cursor3D.CHZAxis.Transform.Transform(pivot);
                        break;
                }
                if (CurSketch.HitPlanePoints3D.Count > 0)
                {
                    Point3D curPoint = CurSketch.HitPlanePoints3D.Last();
                    Matrix3D cursorMtx = Cursor3D.CHXAxis.Transform.Value;

                    if (!Library.preSecTouch.Equals(new Point3D()))
                    {
                        Point3D curCenter = new Point3D((float)cursorMtx.OffsetX, (float)cursorMtx.OffsetY, (float)cursorMtx.OffsetZ);
                        Vector3D preVector = Library.preSecTouch - curCenter;
                        Vector3D curVector = curPoint - curCenter;
                        double angle = Vector3D.AngleBetween(curVector, preVector);
                        var normal = Vector3D.CrossProduct(pivot, preVector);
                        normal.Normalize();
                        double angleToNormal = Vector3D.AngleBetween(normal, curVector);
                        angle = angleToNormal > 90 ? -angle : angle;

                        AxisAngleRotation3D rot3D = new AxisAngleRotation3D(pivot, angle);
                        RotateTransform3D rotTransform = new RotateTransform3D(rot3D);
                        cursorMtx = rotTransform.Value * cursorMtx;
                        Cursor3D.SetTransform(cursorMtx);

                        curRotation = rot3D;
                        GlobalVariables.SelectedItem.Transform.Matrix.TransformRDFGeoms(rotTransform.Value, Library.TransformFace2D);
                        curRotation = null;
                    }
                    Library.preSecTouch = curPoint;
                }
            }
        }

        public static void MoveShape(TouchEventArgs e)
        {
            if ((GlobalVariables.CurrentMode & Mode.MoveShape) != 0 && TouchCount == 1)
            {
                GlobalVariables.CurrentMoveDeviceID = e.TouchDevice.Id;

                TouchPoint mouseposition = e.GetTouchPoint(ViewPort3D);
                HitResultTypeEnum resulttype = CurSketch.Hover(mouseposition.Position);
                int ptcount = CurSketch.HitPlanePoints3D.Count;
                if (ptcount > 1)
                {
                    Point3D prept = CurSketch.HitPlanePoints3D[ptcount - 2];
                    Point3D curpt = CurSketch.HitPlanePoints3D[ptcount - 1];
                    Vector3D offset = curpt - prept;
                    if (GlobalVariables.SelectedItem != null && GlobalVariables.SelectedItem.HasTransform)
                    {
                        RDF_MatrixAbstract mtxabstract = GlobalVariables.SelectedItem.Transform.Matrix;
                        var mtx3D = mtxabstract.Matrix3D.Clone();
                        offset = mtx3D.Transform(offset);
                        //if (mtxabstract is RDF_Matrix)
                        {
                            if (MoveCount > 100)
                            {

                            }
                            MoveCount++;
                            switch (TouchOn)
                            {
                                case MovingAxis.xaxis:
                                    offset.Y = 0;
                                    offset.Z = 0;
                                    break;
                                case MovingAxis.yaxis:
                                    offset.X = 0;
                                    offset.Z = 0;
                                    break;
                                case MovingAxis.zaxis:
                                    offset.X = 0;
                                    offset.Y = 0;
                                    break;
                                case MovingAxis.xyplane:
                                    //offset.Z = 0;
                                    break;
                                case MovingAxis.xzplane:
                                    //offset.Y = 0;
                                    break;
                                case MovingAxis.yzplane:
                                    //offset.Z = 0;
                                    break;
                            }
                            mtx3D.Invert();
                            offset = mtx3D.Transform(offset);
                            mtxabstract.MoveRDFGeoms(offset, Library.TransformFace2D);
                        }
                    }
                }
            }
        }

        public static object RotateHitPlanes(WireLine caxis1, WireLine caxis2, bool rotateGrids = true)
        {
            Vector3D vec2 = new Vector3D(caxis2.Point2.X - caxis2.Point1.X, caxis2.Point2.Y - caxis2.Point1.Y, caxis2.Point2.Z - caxis2.Point1.Z);
            vec2 = caxis1.Transform.Transform(vec2);
            return RotateHitPlanes(caxis1, vec2, rotateGrids);
        }

        public static object RotateHitPlanes(WireLine caxis1, Vector3D vec2, bool rotateGrids = true)
        {
            Vector3D vec1 = new Vector3D(caxis1.Point2.X - caxis1.Point1.X, caxis1.Point2.Y - caxis1.Point1.Y, caxis1.Point2.Z - caxis1.Point1.Z);
            Vector3D vec3 = Vector3D.CrossProduct(vec1, vec2);
            vec3 = caxis1.Transform.Transform(vec3);
            vec3.Normalize();
            RotateTransform3D rotate = new RotateTransform3D(GeomLib.BasicGeomLib.GetAxisRotation3D(vec3));

            TranslateTransform3D translate = new TranslateTransform3D();
            translate.OffsetX = caxis1.Transform.Value.OffsetX;
            translate.OffsetY = caxis1.Transform.Value.OffsetY;
            translate.OffsetZ = caxis1.Transform.Value.OffsetZ;

            Transform3DGroup transformgroup = new Transform3DGroup();
            transformgroup.Children.Add(rotate);
            transformgroup.Children.Add(translate);

            GeometryModel3D hitplane = CurSketch.HitPlane;
            hitplane.Transform = transformgroup;

            if (rotateGrids)
            {
                Transform3D oriTransoform = Library.Gridlines.Transform;
                Library.Gridlines.Transform = transformgroup;
                Library.TenGridLines.Transform = transformgroup;

                if (StepLibrary.PSteps.Count > 0 && StepLibrary.PSteps.Last().PStep == StepTypeEnum.AxisTransform)
                {
                    oriTransoform = StepLibrary.PSteps.Last().AxisTransform;
                    StepLibrary.PSteps.Remove(StepLibrary.PSteps.Last());
                }

                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.Transform;
                pstep.PStep = StepTypeEnum.AxisTransform;
                pstep.AxisTransform = oriTransoform;
                StepLibrary.PSteps.Add(pstep);
                StepLibrary.NSteps = new List<Step>();
            }
            return null;
        }


        internal static void SetFirstTouchTown(int id)
        {
            TouchOnID = id;
            preSecTouch = new Point3D();
        }

        internal static bool ChkIfSweptSolidAvailable()
        {

            return true;
        }

        internal static void rotateHitPlaneTowardCameraOnZ()
        {
            var camTransform = Library.ViewPort3D.Camera.Transform;
            var camMatrix = camTransform.Value;
            var vectorY = new Vector3D(1, 0, 0);
            vectorY = camMatrix.Transform(vectorY);
            vectorY.Z = 0;
            if (Math.Round(vectorY.Length, 2) == 0)
            {
                vectorY = new Vector3D(0, 1, 0);
            }
            else
                vectorY.Normalize();

            Library.RotateHitPlanes(Library.Cursor3D.CHZAxis, vectorY, false);
        }
    }
}
