﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf3DTest
{
    public enum HitResultTypeEnum
    {
        None = 0,
        HitOneAxis = 1,
        HitTwoAxes = 2,
        HitGeometry = 4,
        HitPlane = 8,
        HitPlaneOrigin = 16
    }
}
