﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf3DTest
{
    public enum ButtonEnum
    {
        Extrusion,
        Array,
        SweptSolid
    }
    public enum MovingAxis
    {
        xaxis,
        yaxis,
        zaxis,
        xyplane,
        yzplane,
        xzplane,
        none
    }
}
