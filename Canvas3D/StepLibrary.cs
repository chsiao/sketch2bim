﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using WrapperLibrary;
using WrapperLibrary.RDF;
using Petzold.Media3D;
using Utility;

namespace Wpf3DTest
{
    public static class StepLibrary
    {
        public static List<Step> PSteps;
        public static List<Step> NSteps;

        public static void moveToStep(Step laststep, bool forward = false)
        {
            // refresh the selected item to prevent some unexpect behavior
            if (GlobalVariables.SelectedItem != null)
            {
                GlobalVariables.SelectedItem.UnSelect();
            }
            GlobalVariables.PreSelectedItem = null;

            if (laststep.PStep == StepTypeEnum.ObjectTransform)
            {
                RDF_MatrixAbstract mtxabstract = laststep.PreModel.Transform.Matrix;
                mtxabstract.MoveRDFGeoms(laststep.ObjectTranslation, Library.TransformFace2D);
            }
            else
            {
                RDF_GeometricItem rdfgeom = laststep.PreModel;

                if (rdfgeom != null && (laststep.PStep == StepTypeEnum.Add || laststep.PStep == StepTypeEnum.Delete))
                {
                    if ((laststep.PStep == StepTypeEnum.Add && !forward) || (laststep.PStep == StepTypeEnum.Delete && forward))
                        deleteRDFGeom(rdfgeom);
                    else if ((laststep.PStep == StepTypeEnum.Delete && !forward) || (laststep.PStep == StepTypeEnum.Add && forward))
                        addRDFGeom(rdfgeom);
                }
                else if (rdfgeom != null && laststep.PStep == StepTypeEnum.modifypath)
                {
                    if (!forward)
                    {
                        deleteRDFGeom(rdfgeom);
                        if (laststep.ModifiedObj != null && laststep.ModifiedObj is RDF_GeometricItem)
                        {
                            var modifiedgeom = (RDF_GeometricItem)laststep.ModifiedObj;
                            if (modifiedgeom is RDF_Dim1 && modifiedgeom is RDF_Polygon3D &&
                                ((RDF_Polygon3D)modifiedgeom).IsClosed)
                            {
                                var modifiedPolygon = (RDF_Polygon3D)modifiedgeom;
                                if (modifiedPolygon.HasTransform)
                                {
                                    // find the transform and siwtch it back to original transform
                                    // also try to generate the rendering again
                                    var modifiedTransform = modifiedPolygon.Transform;
                                    modifiedTransform.SetTransformGeom(modifiedPolygon);
                                    //RDF_Matrix rdfMatrix = (RDF_Matrix)modifiedTransform.Matrix;
                                    //rdfMatrix.SetTranslate(modifiedPolygon.StartPt.GetPoint3D());
                                    //modifiedTransform.GeometryObject = modifiedPolygon.pathgeom;
                                    RDF_Dim1.RenderConceptLine(modifiedTransform, Library.ViewPort3D);
                                }
                                var modifiedFace = modifiedPolygon.Face;
                                addRDFGeom(modifiedFace);

                                //if(modifiedFace.Transform.Matrix.rel)
                            }
                            else
                                addRDFGeom(modifiedgeom);
                        }
                    }
                    else
                    {
                        addRDFGeom(rdfgeom);
                        if (laststep.ModifiedObj != null && laststep.ModifiedObj is RDF_GeometricItem)
                            deleteRDFGeom((RDF_GeometricItem)laststep.ModifiedObj);
                    }
                }

                if (laststep.ObjectType.HasFlag(StepObjectTypeEnum.Transform))
                {
                    Transform3D transform = laststep.AxisTransform;
                    Library.CurSketch.HitPlane.Transform = transform;
                    Library.Gridlines.Transform = transform;
                    Library.TenGridLines.Transform = transform;
                }
            }
        }

        public static void addRDFGeom(RDF_GeometricItem rdfgeom)
        {
            if (rdfgeom is RDF_Dim1)
            {
                addDim1((RDF_Dim1)rdfgeom);
            }
            else
            {
                GeometryModel3D mesh = rdfgeom.Geometry;
                if (mesh != null)
                    Library.SolidModelGroup.Children.Add(mesh);

                if (rdfgeom is RDF_Face2D)
                {
                    RDF_Face2D face = (RDF_Face2D)rdfgeom;
                    addDim1((RDF_Polygon3D)face.Outer);
                }
                else if (rdfgeom is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid solid = (RDF_SweptAreaSolid)rdfgeom;
                    RDF_Polygon3D profile = solid.SweptProfile;
                    profile.Transform.Matrix.RelatedTransforms.Add(solid.Transform);
                }
                if (!RDFWrapper.All_RDF_Objects.ContainsKey(rdfgeom.GUID))
                    RDFWrapper.All_RDF_Objects.Add(rdfgeom.GUID, rdfgeom);
            }
        }

        public static void deleteRDFGeom(RDF_GeometricItem rdfgeom)
        {
            if (rdfgeom is RDF_Dim1)
            {
                removeDim1((RDF_Dim1)rdfgeom);
            }
            else
            {
                GeometryModel3D mesh = rdfgeom.Geometry;
                if (mesh != null)
                {
                    Library.SolidModelGroup.Children.Remove(mesh);

                    // release from related transform
                    RDF_Transformation itsTransform = rdfgeom.Transform;
                    itsTransform.Matrix.RelatedTransforms.Remove(itsTransform);


                    //RDF_Abstract rdfobj = (RDF_Abstract)mesh.GetValue(RDFExtension.RDFObjectProperty);
                    //if (GlobalVariables.SelectedItem == rdfobj)
                    //    GlobalVariables.SelectedItem.UnSelect();
                }
                if (rdfgeom is RDF_Face2D)
                {
                    RDF_Face2D face = (RDF_Face2D)rdfgeom;
                    removeDim1((RDF_Polygon3D)face.Outer);
                }
                RDFWrapper.All_RDF_Objects.Remove(rdfgeom.GUID);
            }
            if (GlobalVariables.SelectedItem == rdfgeom)
                GlobalVariables.SelectedItem = null;
        }

        public static void addDim1(RDF_Dim1 rdfline)
        {
            // connect the transform back to the geometry
            if (rdfline.HasTransform)
            {
                rdfline.Transform.SetTransformGeom(rdfline);
                rdfline.Transform.GeometryObject = rdfline.pathgeom;
            }

            WireBase line = rdfline.PathGeometry;
            if (!Library.ViewPort3D.Children.Contains(line))
                Library.ViewPort3D.Children.Add(line);
            if (RDFWrapper.All_RDF_Objects.ContainsKey(rdfline.GUID))
                RDFWrapper.All_RDF_Objects[rdfline.GUID] = rdfline;
            else
                RDFWrapper.All_RDF_Objects.Add(rdfline.GUID, rdfline);

            rdfline.Select();
        }

        public static void removeDim1(RDF_Dim1 rdfline)
        {
            WireBase line = rdfline.PathGeometry;
            Library.ViewPort3D.Children.Remove(line);
            RDFWrapper.All_RDF_Objects.Remove(rdfline.GUID);
        }

    }
}
