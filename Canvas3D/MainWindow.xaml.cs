﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Ink;                  
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;

using Utility;
using CanvasSketch;
using WrapperLibrary;
using WrapperLibrary.RDF;
using Petzold.Media3D;
using GeomLib;
using GeomLib.Constraint;

namespace Wpf3DTest {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow() {
			GlobalConstant.SetUpGlobalConstant();
			GlobalVariables.InitializeGlobalVariables();
			InitializeComponent();
            StepLibrary.PSteps = new List<Step>();

            this.SizeChanged += MainWindow_SizeChanged;
			this.Loaded += MainWindow_Loaded;
			this.KeyDown += MainWindow_KeyDown;
			inkCanv.BeforeStrokeGenerated += inkCanv_BeforeStrokeGenerated;
			inkCanv.OnStrokeGenerated += inkCanv_OnStrokeGenerated;
			inkCanv.OnStrokeModified += inkCanv_OnStrokeModified;
			inkCanv.customRenderer.AfterDrawn += customRenderer_AfterDrawn;
			inkCanv.OnFinalGestureGenerated += inkCanv_OnGestureGenerated;
			Touch.FrameReported += Touch_FrameReported;

            RDFWrapper.FilePtr = RDFWrapper.LoadFile(@"test.ttl");
			//paramEngine = new RDF(@"test.ttl");
			//RDFWrapper.FilePtr = RDFWrapper.LoadFile(@"d.ttl");
		}

        void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.viewport.Height = this.ActualHeight;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.curSketch = new HitSketchHelper(viewport);
            this.cursor = new Cursor3D(this.viewport, this.gplans);

            CommandLibrary.SetDefaultCommand(true);
            Library.InitializeCanvasComponents(this.viewport, this.inkCanv, this.cursor, this.group1, this.gplans, this.curSketch);
        }

		#region Sketch Canvas Events
		void inkCanv_OnGestureGenerated(GestureCommand gesture, GestureEnums gestureName)
		{
            gesture.ConstraintSolved += gesture_ConstraintSolved;
            gesture.OnObjectSelected += gesture_OnObjectSelected;
            gesture.OnObjectUnSelected += gesture_OnObjectUnSelected;

			if (GlobalVariables.CurrentMode == Mode.Sketch) 
				GlobalVariables.CurrentMode = Mode.Standard;
            // process gesture
            if ((this.curSketch.FirstHitGeom != null || curSketch.AreaHitWires.Count > 0 || curSketch.HitUtilPlanes.Count > 0))
                gestureName = gesture.processGesture(curSketch.FirstHitGeom, this.viewport, curSketch.FindAreaHitWires, null, curSketch.HitUtilPlanes, Library.RotateHitPlanes, true);
            else if (gestureName == GestureEnums.Point)
            {
                cursor.DestroyCursor();
                if (GlobalVariables.SelectedItem != null)
                    GlobalVariables.SelectedItem.UnSelect();
            }
            else
                gestureName = GestureEnums.None;
            // create the cursor at the gesture place
            if (gestureName == GestureEnums.None && curSketch.HitPoints.Count > 0)
            {
                var rotation = GeomLib.BasicGeomLib.GetAxisRotation3D(curSketch.HitNormal);
                RotateTransform3D rot3D = new RotateTransform3D(rotation);
                var mtx = rot3D.Value;
                mtx.OffsetX = curSketch.HitPoints.First().X;
                mtx.OffsetY = curSketch.HitPoints.First().Y;
                mtx.OffsetZ = curSketch.HitPoints.First().Z;
                cursor.CreateCursor();
                cursor.SetTransform(mtx);
            }

            // if it is in erasing mode, erase the item here
            if (GlobalVariables.SelectedItem != null && (gestureName == GestureEnums.Erase || GlobalVariables.IsErasing))
            {
                Step newstep = new Step();
                newstep.PStep = StepTypeEnum.Delete;
                newstep.PreModel = GlobalVariables.SelectedItem;
                StepLibrary.PSteps.Add(newstep);
                // erasing here
                var selItem = GlobalVariables.SelectedItem;
                GlobalVariables.SelectedItem.Remove();
                selItem.UnSelect();
                cursor.DestroyCursor();
                GlobalVariables.IsErasing = false;
            }

			this.curSketch.EndSketch();
		}

        #region Gesture Events
        void gesture_OnObjectUnSelected(RDF_Abstract selObj)
        {
            cursor.DestroyCursor();
        }
        void gesture_OnObjectSelected(RDF_Abstract selObj)
        {
            if (selObj != null)
            {
                cursor.DestroyCursor();
                cursor.CreateCursor();

                // set up the transformation
                MatrixTransform3D mtxTransform = new MatrixTransform3D(Matrix3D.Identity);
                // only deal with the face2D for now
                if (selObj is RDF_Face2D)
                {
                    RDF_Face2D selFace = (RDF_Face2D)selObj;
                    Point3D centroid = selFace.Outer.Centroid;
                    Matrix3D mtx = Matrix3D.Identity;
                    if (selFace.HasTransform)
                    {
                        Vector3D offsetCentroid = centroid - selFace.Outer.StartPt.GetPoint3D();
                        Point3D tmpcentroid = new Point3D(offsetCentroid.X, offsetCentroid.Y, offsetCentroid.Z);
                        mtx = selFace.Transform.GetMatrix3D();
                        centroid = mtx.Transform(tmpcentroid);
                    }
                    mtx.OffsetX = centroid.X;
                    mtx.OffsetY = centroid.Y;
                    mtx.OffsetZ = centroid.Z;
                    mtxTransform.Matrix = mtx;
                }

                cursor.SetTransform(mtxTransform);
            }
        }
        void gesture_ConstraintSolved(object unsolvedObj, object solvedObj)
        {
            Step newstep = new Step();
            newstep.PStep = StepTypeEnum.modifypath;
            newstep.ObjectType = StepObjectTypeEnum.Line;
            newstep.ModifiedObj = unsolvedObj;

            StepLibrary.PSteps.Add(newstep);
            //if (unsolvedObj is RDF_Abstract)
                gesture_OnObjectUnSelected(null);
            if (solvedObj is RDF_Abstract)
                gesture_OnObjectSelected((RDF_Abstract)solvedObj);
        }
        #endregion
        /// <summary>
		/// Deal with the realtime rendering
		/// </summary>
		/// <param name="newpt"></param>
		private void newSketchPtReceived(Point newpt)
		{
            long curtick = DateTime.Now.Ticks;
			Point3D prept = this.curSketch.SketchPoints3D.Count > 0 ? this.curSketch.SketchPoints3D.Last() : new Point3D();

            // rotate the hit plane to the best orientation for extrusion
            if (GlobalVariables.CurrentMode == Mode.Extrusion)
            {
                if(!Library.ChkIfSweptSolidAvailable())
                    Library.rotateHitPlaneTowardCameraOnZ();
            }
            else if (GlobalVariables.CurrentMode == Mode.UndoRedo && this.curSketch.SketchPoints.Count > 1)
            {
                var oriPoint = this.curSketch.SketchPoints.First();
                var curPoint = this.curSketch.SketchPoints.Last();
                int stepIndex = (int)((oriPoint.X - curPoint.X) / 20);

                // not working good, temporary remove it first
                //if (stepIndex > prestepIndex)
                //    moveBack();
                //else
                //    moveForward();

                this.prestepIndex = stepIndex;
            }
            else if (GlobalVariables.CurrentMode == Mode.Standard)
                GlobalVariables.CurrentMode = Mode.Sketch; // the default mode when creating the new sketch

			HitResultTypeEnum hittype = curSketch.AddSketchPoint(newpt);
            //System.Diagnostics.Debug.WriteLine(curtick - prePtTick);
            if (this.curSketch.SketchPoints.Count % 5 == 0 && GlobalVariables.CurrentMode != Mode.Array && this.curSketch.SketchPoints.Count < 100
                && GlobalVariables.CurrentMode != Mode.Extrusion && curtick - prePtTick < GlobalConstant.DynamicGestureThreshold) 
                DynamicGestureRecognition();

            if (!GlobalVariables.IsErasing)
            {
                #region Extrude
                if (GlobalVariables.CurrentMode == Mode.Extrusion && GlobalVariables.SelectedItem is RDF_Face2D && curSketch.HitPoints.Count > 0)
                {
                    if (CommandLibrary.SweptSolid.Direction == null)
                    {
                        moveCursor(newpt);
                        CommandLibrary.CreateExtrusionCommand();
                    }
                    else
                        CommandLibrary.ProcessExtrusionCommand();

                    CommandLibrary.SweptSolid.RenderConceptMesh(this.group1);
                    CommandLibrary.SweptSolid.RenderConnectedSweptSolid(this.group1);
                    Step pstep = new Step();
                    pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                    pstep.PreModel = CommandLibrary.SweptSolid;
                    pstep.PStep = StepTypeEnum.Add;
                    StepLibrary.PSteps[StepLibrary.PSteps.Count - 1] = pstep;
                }
                #endregion
                #region Swept Solid
                else if (GlobalVariables.CurrentMode == Mode.SweptSolid && GlobalVariables.SelectedItem is RDF_Face2D)
                {
                    /*
                    if (workingRDF != null && workingRDF is RDF_Polygon3D && curSketch.HitPoints.Count > 0)
                    {
                        Point3D curpt = curSketch.HitPoints.First();
                        if (this.curSketch.Count > 1)
                        {
                            //System.Diagnostics.Debug.WriteLine("swept solid!!");
                            RDF_Line3D newline = new RDF_Line3D(prept, curpt);
                            ((RDF_Polygon3D)workingRDF).AddPart(newline);

                            this.sweptsolid.SetDirection((RDF_Polygon3D)workingRDF);
                            this.sweptsolid.RenderConceptMesh(group1);
                            
                            Step pstep = new Step();
                            pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                            pstep.PreModel = this.sweptsolid;
                            pstep.PStep = StepTypeEnum.Add;
                            PSteps[PSteps.Count - 1] = pstep;
                        }

                    }
                    **/
                }
                #endregion
                #region Copy Arrays
                else if (GlobalVariables.CurrentMode == Mode.Array && curSketch.HitPoints.Count > 0
                    && (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_Boolean3D || GlobalVariables.SelectedItem is RDF_SweptAreaSolid))
                {
                    Constraint cst = GlobalVariables.SelectedItem.Constraint;
                    CommandLibrary.ProcessArrayCommand(cst);
                }
                #endregion
            }
            prePtTick = curtick;
        }

        private void DynamicGestureRecognition()
        {
            GestureCommand gm = new GestureCommand(this.curSketch.SketchPoints, this.inkCanv.GestureRecognizer);

            // validate the erase gesture and also mark the deletion object if found
            // Erase is a strong type gesture. Once it is detected as erase gesture, it will be erase gesture until
            // pen up
            if (gm.Gesture == GestureEnums.Erase || GlobalVariables.IsErasing)
            {
                GlobalVariables.IsErasing = true;
                List<WireBase> hitwires = curSketch.FindAreaHitWires().Item1;
                GlobalVariables.IsErasing = curSketch.FirstHitGeom != null || hitwires.Count > 0;

                if (curSketch.FirstHitGeom != null)  // mark the geometry for deletion if validated
                    gm.processGesture(curSketch.FirstHitGeom, this.viewport, curSketch.FindAreaHitWires);
                else if (hitwires.Count > 0)         // mark the wires for deletion if validated
                    gm.processGesture(curSketch.FirstHitGeom, this.viewport, null, hitwires);
            }
        }

		void customRenderer_AfterDrawn(Point newpt)
		{
			Dispatcher.BeginInvoke(new executeParametricCommand(newSketchPtReceived), new object[] { newpt });
		}

		/// <summary>
		/// prepare the command executing process
		/// convert the hit point 3D to the 2D locat coordinate system so that the curve recognizer can process
		/// </summary>
		/// <param name="inputPts"></param>
		/// <param name="convertedPts"></param>
		/// <param name="mtx">the matrix for sketch plane</param>
		/// <param name="hitobj"></param>
		void inkCanv_BeforeStrokeGenerated(List<StylusPoint> inputPts, List<Point> convertedPts, ref Matrix3D mtx, ref object hitobj)
		{
			if (this.curSketch.LastResultType != HitResultTypeEnum.HitTwoAxes && this.curSketch.SketchPoints3D.Count > 0)
			{
				// find first hit object
				if (this.curSketch.CurHitGeoms.Count > 0)
				{
					var firsthitobj = this.curSketch.CurHitGeoms.First().GetValue(RDFExtension.RDFObjectProperty);
					if (firsthitobj is RDF_GeometricItem)
						hitobj = firsthitobj;
				}

				// reverse the order for its needs
				// if the drawing is inside a polygon, we assume it's trying to make a hole (inner polygon)
				double area = inputPts.PolygonArea();

				if (area > 0) inputPts.Reverse();
                // Decide what points to use for projecting the points onto the surface
                List<Point3D> usingPts = new List<Point3D>();

                if (GlobalVariables.CurrentMode == Mode.Array)
                {
                    usingPts = this.curSketch.HitPlanePoints3D;
                    mtx = this.curSketch.HitPlane.Transform.Value;
                }
                else
                {
                    usingPts = this.curSketch.SketchPoints3D;
                    var rotation = GeomLib.BasicGeomLib.GetAxisRotation3D(this.curSketch.HitNormal);
                    RotateTransform3D rot3D = new RotateTransform3D(rotation);
                    mtx = rot3D.Value;
                }    

                Point3D first = usingPts[0];
                // Get the orientation of the sketch lines based on it's contexts
                // Todo: it is not able to create a transformation from a single sketch line
                // I will need to take the meshes orientation to get the orientation of sketch plane
                bool isMerging = false;
                
                if (area == 0 || usingPts.Count > this.curSketch.HitGeoms.Count)
                {
                    if (GlobalVariables.SelectedItem != null && GlobalVariables.SelectedItem.Transform != null)
                    {
                        var testMtx = GlobalVariables.SelectedItem.Transform.Matrix.Matrix3D * mtx;
                        testMtx.OffsetX = 0;
                        testMtx.OffsetY = 0;
                        testMtx.OffsetZ = 0;
                        if (testMtx.IsIdentity && GlobalVariables.SelectedItem is RDF_Face2D)
                            isMerging = true;
                    }
                }
                
                if (!isMerging)
                {
                    mtx.OffsetX = first.X;
                    mtx.OffsetY = first.Y;
                    mtx.OffsetZ = first.Z;
                }
				if (mtx.HasInverse)
					mtx.Invert();

				// can't translate the points to oring here since cornucopia will change the first point
				// in the segmentation process
				int i = 0;
				//Point3D stpt = new Point3D();
                foreach (Point3D pt in usingPts)
				{
					Point3D cvtPt = mtx.Transform(pt);
					//if (i == 0) stpt = cvtPt;
					//convertedPts.Add(new Point(cvtPt.X - stpt.X, cvtPt.Y - stpt.Y));
					convertedPts.Add(new Point(cvtPt.X, cvtPt.Y));
					i++;
				}

				double cvtArea = convertedPts.PolygonArea();
				if (cvtArea < 0) convertedPts.Reverse();

				// ending the sketch and clear the variables
				this.curSketch.EndSketch();

				//// add logic here to decide whether it is drawing a hole or a surface
				//// try hit first... use the first point to hit the
			}
			else
			{
				this.curSketch.EndSketch();
				GlobalVariables.CurrentMode = Mode.Standard;
			}
            GlobalVariables.IsErasing = false;
            Petzold.Media3D.Utility.IsRenderingWireBase = true;
		}

		/// <summary>
		/// Executing the commnad based on the sketch behavior we managed in the previous actions
		/// </summary>
		/// <param name="newStroke"></param>
		/// <param name="mtx"></param>
		/// <param name="hitobj"></param>
		void inkCanv_OnStrokeGenerated(CustomStroke newStroke, ref Matrix3D mtx, object hitobj)
		{
			RDF_Polygon3D polygon = new RDF_Polygon3D(newStroke.GUID);
            if (GlobalVariables.CurrentMode == Mode.Array || GlobalVariables.CurrentMode == Mode.Extrusion || GlobalVariables.CurrentMode == Mode.SweptSolid)
                polygon.IsVirtual = true;

			if (mtx.HasInverse)
				mtx.Invert();

            Matrix3D oriMtx = mtx.Clone();
			Point3D stpt = newStroke.Primitives.CreateRDFSegments(polygon);
            
			if (GlobalVariables.CurrentMode == Mode.SweptSolid)
				stpt = new Point3D(0, 0, 0);

			#region Transformation matrix for moving the polygon back to the global location
            Point3D transformedStPt = mtx.Transform(stpt);
            mtx.OffsetX = transformedStPt.X;// stpt.X;
            mtx.OffsetY = transformedStPt.Y;
            mtx.OffsetZ = transformedStPt.Z;

			RDF_Transformation rdfTran = new RDF_Transformation();
			rdfTran.SetTransformGeom(polygon);
			RDF_MatrixAbstract rdfMatrix = null;
            
			if (this.curSketch.CurHitGeoms.Count > 0)
			{
				RDF_Abstract hitrdf = (RDF_Abstract)this.curSketch.CurHitGeoms[0].GetValue(RDFExtension.RDFObjectProperty);
				if (hitrdf is RDF_SweptAreaSolid)
				{
					RDF_SweptAreaSolid hitswept = (RDF_SweptAreaSolid)hitrdf;
					if (hitswept.HasTransform)
					{
						RDF_Transformation hittransform = hitswept.Transform;
						RDF_MatrixAbstract rdf_hitmtx = (RDF_MatrixAbstract)hittransform.Matrix;
						Matrix3D hitmtx3D = rdf_hitmtx.Matrix3D;
						hitmtx3D.Invert();
						mtx.Invert();
						Matrix3D relationmtx = hitmtx3D.Solve(mtx);// mtx.Solve(hitmtx3D);//hitmtx3D.Solve(mtx);
						relationmtx.Invert();
						rdfMatrix = new RDF_Matrix(relationmtx);
						//hitmtx3D.Invert();

						//Matrix3D tmpmtx = Matrix3D.Multiply(hitmtx3D, relationmtx);
						//tmpmtx.Invert();

                        RDF_MatrixMultiplication mtxMultiply = new RDF_MatrixMultiplication(rdfMatrix, rdf_hitmtx);
						rdfTran.SetMatrix(mtxMultiply);
					}
				}
			}
			if(rdfMatrix == null)
			{
				rdfMatrix = new RDF_Matrix(mtx);
				rdfTran.SetMatrix(rdfMatrix);
			}

			#endregion

			#region Create Solid Model or Render the lines
			if (GlobalVariables.CurrentMode == Mode.SweptSolid)
			{
                /*
				sweptsolid.SetDirection(rdfTran);
				sweptsolid.RenderConceptMesh(group1);
				RDF_Dim1.RenderConceptLine(rdfTran, viewport);
				
				Step pstep = new Step();
				pstep.ObjectType = StepObjectTypeEnum.MeshModel;
				pstep.PreModel = sweptsolid;
				pstep.PStep = StepTypeEnum.Add;
				PSteps[PSteps.Count - 1] = pstep;
                */
			}
			// add a hole
			else if (hitobj is RDF_Face2D && polygon.IsClosed)
			{
				RDF_Face2D hitface = (RDF_Face2D)hitobj;
				if (hitface.Geometry != null)
					this.group1.Children.Remove(((RDF_Face2D)hitobj).Geometry);
                Matrix3D solvedMtx = hitface.Transform.GetMatrix3D().Solve(oriMtx);
                var strokeMtx3D = newStroke.matrix.Clone();
                strokeMtx3D.OffsetX = 0;
                strokeMtx3D.OffsetY = 0;
                strokeMtx3D.OffsetZ = 0;
                Point3D translateStroke = strokeMtx3D.Transform(new Point3D(solvedMtx.OffsetX, solvedMtx.OffsetY, solvedMtx.OffsetZ));
                RDF_Polygon3D innerPL = new RDF_Polygon3D();

                newStroke.TranslateStroke(translateStroke);
                newStroke.Primitives.CreateRDFSegments(innerPL, true);
                newStroke.GUID = innerPL.GUID;

                RDF_Transformation innerTransform = new RDF_Transformation();
                innerTransform.SetTransformGeom(innerPL);
                innerTransform.SetMatrix(hitface.Transform.Matrix);

                RDF_Dim1.RenderConceptLine(innerPL.Transform, viewport, true);
                Step pstep = addPstepLine(innerPL);

                hitface.AddInnerPolygon(innerPL);
                hitface.RenderShapeMesh(pstep, group1);
			}
			else if (GlobalVariables.CurrentMode == Mode.Extrusion)
			{
				// doesn't need to do anything for now
			}
			// add a line (and a surface)
			else
			{
				if (GlobalVariables.SelectedItem != null)
					GlobalVariables.SelectedItem.UnSelect();
				WirePath line = RDF_Dim1.RenderConceptLine(rdfTran, viewport);

                if (line != null)
                {
                    // trasform the points to XY plane so that we can set the 
                    // new points in the stroke
                    newStroke.UpdateCvtPoints(line, mtx, polygon.IsClosed);
                }
                // Todo: Check whether this is the right implementation!!
				Step pstep = addPstepLine(rdfTran.TransformGeom);
				if (polygon.IsClosed && line != null && !(GlobalVariables.CurrentMode == Mode.Array || GlobalVariables.CurrentMode == Mode.Extrusion))
                {
					RDF_Face2D face = new RDF_Face2D(polygon);
					pstep.ObjectType = pstep.ObjectType | StepObjectTypeEnum.MeshModel;
					face.RenderShapeMesh(pstep, group1);
					face.Select();
                    gesture_OnObjectSelected(face);
				}
				else
				{
					polygon.Select();
				}
			}
			#endregion

			setToDefault(false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="oriStroke"></param>
		/// <param name="mtx">the new stroke matrix</param>
		/// <param name="hitobj"></param>
		void inkCanv_OnStrokeModified(CustomStroke oriStroke, ref Matrix3D mtx, object hitobj)
		{
			// find the original RDF object
            if (RDFWrapper.All_RDF_Objects.ContainsKey(oriStroke.GUID))
            {
                RDF_Abstract existingObject = RDFWrapper.All_RDF_Objects[oriStroke.GUID];

                if (existingObject != null && existingObject is RDF_Polygon3D)
                {
                    RDF_Polygon3D oriPolygon = (RDF_Polygon3D)existingObject;
                    // clean the old relations of the polygon
                    //rdfPolygon.CleanAllParts();

                    RDF_Polygon3D newPolygon = new RDF_Polygon3D();
                    oriStroke.GUID = newPolygon.GUID;

                    if (mtx.HasInverse)
                        mtx.Invert();

                    // add the new parts into the polygon
                    Point3D stpt = oriStroke.Primitives.CreateRDFSegments(newPolygon);
                    // update the matrix due to the shifts made by cornucopia
                    mtx.OffsetX += stpt.X;
                    mtx.OffsetY += stpt.Y;
                    mtx.OffsetZ += stpt.Z;

                    #region Transformation matrix for moving the polygon back to the global location
                    //      Since the location will be moved to the origin
                    if (oriPolygon.HasTransform)
                    {
                        //stpt = rdfPolygon.Transform.Matrix.Matrix3D.Transform(stpt);
                        RDF_Transformation rdftransform = oriPolygon.Transform;
                        RDF_Transformation newTransform = new RDF_Transformation();

                        newTransform.SetTransformGeom(newPolygon);
                        if (rdftransform.Matrix is RDF_Matrix)
                        {
                            RDF_Matrix rdfMatrix = (RDF_Matrix)rdftransform.Matrix.Clone();
                            rdfMatrix.SetTranslate(new Point3D(mtx.OffsetX, mtx.OffsetY, mtx.OffsetZ));
                            newTransform.SetMatrix(rdfMatrix);
                        }
                        else if (rdftransform.Matrix is RDF_MatrixInverse)
                        {
                            RDF_MatrixInverse rdfinverseMatrix = (RDF_MatrixInverse)rdftransform.Matrix;
                        }
                        // remove the original polygon from the matrix
                        //rdfMatrix.RelatedTransforms.Remove(oriPolygon.Transform);
                    }
                    #endregion

                    // render the polygon.. all the polygons will associate to a transform at this point
                    if (newPolygon.HasTransform)
                    {
                        //WirePath line = rdfPolygon.RenderConceptLine(viewport);//RDF_Dim1.RenderConceptLine(rdfPolygon.Transform, viewport);
                        WirePath line = RDF_Dim1.RenderConceptLine(newPolygon.Transform, viewport);
                        if (line != null)
                            oriStroke.UpdateCvtPoints(line, mtx, newPolygon.IsClosed);

                        Step pstep = modifyPstepLine(oriPolygon, newPolygon);

                        bool hasface = false;
                        newPolygon.BackRelations.Clear();
                        // render the models depends on what kind of geometry connecting to the polygon
                        foreach (var br in oriPolygon.BackRelations)
                        {
                            var cbr = br.Clone();
                            switch (br.Name)
                            {
                                case BackRelationName.Face_Outer:
                                    RDF_Face2D oriface = (RDF_Face2D)br.RelatedTo;
                                    //oriface.SetOuterPolygon(newPolygon);
                                    this.group1.Children.Remove(oriface.Geometry);

                                    // clean some existing objects store in the reference list
                                    //RDFWrapper.All_RDF_Objects.Remove(oriface.GUID);
                                    //if (oriface.HasTransform)
                                    //{
                                    //    oriface.Transform.Matrix.RelatedTransforms.Remove(oriface.Transform);
                                    //}

                                    RDF_Face2D newface = new RDF_Face2D(newPolygon);
                                    newface.RenderShapeMesh(pstep, group1);
                                    cbr.RelatedTo = newface;
                                    newface.Select();
                                    hasface = true;
                                    //newPolygon.BackRelations.Add(cbr);            
                                    break;
                                case BackRelationName.SweptSolid_Profile_Outer:
                                    RDF_SweptAreaSolid oriSolid = (RDF_SweptAreaSolid)br.RelatedTo;
                                    //oriSolid.Remove();    
                                    this.group1.Children.Remove(oriSolid.Geometry);

                                    RDF_SweptAreaSolid sweptsolid = new RDF_SweptAreaSolid();
                                    //if(RDFWrapper.All_RDF_Objects.ContainsKey(sweptsolid.GUID))
                                    {
                                        sweptsolid.SetSweptProfile(newPolygon);
                                        sweptsolid.SetDirection(oriSolid.Direction);

                                        RDF_Transformation solidTransform = new RDF_Transformation();
                                        solidTransform.SetMatrix(newPolygon.Transform.Matrix);
                                        solidTransform.SetTransformGeom(sweptsolid);

                                        sweptsolid.RenderConceptMesh(this.group1);
                                        pstep.PreModel = sweptsolid;
                                        StepLibrary.PSteps.Add(pstep);
                                        StepLibrary.NSteps = new List<Step>();
                                    }
                                    newPolygon.BackRelations.Add(cbr);
                                    break;
                            }
                        }

                        // try to make a face if it's not closed originally
                        newPolygon.CheckIfClosed();
                        if (newPolygon.IsClosed && line != null && !hasface)
                        {
                            RDF_Face2D face = new RDF_Face2D(newPolygon);
                            pstep.ObjectType = pstep.ObjectType | StepObjectTypeEnum.MeshModel;
                            face.RenderShapeMesh(pstep, group1);
                            face.Select();
                        }
                        else
                            newPolygon.Select();
                        oriPolygon.Remove();
                    }

                    setToDefault(false);
                }
            }
		}

		#endregion

		#region Stylus Event
        protected override void OnStylusButtonDown(StylusButtonEventArgs e)
        {
            if (e.StylusButton.Name.ToLower().Contains("barrel"))
            {
                this.isBarrelBtDown = true;
            }
            else
            {
                if (e.StylusButton.Name.ToLower().Contains("tip") && this.isBarrelBtDown 
                    && GlobalVariables.SelectedItem != null)
                {
                    //GlobalVariables.PreSelectedItem.Select();
                    switch (btstates)
                    {
                        case ButtonEnum.Extrusion:
                            if (GlobalVariables.SelectedItem is RDF_Face2D)
                                GlobalVariables.CurrentMode = Mode.Extrusion;
                            break;
                        case ButtonEnum.Array:
                            GlobalVariables.CurrentMode = Mode.Array;
                            break;
                    }

                    if (GlobalVariables.CurrentMode == Mode.Array || GlobalVariables.CurrentMode == Mode.Extrusion)
                    {
                        Step pstep = new Step();
                        pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                        pstep.PStep = StepTypeEnum.Add;
                        StepLibrary.PSteps.Add(pstep);
                        StepLibrary.NSteps = new List<Step>();
                    }
                }
            }
            base.OnStylusButtonDown(e);
        }
        protected override void OnStylusButtonUp(StylusButtonEventArgs e)
        {
            if (e.StylusButton.Name.ToLower().Contains("barrel"))
            {
                this.isBarrelBtDown = false;
                GlobalVariables.CurrentMode = Mode.Standard;
            }
            base.OnStylusButtonUp(e);
        }
		protected override void OnStylusInRange(StylusEventArgs e)
		{
			if (e.StylusDevice.TabletDevice.Name == "ISD-V4" || (e.StylusDevice.TabletDevice.Name == "HIDI2C Device" && e.StylusDevice.TabletDevice.Type != TabletDeviceType.Touch))
			{
                //System.Diagnostics.Debug.WriteLine("tip switch" + e.StylusDevice.StylusButtons[0].StylusButtonState);
                //System.Diagnostics.Debug.WriteLine("barrel" + e.StylusDevice.StylusButtons[1].StylusButtonState);

                // when stylus is in range, it will cause a touch up event fired
				long diffTime = DateTime.Now.Ticks - preTouchUp;
				if (diffTime < 500000)
				{
                    if (GlobalVariables.PreSelectedItem is RDF_Face2D)
                    {
                        GlobalVariables.PreSelectedItem.Select();
                        GlobalVariables.CurrentMode = Mode.Extrusion;

                        Step pstep = new Step();
                        pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                        pstep.PStep = StepTypeEnum.Add;
                        StepLibrary.PSteps.Add(pstep);
                        StepLibrary.NSteps = new List<Step>();
                    }
                    else if (GlobalVariables.PreSelectedItem == null)
                    {
                        GlobalVariables.CurrentMode = Mode.UndoRedo;

                    }
				}

				base.OnStylusInRange(e);
				this.chkAllowSketch.IsChecked = true;
				this.inkCanv.IsHitTestVisible = true;
				Utility.UIUtility.IsStylusInRange = true;
			}
		}
		protected override void OnStylusOutOfRange(StylusEventArgs e)
		{
            if (e.StylusDevice.TabletDevice.Name == "ISD-V4" || (e.StylusDevice.TabletDevice.Name == "HIDI2C Device" && e.StylusDevice.TabletDevice.Type != TabletDeviceType.Touch))
			{
				base.OnStylusOutOfRange(e);
				this.chkAllowSketch.IsChecked = false;
				this.inkCanv.IsHitTestVisible = false;
				Utility.UIUtility.IsStylusInRange = false;
			}
		}
		protected override void OnStylusInAirMove(StylusEventArgs e)
		{
			base.OnStylusInAirMove(e);

			if (stylusInAirCount % Utility.GlobalConstant.DrawFrequency == 0)
			{
				if (Utility.UIUtility.IsStylusInRange && Utility.GlobalVariables.CurrentMode == Mode.Standard)
				{
					Point pos = e.GetPosition(viewport);
					//Dispatcher.BeginInvoke(new executeParametricCommand(moveCursor), new object[] { pos });
					moveCursor(pos);
				}
				stylusInAirCount = 0;
			}

			stylusInAirCount++;
		}
		#endregion

		#region Touch Event

		protected override void OnTouchDown(TouchEventArgs e)
		{
			base.OnTouchDown(e);
            
			TouchPoint mouseposition = e.GetTouchPoint(viewport);
			HitResultTypeEnum resulttype = this.curSketch.Touch(mouseposition.Position);
			//this.touchcount++;
            if (this.curSketch.HitUtilities.Count > 0)
            {
                var hitutil = this.curSketch.HitUtilities.First();
                if (hitutil.ID == "cxaxis" || hitutil.ID == "cyaxis" || hitutil.ID == "czaxis")
                {
                    //System.Diagnostics.Debug.WriteLine("hit utility");
                    GlobalVariables.CurrentMode = Mode.MoveShape | Mode.MoveShapeAxis;
                    Library.SetFirstTouchTown(mouseposition.TouchDevice.Id);
                    if (hitutil.ID == "czaxis")
                        Library.rotateHitPlaneTowardCameraOnZ();
                    switch (hitutil.ID)
                    {
                        case "cxaxis":
                            cursor.CXAxis.Color = Colors.Pink;
                            cursor.CXAxis.Thickness = 6;
                            cursor.CXAxis.Point2 = new Point3D(30, 0, 0);
                            Library.TouchOn = MovingAxis.xaxis;
                            break;
                        case "cyaxis":
                            cursor.CYAxis.Color = Colors.LightGreen;
                            cursor.CYAxis.Thickness = 6;
                            cursor.CYAxis.Point2 = new Point3D(0, 30, 0);
                            Library.TouchOn = MovingAxis.yaxis;
                            break;
                        case "czaxis":
                            cursor.CZAxis.Color = Colors.LightBlue;
                            cursor.CZAxis.Thickness = 6;
                            cursor.CZAxis.Point2 = new Point3D(0, 0, 30);
                            Library.TouchOn = MovingAxis.zaxis;
                            break;
                    }
                    // try to hit again to hit on the plane
                    this.curSketch.Touch(mouseposition.Position);
                }
            }
            else if (resulttype == HitResultTypeEnum.HitGeometry)
			{
				GlobalVariables.CurrentMode = Mode.MoveShape;
                Library.TouchOn = MovingAxis.xyplane;
				var rdfobj = this.curSketch.CurHitGeoms.First().GetValue(RDFExtension.RDFObjectProperty);
				if (rdfobj is RDF_GeometricItem)
				{
					var rdfGeom = (RDF_GeometricItem)rdfobj;
                    if (GlobalVariables.SelectedItem != rdfGeom)
                    {
                        cursor.CreateCursor();
                        rdfGeom.Select();
                    }
                    if (GlobalVariables.SelectedItem.Transform != null)
                    {
                        RDF_MatrixAbstract mtxabstract = GlobalVariables.SelectedItem.Transform.Matrix;
                        this.oriShapeOffset = new Point3D(mtxabstract.Matrix3D.OffsetX, mtxabstract.Matrix3D.OffsetY, mtxabstract.Matrix3D.OffsetZ);
                    }
                    else
                        this.oriShapeOffset = new Point3D();
				}
            }
            else if(GlobalVariables.SelectedItem != null)
            {
                GlobalVariables.SelectedItem.UnSelect();
                cursor.DestroyCursor();
            }

		}
		protected override void OnTouchMove(TouchEventArgs e)
		{
			base.OnTouchMove(e);
            // move the objects that has been selected
            // by offseting the matrix
            Library.MoveShape(e);
		} 
		protected override void OnTouchUp(TouchEventArgs e)
		{
			base.OnTouchUp(e);
			preTouchUp = DateTime.Now.Ticks;
			GlobalVariables.CurrentMoveDeviceID = null;
			//this.touchcount--;
            if ((GlobalVariables.CurrentMode & Mode.MoveShape) != 0)
			{
                cursor.setDefaultContextAxis();
                setToDefault(false);
                Library.MoveShape(e);

                if (this.oriShapeOffset.X != double.MaxValue)
                {
                    Step pstep = new Step();
                    pstep.ObjectType = StepObjectTypeEnum.Line | StepObjectTypeEnum.MeshModel;
                    pstep.PStep = StepTypeEnum.ObjectTransform;
                    pstep.ObjectTranslation = this.oriShapeOffset;
                    pstep.PreModel = GlobalVariables.SelectedItem;
                    StepLibrary.PSteps.Add(pstep);

                    //System.Diagnostics.Debug.WriteLine("object transform");
                    StepLibrary.NSteps = new List<Step>();
                }
                
                //if (GlobalVariables.SelectedItem != null)
                //{
                //    GlobalVariables.PreSelectedItem = GlobalVariables.SelectedItem; 
                //    GlobalVariables.SelectedItem.UnSelect();
                //}
			}
		}

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
		{
			TouchPointCollection tpts = e.GetTouchPoints(this);
			Library.TouchCount = tpts.Count;
            if (tpts.Count == 2)
            {
                Library.RotateShape(tpts);
            }
		}
		#endregion

		#region Mouse Keyboard Events
		private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			camera.Position = new Point3D(camera.Position.X, camera.Position.Y, camera.Position.Z - e.Delta / 250D);
		}
		private void CursorMove(object sender, MouseEventArgs e)
		{
			if (Utility.GlobalVariables.CurrentMode == Mode.Standard)
			{
				Point mouseposition = e.GetPosition(viewport);
				// cause some jaggy problem in profile
                moveCursor(mouseposition);
			}
		}
		private void MainWindow_KeyDown(object sender, KeyEventArgs e)
		{
			TranslateTransform3D translate = new TranslateTransform3D(0, 0, 0);
			Transform3DGroup transform = new Transform3DGroup();
			switch (e.Key)
			{
				case Key.S:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        RDFWrapper.SaveAsFile();
                    }
                    else
                    {
                        this.chkAllowSketch.IsChecked = !this.chkAllowSketch.IsChecked;
                        this.inkCanv.IsHitTestVisible = (bool)chkAllowSketch.IsChecked;
                    }
                    break;
				case Key.O:
					break;
				case Key.X:
					RotateTransform3D transformXY = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), 0));
					transform.Children.Add(transformXY);
					transform.Children.Add(translate);
					this.curSketch.HitPlane.Transform = transform;
					break;
				case Key.Y:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        moveForward();
                    }
                    else
                    {
                        RotateTransform3D transformYZ = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), 90));
                        transform.Children.Add(transformYZ);
                        transform.Children.Add(translate);
                        this.curSketch.HitPlane.Transform = transform;
                    }
					break;
				case Key.Z:
					// Undo!!
					if (Keyboard.IsKeyDown(Key.LeftCtrl))
					{
						moveBack();
					}
					else
					{
						RotateTransform3D transformXZ = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), 90));
						transform.Children.Add(transformXZ);
						transform.Children.Add(translate);
						this.curSketch.HitPlane.Transform = transform;
					}
					break;
				case Key.E:
					isSweptSolid = !isSweptSolid;
					this.chkSwpetSolid.IsChecked = isSweptSolid;
					break;
				case Key.H:

					break;
				case Key.G:
					inkCanv.SaveLastStrokeAsGesture();
					break;
			}
		}
		#endregion

		#region UI Event
		private void chkAllowSketch_Click_1(object sender, RoutedEventArgs e)
		{
			CheckBox chkAllowSketch = (CheckBox)sender;
			this.inkCanv.IsHitTestVisible = (bool)chkAllowSketch.IsChecked;
		}
		private void chkSweptSolid(object sender, RoutedEventArgs e)
		{
			CheckBox chkbox = (CheckBox)sender;
			this.isSweptSolid = (bool)chkbox.IsChecked;
		}
        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rdoMode = (RadioButton)sender;
            switch (rdoMode.Content.ToString())
            {
                case "Extrusion":
                    btstates = ButtonEnum.Extrusion;
                    break;
                case "Array":
                    btstates = ButtonEnum.Array;
                    break;
            }
        }
		#endregion
		/// <summary>
		/// move the axes cursor to the location where we need
		/// Todo: may need to create a cursor class to do it
		/// </summary>
		/// <param name="pos"></param>
		private void moveCursor(Point pos)
		{
            this.cursor.UnHighlightAllElements();
            //this.cursor.DestroyCursor();

			// create hit point
			HitResultTypeEnum resulttype = this.curSketch.Hover(pos);
            if (resulttype != HitResultTypeEnum.None && (GlobalVariables.CurrentMode == Mode.Standard || GlobalVariables.CurrentMode == Mode.Extrusion))
			{
				if (this.curSketch.HitPoints.Count > 0)
				{
                    //tartranslate.OffsetX = this.curSketch.HitPoints.First().X;
                    //tartranslate.OffsetY = this.curSketch.HitPoints.First().Y;
                    //tartranslate.OffsetZ = this.curSketch.HitPoints.First().Z;
                    GlobalVariables.CursorPosition = new Point3D( this.curSketch.HitPoints.First().X,
                        this.curSketch.HitPoints.First().Y, this.curSketch.HitPoints.First().Z);
                }

				// I can filter out the hitgeoms here to match the one I want
				//System.Diagnostics.Debug.WriteLine("hit count: " + hitgeoms.Count);
				this.curSketch.EndSketch();
            }
		}
		private Step addPstepLine(RDF_GeometricItem line)
		{
			if (line != null)
			{
				Step pstep = new Step();
				pstep.ObjectType = StepObjectTypeEnum.Line;
				pstep.PreModel= line;
				pstep.PStep = StepTypeEnum.Add;
                StepLibrary.PSteps.Add(pstep);
                StepLibrary.NSteps = new List<Step>();
				return pstep;
			}
			return new Step();
		}
        private Step modifyPstepLine(RDF_GeometricItem oldline, RDF_GeometricItem newline)
        {
            if (newline != null && oldline != null)
            {
                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.Line;
                pstep.PreModel = newline;
                pstep.ModifiedObj = oldline;
                pstep.PStep = StepTypeEnum.modifypath;
                StepLibrary.PSteps.Add(pstep);
                StepLibrary.NSteps = new List<Step>();
                return pstep;
            }
            return new Step();
        }
        private void moveForward()
        {
            if (StepLibrary.NSteps != null && StepLibrary.NSteps.Count > 0)
            {
                Step laststep = StepLibrary.NSteps.Last();

                StepLibrary.moveToStep(laststep, true);

                StepLibrary.NSteps.Remove(laststep);
                StepLibrary.PSteps.Add(laststep);
            }
        }
		private void moveBack()
		{
            if (StepLibrary.PSteps.Count > 0)
			{
                Step laststep = StepLibrary.PSteps.Last();

                StepLibrary.moveToStep(laststep);

                StepLibrary.PSteps.Remove(laststep);
                StepLibrary.NSteps.Add(laststep);
			}
		}

		/// <summary>
		/// set the mode back to standard and rotate the hitplane back with the gridlines
		///  the command ends when user finish a stroke.
		/// </summary>
		private void setToDefault(bool resetSelection = true)
		{
            CommandLibrary.SetDefaultCommand();
			if (GlobalVariables.CurrentMode != Mode.Standard)
			{
				this.curSketch.EndSketch();
				this.curSketch.HitPlane.Transform = Library.Gridlines.Transform;
                Library.TouchOn = MovingAxis.none;
				GlobalVariables.CurrentMode = Mode.Standard;
                if (GlobalVariables.SelectedItem != null && resetSelection)
					GlobalVariables.SelectedItem.UnSelect();
			}
            prePtTick = long.MaxValue;
		}

		private delegate void executeParametricCommand(Point newpt);

		private bool isSweptSolid = false;
		private RDF_Abstract workingRDF;
        private Cursor3D cursor;
		
		private HitSketchHelper curSketch;

		private int stylusInAirCount = 0;
		private object stylusInAirLock = new object();

        private Point3D oriShapeOffset = new Point3D(double.MaxValue, double.MaxValue, double.MaxValue);
        private int prestepIndex;

        public bool isBarrelBtDown { get; set; }

        private ButtonEnum btstates = ButtonEnum.Extrusion;
        private long prePtTick = long.MaxValue;
        private long preTouchUp;
    }
}
