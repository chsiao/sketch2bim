﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using Petzold.Media3D;
using Utility;

namespace Wpf3DTest
{
    public class Cursor3D
    {
        private WireLine chxaxis = new WireLine();
        private WireLine chyaxis = new WireLine();
        private WireLine chzaxis = new WireLine();
        private WireLine cxaxis = new WireLine();
        private WireLine cyaxis = new WireLine();
        private WireLine czaxis = new WireLine();
        private GeometryModel3D cPlaneXY = new GeometryModel3D();
        private GeometryModel3D cPlaneXZ = new GeometryModel3D();
        private GeometryModel3D cPlaneYZ = new GeometryModel3D();
        private GeometryModel3D cOriPlaneXY;
        private GeometryModel3D cOriPlaneXZ;
        private GeometryModel3D cOriPlaneYZ;

        private Model3DGroup cplaneGroup;
        private Viewport3D container;
        private SolidColorBrush planecolor;

        public Cursor3D(Viewport3D viewport, Model3DGroup planegroup)
        {
            this.container = viewport;
            this.cplaneGroup = planegroup;
            Color transparent = Colors.AliceBlue;
            transparent.A = 150;
            planecolor = new SolidColorBrush(transparent);
            CreateOriginCursorPlane();
        }
        public void CreateCursor()
        {
            if (!this.container.Children.Contains(chxaxis))
            {
                chxaxis = new WireLine();
                chxaxis.ID = "cxaxis";
                chxaxis.WireType = WireTypeEnum.HitAxis;
                chxaxis.Opacity = 0;
                chxaxis.Color = Colors.Red;
                chxaxis.Thickness = 12;
                chyaxis = new WireLine();
                chyaxis.ID = "cyaxis";
                chyaxis.WireType = WireTypeEnum.HitAxis;
                chyaxis.Opacity = 0;
                chyaxis.Color = Colors.Green;
                chyaxis.Thickness = 12;
                chzaxis = new WireLine();
                chzaxis.ID = "czaxis";
                chzaxis.WireType = WireTypeEnum.HitAxis;
                chzaxis.Opacity = 0;
                chzaxis.Color = Colors.Blue;
                chzaxis.Thickness = 12;


                chxaxis.Point1 = new Point3D(0, 0, 0);
                chxaxis.Point2 = new Point3D(15, 0, 0);
                chyaxis.Point1 = new Point3D(0, 0, 0);
                chyaxis.Point2 = new Point3D(0, 15, 0);
                chzaxis.Point1 = new Point3D(0, 0, 0);
                chzaxis.Point2 = new Point3D(0, 0, 15);

                this.container.Children.Add(chxaxis);
                this.container.Children.Add(chyaxis);
                this.container.Children.Add(chzaxis);
            }

            if (!this.container.Children.Contains(cxaxis))
            {
                cxaxis = new WireLine();
                cxaxis.ID = "cmxaxis";
                cxaxis.WireType = WireTypeEnum.HitAxis;
                cyaxis = new WireLine();
                cyaxis.ID = "cmyaxis";
                cyaxis.WireType = WireTypeEnum.HitAxis;
                czaxis = new WireLine();
                czaxis.ID = "cmzaxis";
                czaxis.WireType = WireTypeEnum.HitAxis;

                setDefaultContextAxis();

                this.container.Children.Insert(0, czaxis);
                this.container.Children.Insert(0, cyaxis);
                this.container.Children.Insert(0, cxaxis);
            }
            CreateCursorPlanes();
        }
        public void setDefaultContextAxis()
        {
            cxaxis.Color = Colors.Red;
            cxaxis.Thickness = 3;
            cyaxis.Color = Colors.Green;
            cyaxis.Thickness = 3;
            czaxis.Color = Colors.Blue;
            czaxis.Thickness = 3;

            cxaxis.Point1 = new Point3D(0, 0, 0);
            cxaxis.Point2 = new Point3D(15, 0, 0);
            cyaxis.Point1 = new Point3D(0, 0, 0);
            cyaxis.Point2 = new Point3D(0, 15, 0);
            czaxis.Point1 = new Point3D(0, 0, 0);
            czaxis.Point2 = new Point3D(0, 0, 15);
        }
        private void CreateCursorPlanes()
        {
            if (!this.cplaneGroup.Children.Contains(cPlaneXY))
            {
                MeshGeometry3D planeXY = new MeshGeometry3D();
                planeXY.Positions.Add(new Point3D(0, 0, 1));
                planeXY.Positions.Add(new Point3D(8, 0, 1));
                planeXY.Positions.Add(new Point3D(8, 8, 1));
                planeXY.Positions.Add(new Point3D(0, 8, 1));
                planeXY.TriangleIndices.Add(0);
                planeXY.TriangleIndices.Add(1);
                planeXY.TriangleIndices.Add(2);
                planeXY.TriangleIndices.Add(2);
                planeXY.TriangleIndices.Add(3);
                planeXY.TriangleIndices.Add(0);

                cPlaneXY = new GeometryModel3D(planeXY, new DiffuseMaterial(planecolor));
                cPlaneXY.SetValue(GeomLib.Extensions.IDProperty, "cxyplane");
                cPlaneXY.SetValue(GeomLib.Extensions.AxisProperty1, cxaxis);
                cPlaneXY.SetValue(GeomLib.Extensions.AxisProperty2, cyaxis);
                cPlaneXY.BackMaterial = new DiffuseMaterial(planecolor);

                cplaneGroup.Children.Add(cPlaneXY);

                MeshGeometry3D planeYZ = new MeshGeometry3D();
                planeYZ.Positions.Add(new Point3D(0, 0, 0));
                planeYZ.Positions.Add(new Point3D(0, 8, 0));
                planeYZ.Positions.Add(new Point3D(0, 8, 8));
                planeYZ.Positions.Add(new Point3D(0, 0, 8));
                planeYZ.TriangleIndices.Add(0);
                planeYZ.TriangleIndices.Add(1);
                planeYZ.TriangleIndices.Add(2);
                planeYZ.TriangleIndices.Add(2);
                planeYZ.TriangleIndices.Add(3);
                planeYZ.TriangleIndices.Add(0);

                cPlaneYZ = new GeometryModel3D(planeYZ, new DiffuseMaterial(planecolor));
                cPlaneYZ.SetValue(GeomLib.Extensions.IDProperty, "cyzplane");
                cPlaneYZ.SetValue(GeomLib.Extensions.AxisProperty1, cyaxis);
                cPlaneYZ.SetValue(GeomLib.Extensions.AxisProperty2, czaxis);
                cPlaneYZ.BackMaterial = new DiffuseMaterial(planecolor);

                cplaneGroup.Children.Add(cPlaneYZ);

                MeshGeometry3D planeXZ = new MeshGeometry3D();
                planeXZ.Positions.Add(new Point3D(0, 0, 0));
                planeXZ.Positions.Add(new Point3D(8, 0, 0));
                planeXZ.Positions.Add(new Point3D(8, 0, 8));
                planeXZ.Positions.Add(new Point3D(0, 0, 8));
                planeXZ.TriangleIndices.Add(0);
                planeXZ.TriangleIndices.Add(1);
                planeXZ.TriangleIndices.Add(2);
                planeXZ.TriangleIndices.Add(2);
                planeXZ.TriangleIndices.Add(3);
                planeXZ.TriangleIndices.Add(0);

                cPlaneXZ = new GeometryModel3D(planeXZ, new DiffuseMaterial(planecolor));
                cPlaneXZ.SetValue(GeomLib.Extensions.IDProperty, "cxzplane");
                cPlaneXZ.SetValue(GeomLib.Extensions.AxisProperty1, cxaxis);
                cPlaneXZ.SetValue(GeomLib.Extensions.AxisProperty2, czaxis);
                cPlaneXZ.BackMaterial = new DiffuseMaterial(planecolor);

                cplaneGroup.Children.Add(cPlaneXZ);
            }
        }
        public void CreateOriginCursorPlane()
        {
            WireLine xaxis = new WireLine();
            xaxis.ID = "cmxaxis";
            xaxis.WireType = WireTypeEnum.HitAxis;
            cxaxis.Opacity = 0;
            xaxis.Color = Colors.Red;
            xaxis.Thickness = 3;
            WireLine yaxis = new WireLine();
            yaxis.ID = "cmyaxis";
            yaxis.WireType = WireTypeEnum.HitAxis;
            cyaxis.Opacity = 0;
            yaxis.Color = Colors.Green;
            yaxis.Thickness = 3;
            WireLine zaxis = new WireLine();
            zaxis.ID = "cmzaxis";
            zaxis.WireType = WireTypeEnum.HitAxis;
            czaxis.Opacity = 0;
            zaxis.Color = Colors.Blue;
            zaxis.Thickness = 3;

            xaxis.Point1 = new Point3D(0, 0, 0);
            xaxis.Point2 = new Point3D(15, 0, 0);
            yaxis.Point1 = new Point3D(0, 0, 0);
            yaxis.Point2 = new Point3D(0, 15, 0);
            zaxis.Point1 = new Point3D(0, 0, 0);
            zaxis.Point2 = new Point3D(0, 0, 15);


            MeshGeometry3D oriplaneXY = new MeshGeometry3D();
            oriplaneXY.Positions.Add(new Point3D(-8, -8, 1));
            oriplaneXY.Positions.Add(new Point3D(8, -8, 1));
            oriplaneXY.Positions.Add(new Point3D(8, 8, 1));
            oriplaneXY.Positions.Add(new Point3D(-8, 8, 1));
            oriplaneXY.TriangleIndices.Add(0);
            oriplaneXY.TriangleIndices.Add(1);
            oriplaneXY.TriangleIndices.Add(2);
            oriplaneXY.TriangleIndices.Add(2);
            oriplaneXY.TriangleIndices.Add(3);
            oriplaneXY.TriangleIndices.Add(0);

            cOriPlaneXY = new GeometryModel3D(oriplaneXY, new DiffuseMaterial(planecolor));
            cOriPlaneXY.SetValue(GeomLib.Extensions.IDProperty, "orixyplane");
            cOriPlaneXY.SetValue(GeomLib.Extensions.AxisProperty1, xaxis);
            cOriPlaneXY.SetValue(GeomLib.Extensions.AxisProperty2, yaxis);
            cOriPlaneXY.BackMaterial = new DiffuseMaterial(planecolor);

            cplaneGroup.Children.Add(cOriPlaneXY);

            MeshGeometry3D oriplaneYZ = new MeshGeometry3D();
            oriplaneYZ.Positions.Add(new Point3D(0, -8, -8));
            oriplaneYZ.Positions.Add(new Point3D(0, 8, -8));
            oriplaneYZ.Positions.Add(new Point3D(0, 8, 8));
            oriplaneYZ.Positions.Add(new Point3D(0, -8, 8));
            oriplaneYZ.TriangleIndices.Add(0);
            oriplaneYZ.TriangleIndices.Add(1);
            oriplaneYZ.TriangleIndices.Add(2);
            oriplaneYZ.TriangleIndices.Add(2);
            oriplaneYZ.TriangleIndices.Add(3);
            oriplaneYZ.TriangleIndices.Add(0);

            cOriPlaneYZ = new GeometryModel3D(oriplaneYZ, new DiffuseMaterial(planecolor));
            cOriPlaneYZ.SetValue(GeomLib.Extensions.IDProperty, "oriyzplane");
            cOriPlaneYZ.SetValue(GeomLib.Extensions.AxisProperty1, yaxis);
            cOriPlaneYZ.SetValue(GeomLib.Extensions.AxisProperty2, zaxis);
            cOriPlaneYZ.BackMaterial = new DiffuseMaterial(planecolor);

            cplaneGroup.Children.Add(cOriPlaneYZ);

            MeshGeometry3D oriplaneXZ = new MeshGeometry3D();
            oriplaneXZ.Positions.Add(new Point3D(-8, 0, -8));
            oriplaneXZ.Positions.Add(new Point3D(8, 0, -8));
            oriplaneXZ.Positions.Add(new Point3D(8, 0, 8));
            oriplaneXZ.Positions.Add(new Point3D(-8, 0, 8));
            oriplaneXZ.TriangleIndices.Add(0);
            oriplaneXZ.TriangleIndices.Add(1);
            oriplaneXZ.TriangleIndices.Add(2);
            oriplaneXZ.TriangleIndices.Add(2);
            oriplaneXZ.TriangleIndices.Add(3);
            oriplaneXZ.TriangleIndices.Add(0);

            cOriPlaneXZ = new GeometryModel3D(oriplaneXZ, new DiffuseMaterial(planecolor));
            cOriPlaneXZ.SetValue(GeomLib.Extensions.IDProperty, "orixzplane");
            cOriPlaneXZ.SetValue(GeomLib.Extensions.AxisProperty1, xaxis);
            cOriPlaneXZ.SetValue(GeomLib.Extensions.AxisProperty2, zaxis);
            cOriPlaneXZ.BackMaterial = new DiffuseMaterial(planecolor);

            cplaneGroup.Children.Add(cOriPlaneXZ);
        }
        public void HideCursor()
        {
            
        }
        public void DestroyCursor()
        {
            if (chxaxis != null)
                this.container.Children.Remove(chxaxis);
            if (chyaxis != null)
                this.container.Children.Remove(chyaxis);
            if (chzaxis != null)
                this.container.Children.Remove(chzaxis);
            if (cxaxis != null)
                this.container.Children.Remove(cxaxis);
            if (cyaxis != null)
                this.container.Children.Remove(cyaxis);
            if (czaxis != null)
                this.container.Children.Remove(czaxis);
            if (cPlaneXY != null)
                this.cplaneGroup.Children.Remove(cPlaneXY);
            if (cPlaneXZ != null)
                this.cplaneGroup.Children.Remove(cPlaneXZ);
            if (cPlaneYZ != null)
                this.cplaneGroup.Children.Remove(cPlaneYZ);
        }
        public void UnHighlightAllElements()
        {
            cPlaneXY.ChangeFrontBackOpacity(100);
            cPlaneXZ.ChangeFrontBackOpacity(100);
            cPlaneYZ.ChangeFrontBackOpacity(100);
            cOriPlaneXY.ChangeFrontBackOpacity(100);
            cOriPlaneYZ.ChangeFrontBackOpacity(100);
            cOriPlaneXZ.ChangeFrontBackOpacity(100);
        }
        public void SetTransform(Transform3D transform)
        {
            chxaxis.Transform = transform;
            chyaxis.Transform = transform;
            chzaxis.Transform = transform;
            cxaxis.Transform = transform;
            cyaxis.Transform = transform;
            czaxis.Transform = transform;
            cPlaneXY.Transform = transform;
            cPlaneXZ.Transform = transform;
            cPlaneYZ.Transform = transform;
        }
        public void SetTransform(Matrix3D mtx)
        {
            MatrixTransform3D transform = new MatrixTransform3D(mtx);
            chxaxis.Transform = transform;
            chyaxis.Transform = transform;
            chzaxis.Transform = transform;
            cxaxis.Transform = transform;
            cyaxis.Transform = transform;
            czaxis.Transform = transform;
            cPlaneXY.Transform = transform;
            cPlaneXZ.Transform = transform;
            cPlaneYZ.Transform = transform;
        }
        public Transform3D GetTransform()
        {
            return this.chxaxis.Transform;
        }
        public WireLine CHXAxis { get { return this.chxaxis; } }
        public WireLine CHYAxis { get { return this.chyaxis; } }
        public WireLine CHZAxis { get { return this.chzaxis; } }
        public WireLine CXAxis { get { return this.cxaxis; } }
        public WireLine CYAxis { get { return this.cyaxis; } }
        public WireLine CZAxis { get { return this.czaxis; } }
    }
}
