﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using WrapperLibrary;
namespace Utility
{
    public static class UIUtility
    {
        public static bool IsStylusInRange = false;
        public static List<Step> PSteps;
        public static List<Step> NSteps;

        public static void ChangeFrontBackOpacity(this GeometryModel3D model, byte a)
        {
            var diffuseMat = (DiffuseMaterial)model.Material;
            if (diffuseMat != null && diffuseMat.Brush is SolidColorBrush)
            {
                var diffuseBrush = (SolidColorBrush)diffuseMat.Brush;
                var diffuseColor = diffuseBrush.Color;
                diffuseColor.A = a;
                model.ChangeFrontBackDiffuseMaterials(diffuseColor);
            }
        }

        public static void ChangeFrontBackDiffuseMaterials(this GeometryModel3D model, Color color)
        {
            SolidColorBrush solBrush = new SolidColorBrush(color);
            if (model.Material is DiffuseMaterial)
            {
                var diffuseMat = (DiffuseMaterial)model.Material;
                // use solid brush for the diffuse material
                diffuseMat.Brush = solBrush;
                model.Material = diffuseMat;
            }
            if (model.BackMaterial is DiffuseMaterial)
            {
                var diffuseMat = (DiffuseMaterial)model.BackMaterial;
                diffuseMat.Brush = solBrush;
                model.BackMaterial = diffuseMat;
            }
        }

    }
}
