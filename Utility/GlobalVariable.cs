﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperLibrary.RDF;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Petzold.Media3D;

namespace Utility
{
    public static class GlobalVariables
    {
        public static void InitializeGlobalVariables()
        {
            CurrentMode = Mode.Standard;
            RDFWrapper.All_RDF_Objects = new Dictionary<string, RDF_Abstract>();
            RDFTimingObj = new TimerObject<RDF_GeometricItem>(3000);
            WireTimingObj = new TimerObject<WireBase>(3000);
            CurrentTouchObjects = new Dictionary<int, Tuple<System.Windows.Point, RDF_GeometricItem>>();
        }
        public static Point3D CursorPosition { set; get; }
        public static RDF_GeometricItem SelectedItem { set; get; }
        public static RDF_GeometricItem PreSelectedItem { set; get; }
        /// <summary>
        /// Different modes to coordinate different module to execute different commands
        /// </summary>
        public static Mode CurrentMode { set; get; }
        public static bool IsRecordingPrototype = false;
        public static bool IsPrototyping = false;
        public static Dictionary<int, Tuple<System.Windows.Point, RDF_GeometricItem>> CurrentTouchObjects { set; get; }
        public static bool ContainsValue(this Dictionary<int, Tuple<System.Windows.Point, RDF_GeometricItem>> testObj, RDF_GeometricItem value)
        {
            foreach (var extvalue in testObj.Values)
            {
                if (extvalue.Item2 == value)
                    return true;
            }
            return false;
        }
        public static bool HasFinger(this Dictionary<int, Tuple<System.Windows.Point, RDF_GeometricItem>> testObj, System.Windows.Point testPt)
        {
            foreach (var extvalue in testObj.Values)
            {
                var pt1 = extvalue.Item1;
                var vec = testPt - pt1;
                if (vec.Length < 3)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// when erase gesture is detected, this stroke will be used for erase 
        /// even if the following gestures are not erase
        /// </summary>
        private static bool iserasing = false;
        public static bool IsErasing { 
            set { GlobalVariables.iserasing = value; } 
            get { return GlobalVariables.iserasing; } 
        }

        public static System.Windows.Ink.StrokeCollection CanvasStrokes { get; set; }

        public static TimerObject<RDF_GeometricItem> RDFTimingObj { get; set; }
        public static TimerObject<WireBase> WireTimingObj { get; set; }

        public static bool isSketching { get; set; }

        public static bool IsSketchingOverlay { get; set; }
    }
}
