﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public enum Mode
    {
        Standard = 0,
        Sketch = 1,
        Extrusion = 2,
        SweptSolid = 4,
        MoveShape = 8,
        MoveShapeAxis = 16,
        UndoRedo = 32,
        Array = 64,
        AddOneDimension = 128,
        ModifyAddOneDimension = 256,
        RotateShape = 512
    }
}
