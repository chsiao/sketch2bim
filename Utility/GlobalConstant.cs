﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Windows.Media;

namespace Utility
{
    public static class GlobalConstant
    {
        public static SolidColorBrush SelectedMeshBrush = new SolidColorBrush(Colors.LightSlateGray);
        public static SolidColorBrush UnSelectedMeshBrush = new SolidColorBrush(Colors.SlateGray);
        public static MaterialGroup SelectedMeshMaterials = new MaterialGroup();
        public static MaterialGroup UnSelectedMeshMaterials = new MaterialGroup();
        public static Color SelectedLineColor = Color.FromArgb(100, 200, 0, 0);
        public static Color UnSelectedLineColor = Colors.Gray;//Color.FromArgb(100, 200, 200, 0);
        public static Color UnSelectedVirtualLineColor = Color.FromArgb(20, 200, 200, 200);
        public const int DrawFrequency = 10;
        public const double SketchMergeThreshold = 3;
        public const long ErasingThreshold = 7000000;
        public const long DynamicGestureThreshold = 20000000;
        public const int SmallGestureThreshold = 30;
        public const int ArrayInterval = 15;
        public static void SetUpGlobalConstant()
        {
            GlobalConstant.SelectedMeshBrush.Opacity = 0.3;
            GlobalConstant.UnSelectedMeshBrush.Opacity = 0.9;

            DiffuseMaterial selectedDiffM = new DiffuseMaterial(SelectedMeshBrush);
            SelectedMeshMaterials.Children.Add(selectedDiffM);
            SelectedMeshMaterials.Children.Add(new SpecularMaterial(Brushes.White, 80));

            DiffuseMaterial unSelectedDiffM = new DiffuseMaterial(UnSelectedMeshBrush);
            UnSelectedMeshMaterials.Children.Add(unSelectedDiffM);
            UnSelectedMeshMaterials.Children.Add(new SpecularMaterial(Brushes.White, 80));

        }
    }
}
