﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Windows.Media;


namespace Utility
{
    public class HitObjects
    {
        //private List<DependencyObject> hitObjects;
        //private List<HitObjectEnum> hitObjectTypes;
        //private List<double> hitDistances;
        //private List<Point3D> hitPoints;
        private SortedDictionary<double, Tuple<HitObjectEnum, DependencyObject, Point3D>> hitTable;
        public HitObjects()
        {
            //this.hitObjects = new List<DependencyObject>();
            //this.hitObjectTypes = new List<HitObjectEnum>();
            //this.hitDistances = new List<double>();
            //this.hitPoints = new List<Point3D>();

            hitTable = new SortedDictionary<double, Tuple<HitObjectEnum, DependencyObject, Point3D>>();
        }

        public bool AddHitOjbect(DependencyObject obj, HitObjectEnum type, double distance, Point3D hitpt)
        {
            //hitObjectTypes.Add(type);
            //hitObjects.Add(obj);
            //hitDistances.Add(distance);
            //hitPoints.Add(hitpt);
            if (this.hitTable.ContainsKey(distance))
                return false;
            Tuple<HitObjectEnum, DependencyObject, Point3D> hitTypeObjPts = new Tuple<HitObjectEnum, DependencyObject, Point3D>(type, obj, hitpt);
            hitTable.Add(distance, hitTypeObjPts);
            return true;
        }

        public List<DependencyObject> FindAllObjectsInType(HitObjectEnum type)
        {
            List<DependencyObject> results = new List<DependencyObject>();

            foreach (var hitvalue in hitTable.Values)
            {
                if (hitvalue.Item1 == type)
                {
                    results.Add(hitvalue.Item2);
                }
            }
            return results;
        }
        public int GetHitOrderByObjectType(HitObjectEnum hittype)
        {
            var hitvalues = hitTable.Values.ToList();
            var index = hitvalues.FindIndex(tp => tp.Item1 == hittype);
            return index;
        }
        public int GetHitOrder(DependencyObject hitobj)
        {
            var hitvalues = hitTable.Values.ToList();
            var index = hitvalues.FindIndex(tp => tp.Item2 == hitobj);
            return index;
        }
        public HitObjectEnum GetHitType(DependencyObject hitobj)
        {
            var hitvalues = hitTable.Values.ToList();
            var foundTp = hitvalues.Find(tp => tp.Item2 == hitobj);
            if (foundTp != null)
                return foundTp.Item1;
            else
                return HitObjectEnum.None;
        }
        public double GetHitDistance(DependencyObject hitobj)
        {
            var keyvalue = hitTable.First(kv => kv.Value.Item2 == hitobj);
            return keyvalue.Key;
        }
        public Point3D? GetHitPointByType(HitObjectEnum hitobjtype, int index = 0)
        {
            var hitvalues = hitTable.Values.ToList();
            int i = 0;
            foreach(var hitvalue in hitvalues)
            {
                if (hitvalue.Item1 == hitobjtype)
                {
                    if (i == index)
                        return hitvalue.Item3;
                    i++;
                }
            }
            return null;
        }
        public Point3D? GetHitPointByGeom(DependencyObject hitobj, int index = 0)
        {
            var hitvalues = hitTable.Values.ToList();
            int i = 0;
            foreach (var hitvalue in hitvalues)
            {
                if (hitvalue.Item2 == hitobj)
                {
                    if (i == index)
                        return hitvalue.Item3;
                    i++;
                }
            }
            return null;
        }
        public Dictionary<DependencyObject, int> FindHitCounts()
        {
            Dictionary<DependencyObject, int> countsForObject = new Dictionary<DependencyObject, int>();
            var hitvalues = this.hitTable.Values;
            foreach (var hitValue in hitvalues)
            {
                if (countsForObject.ContainsKey(hitValue.Item2))
                {
                    countsForObject[hitValue.Item2] = countsForObject[hitValue.Item2] + 1;
                }
                else
                {
                    countsForObject.Add(hitValue.Item2, 1);
                }
            }
            return countsForObject;
        }
        public int HitCount { get { return this.hitTable.Count; } }
    }

    public enum HitObjectEnum
    {
        Plane,
        Mesh2D,
        Mesh3D,
        MeshUtility,
        Wire,
        WireUtility,
        None
    }
}
