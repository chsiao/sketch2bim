﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperLibrary.RDF;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Petzold.Media3D;

namespace Utility
{
    public static class RDFUtility
    {
        /// <summary>
        /// Unselect the selected item and update the new selected item
        /// </summary>
        /// <param name="itemToSelect"></param>
        public static void Select(this RDF_GeometricItem itemToSelect)
        {
            itemToSelect.IsSelected = true;
            if (GlobalVariables.SelectedItem != null)
                GlobalVariables.SelectedItem.UnSelect();

            GlobalVariables.SelectedItem = itemToSelect;
            if (GlobalVariables.SelectedItem.Geometry != null)
            {
                GlobalVariables.SelectedItem.Geometry.Material = GlobalConstant.SelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.SelectedMeshBrush);
                GlobalVariables.SelectedItem.Geometry.BackMaterial = GlobalConstant.SelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.SelectedMeshBrush);
            }

            // select the wirepaths surrended to the surfaces as well
            if (itemToSelect is RDF_Polygon3D)
                selectPolygon((RDF_Polygon3D)itemToSelect);
            else if (itemToSelect is RDF_Face2D)
                selectFace2D(itemToSelect);
            else if (itemToSelect is RDF_SweptAreaSolid)
            {
                RDF_SweptAreaSolid sweptSolid = (RDF_SweptAreaSolid)itemToSelect;
                RDF_Face2D face = sweptSolid.SweptProfile.Face;
                selectFace2D(face);
            }
            else if (itemToSelect is RDF_Boolean3D)
            {
                RDF_Boolean3D rdfBool = (RDF_Boolean3D)itemToSelect;
                if (rdfBool.First is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid sweptSolid = (RDF_SweptAreaSolid)rdfBool.First;
                    RDF_Face2D face = sweptSolid.SweptProfile.Face;
                    selectFace2D(face);
                }
            }
        }
        public static void UnSelect(this RDF_GeometricItem itemToUnselect)
        {
            if (itemToUnselect == null) return;
            if (GlobalVariables.SelectedItem != null && GlobalVariables.SelectedItem == itemToUnselect)
            {
                GlobalVariables.SelectedItem = null;
            }
            itemToUnselect.IsSelected = false;
            if (itemToUnselect.Geometry != null)
            {
                itemToUnselect.Geometry.Material = GlobalConstant.UnSelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.UnSelectedMeshBrush);
                itemToUnselect.Geometry.BackMaterial = GlobalConstant.UnSelectedMeshMaterials;//new DiffuseMaterial(GlobalConstant.UnSelectedMeshBrush);
            }

            // unselect the wirelines surrended to the surfaces as well
            if (itemToUnselect is RDF_Polygon3D)
                unselectPolygon((RDF_Polygon3D)itemToUnselect);
            else if (itemToUnselect is RDF_Face2D)
                unselectFace2D(itemToUnselect);
            else if (itemToUnselect is RDF_SweptAreaSolid)
            {
                RDF_SweptAreaSolid sweptSolid = (RDF_SweptAreaSolid)itemToUnselect;
                RDF_Face2D face = sweptSolid.SweptProfile.Face;
                if(face != null)
                    unselectFace2D(face);
            }
        }

        private static void unselectFace2D(RDF_GeometricItem itemToUnselect)
        {
            RDF_Face2D face = (RDF_Face2D)itemToUnselect;
            RDF_Polygon3D polygon = face.Outer;
            unselectPolygon(polygon);
        }

        private static void unselectPolygon(RDF_Polygon3D polygon)
        {
            WirePath path = null;
            if (polygon.HasTransform)
                path = (WirePath)polygon.Transform.GeometryObject;
            if(path == null)
                path = polygon.PathGeometry;

            if (path != null)
            {
                path.Thickness = polygon.IsVirtual ? 1 : 
                    path.WireType == WireTypeEnum.Pivot? 3 : 2;
                if (polygon.IsVirtual)
                    path.Color = GlobalConstant.UnSelectedVirtualLineColor;
                else if (path.WireType == WireTypeEnum.Pivot)
                    path.Color = Colors.OrangeRed;
                else
                    path.Color = GlobalConstant.UnSelectedLineColor;
            }
        }
        private static void selectFace2D(RDF_GeometricItem itemToSelect)
        {
            if (itemToSelect != null)
            {
                RDF_Face2D face = (RDF_Face2D)itemToSelect;
                RDF_Polygon3D polygon = face.Outer;
                selectPolygon(polygon);
            }
        }

        private static void selectPolygon(RDF_Polygon3D polygon)
        {
            WirePath path = null;
            if (polygon.HasTransform)
                path = (WirePath)polygon.Transform.GeometryObject;
            if (path == null)
                path = polygon.PathGeometry;

            if (path != null)
            {
                path.Thickness = polygon.IsVirtual ? 1 : 4;
                path.Color = GlobalConstant.SelectedLineColor;
            }
        }
    }
}
