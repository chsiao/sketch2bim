﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utility
{
    public class TimerObject<T> : List<T>
    {
        private int duration = 0;
        private Timer timer;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="duration">in minisecond</param>
        public TimerObject(int duration)
        {
            this.timer = new Timer();
            timer.Interval = duration;
            timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("tick!!");
            this.timer.Stop();
            this.Clear();
        }
        /// <summary>
        /// return true if this object is not added into the countdown list
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool AddCountDownObject(T obj)
        {
            if (!this.Contains(obj))
            {
                this.timer.Stop();
                this.timer.Start();
                this.Add(obj);
                return true;
            }
            else
                return false; 
        }
        public void ClearCountDownObject()
        {
            this.Clear();
            this.timer.Stop();
        }
    }
}
