﻿using System;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input.StylusPlugIns;
using System.Windows.Input;
using System.Windows.Ink;


namespace CanvasSketch
{


    // A StylusPlugin that renders ink with a linear gradient brush effect.
    public class CustomDynamicRenderer : DynamicRenderer
    {
        public delegate void OnNewPointDrawn(Point newpt);
        public event OnNewPointDrawn AfterDrawn;
        private int drawcount = 0;

        [ThreadStatic]
        static private Brush brush = null;

        [ThreadStatic]
        static private Pen pen = null;

        private Point prevPoint;

        public bool IsSavingGestures { get; set; }

        protected override void OnStylusDown(RawStylusInput rawStylusInput)
        {
            // if it's from touch input, don't do anything
            if(Filter(rawStylusInput)) return;
            drawcount = 0;
            // Allocate memory to store the previous point to draw from.
            prevPoint = new Point(double.NegativeInfinity, double.NegativeInfinity);
            base.OnStylusDown(rawStylusInput);
        }

        protected override void OnStylusMove(RawStylusInput rawStylusInput)
        {
            // if it's from touch input, don't do anything
            if (Filter(rawStylusInput)) return;
            base.OnStylusMove(rawStylusInput);
        }

        protected override void OnStylusUp(RawStylusInput rawStylusInput)
        {
            // if it's from touch input, don't do anything
            if (Filter(rawStylusInput)) return;
            base.OnStylusUp(rawStylusInput);
        }

        protected override void OnDraw(DrawingContext drawingContext,
                                       StylusPointCollection stylusPoints,
                                       Geometry geometry, Brush fillBrush)
        {
            if (drawcount % 5 == 0) // reduce the rendering frequency to gain the performance 
            {
                // Draw linear gradient ellipses between 
                // all the StylusPoints that have come in.
                for (int i = 0; i < stylusPoints.Count; i++)
                {
                    double opacity = stylusPoints[i].PressureFactor / 2;
                    Brush curbrush = new SolidColorBrush(Colors.Black);
                    curbrush.Opacity = opacity;
                    pen = new Pen(curbrush, 2d);

                    Point pt = (Point)stylusPoints[i];
                    Vector v = Point.Subtract(prevPoint, pt);

                    // Only draw if we are at least 4 units away 
                    // from the end of the last ellipse. Otherwise, 
                    // we're just redrawing and wasting cycles.
                    if (v.Length > 4)
                    {
                        // Set the thickness of the stroke based 
                        // on how hard the user pressed.
                        if (IsSavingGestures)
                        {
                            double radius = stylusPoints[i].PressureFactor * 10d;
                            drawingContext.DrawEllipse(curbrush, pen, pt, 2, 2);
                        }
                        else
                        {
                            drawingContext.DrawLine(pen, prevPoint, pt);
                            prevPoint = pt;
                        }
                    }
                    if (i == stylusPoints.Count - 1 && v.Length > 0)
                    {
                        AfterDrawn(pt);
                    }
                }
            }
            drawcount++;
        }

        private bool Filter(RawStylusInput input)
        {
            if (input.StylusDeviceId > 2 && !(input.StylusDeviceId == 10 || input.StylusDeviceId == 6))
            {
                //StylusPointCollection stylusPoints = input.GetStylusPoints();
                //stylusPoints.Clear();
                //input.SetStylusPoints(stylusPoints);
                return true;
            }
            return false;
        }
    }
}