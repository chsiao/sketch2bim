﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using Recognizer.NDollar;
using Utility;
using GeomLib;
using WrapperLibrary;
using WrapperLibrary.Cornucopia;
using WrapperLibrary.RDF;
using System.Windows.Ink;
using Petzold.Media3D;
using _3DTools;
using System.Windows.Input;
using GeomLib.Constraint;

namespace CanvasSketch
{
    public class GestureCommand
    {
        #region Field        
        private GestureEnums gesture = GestureEnums.None;
        private GeometricRecognizer _rec;
        private const double score_threshold = 2;
        #endregion

        #region Property
        public GestureEnums Gesture { get { return this.gesture; } }
        public Stroke GestureStroke { get; set; }
        public PointF GestureLocation { get; private set; }
        #endregion

        private delegate void UIDelegate(object value);
        public delegate void ConstraintCallBack(object unsolvedObj, object solvedObj);
        public delegate void SelectionDelegate(RDF_Abstract selObj);

        public event ConstraintCallBack CommandExecuted;
        public event SelectionDelegate OnObjectSelected;
        public event SelectionDelegate OnObjectUnSelected;

        public GestureCommand(Stroke drewPl, GeometricRecognizer rec)
        {
            this.GestureStroke = drewPl;
            this._rec = rec;

            InitializeRec();
        }

        public GestureCommand(List<System.Windows.Point> drewpts, GeometricRecognizer rec)
        {
            StylusPointCollection ptcol = new StylusPointCollection(drewpts);
            Stroke sketchSt = new Stroke(ptcol);
            this.GestureStroke = sketchSt;
            this._rec = rec;

            this.InitializeRec();
        }

        private void InitializeRec()
        {
            Rect bound = GestureStroke.GetBounds();
            if (bound.Width * bound.Height < 20) // see if the gesture is very small
                this.gesture = GestureEnums.Point;
            else // finding initial gestures by using N$ // could be improved by using corucopia
                this.gesture = GestureRecognition(GestureStroke);
        }

        private GestureEnums GestureRecognition(Stroke drawingstroke)
        {
            if (drawingstroke.StylusPoints.Count > 2)
            {
                try
                {
                    NBestList result = this._rec.Recognize(drawingstroke.ConvertToPointRList(), 1);
                    if (result.Score != -1)
                    {
                        string[] names = result.Names;
                        double[] scores = result.Scores;
                        string gestureName = "";
                        double maxScore = 0;
                        // use sum to find the highest score gesture
                         
                        Dictionary<string, double> scoreTable = new Dictionary<string, double>();
                        int index = 0;
                        foreach (string name in names)
                        {
                            if (name.Length > 0)
                            {
                                string newname = name.Remove(name.Length - 3);
                                if (scoreTable.ContainsKey(newname))
                                    scoreTable[newname] += scores[index];
                                else
                                    scoreTable.Add(newname, scores[index]);
                            }
                            if (index > 5) break;
                            index++;
                        }

                        maxScore = scoreTable.Values.ToList().Max();
                        foreach (string key in scoreTable.Keys)
                        {
                            if (scoreTable[key] == maxScore)
                            {
                                gestureName = key;
                            }
                        }
                        

                        //maxScore = scores[0];
                        //gestureName = names[0];

                        GestureEnums gesture = GestureMapping.GetGestureEnum(gestureName);
                        //System.Diagnostics.Debug.WriteLine("0: " + gestureName + "; " + maxScore);
#if false
                        System.Diagnostics.Debug.WriteLine("1: " + names[0] + "; " + scores[0]);
                        System.Diagnostics.Debug.WriteLine("2: " + names[1] + "; " + scores[1]);
                        System.Diagnostics.Debug.WriteLine("3: " + names[2] + "; " + scores[2]);
#endif
                        Rect bound = GestureStroke.GetBounds();
                        if (gesture == GestureEnums.Erase)
                        {
                            var firstpt = drawingstroke.StylusPoints.First();
                            var lastpt = drawingstroke.StylusPoints.Last();
                            var dist = firstpt.distanceTo(lastpt);
                            if (maxScore < score_threshold || bound.Width * bound.Height < 200 || dist < 30)
                                gesture = GestureEnums.None;
                        }
                        else if (bound.Width > GlobalConstant.SmallGestureThreshold || bound.Height > GlobalConstant.SmallGestureThreshold)
                            gesture = GestureEnums.None;

                        if (gesture != GestureEnums.None)
                        {
                            System.Windows.Rect bbox = drawingstroke.GetBounds();
                            this.GestureLocation = new PointF((float)bbox.X + (float)bbox.Width / 2, (float)bbox.Y + (float)bbox.Height / 2);
                        }

                        return gesture;
                    }
                    else
                    {
                        //if (this.lbgesture != null)
                        //    this.lbgesture.BeginInvoke(new UIDelegate(updateGestureValue), "");
                        return GestureEnums.None;
                    }
                }
                catch { return GestureEnums.None; }
            }
            else
            {
                PointF glocation = new PointF();
                foreach (var pt in this.GestureStroke.StylusPoints)
                {
                    glocation.X += (float)pt.X;
                    glocation.Y += (float)pt.Y;
                }
                glocation.X = glocation.X / this.GestureStroke.StylusPoints.Count;
                glocation.Y = glocation.Y / this.GestureStroke.StylusPoints.Count;
                this.GestureLocation = glocation;
                return GestureEnums.Point;
            }
        }

        /// <summary>
        /// use stroke size, cornucopia segment amount, cornucopia result (curve vs line) for further recognition
        /// </summary>
        internal void RefineGesture(long penUpDownDiff)
        {
            int size = this.GestureStroke.StylusPoints.Count;
            var fstPt = this.GestureStroke.StylusPoints.First();
            var lastPt = this.GestureStroke.StylusPoints.Last();
            var dist = fstPt.distanceTo(lastPt);
            int[,] pts = new int[size, 2];
            int prex = int.MaxValue;
            int prey = int.MaxValue;
            int j = 0;
            for (int i = 0; i < size; i++)
            {
                System.Windows.Input.StylusPoint pt = this.GestureStroke.StylusPoints[i];
                if ((int)pt.X == prex && (int)pt.Y == prey)
                    continue;
                pts[j, 0] = (int)pt.X;
                pts[j, 1] = (int)pt.Y;
                prex = (int)pt.X;
                prey = (int)pt.Y;
                j++;
            }
            int[,] resizePts = new int[j, 2];
            Array.Copy(pts, resizePts, j * 2);

            // 1. when the original is angle, right, or same length, we need to distinguish how many segments we have
            if (this.gesture == GestureEnums.RightAngGesture || this.gesture == GestureEnums.SameAngGesture || 
                this.gesture == GestureEnums.SameLengthGesture || this.gesture == GestureEnums.SetAng || penUpDownDiff < 7000000
                && this.gesture != GestureEnums.Point && j > 3)
            {
                try
                {
                    IntPtr curvedata = new IntPtr();
                    IntPtr ptrPMResult = cornucopia.getBasicPrimitives(resizePts, j, 6, 20);
                    int segcount = resizePts[0, 0];
                    var box = this.GestureStroke.GetBounds();
                    bool isSmallGesture = box.Width < GlobalConstant.SmallGestureThreshold && box.Height < GlobalConstant.SmallGestureThreshold;
                    // same length or same angle
                    if (segcount == 1 && isSmallGesture)
                    {
                        BasicPrimitive bpm = ptrPMResult.ElementAt<BasicPrimitive>(0);
                        if (bpm.type == BasicPrimitive.PrimitiveType.ARC)
                        {
                            this.gesture = GestureEnums.SetAng;
                        }
                        else if (bpm.type == BasicPrimitive.PrimitiveType.LINE)
                        {
                            this.gesture = GestureEnums.SameLengthGesture;
                        }
                    }
                    // right angle
                    else if (segcount == 2 && isSmallGesture && dist > 15)
                    {
                        this.gesture = GestureEnums.RightAngGesture;
                    }
                    // erasing
                    else if (segcount > 4 && penUpDownDiff < GlobalConstant.ErasingThreshold)
                    {
                        this.gesture = GestureEnums.Erase;
                    }
                    // Glue
                    else if (box.Width * box.Height < 1000 && (this.gesture == GestureEnums.Glue || segcount >= 2))
                    {
                        this.gesture = GestureEnums.Glue;
                    }
                    // nothing
                    else
                    {
                        this.gesture = GestureEnums.None;
                    }
                }
                catch (Exception e)
                {

                }
            }
        }

        private void updateGestureValue(object gesture)
        {
            //this.lbgesture.Text = gesture.ToString();
        }

        /// <summary>
        /// process the gestures according to its context => the selected geometry, the hit geometry, the sketch status,
        /// 
        /// </summary>
        /// <param name="hitmodel"></param>
        /// <param name="viewport"></param>
        /// <param name="valideEdges"></param>
        /// <param name="hitedges"></param>
        /// <param name="isEndSketch"></param>
        /// <returns>the final detected gesture</returns>
        public GestureEnums processGesture(GeometryModel3D hitmodel, Viewport3D viewport, HitObjects curHitObjects, HashSet<WireBase> hitedges = null, 
            List<GeometryModel3D> hitUtilGeoms = null, Func<WireLine, Vector3D, bool, string, object> rotatePlane = null, bool isEndSketch = false, List<Point3D> hitPts = null)
        {
            //hitedges = hitedges != null ? hitedges :
            //               valideEdges != null ? valideEdges().Item1 :
            //               new List<WireBase>();
            int hitmeshOrder = curHitObjects.GetHitOrderByObjectType(HitObjectEnum.Mesh2D);
            hitmeshOrder = hitmeshOrder == -1 ? int.MaxValue : hitmeshOrder;
            int hitwireOrder = curHitObjects.GetHitOrderByObjectType(HitObjectEnum.Wire);
            bool isEdgeFirst = hitwireOrder != -1 && hitwireOrder < hitmeshOrder;
            int hitmode = 0;
            if (hitUtilGeoms != null && hitUtilGeoms.Count > 0 && gesture == GestureEnums.Point)
                hitmode = 1;
            else if (hitedges != null && hitedges.Count > 0 && isEdgeFirst && (gesture == GestureEnums.Glue || gesture == GestureEnums.Erase))
                hitmode = 3;
            else if (hitmodel != null)// && gesture == GestureEnums.Point || gesture == GestureEnums.Erase)
                hitmode = 2;
            else if (hitedges != null && hitedges.Count > 0)
                hitmode = 3;

            System.Diagnostics.Debug.WriteLine(hitmode);

            //if (hitUtilGeoms != null && hitUtilGeoms.Count > 0 && gesture == GestureEnums.Point) 
            if(hitmode == 1)
            {
                var hitutilgeom = hitUtilGeoms.First();
                var utilGeomID = (string)hitUtilGeoms.First().GetValue(GeomLib.Extensions.IDProperty);
                if (utilGeomID == "cxyplane" || utilGeomID == "cyzplane" || utilGeomID == "cxzplane" ||
                    utilGeomID == "orixyplane" || utilGeomID == "oriyzplane" || utilGeomID == "orixzplane")
                {
                    var axis1 = (WireLine)hitutilgeom.GetValue(GeomLib.Extensions.AxisProperty1);
                    var axis2 = (WireLine)hitutilgeom.GetValue(GeomLib.Extensions.AxisProperty2);
                    var axisTransform = axis2.Transform;
                    Vector3D vec2 = new Vector3D(axis2.Point2.X - axis2.Point1.X, axis2.Point2.Y - axis2.Point1.Y, axis2.Point2.Z - axis2.Point1.Z);
                    //vec2 = axisTransform.Transform(vec2);

                    rotatePlane(axis1, vec2, true, utilGeomID);

                    this.gesture = GestureEnums.None;
                    return this.gesture;
                }
            }
            if (hitmode == 2)//hitmodel != null)//return GestureEnums.None;
            {
                var hitreturn = ChkHitModelGesture(hitmodel, viewport, hitedges, isEndSketch, hitPts);
                if (hitreturn != GestureEnums.None)
                    return hitreturn;
            }
            if (hitmode <= 3)
            {
                if (this.gesture != GestureEnums.Erase && (this.gesture == GestureEnums.RightAngGesture || this.gesture == GestureEnums.SameAngGesture))
                {
                    if (hitedges.Count > 0 && RDFWrapper.All_RDF_Objects.ContainsKey(hitedges.First().ID))
                    {
                        var rdfobj = RDFWrapper.All_RDF_Objects[hitedges.First().ID];
                        if (rdfobj is RDF_Transformation)
                            rdfobj = ((RDF_Transformation)rdfobj).TransformGeom;

                        if (rdfobj is RDF_Polygon3D)
                        {
                            var rdfpolygon = (RDF_Polygon3D)rdfobj;
                            RDF_Node3D onNode = validateAngleGesture(rdfpolygon, viewport);
                            if (onNode != null && onNode.Angle != null)
                                return processAngleCommands(rdfpolygon, onNode, viewport);
                        }
                    }
                }
                else if (this.gesture == GestureEnums.Glue && hitedges.Count > 1 && isEndSketch)
                {
                    HashSet<RDF_Transformation> edges = new HashSet<RDF_Transformation>();
                    foreach (var edge in hitedges)
                    {
                        if (edge.ID != null && RDFWrapper.All_RDF_Objects.ContainsKey(edge.ID) && RDFWrapper.All_RDF_Objects[edge.ID] is RDF_Transformation)
                        {
                            RDF_Transformation rdfedge = (RDF_Transformation)RDFWrapper.All_RDF_Objects[edge.ID];
                            edges.Add(rdfedge);
                        }
                    }
                    if (edges.Count == 2)
                    {
                        CustomStroke newstroke = Library.tryMergeTwoStrokes(edges.First(), edges.Last());
                        if (newstroke != null)
                        {
                            GlobalVariables.CanvasStrokes.Add(newstroke);
                            newstroke.FitintoCurves();
                            if (CommandExecuted != null)
                            {
                                CommandExecuted(null, newstroke);
                            }
                        }
                    }
                }
                // dealing with 2 conditions here
                // 1. during sketching, if the erasing gesture is detected, keap erasing when finding strokes/objects is under this erase gesture
                // 2. select/erase the stroke/ under this gesture
                else if (this.gesture == GestureEnums.Erase || isEndSketch)
                {
                    if (hitedges.Count > 0)
                    {
                        bool hasEdgeSelected = false;
                        WirePath selpath = (WirePath)hitedges.First();
                        if (selpath.ID != null && RDFWrapper.All_RDF_Objects.ContainsKey(selpath.ID))
                        {
                            RDF_Abstract selRDFPath = RDFWrapper.All_RDF_Objects[selpath.ID];
                            if (selRDFPath != null)
                            {
                                RDF_GeometricItem selRDFGeom = null;
                                if (selRDFPath is RDF_Transformation)
                                    selRDFGeom = ((RDF_Transformation)selRDFPath).TransformGeom;
                                else if (selRDFPath is RDF_GeometricItem)
                                    selRDFGeom = (RDF_GeometricItem)selRDFPath;
                                if (selRDFGeom != null)
                                {
                                    selRDFGeom.Select();
                                    hasEdgeSelected = true;
                                }
                            }
                        }
                        else
                        {
                            var removingEdge = viewport.Children.Remove(selpath);
                        }
                        if (hasEdgeSelected)
                            return this.gesture;
                    }
                }
            }
            // nothing is detected
            this.gesture = GestureEnums.None;
            return this.gesture;
        }

        private GestureEnums ChkHitModelGesture(GeometryModel3D hitmodel, Viewport3D viewport, HashSet<WireBase> hitedges, bool isEndSketch, List<Point3D> hitPts)
        {
            var hitobj = hitmodel.GetValue(RDFExtension.RDFObjectProperty);
            if (hitobj is RDF_GeometricItem && this.gesture != GestureEnums.Glue)
            {
                RDF_GeometricItem rdfgeom = (RDF_GeometricItem)hitobj;
                if (rdfgeom is RDF_SweptAreaSolid &&
                    (this.gesture == GestureEnums.RightAngGesture || this.gesture == GestureEnums.SameAngGesture || this.gesture == GestureEnums.SameLengthGesture)) // 2D gesture
                {
                    RDF_SweptAreaSolid rdf_swept3D = (RDF_SweptAreaSolid)rdfgeom;
                    RDF_Face2D rdf_face = rdf_swept3D.SweptProfile.Face;
                    rdfgeom = rdf_face;
                }

                if (rdfgeom is RDF_Face2D)
                {
                    RDF_Face2D rdf_face = (RDF_Face2D)rdfgeom;
                    RDF_Polygon3D outer = rdf_face.Outer;
                    var oriMtx = outer.Transform.Matrix;
                    bool isSubstrating = false;
                    if (oriMtx is RDF_MatrixMultiplication || GlobalVariables.SelectedItem is RDF_Boolean3D)
                    {
                        if (GlobalVariables.SelectedItem is RDF_Boolean3D)
                            isSubstrating = true;
                        else
                        {
                            var oriMtxMulti = (RDF_MatrixMultiplication)oriMtx;
                            if (this.gesture == GestureEnums.Erase && oriMtxMulti.SecondMatrix != null
                                && oriMtxMulti.SecondMatrix.RelatedTransforms.Count > 0
                                && oriMtxMulti.SecondMatrix.RelatedTransforms.Exists(tr => tr.TransformGeom == GlobalVariables.SelectedItem))
                            {
                                isSubstrating = true;
                            }
                        }
                    }

                    if (this.gesture == GestureEnums.RightAngGesture && !outer.IsVirtual)
                    {
                        RDF_Node3D onNode = validateAngleGesture(outer, viewport);
                        // if onNode is null, the user performs the gesture in a wrong location
                        if (onNode != null && hitedges.Count < 2)
                            return processAngleCommands(rdf_face, onNode, viewport);
                        else if (hitedges.Count > 0)
                        {
                            // 1. validate whether the gesture is performed on two different set of RDF_Dim2
                            validateAngleGesture2(hitedges, hitPts);
                            // 2. get the location where the gesture is performed related to the parent shape
                            // 3. if one of the RDF_Dim2 is a controlling object, then traverse into all the constrained objects to do the constraint

                        }
                    }
                    else if (this.gesture == GestureEnums.SameLengthGesture && !outer.IsVirtual)
                    {
                        RDF_Segment3D onSegment = validateLengthGesture(outer, viewport);
                        if (onSegment != null)
                        {
                            GlobalVariables.RDFTimingObj.AddCountDownObject(onSegment);

                            return processLengthCommands(rdf_face, viewport);
                        }
                        //else if(valideEdges != null)
                        //{
                        //    hitedges = valideEdges().Item2;
                        //    if (hitedges.Count > 0)
                        //    {
                        //        GlobalVariables.WireTimingObj.AddCountDownObject(hitedges.First());
                        //        return processAxisCommands(hitedges.First(), rotatePlane);
                        //    }
                        //}
                    }
                    else if (isSubstrating)
                    {
                        this.gesture = GestureEnums.DigHole;

                        Point3D origin = new Point3D(0, 0, -1000);
                        Point3D end = new Point3D(0, 0, 1000);
                        RDF_Line3D dirPath = new RDF_Line3D(origin, end);

                        RDF_SweptAreaSolid sweptSolid = new RDF_SweptAreaSolid();
                        sweptSolid.SetDirection(dirPath);
                        sweptSolid.SetSweptProfile(rdf_face.Outer);

                        var oriMtx3D = oriMtx.Matrix3D.Clone();

                        RDF_Matrix flippingMtx = new RDF_Matrix(Matrix3D.Identity);
                        //var newTraslate = new Point3D(0, 0, -500);
                        //newTraslate = oriMtx3D.Transform(newTraslate);
                        //flippingMtx.SetTranslate(newTraslate);

                        RDF_MatrixMultiplication sweptSolidMtx = new RDF_MatrixMultiplication(rdf_face.Outer.Transform.Matrix, flippingMtx);

                        RDF_Transformation translate = new RDF_Transformation();
                        translate.SetMatrix(sweptSolidMtx);
                        translate.SetTransformGeom(sweptSolid);

                        sweptSolid.group = rdf_face.group;
                        //RDF_Dim3.RenderConceptMesh(translate, rdf_face.group);

                        var boolProduct = ProcessBooleanCommand(GlobalVariables.SelectedItem, sweptSolid);
                        boolProduct.ShapeSemantics = GlobalVariables.SelectedItem.ShapeSemantics;
                        if (boolProduct != null)
                        {
                            Step pstep = new Step();
                            pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                            pstep.PreModel = boolProduct;
                            pstep.ModifiedObjs.Add(GlobalVariables.SelectedItem);
                            pstep.ModifiedObjs.Add(rdf_face);
                            pstep.PStep = StepTypeEnum.modifysolid;

                            Utility.UIUtility.PSteps.Add(pstep);
                        }
                        return GestureEnums.DigHole;
                    }
                    else if (this.gesture == GestureEnums.Point || this.gesture == GestureEnums.Erase)
                    {
                        if (this.gesture == GestureEnums.Erase)
                            GlobalVariables.IsErasing = true;
                        return processSelectionCommand(rdf_face);
                    }
                }
                else if (rdfgeom is RDF_SweptAreaSolid || rdfgeom is RDF_Boolean3D)
                {
                    if (this.gesture == GestureEnums.Point)
                        return processSelectionCommand(rdfgeom);
                    else if (this.gesture == GestureEnums.Erase || GlobalVariables.IsErasing)
                    {
                        RDF_GeometricItem firstGeom = GlobalVariables.SelectedItem;
                        // use erase gesture to create boolean subastraction operation
                        if (firstGeom != null && (firstGeom is RDF_SweptAreaSolid || (firstGeom is RDF_Boolean3D && firstGeom != rdfgeom)) && isEndSketch)
                        {
                            var boolProduct = ProcessBooleanCommand(firstGeom, rdfgeom);

                            if (rdfgeom is RDF_SweptAreaSolid && ((RDF_SweptAreaSolid)rdfgeom).SweptProfile.Face != null)
                            {
                                var sweptCst = ((RDF_SweptAreaSolid)rdfgeom).SweptProfile.Face.Constraint;
                                if (sweptCst.ConstraintName == GeometricConstraintEnum.Array)
                                {
                                    foreach (var cstobj in sweptCst.ConstraintObjects)
                                    {
                                        if (cstobj is RDF_Face2D)
                                        {
                                            var cstface = (RDF_Face2D)cstobj;
                                            foreach (var facebr in cstface.BackRelations)
                                            {
                                                if (facebr.RelatedTo is RDF_SweptAreaSolid)
                                                {
                                                    var substrctSolid = (RDF_SweptAreaSolid)facebr.RelatedTo;
                                                    boolProduct = ProcessBooleanCommand(boolProduct, substrctSolid);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    sweptCst = ((RDF_SweptAreaSolid)rdfgeom).Constraint;
                                    if (sweptCst.ConstraintName == GeometricConstraintEnum.Array)
                                    {
                                        foreach (var cstobj in sweptCst.ConstraintObjects)
                                        {
                                            if (cstobj is RDF_SweptAreaSolid)
                                            {
                                                boolProduct = ProcessBooleanCommand(boolProduct, (RDF_SweptAreaSolid)cstobj);
                                            }

                                        }
                                    }
                                }
                            }

                            if (boolProduct != null)
                            {
                                Step pstep = new Step();
                                pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                                pstep.PreModel = boolProduct;
                                pstep.ModifiedObjs.Add(firstGeom);
                                pstep.ModifiedObjs.Add(rdfgeom);
                                pstep.PStep = StepTypeEnum.modifysolid;

                                Utility.UIUtility.PSteps.Add(pstep);
                                GlobalVariables.IsErasing = false;
                                return GestureEnums.None;
                            }
                        }
                        // Record the erasing object for earse 
                        else if (GlobalVariables.SelectedItem == null || isEndSketch)
                        {
                            rdfgeom.Select();
                            this.gesture = GestureEnums.Erase;
                            return GestureEnums.Erase;
                        }
                    }
                }
            }
            //this.gesture = ;
            return GestureEnums.None;
        }

        public static RDF_Boolean3D ProcessBooleanCommand(RDF_GeometricItem firstGeom, RDF_GeometricItem secondgeom)
        {
            RDF_Boolean3D boolobj = new RDF_Boolean3D(RDF_Boolean3D.BooleanType.SUBSTRACT_1);
            boolobj.SetFirstObject((RDF_Dim3)firstGeom);
            boolobj.SetSecondObject((RDF_Dim3)secondgeom);

            GeometryModel3D product = boolobj.RenderConceptMesh(((RDF_Dim3)firstGeom).group);
            // remove the original geometry
            if (product != null)
            {
                RDF_Dim3 dim3D1 = (RDF_Dim3)firstGeom;
                RDF_Dim3 dim3D2 = (RDF_Dim3)secondgeom;
                dim3D1.IsVirtual = false;
                dim3D2.IsVirtual = false;
                dim3D1.group.Children.Remove(dim3D1.Geometry);
                dim3D2.group.Children.Remove(dim3D2.Geometry);

                if (dim3D2 is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid swptSolid2 = (RDF_SweptAreaSolid)dim3D2;
                    if (swptSolid2.SweptProfile.Face != null)
                    {
                        swptSolid2.SweptProfile.Face.group.Children.Remove(swptSolid2.SweptProfile.Face.Geometry);
                        swptSolid2.SweptProfile.PathGeometry.Color = System.Windows.Media.Color.FromArgb(0, 0, 0, 0);
                    }
                    
                }
                BackRelation br1 = new BackRelation(dim3D1);
                br1.Name = BackRelationName.Dim3_BooleanProduct1;
                br1.RelatedTo = boolobj;

                BackRelation br2 = new BackRelation(dim3D2);
                br2.Name = BackRelationName.Dim3_BooleanProduct2;
                br2.RelatedTo = boolobj;

                dim3D1.BackRelations.Add(br1);
                dim3D2.BackRelations.Add(br2);

                return boolobj;
            }
            else return null;
        }
        
        private GestureEnums processAngleCommands(RDF_Abstract rdf_parent, RDF_Node3D onNode, Viewport3D viewport)
        {
            // Todo: set the property at the top level, which is RDF_Face2D
            // don't deal with the arc for now

            // apply the property name to its children elements (such as each segments)
            double angle = onNode.Angle.Value;
            //set the constraint in the RDF engine
            //RDFWrapper.AddDataProperty(RDFWrapper.FilePtr, rdf_parent.PtrToConcept, "angle", Math.Round(angle, 4).ToString());
            // set up the constraint
            onNode.Constraint.Type = ConstraintType.Angle;
            onNode.Constraint.ConstraintName = GeometricConstraintEnum.Angle;
            onNode.Constraint.ConstraintObjects.Add(onNode.PreEdge);
            onNode.Constraint.ConstraintObjects.Add(onNode.NextEdge);
            onNode.Constraint.OriConstraintValue = angle;
            onNode.Constraint.TarConstraintValue = 90.0;
            onNode.Constraint.IsSolved = angle == 90.0;
            onNode.Constraint.onSolvedConstraint += Constraint_onSolvedConstraint;

            if (!onNode.Constraint.IsSolved)
            {
                // solve the constraint
                onNode.Constraint.SolveConstraint();
            }

            // remain the original gesture
            return this.gesture;
        }
        private GestureEnums processLengthCommands(RDF_Abstract rdf_parent, Viewport3D viewport)
        {
            if (GlobalVariables.RDFTimingObj.Count > 1)
            {
                var onLine = GlobalVariables.RDFTimingObj.Last();
                var baseLine = GlobalVariables.RDFTimingObj.First();
                if (onLine is RDF_Line3D && baseLine is RDF_Line3D)
                {
                    double orilength = ((RDF_Line3D)onLine).GetLength();
                    double tarlength = ((RDF_Line3D)baseLine).GetLength();
                    //set the constraint in the RDF engine
                    //RDFWrapper.AddDataProperty(RDFWrapper.FilePtr, rdf_parent.PtrToConcept, "length", Math.Round(tarlength, 4).ToString());

                    onLine.Constraint.Type = ConstraintType.Length;
                    onLine.Constraint.ConstraintName = GeometricConstraintEnum.SameLength;
                    onLine.Constraint.ConstraintObjects.Add(onLine);
                    onLine.Constraint.ConstraintObjects.Add(baseLine);
                    onLine.Constraint.OriConstraintValue = orilength;
                    onLine.Constraint.TarConstraintValue = tarlength;
                    onLine.Constraint.IsSolved = false;
                    onLine.Constraint.onSolvedConstraint += Constraint_onSolvedConstraint;

                    GlobalVariables.RDFTimingObj.Clear();
                    // solve the constraint
                    onLine.Constraint.SolveConstraint();
                }
            }
            return this.gesture;
        }
        private GestureEnums processAxisCommands(WireBase axis, Func<WireLine, Vector3D, bool, string, object> rotatePlane)
        {
            if (GlobalVariables.WireTimingObj.Count > 1)
            {
                var axis1 = GlobalVariables.WireTimingObj[0];
                if (axis1 is WireLine && axis is WireLine && (axis1.ID == "cxaxis" || axis1.ID == "cyaxis" || axis1.ID == "czaxis") && (axis.ID == "cxaxis" || axis.ID == "cyaxis" || axis.ID == "czaxis"))
                {
                    var axisLine = (WireLine)axis;
                    Matrix3D mtx = axis.Transform.Value;
                    Vector3D vec = new Vector3D(axisLine.Point2.X - axisLine.Point1.X, axisLine.Point2.Y - axisLine.Point1.Y, axisLine.Point2.Z - axisLine.Point1.Z);
                    //vec = mtx.Transform(vec);
                    rotatePlane((WireLine)axis1, vec, true, "");
                }
            }
            return this.gesture;
        }
        /// <summary>
        /// Fired when the constraints are solved
        /// </summary>
        /// <param name="constrainObj"></param>
        void Constraint_onSolvedConstraint(Constraint constrainObj)
        {
            var subject = constrainObj.Subject;
            if (subject is RDF_Node3D)
            {
                var node = (RDF_Node3D)subject;
                // cancel the constraint
                node.Constraint.onSolvedConstraint -= Constraint_onSolvedConstraint;

                var polyline = node.PreEdge.InPartOf;
                if (polyline is RDF_Polygon3D)
                {
                    foreach (var st in GlobalVariables.CanvasStrokes)
                    {
                        if (st is CustomStroke)
                        {
                            var customSt = (CustomStroke)st;
                            if (customSt.GUID == polyline.GUID)
                            {
                                // convert the wirepath points to sketch coordinate
                                List<System.Windows.Point> newcvtPts = new List<System.Windows.Point>();
                                var mtx = polyline.Transform.GetMatrix3D().Clone();
                                mtx.Invert();
                                int index = 0;
                                Point3D prept = new Point3D();
                                foreach (var pt3d in polyline.PathGeometry.Paths[0])
                                {
                                    var cvtpt = mtx.Transform(pt3d);
                                    if (index > 0)
                                    {
                                        double xstep = (cvtpt.X - prept.X) / 10;
                                        double ystep = (cvtpt.Y - prept.Y) / 10;
                                        // insert more points to mimic the drawing behavior
                                        for (int i = 1; i < 10; i++)
                                        {
                                            newcvtPts.Add(new System.Windows.Point(i * xstep + prept.X, i * ystep + prept.Y));
                                        }
                                    }
                                    newcvtPts.Add(new System.Windows.Point(cvtpt.X, cvtpt.Y));
                                    prept = cvtpt;
                                    index ++;
                                }

                                var clonedSt = customSt.Clone();
                                customSt.cvtPts = newcvtPts;
                                //customSt.UpdateCvtPoints(line, mtx, polygon.IsClosed);
                                if (CommandExecuted != null)
                                {
                                    CommandExecuted(clonedSt, customSt);
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        private GestureEnums processSelectionCommand(RDF_GeometricItem rdfgeom)
        {
            if (GlobalVariables.SelectedItem != rdfgeom)
            {
                rdfgeom.Select();
                if (GlobalVariables.IsErasing)
                {
                    if (OnObjectUnSelected != null)
                        OnObjectUnSelected(rdfgeom);
                    return GestureEnums.Erase;
                }
                else if(OnObjectSelected != null)
                    OnObjectSelected(rdfgeom);

                return GestureEnums.Point;
            }
            else if (!GlobalVariables.IsErasing)
            {
                GlobalVariables.SelectedItem.UnSelect();
                GlobalVariables.SelectedItem = null;
                if (OnObjectUnSelected != null)
                    OnObjectUnSelected(rdfgeom);

                return GestureEnums.None;
            }
            else
            {
                if (OnObjectUnSelected != null)
                    OnObjectUnSelected(rdfgeom);
                return GestureEnums.Erase;
            }
        }

        private RDF_Node3D validateAngleGesture(RDF_Polygon3D polygon, Viewport3D viewport)
        {
            double shortestDist = double.MaxValue;
            RDF_Node3D onNode = null;
            var transformMtx = polygon.Transform.GetMatrix3D().Clone();
            // to offset the polygon to origin
            Point3D firstPoint = polygon.Segments.First().StartNode.GetPoint3D();
            //transformMtx.Invert();
            foreach (var seg in polygon.Segments)
            {
                Point3D stpt = seg.StartNode.GetPoint3D();
                stpt = new Point3D(stpt.X - firstPoint.X, stpt.Y - firstPoint.Y, stpt.Z - firstPoint.Z);
                stpt = transformMtx.Transform(stpt);
                
                System.Windows.Point? screenPt = stpt.Point3DToScreen2D(viewport);
                //System.Diagnostics.Debug.WriteLine("pt location:" + screenPt.Value);
                if (screenPt != null)
                {
                    double dist = screenPt.Value.distanceTo(this.GestureLocation.X, this.GestureLocation.Y);
                    if (dist < 30 && shortestDist > dist)
                    {
                        shortestDist = dist;
                        onNode = seg.StartNode;
                    }
                }
            }

            return onNode;
        }

        private Point3D? validateAngleGesture2(HashSet<WireBase> hitedges, List<Point3D> hitplanePts)
        {
            var fstEdge = hitedges.First();
            var lstEdge = hitedges.Last();

            if (fstEdge.ID != lstEdge.ID)
            {
                var trans1 = RDFWrapper.All_RDF_Objects[fstEdge.ID];
                var trans2 = RDFWrapper.All_RDF_Objects[lstEdge.ID];

                if (trans1 != null && trans2 != null && trans1 is RDF_Transformation && trans2 is RDF_Transformation)
                {
                    var geom1 = (trans1 as RDF_Transformation).TransformGeom;
                    var geom2 = (trans2 as RDF_Transformation).TransformGeom;
                    if (geom1 is RDF_Polygon3D && geom2 is RDF_Polygon3D)
                    {
                        // get the closet edge and the points on edge
                        var dist1 = fstEdge.LineCollection.ToList<Point3D>().FindClosetPointOnPolyline(hitplanePts.First(), fstEdge.Transform.Value);
                        var dist2 = lstEdge.LineCollection.ToList<Point3D>().FindClosetPointOnPolyline(hitplanePts.First(), lstEdge.Transform.Value);

                        var poly1 = (RDF_Polygon3D)geom1;
                        var poly2 = (RDF_Polygon3D)geom2;

                        // rotate poly2
                        if (poly2.IsClosed && poly2.Face != null)
                        {
                            var mtx = poly2.Face.Transform.GetMatrix3D();
                            mtx.Invert();
                            var rotateOn = mtx.Transform(dist2.Item2);
                            //var rotateOn = 
                            poly2.Face.Constraint.onSolvedConstraint += Constraint_onSolvedConstraint;
                            RDFLibrary.ProcessRotationCommands(poly2.Face, poly1, rotateOn);
                            // populate the rotation according to constraints
                            foreach (var ctlObj in poly1.Controlling)
                            {
                                foreach (var cstshp in ctlObj.ConstraintObjects)
                                {
                                    if (cstshp is RDF_Face2D)
                                    {
                                        ((RDF_Face2D)cstshp).Constraint.onSolvedConstraint += Constraint_onSolvedConstraint;
                                        RDFLibrary.ProcessRotationCommands((RDF_Face2D)cstshp, poly1, rotateOn);
                                    }
                                }
                            }
                        }
                        // rotate poly1
                        else if (poly1.IsClosed && poly1.Face != null)
                        {
                            var mtx = poly1.Face.Transform.GetMatrix3D();
                            mtx.Invert();
                            var rotateOn = mtx.Transform(dist1.Item2);
                            //var rotateOn = 
                            poly1.Face.Constraint.onSolvedConstraint += Constraint_onSolvedConstraint;
                            RDFLibrary.ProcessRotationCommands(poly1.Face, poly2, rotateOn);
                            foreach (var ctlObj in poly2.Controlling)
                            {
                                foreach (var cstshp in ctlObj.ConstraintObjects)
                                {
                                    if (cstshp is RDF_Face2D)
                                    {
                                        ((RDF_Face2D)cstshp).Constraint.onSolvedConstraint += Constraint_onSolvedConstraint;
                                        RDFLibrary.ProcessRotationCommands((RDF_Face2D)cstshp, poly2, rotateOn);
                                    }
                                }
                            }
                        }
                        else
                        {

                        }

                        //GeomLib.BasicGeomLib.FindClosetPointOnPolyline()
                        // find the angle between the first and second edges
                        //fstEdge.Transform
                    }
                }
            }
            return null;
        }



        private RDF_Segment3D validateLengthGesture(RDF_Polygon3D polygon, Viewport3D viewport)
        {
            double shortestDist = double.MaxValue;
            RDF_Segment3D onSegment = null;
            var transformMtx = polygon.Transform.GetMatrix3D().Clone();
            Point3D firstPoint = polygon.Segments.First().StartNode.GetPoint3D();

            foreach (var seg in polygon.Segments)
            {
                Point3D stpt = seg.StartNode.GetPoint3D();
                stpt = new Point3D(stpt.X - firstPoint.X, stpt.Y - firstPoint.Y, stpt.Z - firstPoint.Z);
                stpt = transformMtx.Transform(stpt);

                Point3D endpt = seg.EndNode.GetPoint3D();
                endpt = new Point3D(endpt.X - firstPoint.X, endpt.Y - firstPoint.Y, endpt.Z - firstPoint.Z);
                endpt = transformMtx.Transform(endpt);

                System.Windows.Point? screenStPt = stpt.Point3DToScreen2D(viewport);
                System.Windows.Point? screenEndPt = endpt.Point3DToScreen2D(viewport);
                System.Windows.Point gestureLoc = new System.Windows.Point(this.GestureLocation.X, this.GestureLocation.Y);

                if (stpt != null && endpt != null)
                {
                    double dist = GeomLib.BasicGeomLib.GetShortestDistBtwLineSegAndPoint(screenStPt.Value, screenEndPt.Value, gestureLoc);
                    if (dist < 30 && shortestDist > dist)
                    {
                        shortestDist = dist;
                        onSegment = seg;
                    }
                }
            }

            return onSegment;
        }
    }
}
