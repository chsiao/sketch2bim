﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using System.Drawing;

using GeomLib.QuadTree;
using wpf = System.Windows;
using WrapperLibrary.Cornucopia;

namespace CanvasSketch
{
    public class ControlPoint : IHasRect
    {
        /// <summary>
        /// Bounds of this item
        /// </summary>
        private RectangleF boundingbox;
        private string guid;
        private wpf.Point center;
        private const int ctlr = 6;
        /// <summary>
        /// the default size of this item
        /// </summary>
        int m_size = 2;

        /// <summary>
        /// Create an item at the given location with the given size.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="size"></param>
        public ControlPoint(wpf.Point p, int size) : this()
        {
            m_size = size;
            boundingbox = new RectangleF((float)p.X, (float)p.Y, m_size, m_size);
            this.center = p;
            generatePresentation();
        }
        public ControlPoint()
        {
            this.guid = Guid.NewGuid().ToString();
            this.center = new wpf.Point(double.MinValue, double.MinValue);
            this.ConnPrimitives = new List<Primitive>();
        }

        public string GUID { get { return this.guid; } }
        public wpf.Point Center { 
            get 
            { 
                return this.center; 
            }
            set 
            {
                this.center = value;
                this.boundingbox = new RectangleF((float)this.center.X, (float)this.center.Y, m_size, m_size);
                generatePresentation();
            }
        }
        public List<Primitive> ConnPrimitives { get; set; }
        public double X
        {
            get
            {
                return this.center.X;
            }
        }
        public double Y
        {
            get
            {
                return this.center.Y;
            }
        }
        public System.Windows.Shapes.Ellipse Presentation { set; get; }

        public double distanceTo(double x, double y)
        {
            double diffX = this.X - x;
            double diffY = this.Y - y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }
        public void generatePresentation()
        {
            if (this.Presentation == null)
                this.Presentation = new Ellipse();
            Ellipse ellipse = this.Presentation;
       
            ellipse.Width = ctlr;
            ellipse.Height = ctlr;
            ellipse.StrokeThickness = 2;
            ellipse.Stroke = wpf.Media.Brushes.Gray;
            //ellipse.MouseEnter += ctl_MouseEnter;
            SketchCanvas.SetTop(ellipse, this.Center.Y - ctlr / 2);
            SketchCanvas.SetLeft(ellipse, this.Center.X - ctlr / 2);
        }
        #region IHasRect Members
        /// <summary>
        /// The rectangular bounds of this item
        /// </summary>
        public RectangleF Rectangle { get { return boundingbox; } }

        #endregion

        public static ControlPoint getStartCtlPoint(BasicPrimitive pm)
        {
            return new ControlPoint(new System.Windows.Point(pm.start.x, pm.start.y), 3);
        }
        public static ControlPoint getEndCtlPoint(BasicPrimitive pm)
        {
            return new ControlPoint(pm.getEndPoint(), 3);
        }
        public override string ToString()
        {
            return "(" + this.X + ", " +this.Y + ")";
        }
    }
}
