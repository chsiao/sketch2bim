﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Recognizer.NDollar;
using GeomLib;
using WrapperLibrary.RDF;
using Utility;

namespace CanvasSketch
{
    public static class Library
    {
        public static List<PointR> ConvertToPointRList(this Stroke stroke)
        {
            List<PointR> result = new List<PointR>();
            foreach (StylusPoint stpt in stroke.StylusPoints)
            {
                result.Add(new PointR(stpt.X, stpt.Y));
            }
            return result;
        }

        public static List<AForge.IntPoint> ConvertToIntPointList(int[,] input)
        {
            List<AForge.IntPoint> output = new List<AForge.IntPoint>();
            for (int i = 0; i < input.Length / 2; i++)
            {
                int x = input[i, 0],
                    y = input[i, 1];
                AForge.IntPoint intpoint = new AForge.IntPoint(x, y);
                output.Add(intpoint);
            }
            return output;
        }

        public static CustomStroke tryMergeTwoStrokes(RDF_Transformation edge1, RDF_Transformation edge2)
        {
            CustomStroke newstroke = null;
            RDF_Polygon3D pl1 = edge1.TransformGeom is RDF_Polygon3D ? (RDF_Polygon3D)edge1.TransformGeom : null;
            RDF_Polygon3D pl2 = edge2.TransformGeom is RDF_Polygon3D ? (RDF_Polygon3D)edge2.TransformGeom : null;

            if (pl1 != null && pl2 != null)
            {
                CustomStroke st1 = null;
                CustomStroke st2 = null;

                foreach (CustomStroke cst in GlobalVariables.CanvasStrokes)
                {
                    if (cst.GUID == pl1.GUID)
                        st1 = cst;
                    if (cst.GUID == pl2.GUID)
                        st2 = cst;
                }
                if (st1 != null && st2 != null)
                {
                    var clonedStroke = (CustomStroke)st1.Clone();
                    bool ismerged = clonedStroke.TryMergeIntoCurves(st2);
                    if (ismerged)
                    {
                        pl2.Remove();
                        newstroke = clonedStroke;
                    }
                }
            }
            return newstroke;
        }

        public static bool tryMergeWithSelectedStroke(ref Matrix3D mtx, ref CustomStroke newStroke)
        {
            bool ismerged = false;
            RDF_Face2D rdfface = null;
            if (GlobalVariables.SelectedItem is RDF_Face2D)
                rdfface = (RDF_Face2D)GlobalVariables.SelectedItem;
            else if (GlobalVariables.SelectedItem is RDF_SweptAreaSolid)
            {
                RDF_SweptAreaSolid sweptsolid = (RDF_SweptAreaSolid)GlobalVariables.SelectedItem;
                rdfface = (RDF_Face2D)sweptsolid.SweptProfile.Face;
            }

            RDF_Polygon3D selPolygon = GlobalVariables.SelectedItem is RDF_Polygon3D ? (RDF_Polygon3D)GlobalVariables.SelectedItem :
                rdfface != null ? rdfface.Outer : null;
            if (selPolygon != null)
            {
                CustomStroke selStroke = null;
                // find the selected stroke
                foreach (var stroke in GlobalVariables.CanvasStrokes)
                {
                    CustomStroke cstroke = (CustomStroke)stroke;
                    if (selPolygon.GUID == cstroke.GUID)
                    {
                        selStroke = cstroke;
                        break;
                    }
                }
                // if found, start to merge the two strokes
                if (selStroke != null)
                {
                    var cloneSelStroke = (CustomStroke)selStroke.Clone();
                    // see if the two polygons are closed enough to have a merger
                    ismerged = cloneSelStroke.TryMergeIntoCurves(newStroke);

                    // If merged, switch the newStroke to the selected stroke in order for rendering
                    if (ismerged)
                    {
                        newStroke = cloneSelStroke;
                        mtx = newStroke.matrix;
                    }
                }
            }
            return ismerged;
        }
    }
}
