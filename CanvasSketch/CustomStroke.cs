﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input.StylusPlugIns;
using System.Windows.Input;
using System.Windows.Ink;
using d2d = System.Drawing;
using System.Linq;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using WrapperLibrary;
using WrapperLibrary.Cornucopia;
using AForge.Math.Geometry;
using Petzold.Media3D;
using System.Windows.Media.Media3D;
using GeomLib;
using Utility;

namespace CanvasSketch
{
    // A class for rendering custom strokes
    public class CustomStroke : Stroke
    {
        Brush brush;
        Pen pen;
        private List<BasicBezier> curves;
        private List<Primitive> primitives;
        /// <summary>
        /// the point list for curve fitting process
        /// this is a local point list on 2D space, with the points that are close to 0,0
        /// </summary>
        internal List<Point> cvtPts = null;
        private const double factor = 10;
        private string guid = "";
        private const double mergeDist = 20;
        public Matrix3D matrix = new Matrix3D();

        public CustomStroke(StylusPointCollection stylusPoints, List<Point> cvtPoints, Matrix3D mtx)
            : base(stylusPoints)
        {
            // Create the Brush and Pen used for drawing.
            this.brush = null;//new LinearGradientBrush(Colors.Red, Colors.Blue, 1d);
            this.pen = new Pen(Brushes.Gray, 2d);
            this.curves = new List<BasicBezier>();
            this.primitives = new List<Primitive>();
            this.isUsingBezierCurves = false;
            this.cvtPts = cvtPoints;
            this.guid = Guid.NewGuid().ToString();
            this.matrix = mtx;
        }

        public CustomStroke(StylusPointCollection stylusPoints, bool isFittingBezierCurves = false)
            : base(stylusPoints)
        {
            // Create the Brush and Pen used for drawing.
            this.brush = null;//new LinearGradientBrush(Colors.Red, Colors.Blue, 1d);
            this.pen = new Pen(Brushes.Gray, 2d);
            this.curves = new List<BasicBezier>();
            this.primitives = new List<Primitive>();
            this.isUsingBezierCurves = isFittingBezierCurves;
            this.guid = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// This will first find the closed nodes and merge into the node in this stroke
        /// then insert the rest of newly created nodes into quadtree
        /// </summary>
        /// <param name="quadtree"></param>
        public void setQuadTree(GeomLib.QuadTree.QuadTree<ControlPoint> quadtree)
        {
            mergeintoExistingNodes(quadtree);

            foreach (ControlPoint ctlpoint in this.ControlPts)
            {
                // Todo: need to recompute the quadtree when point is changed
                quadtree.Insert(ctlpoint);
            }
        }

        private void mergeintoExistingNodes(GeomLib.QuadTree.QuadTree<ControlPoint> quadtree)
        {
            List<ControlPoint> newpts = this.ControlPts.FindAll(pt => pt.ConnPrimitives.Count == 1).ToList();

            // find whether the newly created stroke should connect to existing strokes
            for (int i = 0; i < newpts.Count; i++)
            {
                double mindist = double.MaxValue;
                ControlPoint connectTo = null;
                int ptindex = 0;

                ControlPoint testPt = newpts[i];
                d2d.RectangleF testArea = new d2d.RectangleF((float)testPt.X - 50, (float)testPt.Y - 50, 100f, 100f);
                List<ControlPoint> candidates = quadtree.Query(testArea);
                foreach (ControlPoint candidate in candidates)
                {
                    double newdist = candidate.distanceTo(testPt.X, testPt.Y);
                    if (mindist > newdist)
                    {
                        mindist = newdist;
                        connectTo = candidate;
                        ptindex = i;
                    }
                }

                // if found one..
                if (mindist < mergeDist && connectTo != null)
                {
                    List<Primitive> connPrims = newpts[ptindex].ConnPrimitives;
                    foreach (Primitive prim in connPrims)
                    {
                        if (prim.NextNode == newpts[ptindex])
                            prim.NextNode = connectTo;
                        else if (prim.PreviousNode == newpts[ptindex])
                            prim.PreviousNode = connectTo;
                    }
                    connectTo.ConnPrimitives.AddRange(connPrims);
                    newpts[ptindex] = connectTo;
                }
            }
        }

        public bool TryMergeIntoCurves(CustomStroke mergingCurve)
        {
            List<Point> oriPts = new List<Point>(this.DrawnPts);
            List<Point> mergingPts = new List<Point>(mergingCurve.DrawnPts);
            var mergingMtx = mergingCurve.matrix.Clone();
            mergingMtx.Invert();
            var thisMtx = this.matrix.Clone();
            var cvtMtx = mergingMtx * thisMtx;

            bool isClosed = oriPts.First().distanceTo(oriPts.Last()) < 5;

            // temporary sollution to translate the points
            // to the right location when the polygon is created on existing polygon
            for (int j = 0; j < mergingPts.Count; j++)
            {
                var locMergingPt = mergingPts[j];
                var locMergingPt3D = new Point3D(locMergingPt.X, locMergingPt.Y, 0);
                var cvtMergingPt3D = cvtMtx.Transform(locMergingPt3D);
                var tmpPt3D = mergingMtx.Transform(locMergingPt3D);
                tmpPt3D = thisMtx.Transform(tmpPt3D);

                mergingPts[j] = new Point(cvtMergingPt3D.X, cvtMergingPt3D.Y);
            }
                
            for (int j = 0; j < oriPts.Count; j++)
                oriPts[j] = new Point(oriPts[j].X, oriPts[j].Y);
            //var oriPLArea = oriPts.PolygonArea();
            //if (oriPLArea > 0) oriPts.Reverse();
            Point stpoint = mergingPts[0];
            Point endpoint = mergingPts[mergingPts.Count - 1];

            double sDistStart = GlobalConstant.SketchMergeThreshold;
            double sDistEnd = GlobalConstant.SketchMergeThreshold;
            double toVtxStart = -1;
            double toVtxEnd = -1;
            Point sPtSt = new Point();
            Point sPtEnd = new Point();
            int connStart = -1;
            int connEnd = -1;
            bool isInserting = false;

            int i = 0;
            foreach (var pt in oriPts)
            {
                if (i + 1 >= oriPts.Count && !isClosed) break; 
                int nextI = i + 1 >= oriPts.Count ? 0 : i + 1;
                Point B = oriPts[nextI];

                Point? tsPtSt = BasicGeomLib.GetProjectedPointNearLineSeg(pt, B, stpoint);
                Point? tsPtEnd = BasicGeomLib.GetProjectedPointNearLineSeg(pt, B, endpoint);
                int inserting = 0;

                if (tsPtSt.HasValue)
                {
                    double tmpDistSt = tsPtSt.Value.distanceTo(stpoint);
                    if (tmpDistSt < sDistStart)
                    {
                        double toI = tsPtSt.Value.distanceTo(pt);
                        double toNext = tsPtSt.Value.distanceTo(B);

                        sPtSt = tsPtSt.Value;
                        sDistStart = tmpDistSt;
                        connStart = toI < toNext ? i : nextI;
                        toVtxStart = toI < toNext ? toI : toNext;
                        inserting++;
                    }
                }
                else
                {
                    double tmpDistSt = pt.distanceTo(stpoint);
                    if (tmpDistSt < sDistStart)
                    {
                        sPtSt = pt;
                        sDistStart = tmpDistSt;
                        connStart = i;
                    }
                }

                if (tsPtEnd.HasValue)
                {
                    double tmpDistEnd = tsPtEnd.Value.distanceTo(endpoint);
                    if (tmpDistEnd < sDistEnd)
                    {
                        double toI = tsPtEnd.Value.distanceTo(pt);
                        double toNext = tsPtEnd.Value.distanceTo(B);

                        sPtEnd = tsPtEnd.Value;
                        sDistEnd = tmpDistEnd;
                        connEnd = toI < toNext ? i : nextI;
                        toVtxEnd = toI < toNext ? toI : toNext;
                        inserting++;
                    }
                }
                else
                {
                    double tmpDistEnd = pt.distanceTo(endpoint);
                    if (tmpDistEnd < sDistEnd)
                    {
                        sPtEnd = pt;
                        sDistEnd = tmpDistEnd;
                        connEnd = nextI;
                    }

                }
                isInserting = inserting == 2 ? true : inserting == 1 ? false : isInserting;
                i++;
            }

            bool ismerged = false;
            List<Point> newPts = new List<Point>();
            // When modifying a closed profile
            if (connStart >= 0 && connEnd >= 0 && isClosed)
            {
                // when the to end of polylines are close to one vertex
                if (connStart == connEnd && toVtxEnd > toVtxStart)
                    connEnd += connEnd;

                //int realstart = connStart + 1;
                //int removeCount = Math.Abs(connEnd - realstart);
                int removeCount = Math.Abs(connEnd - connStart);

                // when there is no need to remove the points
                // we need to check whether the merging points need to be reversed or not
                bool isreversed = false;
                if (removeCount == 0)
                {
                    var chkstart = oriPts[connStart];
                    var distSt = chkstart.distanceTo(stpoint);
                    var distEnd = chkstart.distanceTo(endpoint);
                    if (distEnd < distSt)
                    {
                        mergingPts.Reverse();
                        isreversed = true;
                    }
                }
                // switch the start and end point to the intersection point
                if (isreversed)
                {
                    mergingPts[mergingPts.Count - 1] = sPtSt;
                    mergingPts[0] = sPtEnd;
                }
                else
                {
                    mergingPts[0] = sPtSt;
                    mergingPts[mergingPts.Count - 1] = sPtEnd;
                }
                List<Point> cad1 = new List<Point>(oriPts);
                List<Point> cad2 = new List<Point>(oriPts);
                List<Point> merg1 = new List<Point>(mergingPts);
                List<Point> merg2 = new List<Point>(mergingPts);

                if (connStart < connEnd)
                {
                    merg1.Reverse();
                    if (cad1.Count > connEnd)
                    {
                        cad1.RemoveRange(connEnd, cad1.Count - connEnd);
                        cad1.RemoveRange(0, connStart + 1);
                        cad1.AddRange(merg1);
                        // close the loop
                        if (isClosed)
                            cad1.Add(cad1.First());
                    }
                }
                else
                {
                    //mergingPts.Reverse();
                    cad1.RemoveRange(connStart, cad1.Count - connStart);
                    cad1.RemoveRange(0, connEnd);
                    cad1.AddRange(merg1);
                    if (isClosed)
                        cad1.Add(cad1.First());
                }

                int realstart = connStart;
                if (connStart + 1 > connEnd)
                {
                    merg2.Reverse();
                    realstart = connEnd;
                }
                if (isInserting)
                {
                    removeCount = 0;
                    realstart++;
                }
                if (realstart + removeCount < cad2.Count)
                {
                    cad2.RemoveRange(realstart, removeCount);
                    // decide whether we need to remove one more point in the list or not
                    int lindex = realstart + removeCount;
                    int llindex = lindex - 1 < 0 ? oriPts.Count - 1 : lindex - 1;
                    Vector lastV = oriPts[lindex] - oriPts[llindex];
                    Vector newLastV = oriPts[lindex] - merg2[merg2.Count - 1];
                    lastV.Normalize();
                    newLastV.Normalize();
                    var angle = Vector.AngleBetween(lastV, newLastV);
                    if(Math.Abs(angle) > 1)
                    {
                        // remove one more
                        cad2.RemoveRange(realstart, 1);
                    }
                }
                cad2.InsertRange(realstart, merg2);

                
                var area1 = cad1.PolygonArea();
                var area2 = cad2.PolygonArea();
                if (Math.Abs(area1) > Math.Abs(area2))
                    newPts = cad1;
                else
                    newPts = cad2;
                    
                ismerged = true;
            }
            // add at the starting index
            else if (connStart >= 0 && connEnd == -1 && !isClosed)
            {
                if (connStart <= 1)
                {
                    mergingPts.Reverse();
                    if (oriPts.Count == 2 && connStart == 1)
                    {
                        mergingPts.Reverse();
                        oriPts.AddRange(mergingPts);
                    }
                    else
                        oriPts.InsertRange(0, mergingPts);
                }
                else
                    oriPts.InsertRange(oriPts.Count, mergingPts);
                newPts = oriPts;
                ismerged = true;
            }
            // add at the ending index
            else if (connEnd >= 0 && connStart == -1 && !isClosed)
            {
                if (connEnd == 0)
                    oriPts.InsertRange(0, mergingPts);
                else
                {
                    mergingPts.Reverse();
                    oriPts.InsertRange(oriPts.Count, mergingPts);
                }
                newPts = oriPts;
                ismerged = true;
            }
            else if (connStart >= 0 && connEnd >= 0 && !isClosed)
            {
                // connect the end of original points to the start of the new stroke
                if (oriPts.Count - connEnd <= 3 && connStart < 3)
                {
                    // reverse the mergingpts and add it to the end of original point
                    mergingPts.Reverse();
                    oriPts.AddRange(mergingPts);
                    newPts = oriPts;
                    ismerged = true;
                }
                else if (oriPts.Count - connStart <= 3 && connEnd < 3)
                {
                    oriPts.AddRange(mergingPts);
                    newPts = oriPts;
                    ismerged = true;
                }
                //else if (oriPts.Count - connStart <= 3 && connEnd < 3)
                //{
                //    oriPts.RemoveRange(connStart, oriPts.Count - connStart);
                //    oriPts.RemoveRange(0, connEnd);
                //    oriPts.InsertRange(oriPts.Count, mergingPts);
                //    newPts = oriPts;
                //    ismerged = true;
                //}
                //else if (connStart < 3 && oriPts.Count - connEnd <= 3)
                //{
                //    oriPts.RemoveRange(connEnd, oriPts.Count - connEnd);
                //    oriPts.RemoveRange(0, connStart);
                //    mergingPts.Reverse();
                //    oriPts.InsertRange(oriPts.Count, mergingPts);
                //    newPts = oriPts;
                //    ismerged = true;
                //}
            }
            // assign new offset
            if (ismerged)
            {
                List<Point> cvtNewPts = new List<Point>();
                foreach (var pt in newPts)
                    cvtNewPts.Add(new Point(pt.X, pt.Y));

                this.cvtPts = newPts; //cvtNewPts;
            }
            else
            {
            }
                return ismerged;
        }

        public void FitintoCurves(double speed = -1)
        {
            this.primitives = new List<Primitive>();
            // Allocate memory to store the previous point to draw from.
            Point prevPoint = new Point(double.NegativeInfinity,
                                        double.NegativeInfinity);
#if false //DEBUG
            // Draw linear gradient ellipses between 
            // all the StylusPoints in the Stroke.
            for (int i = 0; i < this.StylusPoints.Count; i++)
            {
                Point pt = (Point)this.StylusPoints[i];
                Vector v = Point.Subtract(prevPoint, pt);

                // Only draw if we are at least 4 units away 
                // from the end of the last ellipse. Otherwise, 
                // we're just redrawing and wasting cycles.
                if (v.Length > 4)
                {
                    // Set the thickness of the stroke 
                    // based on how hard the user pressed.
                    double radius = 1d;//this.StylusPoints[i].PressureFactor * 10d;
                    drawingContext.DrawEllipse(brush, pen, pt, radius, radius);
                    
                    prevPoint = pt;
                }
            }
#endif

            // fit the points into either primitives (spline) or bezier curves
            int size = this.cvtPts.Count;

            int[,] pts = new int[size, 2];
            int prex = int.MaxValue;
            int prey = int.MaxValue;
            int j = 0;
            for (int i = 0; i < size; i++)
            {
                int curx = 0;
                int cury = 0;

                if (this.cvtPts != null)
                {
                    curx = (int)(this.cvtPts[i].X * factor);
                    cury = (int)(this.cvtPts[i].Y * factor);
                }
                else
                {
                    System.Windows.Input.StylusPoint pt = this.StylusPoints[i];
                    curx = (int)pt.X;
                    cury = (int)pt.Y;
                }

                if (curx == prex && cury == prey)
                    continue;
                pts[j, 0] = curx;
                pts[j, 1] = cury;
                prex = curx;
                prey = cury;
                j++;
            }

            if (j < 3) return;

            int[,] resizePts = new int[j, 2];
            Array.Copy(pts, resizePts, j * 2);

            // fit into the splines
            if (this.isUsingBezierCurves)
            {
                fitintoBasicBeziers(resizePts, j);
            }
            else
            {
                fitintoBasicPrimitives(resizePts, j, speed);
            }
        }
        /// <summary>
        /// We are using this as default right now as the proposed 3D library can only use curve for generate 3D objects
        /// </summary>
        /// <param name="pts"></param>
        /// <param name="size"></param>
        private void fitintoBasicPrimitives(int[,] pts, int size, double speed)
        {
            if (size > 1)
            {
                SimpleShapeChecker shape = new SimpleShapeChecker();
                this.primitives = new List<Primitive>();

                AForge.Point center = new AForge.Point();
                float radius;
                // making a circle
                if (shape.IsCircle(Library.ConvertToIntPointList(pts), out center, out radius))
                {
                    //Point pt1 = new Point();
                    //Point pt2 = new Point(-radius, radius);
                    //Point pt3 = new Point(-2 * radius, 0);
                    //Point pt4 = new Point(-radius, -radius);

                    BasicPrimitive bpm = new BasicPrimitive();
                    bpm.type = BasicPrimitive.PrimitiveType.ARC;
                    bpm.startAngle = Math.PI / 2;
                    bpm.startCurvature = 1 / radius;
                    bpm.length = radius * Math.PI / 2;
                    bpm.start = new SPoint();
                    bpm.start.x = center.X + radius;
                    bpm.start.y = center.Y;
                    bpm.Scale(1 / factor);

                    BasicPrimitive bpm2 = new BasicPrimitive();
                    bpm2.type = BasicPrimitive.PrimitiveType.ARC;
                    bpm2.startAngle = -Math.PI;
                    bpm2.startCurvature = 1 / radius;
                    bpm2.length = radius * Math.PI / 2;
                    bpm2.start = new SPoint();
                    bpm2.start.x = center.X;
                    bpm2.start.y = center.Y + radius;
                    bpm2.Scale(1 / factor);

                    BasicPrimitive bpm3 = new BasicPrimitive();
                    bpm3.type = BasicPrimitive.PrimitiveType.ARC;
                    bpm3.startAngle = -Math.PI / 2;
                    bpm3.startCurvature = 1 / radius;
                    bpm3.length = radius * Math.PI / 2;
                    bpm3.start = new SPoint();
                    bpm3.start.x = center.X - radius;
                    bpm3.start.y = center.Y;
                    bpm3.Scale(1 / factor);

                    BasicPrimitive bpm4 = new BasicPrimitive();
                    bpm4.type = BasicPrimitive.PrimitiveType.ARC;
                    bpm4.startAngle = 0;
                    bpm4.startCurvature = 1 / radius;
                    bpm4.length = radius * Math.PI / 2;
                    bpm4.start = new SPoint();
                    bpm4.start.x = center.X;
                    bpm4.start.y = center.Y - radius;
                    bpm4.Scale(1 / factor);

                    ControlPoint cpt1 = new ControlPoint(new Point((center.X + radius) / factor, center.Y / factor), 3);
                    ControlPoint cpt2 = new ControlPoint(new Point((center.X) / factor, (center.Y + radius) / factor), 3);
                    ControlPoint cpt3 = new ControlPoint(new Point((center.X - radius) / factor, center.Y / factor), 3);
                    ControlPoint cpt4 = new ControlPoint(new Point(center.X / factor, (center.Y - radius) / factor), 3);
                    
                    Primitive pm = new Primitive(bpm, this);
                    pm.PreviousNode = cpt1;
                    pm.NextNode = cpt2;
                    this.primitives.Add(pm);

                    Primitive pm2 = new Primitive(bpm2, this);
                    pm2.PreviousNode = cpt2;
                    pm2.NextNode = cpt3;
                    this.primitives.Add(pm2);

                    Primitive pm3 = new Primitive(bpm3, this);
                    pm3.PreviousNode = cpt3;
                    pm3.NextNode = cpt4;
                    this.primitives.Add(pm3);

                    Primitive pm4 = new Primitive(bpm4, this);
                    pm4.PreviousNode = cpt4;
                    pm4.NextNode = cpt1;
                    this.primitives.Add(pm4);

                    cpt1.ConnPrimitives.Add(pm);
                    cpt1.ConnPrimitives.Add(pm2);
                    cpt2.ConnPrimitives.Add(pm2);
                    cpt2.ConnPrimitives.Add(pm3);
                    cpt3.ConnPrimitives.Add(pm3);
                    cpt3.ConnPrimitives.Add(pm4);
                    cpt4.ConnPrimitives.Add(pm4);
                    cpt4.ConnPrimitives.Add(pm);
                    this.isCircle = true;
                }
                else
                {
                    // get basic primitives from the fitting
                    IntPtr curvedata = new IntPtr();
                    //System.Diagnostics.Debug.WriteLine(300000 / speed);
                    int arccost = (int)Math.Pow(300000 / speed, 3) - 250;
                    //arccost = arccost > 0 ? arccost / 10 : 0;
                    //arccost = speed == -1 ? 20 : arccost;
                    //arccost = arccost < 10 ? 1 : arccost > 30 ? 1000 : 20;

                    arccost = 20; 
                    System.Diagnostics.Debug.WriteLine("arc cost: " + arccost);

                    IntPtr ptrPMResult = cornucopia.getBasicPrimitives(pts, size, 6, arccost);
                    ControlPoint preEndPt = new ControlPoint();
                    int originIndex = 0;
                    double mindist = double.MaxValue;
                    for (int i = 0; i < pts[0, 0]; i++)
                    {
                        BasicPrimitive bpm = ptrPMResult.ElementAt<BasicPrimitive>(i);
                        bpm.Scale(1 / factor);
                        ControlPoint stpt = ControlPoint.getStartCtlPoint(bpm);
                        ControlPoint endpt = ControlPoint.getEndCtlPoint(bpm);

                        var curdist = stpt.distanceTo(0, 0);
                        if (curdist < mindist)
                        {
                            mindist = curdist;
                            originIndex = i;
                        }
#if DEBUG
                        //System.Diagnostics.Debug.WriteLine("stpt: " + stpt.ToString());
                        //System.Diagnostics.Debug.WriteLine("endpt: " + endpt.ToString());
#endif
                        // bad data
                        if (/*stpt.X - (int)stpt.X == 0 ||*/ (bpm.type == BasicPrimitive.PrimitiveType.ARC && 1 / bpm.startCurvature > 10000))
                            continue;

                        if (Math.Round(stpt.X - preEndPt.X, 4) == 0 && Math.Round(stpt.Y - preEndPt.Y, 4) == 0)
                            stpt = preEndPt;

                        Primitive pm = new Primitive(bpm, this);
                        pm.PreviousNode = stpt;
                        pm.NextNode = endpt;
                        this.primitives.Add(pm);

                        stpt.ConnPrimitives.Add(pm);
                        endpt.ConnPrimitives.Add(pm);

                        preEndPt = endpt;
                    }
                    List<Primitive> orderedPrimitives = new List<Primitive>(this.primitives);
                    var dist = this.cvtPts.Last().distanceTo(this.cvtPts.First());
                    if (dist < 5 && orderedPrimitives.Count > 0)
                    {
                        orderedPrimitives.RemoveRange(0, originIndex);
                        for (int i = 0; i < originIndex; i++)
                            orderedPrimitives.Add(this.primitives[i]);
                        this.primitives = orderedPrimitives;
                    }
                    else
                        this.primitives = orderedPrimitives;
                }
            }
        }
        /// <summary>
        /// This is not finished yet as we are not creating control points and add the node and edge relationships into the geometry
        /// </summary>
        /// <param name="pts"></param>
        /// <param name="size"></param>
        private void fitintoBasicBeziers(int[,] pts, int size)
        {
            this.curves = new List<BasicBezier>();
            // get Bezier curves from the fitting process
            IntPtr curvedata = new IntPtr();
            IntPtr ptrBZRResult = cornucopia.getBasicBezier(pts, size, 6);
            for (int i = 0; i < pts[0, 0]; i++)
            {
                int offset = Marshal.SizeOf(typeof(SPoint)) * i * 4;
                IntPtr offsetPtr = ptrBZRResult.Increment(offset);
                SPoint[] ctrPts = new SPoint[4];
                for (int j = 0; j < 4; j++)
                {
                    ctrPts[j] = offsetPtr.ElementAt<SPoint>(j);
                }
                BasicBezier bez = new BasicBezier();
                bez.controlPoint = ctrPts;
                this.curves.Add(bez);
                // Todo: Add the control points here
            }
        }

        private void drawBezierCurves(DrawingContext context, List<BasicBezier> curves)
        {
            // draw the bezier curves from the results of fitting process
            PathSegmentCollection pscollection = new PathSegmentCollection();
            PathFigure pf = new PathFigure();
            pf.Segments = pscollection;
            pf.StartPoint = new Point(curves[0].controlPoint[0].x, curves[0].controlPoint[0].y);

            context.DrawEllipse(brush, pen, pf.StartPoint, 5, 5);
            
            for (int i = 0; i < curves.Count; i++)
            {
                BasicBezier curve = curves[i];
                Point pt1 = new Point(curve.controlPoint[1].x, curve.controlPoint[1].y);
                Point pt2 = new Point(curve.controlPoint[2].x, curve.controlPoint[2].y);
                Point pt3 = new Point(curve.controlPoint[3].x, curve.controlPoint[3].y);
#if false//DEBUG
                context.DrawEllipse(Brushes.Red, null, pt1, 5, 5);
                context.DrawEllipse(Brushes.Green, null, pt2, 5, 5);
#endif
                context.DrawEllipse(brush, pen, pt3, 2, 2);

                BezierSegment segment = new BezierSegment(pt1, pt2, pt3, true);
                pscollection.Add(segment);
            }

            PathFigureCollection pfcollection = new PathFigureCollection();
            pfcollection.Add(pf);

            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures = pfcollection;

            Brush curvebrush = Brushes.Blue;
            Pen curvepen = new Pen(Brushes.Black, 3);
            context.DrawGeometry(null, curvepen, pathGeometry);
        }

        public void drawPrimitives(System.Windows.Controls.InkCanvas canvas)
        {
            Geometry path = this.GetPathData();

            Brush curvebrush = new SolidColorBrush(Color.FromArgb(200,200,200,0));

            if (this.Presentation == null)
            {
                this.Presentation = new Path();
                canvas.Children.Add(this.Presentation);
            }
                
            this.Presentation.StrokeThickness = 3;
            this.Presentation.Stroke = curvebrush;
            this.Presentation.Data = path;

            //// draw contorl points
            foreach (ControlPoint ctlpt in this.ControlPts)
            {
                if (!canvas.Children.Contains(ctlpt.Presentation))
                    canvas.Children.Add(ctlpt.Presentation);
            }
        }

        public Geometry GetPathData()
        {
            PathSegmentCollection pscollection = new PathSegmentCollection();
            PathFigure pf = new PathFigure();
            pf.Segments = pscollection;
            pf.StartPoint = new Point(this.primitives[0].PreviousNode.X, primitives[0].PreviousNode.Y);

            foreach (Primitive pm in primitives)
            {
                if (pm.Segment != null)
                    pscollection.Add(pm.Segment);
            }

            PathFigureCollection pfcollection = new PathFigureCollection();
            pfcollection.Add(pf);

            PathGeometry path = new PathGeometry();
            path.Figures = pfcollection;

            return path;
        }

        public PathGeometry3D GetPathData3D(Matrix3D hostmatrix)
        {
            PathSegment3DCollection pscollection = new PathSegment3DCollection();
            
            Point3D stpt = new Point3D(this.primitives[0].PreviousNode.X, primitives[0].PreviousNode.Y, 0);
            stpt = hostmatrix.Transform(stpt);

            PathFigure3D pf = new PathFigure3D();
            pf.Segments = pscollection;
            pf.StartPoint = stpt;

            foreach (Primitive pm in primitives)
            {
                if (pm.Segment3D != null || pm.generateSegment3D(hostmatrix))
                    pscollection.Add(pm.Segment3D);
            }

            PathFigure3DCollection pfcollection = new PathFigure3DCollection();
            pfcollection.Add(pf);

            PathGeometry3D path = new PathGeometry3D();
            path.Figures = pfcollection;

            return path;
        }

        public void UpdateCvtPoints(WirePath line, Matrix3D? mtx = null, bool isclosed = false, bool isInserting = false)
        {
            if (line != null)
            {

                this.cvtPts.Clear();
                var invertMtx = mtx != null ? mtx.Value.Clone() : this.matrix;
                if (mtx != null)
                    invertMtx.Invert();
                foreach (var pt in line.Paths.First())
                {
                    //Point3D newpt = invertMtx.Transform(pt);
                    Point newcvtPt = new Point(pt.X, pt.Y); // since it's a local coords pt, it only has X and Y
                    this.cvtPts.Add(newcvtPt);
                }
                if (this.cvtPts.Last().distanceTo(this.cvtPts.First()) > 0.1 && isclosed)
                {
                    this.cvtPts.Add(this.cvtPts.First());
                }
                if (this.cvtPts.Count == 3 && isInserting)
                {
                    var midPt = new Point((this.cvtPts.First().X + this.cvtPts.Last().X) / 2, (this.cvtPts.First().Y + this.cvtPts.Last().Y) / 2);
                    this.cvtPts.Insert(this.cvtPts.Count - 1, midPt);
                }
                this.matrix = invertMtx;
            }
        }

        public static List<Point> CreateConvertedPoints(WirePath line, Matrix3D mtx, bool isclosed = false)
        {
            List<Point> cvtPts = new List<Point>();
            Matrix3D invertMtx = mtx.Clone();
            invertMtx.Invert();
            foreach (var pt in line.Paths.First())
            {
                //Point3D newpt = invertMtx.Transform(pt);
                Point newcvtPt = new Point(pt.X, pt.Y); // since it's a local coords pt, it only has X and Y
                cvtPts.Add(newcvtPt);
            }
            if (cvtPts.Last().distanceTo(cvtPts.First()) > 0.1 && isclosed)
            {
                cvtPts.Add(cvtPts.First());
            }
            return cvtPts;
        }

        public void ctl_MouseEnter(object sender, MouseEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public List<BasicBezier> BezierCurves { get { return this.curves; } }
        public List<Primitive> Primitives { get { return this.primitives; } }
        public bool isUsingBezierCurves { get; set; }
        public List<ControlPoint> ControlPts
        {
            get
            {
                Dictionary<string, ControlPoint> allctlPts = new Dictionary<string, ControlPoint>();
                foreach (Primitive pm in this.primitives)
                {
                    ControlPoint pnode = pm.PreviousNode;
                    ControlPoint nnode = pm.NextNode;
                    if(!allctlPts.ContainsKey(pnode.GUID))
                        allctlPts.Add(pnode.GUID, pnode);
                    if(!allctlPts.ContainsKey(nnode.GUID))
                        allctlPts.Add(nnode.GUID, nnode);
                }
                return allctlPts.Values.ToList<ControlPoint>();
            }
        }
        public string GUID { get { return this.guid; } set { this.guid = value; } }
        public Path Presentation { get; set; }
        public List<Point> DrawnPts { get { return this.cvtPts; } }
        public void TranslateStroke(Point3D offset)
        {
            // transform all the cvtpts
            for(int i=0;i<cvtPts.Count;i++)
            {
                var cvtpt = cvtPts[i];
                cvtpt.X += (double)offset.X;
                cvtpt.Y += (double)offset.Y;
                cvtPts[i] = cvtpt;
            }

            this.matrix.OffsetX -= offset.X;
            this.matrix.OffsetX -= offset.Y;
            this.matrix.OffsetZ -= offset.Z;

            this.FitintoCurves();
        }
        public bool isCircle { get; private set; }


        #region Stroke override
        protected override void DrawCore(DrawingContext drawingContext,
                                        DrawingAttributes drawingAttributes)
        {
            if (this.isUsingBezierCurves && this.curves.Count > 0)
            {
                drawBezierCurves(drawingContext, this.curves);
            }
            else if (this.primitives.Count > 0)
            {
                //drawPrimitives(drawingContext);
            }
        }
        public override Stroke Clone()
        {
            var clonestroke = base.Clone();

            return clonestroke;
        }
        #endregion
    }
}