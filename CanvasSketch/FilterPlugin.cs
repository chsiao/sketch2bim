﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Input.StylusPlugIns;
using System.Windows.Input;
using System.Windows.Ink;

namespace CanvasSketch
{
    class FilterPlugin : StylusPlugIn
    {
        protected override void OnStylusDown(RawStylusInput rawStylusInput)
        {
            if (rawStylusInput.StylusDeviceId == 2) return;
            // Call the base class before modifying the data. 
            base.OnStylusDown(rawStylusInput);
        }

        protected override void OnStylusMove(RawStylusInput rawStylusInput)
        {
            if (rawStylusInput.StylusDeviceId == 2) return;
            // Call the base class before modifying the data. 
            base.OnStylusMove(rawStylusInput);
        }

        protected override void OnStylusUp(RawStylusInput rawStylusInput)
        {
            if (rawStylusInput.StylusDeviceId == 2) return;
            // Call the base class before modifying the data. 
            base.OnStylusUp(rawStylusInput);
        }
    }
}
