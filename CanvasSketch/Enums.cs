﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CanvasSketch
{
    public enum GestureEnums
    {
        RightAngGesture,    // 2D gesture
        SameAngGesture,     // 2D gesture
        SameLengthGesture,  // 2D gesture
        SetAng,             // 2D gesture
        SetLength,          // 2D gesture
        Glue,               // 2D gesture
        DigHole,            // 3D gesture
        Erase,              // 2D,3D gesture
        Point,              // 2D,3D gesture
        None,
    }

    public static class GestureMapping
    {
        public static GestureEnums GetGestureEnum(string gestureName)
        {
            if (gestureName.ToLower().Contains("right"))
                return GestureEnums.RightAngGesture;
            else if (gestureName.ToLower().Contains("scratch"))
                return GestureEnums.Erase;
            else if (gestureName.ToLower().Contains("seg"))
                return GestureEnums.SameLengthGesture;
            else if (gestureName.ToLower().Contains("snap"))
                return GestureEnums.Glue;
            else if (gestureName.ToLower().Contains("angle"))
                return GestureEnums.SetAng;
            else if (gestureName.ToLower().Contains("point"))
                return GestureEnums.Point;
            else return GestureEnums.None;
        }

    }
}