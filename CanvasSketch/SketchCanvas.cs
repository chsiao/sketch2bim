﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Ink;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using AForge.Imaging;
using GeomLib;
using GeomLib.QuadTree;
using Recognizer.NDollar;
using Petzold.Media3D;
using Utility;
using WrapperLibrary.RDF;

namespace CanvasSketch
{
    public class SketchCanvas : InkCanvas
    {
        public delegate void CanvasStrokeInitiating(List<StylusPoint> inputPts, List<System.Windows.Point> convertedPts, ref Matrix3D mtx, ref object hitobj, ref bool isOnSamePlane);
        public event CanvasStrokeInitiating BeforeStrokeGenerated;
        public delegate void CanvasStrokeInitiated(CustomStroke primitives, ref Matrix3D mtx, object hitobj);
        public event CanvasStrokeInitiated OnStrokeGenerated;
        public event CanvasStrokeInitiated OnStrokeModified;
        public delegate void CanvasGesture(GestureCommand gesture, GestureEnums gestureName);
        /// <summary>
        /// Final gesture is generated when pen up
        /// </summary>
        public event CanvasGesture OnFinalGestureGenerated;

        public CustomDynamicRenderer customRenderer = new CustomDynamicRenderer();
        private GeometricRecognizer _rec;
        private const string gestureDir = @"gesture\";
        private FilterPlugin stylusFilter = new FilterPlugin();
        private double selTranslateX = 0;
        private double selTranslateY = 0;
        private QuadTree<ControlPoint> quadtree;
        private bool isStylusBtnDown = false;
        private ControlPoint selectedPt = null;
        private bool isTouched = false;

        private bool isSavingGesture = false;
        public bool SaveGesture { 
            set{
                this.isSavingGesture = value;
                customRenderer.IsSavingGestures = this.isSavingGesture;
            }
            get
            {
                return this.isSavingGesture;
            }
        }

        private long stylusDownTicks = 0;
        private long penDownUpDiff = long.MaxValue;

        public List<Primitive> Primitives
        {
            get
            {
                List<Primitive> result = new List<Primitive>();
                foreach (Stroke st in this.Strokes)
                    if (st is CustomStroke)
                        result.AddRange(((CustomStroke)st).Primitives);
                return result;
            }
        }

        public GeometricRecognizer GestureRecognizer { get { return this._rec; } }

        public SketchCanvas()
            : base()
        {
            // Use the custom dynamic renderer on the
            // custom InkCanvas.
            this.DynamicRenderer = customRenderer;
            this.EditingMode = InkCanvasEditingMode.Ink;

            //this.StylusPlugIns.Add(stylusFilter);
            this.Gesture += SketchCanvas_Gesture;
            this.StylusButtonDown += SketchCanvas_StylusButtonDown;
            this.StylusButtonUp += SketchCanvas_StylusButtonUp;
            this.StylusDown += SketchCanvas_StylusDown;
            this.StylusMove += SketchCanvas_StylusMove;
            this.StylusUp += SketchCanvas_StylusUp;
            this.SelectionChanging += SketchCanvas_SelectionChanging;
            this.SelectionMoved += SketchCanvas_SelectionMoved;
            this.SelectionMoving += SketchCanvas_SelectionMoving;
            this.SelectionResizing += SketchCanvas_SelectionResizing;
            GlobalVariables.CanvasStrokes = this.Strokes;
            this.quadtree = new QuadTree<ControlPoint>(new RectangleF(0f, 0f, 2000f, 2000f));

            _rec = new GeometricRecognizer();
            foreach (string gestureName in Directory.EnumerateFiles(gestureDir, "*.xml"))
                _rec.LoadGesture(gestureName);

            //this.SetEnabledGestures(new ApplicationGesture[]
            //        {//ApplicationGesture.Down, 
            //         ApplicationGesture.ArrowDown,
            //         //ApplicationGesture.Circle,
            //         ApplicationGesture.ScratchOut});

        }


        void SketchCanvas_StylusDown(object sender, StylusDownEventArgs e)
        {
            // Don't do anything if the stylus is a touch
            if (e.StylusDevice.TabletDevice.Type == TabletDeviceType.Touch) return;

            if (e.StylusDevice.StylusButtons[1].StylusButtonState == StylusButtonState.Up)
            {
                //this.EditingMode = InkCanvasEditingMode.InkAndGesture;
            }
            stylusDownTicks = DateTime.Now.Ticks;
            GlobalVariables.isSketching = true;
        }

        void SketchCanvas_StylusUp(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice.TabletDevice.Type == TabletDeviceType.Touch) return;
            GlobalVariables.CurrentMode = Mode.Standard;
        }

        void SketchCanvas_StylusMove(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice.TabletDevice.Type == TabletDeviceType.Touch) return;
            StylusPointCollection stypts = e.GetStylusPoints(this);
        }

        void SketchCanvas_SelectionResizing(object sender, InkCanvasSelectionEditingEventArgs e)
        {

        }

        void SketchCanvas_SelectionMoving(object sender, InkCanvasSelectionEditingEventArgs e)
        {
            selTranslateX += e.NewRectangle.Left - e.OldRectangle.Left;
            selTranslateY += e.NewRectangle.Top - e.OldRectangle.Top;
        }

        void SketchCanvas_SelectionMoved(object sender, EventArgs e)
        {

        }

        void SketchCanvas_SelectionChanging(object sender, InkCanvasSelectionChangingEventArgs e)
        {
            
        }

        void SketchCanvas_StylusButtonUp(object sender, StylusButtonEventArgs e)
        {
            if (Utility.UIUtility.IsStylusInRange)
            {
                isStylusBtnDown = false;
                if (e.StylusButton.Name == "Barrel Switch" || e.StylusButton.Name == "Tip Switch")
                {
                    if (this.GetSelectedStrokes().Count == 0)
                    {
                        //this.EditingMode = InkCanvasEditingMode.Ink;
                    }
                }
            }
        }

        void SketchCanvas_StylusButtonDown(object sender, StylusButtonEventArgs e)
        {
            if (Utility.UIUtility.IsStylusInRange)
            {
                isStylusBtnDown = true;
                if (e.StylusButton.Name == "Barrel Switch" && this.selectedPt == null)
                {
                    //this.EditingMode = InkCanvasEditingMode.Select;
                }
            }
        }

        void SketchCanvas_Gesture(object sender, InkCanvasGestureEventArgs e)
        {
            ReadOnlyCollection<GestureRecognitionResult> gestures = e.GetGestureRecognitionResults();

            foreach (GestureRecognitionResult gest in gestures)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("Gesture: {0}, Confidence: {1}", gest.ApplicationGesture, gest.RecognitionConfidence));
            }
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
        }

        protected override void OnStrokeCollected(InkCanvasStrokeCollectedEventArgs e)
        {
            penDownUpDiff = DateTime.Now.Ticks - stylusDownTicks;
            //System.Diagnostics.Debug.WriteLine(penDownUpDiff);

            // decides whether we should pass this stroke to gesture recognition engine
            Rect stsize = e.Stroke.GetBounds();

            // Remove the original stroke and add a custom stroke.
            this.Strokes.Remove(e.Stroke);

            #region Save Gesture Utility
            if (this.isSavingGesture)
            {
                SaveAsSketchGesture(e.Stroke.ConvertToPointRList());
            }
            #endregion
            else
            {
                GestureCommand gc = new GestureCommand(e.Stroke, _rec);
                var stbound = e.Stroke.GetBounds();
                var distance = e.Stroke.StylusPoints.TotalLength();
                var drawSpeed = penDownUpDiff / distance;
                //System.Diagnostics.Debug.WriteLine("speed: " + drawSpeed);
                //System.Diagnostics.Debug.WriteLine("time: " + penDownUpDiff);
                //System.Diagnostics.Debug.WriteLine("distance: " + distance);

                #region Find a Gesture
                if (GlobalVariables.CurrentMode != Mode.Extrusion && GlobalVariables.CurrentMode != Mode.UndoRedo && /*!GlobalVariables.IsSketchingOverlay &&*/
                    (gc.Gesture == GestureEnums.Erase || penDownUpDiff < GlobalConstant.ErasingThreshold) && (drawSpeed < 200000 || e.Stroke.StylusPoints.Count <= 10))
                {
                    gc.RefineGesture(penDownUpDiff);
                        
                    System.Diagnostics.Debug.WriteLine(gc.Gesture);
                    // do the command according to the determined gesture
                    if (OnFinalGestureGenerated != null && gc.Gesture != GestureEnums.None)
                        OnFinalGestureGenerated(gc, gc.Gesture);
                }
                // don't create stroke when it is too small
                #endregion
                if (!isTouched && (gc.Gesture == GestureEnums.None || drawSpeed > 200000) && GlobalVariables.CurrentMode != Mode.UndoRedo)
                {

                    List<System.Windows.Point> cvtPts = new List<System.Windows.Point>();
                    object hitobj = null;
                    Matrix3D mtx = new Matrix3D();
                    bool isOnSamePlane = false;
                    if (BeforeStrokeGenerated != null)
                        BeforeStrokeGenerated(e.Stroke.StylusPoints.ToList(), cvtPts, ref mtx, ref hitobj, ref isOnSamePlane);

                    // This stroke tends to be a drawing stroke
                    CustomStroke newStroke;

                    #region semantic drawing mode
                    if (cvtPts.Count > 2 && !GlobalVariables.IsSketchingOverlay)
                    {
                        newStroke = new CustomStroke(e.Stroke.StylusPoints, cvtPts, mtx);
                        bool ismerged = false;

                        if (GlobalVariables.SelectedItem != null && GlobalVariables.CurrentMode != Mode.Extrusion 
                            && GlobalVariables.CurrentMode != Mode.Array && GlobalVariables.CurrentMode != Mode.AddOneDimension
                            && !(hitobj is RDF_SweptAreaSolid && cvtPts.IsClosed()) && isOnSamePlane)
                        {
                            ismerged = Library.tryMergeWithSelectedStroke(ref mtx, ref newStroke);
                        }

                        // Use Cornucopia to Fit the Sketch Points into curves
                        if (GlobalVariables.CurrentMode != Mode.Extrusion)
                            newStroke.FitintoCurves(ismerged ? -1 : drawSpeed);

                        // add this new stroke into the collection will invalidate the canvas to draw this stroke
                        if (!ismerged)
                        {
                            var clonemtx = mtx.Clone();
                            this.Strokes.Add(newStroke);
                            if (OnStrokeGenerated != null)
                                OnStrokeGenerated(newStroke, ref mtx, hitobj);
                            //newStroke.matrix = mtx;
                            mtx.Invert();
                        }
                        else if (OnStrokeModified != null)
                        {
                            this.Strokes.Add(newStroke);
                            OnStrokeModified(newStroke, ref mtx, hitobj);
                        }
                        // need to translate the cvtPts based on the mtx
                        //for (int i = 0; i < newStroke.cvtPts.Count; i++)
                        //{
                        //    System.Windows.Point pt = newStroke.cvtPts[i];
                        //    Point3D newpt = mtx.Transform(new Point3D(pt.X, pt.Y, 0));
                        //    newStroke.cvtPts[i] = new System.Windows.Point(newpt.X, newpt.Y);
                        //}

                        InkCanvasStrokeCollectedEventArgs args = new InkCanvasStrokeCollectedEventArgs(newStroke);
                    }
                    #endregion
                }
            }
            this.isTouched = false;
            GlobalVariables.isSketching = false;
            // clean the contextual assist visualzations
            this.Children.Clear();
            base.OnStrokeCollected(e);
        }
      
        public void SaveLastStrokeAsGesture()
        {
            SaveAsSketchGesture(this.Strokes.Last().ConvertToPointRList());
        }

        private void SaveAsSketchGesture(List<PointR> pts)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "Gestures (*.xml)|*.xml";
            dlg.Title = "Save Gesture As";
            dlg.AddExtension = true;
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                List<List<PointR>> multistroke = new List<List<PointR>>();
                multistroke.Add(pts);
                List<int> numpoints = new List<int>();
                numpoints.Add(pts.Count);
                // resample, scale, translate to origin
                _rec.SaveGesture(dlg.FileName, multistroke, numpoints);
            }
        }

        protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
        {
            System.Windows.Point curloc = e.GetPosition(this);
            RectangleF testArea = new RectangleF((float)curloc.X - 5, (float)curloc.Y - 5, 10f, 10f);
            List<ControlPoint> resultPts = quadtree.Query(testArea);

            if (resultPts.Count > 0)
            {
                Mouse.OverrideCursor = Cursors.Hand;
                // set the selected point
                selectedPt = resultPts[0];
            }
            else
            {
                //System.Diagnostics.Debug.WriteLine("OnPreviewMouseRightButtonDown");
                selectedPt = null;
                Mouse.OverrideCursor = Cursors.Cross; 
            }

            base.OnPreviewMouseRightButtonDown(e);
        }

        /*
        protected override void OnMouseMove(MouseEventArgs e)
        {
            // find whether the current location is on the control point
            System.Windows.Point curloc = e.GetPosition(this);
            RectangleF testArea = new RectangleF((float)curloc.X - 5, (float)curloc.Y - 5, 10f, 10f);
            List<ControlPoint> resultPts = quadtree.Query(testArea);
            if (resultPts.Count > 0 || selectedPt != null)
            {
                Mouse.OverrideCursor = Cursors.Hand;
                if (e.RightButton == MouseButtonState.Pressed && selectedPt != null)
                {
                    // move the control point
                    moveSelPoint(selectedPt, curloc);
                }
            }
            else
            {
                Mouse.OverrideCursor = Cursors.Cross;
            }
            //System.Diagnostics.Debug.WriteLine(curloc);
            base.OnMouseMove(e);
        }*/

        protected override void OnPreviewMouseRightButtonUp(MouseButtonEventArgs e)
        {
            selectedPt = null;
            base.OnPreviewMouseRightButtonUp(e);
        }

        protected override void OnStylusDown(StylusDownEventArgs e)
        {
            if (isStylusBtnDown)
            {
                System.Windows.Point curloc = e.GetPosition(this);
                RectangleF testArea = new RectangleF((float)curloc.X - 5, (float)curloc.Y - 5, 10f, 10f);
                List<ControlPoint> resultPts = quadtree.Query(testArea);

                if (resultPts.Count > 0)
                {
                    selectedPt = resultPts[0];
                    Mouse.OverrideCursor = Cursors.Hand;
                    this.EditingMode = InkCanvasEditingMode.None;
                }
                else if (!(e.StylusDevice.StylusButtons.Count > 1 && e.StylusDevice.StylusButtons[1].Name == "Barrel Switch" && e.StylusDevice.StylusButtons[1].StylusButtonState == StylusButtonState.Down))
                {
                    selectedPt = null;
                    Mouse.OverrideCursor = Cursors.Cross;
                    this.EditingMode = InkCanvasEditingMode.Ink;
                }

                base.OnStylusDown(e);
            }
        }

        protected override void OnStylusMove(StylusEventArgs e)
        {
            if (selectedPt != null)
            {
                System.Windows.Point curloc = e.GetPosition(this);
                moveSelPoint(selectedPt, curloc);
            }
        }

        protected override void OnStylusUp(StylusEventArgs e)
        {
            selectedPt = null;
            base.OnStylusUp(e);
        }

        protected override void OnTouchDown(TouchEventArgs e)
        {
            // mark for ink collector to know it's from touch
            // when the ink is collected, dismark this
            this.isTouched = true;
            base.OnTouchDown(e);
        }

        protected override void OnTouchMove(TouchEventArgs e)
        {
            base.OnTouchMove(e);
        }

        protected override void OnTouchUp(TouchEventArgs e)
        {
            base.OnTouchUp(e);
        }

        private void moveSelPoint(ControlPoint oriPt, System.Windows.Point tarLoc)
        {
            // renew the control point in quadtree
            this.quadtree.Remove(oriPt);
            this.quadtree.Insert(oriPt);

            oriPt.Center = tarLoc;
            foreach (Primitive pm in oriPt.ConnPrimitives)
            {
                //if (pm.type == BasicPrimitive.PrimitiveType.LINE)
                //{
                    pm.generateSegment();
                    //pm.generateSegment3D();
                //}
            }
        }
    }
}
