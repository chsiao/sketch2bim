﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using WrapperLibrary.Cornucopia;
using Petzold.Media3D;
using System.Windows.Media.Media3D;


namespace CanvasSketch
{
    public class Primitive : BasicPrimitive
    {
        private ControlPoint pnode;
        private ControlPoint nnode;
        private string guid = "";

        /// <summary>
        /// for topology
        /// and generate the segement when both previous node and next node are available
        /// </summary>
        public ControlPoint PreviousNode {
            get
            {
                return pnode;
            }
            set
            {
                pnode = value;
                this.generateSegment();
                //this.generateSegment3D();
            }
        }
        public ControlPoint NextNode
        {
            get
            {
                return nnode;
            }
            set
            {
                nnode = value;
                this.generateSegment();
                //this.generateSegment3D();
            }
        }

        public string GUID { set { this.guid = value; } get { return this.guid; } }
        public CustomStroke Host { get; set; }
        /// <summary>
        /// for drawing
        /// </summary>
        public PathSegment Segment { get; private set; }
        public PathSegment3D Segment3D { get; private set; }
        

        public Primitive()
        {
            this.pnode = null;
            this.nnode = null;
            this.guid = Guid.NewGuid().ToString();
            this.Host = null;
        }
        public Primitive(BasicPrimitive bpm, CustomStroke host = null) : this()
        {
            // these inherited properties are for the storage of
            // generated parameters from cornucopia engine
            this.length = bpm.length;
            this.startAngle = bpm.startAngle;
            this.start = bpm.start;
            this.startCurvature = bpm.startCurvature;
            this.type = bpm.type;
            this.curvatureDerivative = bpm.curvatureDerivative;
            this.Host = host;
        }
        /// <summary>
        /// generate segment when the PreviousNode and NextNode is defined
        /// </summary>
        /// <returns>return ture when successfully genrated</returns>
        public bool generateSegment()
        {
            if (this.pnode == null || this.nnode == null) return false;
            // determine if the previous node is the first node of the path
            // if yes, reset the start point of the whole path again
            if (this.PreviousNode.ConnPrimitives.Count == 1)
            {
                Point strpt = new Point(this.PreviousNode.X, this.PreviousNode.Y);
                // check whether we have created the presentation or not
                if (this.Host != null && this.Host.Presentation != null)
                {
                    // get the path geometry
                    PathGeometry geometry = (PathGeometry)this.Host.Presentation.Data;
                    if (geometry.Figures.Count > 0)
                    {
                        // reset the start point here
                        geometry.Figures[0].StartPoint = strpt;
                    }
                }
            }
            
            Point endpt = new Point(this.NextNode.X, this.NextNode.Y);
            double angle = this.startAngle;
            double length = this.length;
            double strcurveture = this.startCurvature;
            bool isgenerated = false;

            switch ((int)this.type)
            {
                case 0:
                    if (this.Segment == null)
                        this.Segment = new LineSegment(endpt, true);
                    else
                        ((LineSegment)this.Segment).Point = endpt;
                    isgenerated = true;
                    break;
                case 1:
                    //segment = new LineSegment(new Point(nextX, nextY), true);
                    double radius = 1 / strcurveture;
                    Size cruveSize = new Size(Math.Abs(radius), Math.Abs(radius));
                    SweepDirection direction = radius > 0 ? SweepDirection.Clockwise : SweepDirection.Counterclockwise;
                    double a = (this.length / radius) * 180 / Math.PI;
                    bool islargearc = Math.Round(a) >= 180 ? true : false;
                    if (this.Segment == null)
                        this.Segment = new ArcSegment(endpt, cruveSize, a, islargearc, direction, true);
                    else
                        ((ArcSegment)this.Segment).Point = endpt;
                    isgenerated = true;
                    break;
                case 2:
                    break;
            }
            return isgenerated;
        }


        /// <summary>
        /// generate segment3D when beening called
        /// </summary>
        /// <returns>return ture when successfully genrated</returns>
        public bool generateSegment3D(Matrix3D hostmatrix)
        {
            if (this.pnode == null || this.nnode == null) return false;

            Point3D endpt = new Point3D(this.NextNode.X, this.NextNode.Y, 0);
            endpt = hostmatrix.Transform(endpt);

            double angle = this.startAngle;
            double length = this.length;
            double strcurveture = this.startCurvature;
            bool isgenerated = false;

            switch ((int)this.type)
            {
                case 0:
                    if (this.Segment3D == null)
                        this.Segment3D = new LineSegment3D();
                       
                    ((LineSegment3D)this.Segment3D).Point = endpt;
                    isgenerated = true;
                    break;
                case 1:
                    //segment = new LineSegment(new Point(nextX, nextY), true);
                    double radius = 1 / strcurveture;
                    Size cruveSize = new Size(Math.Abs(radius), Math.Abs(radius));
                    SweepDirection direction = radius > 0 ? SweepDirection.Clockwise : SweepDirection.Counterclockwise;
                    double a = (this.length / radius) * 180 / Math.PI;
                    bool islargearc = Math.Round(a) >= 180 ? true : false;
                    //if (this.Segment3D == null)
                    //    this.Segment3D = new  // new ArcSegment3D(endpt, cruveSize, a, islargearc, direction, true);
                    //else
                    //    ((ArcSegment)this.Segment).Point = endpt;
                    isgenerated = true;
                    isgenerated = false;
                    break;
                case 2:
                    break;
            }
            return isgenerated;
        }
    }
}
