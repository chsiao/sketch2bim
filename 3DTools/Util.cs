﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Controls;
using GeomLib;

namespace _3DTools
{
    public static class Util
    {
        /// <summary>
        /// Takes a 3D point and returns the corresponding 2D point (X,Y) within the viewport.  
        /// Requires the 3DUtils project available at http://www.codeplex.com/Wiki/View.aspx?ProjectName=3DTools
        /// </summary>
        /// <param name="point3D">A point in 3D space</param>
        /// <param name="viewPort">An instance of Viewport3D</param>
        /// <returns>The corresponding 2D point or null if it could not be calculated</returns>
        public static System.Windows.Point? Point3DToScreen2D(this Point3D point3D, Viewport3D viewPort)
        {
            bool bOK = false;

            // We need a Viewport3DVisual but we only have a Viewport3D.
            Viewport3DVisual vpv = VisualTreeHelper.GetParent(viewPort.Children[0]) as Viewport3DVisual;

            // Get the world to viewport transform matrix
            Matrix3D m = MathUtils.TryWorldToViewportTransform(vpv, out bOK);

            if (bOK)
            {
                // Transform the 3D point to 2D
                Point3D transformedPoint = m.Transform(point3D);

                System.Windows.Point screen2DPoint = new System.Windows.Point((int)transformedPoint.X, (int)transformedPoint.Y);

                return new Nullable<System.Windows.Point>(screen2DPoint);
            }
            else
            {
                return null;
            }
        }

    }
}
