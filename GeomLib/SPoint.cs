﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace GeomLib
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SPoint //basic point class
    {
        public double x;
        public double y;
    };
}
