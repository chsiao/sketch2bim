﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using DotNetMatrix;

namespace GeomLib
{
    public class Math3D
    {
        public static double EPSILON = 0.001D;
        public static double NULL = 0.10678112038011D;
        public static double PI = 3.141592;
        public static double RADIAN = 3.141592 / 180.0;


        public Math3D()
        {

        }


        public Point3D Multiply(GeneralMatrix m, Point3D p)
        {
            Point3D pt = new Point3D();
            pt.X = m.A[0][0] * p.X + m.A[0][1] * p.Y + m.A[0][2] * p.Z + m.A[0][3];
            pt.Y = m.A[1][0] * p.X + m.A[1][1] * p.Y + m.A[1][2] * p.Z + m.A[1][3];
            pt.Z = m.A[2][0] * p.X + m.A[2][1] * p.Y + m.A[2][2] * p.Z + m.A[2][3];

            return pt;
        }


        public GeneralMatrix TransformMatrix(Point3D basepoint, Point3D referencepoint)
        {
            Vector3D vect_p3toP4 = new Vector3D(referencepoint.X - basepoint.X, referencepoint.Y - basepoint.Y, 0);
            Vector3D vect_axisX = new Vector3D(1, 0, 0);

            double angle = Vector3D.AngleBetween(vect_p3toP4, vect_axisX);
            if (referencepoint.Y - basepoint.Y < 0) // angle is over 180 degree. 
                angle = 2 * 3.141592 - angle;

            GeneralMatrix M0 = new GeneralMatrix(4, 4);

            M0.Identity();
            M0.A[0][0] = Math.Cos(angle);
            M0.A[0][1] = -Math.Sin(angle);
            M0.A[1][0] = Math.Sin(angle);
            M0.A[1][1] = Math.Cos(angle);
            M0.A[0][3] = basepoint.X;
            M0.A[1][3] = basepoint.Y;

            return M0;

        }

        public bool isCCW(List<Point3D> polygon)
        {
            Vector3D X1 = new Vector3D();
            Vector3D X2 = new Vector3D();
            Vector3D X3 = new Vector3D();

            int i = 0;
            double d = Double.MinValue;
            for (int j = 1; j < polygon.Count - 1; j++)
                if (polygon[j].X >= d)
                {
                    d = polygon[j].X;
                    i = j;
                }

            X1.X = polygon[i].X;
            X1.Y = polygon[i].Y;
            X1.Z = polygon[i].Z;

            int k = i;
            if (i == 0)
                k = polygon.Count;
            X1.X -= polygon[k - 1].X;
            X1.Y -= polygon[k - 1].Y;
            X1.Z -= polygon[k - 1].Z;

            X2.X = polygon[(i + 1) % polygon.Count].X;
            X2.Y = polygon[(i + 1) % polygon.Count].Y;
            X2.Z = polygon[(i + 1) % polygon.Count].Z;

            X2.X -= polygon[i].X;
            X2.Y -= polygon[i].Y;
            X2.Z -= polygon[i].Z;


            double d1 = Vector3D.AngleBetween(X1, X2);
            X3 = Vector3D.CrossProduct(X1, X2);

            if (X3.Z < 0.0D)
                d1 = -d1;
            return d1 > 0.0D;
        }

        public Vector3D VectorRotate2D(double degree_angle, Vector3D v)
        {
            Vector3D rv = new Vector3D();
            double raidan_angle = degree_angle * RADIAN;

            rv.X = Math.Cos(raidan_angle) * v.X - Math.Sin(raidan_angle) * v.Y;
            rv.Y = Math.Sin(raidan_angle) * v.X + Math.Cos(raidan_angle) * v.Y;
            rv.Z = v.Z;
            return rv;
        }



        /// <summary>
        ///  line to line intersection checking
        /// </summary>
        /// <param name="L1p1"></param>
        /// <param name="L1p2"></param>
        /// <param name="L2p1"></param>
        /// <param name="L2p2"></param>
        /// <param name="rp"></param> rp : intersection point. 0: NULL value, 1: intersection point, 2: one of intersection points. 
        /// <returns></returns> 0 : not  intersect, 1 : intersection 2: overlap
        public int lineLineIntersection_old(Point3D L1p1, Point3D L1p2, Point3D L2p1, Point3D L2p2, Point3D rp)
        {

            Vector3D v01 = new Vector3D(L1p1.X - L1p2.X, L1p1.Y - L1p1.Y, L1p1.Z - L1p1.Z);
            Vector3D v23 = new Vector3D(L2p1.X - L2p2.X, L2p1.Y - L2p2.Y, L2p1.Z - L2p2.Z);

            if (v01.Length == 0 || v23.Length == 0) // in case the line is just a point. 
            {
                rp.X = NULL;
                rp.Y = NULL;
                return 0;
            }


            v01.Normalize();
            v23.Normalize();

            if ((isPointOnLine(L1p1, L2p1, L2p2, rp, EPSILON) || isPointOnLine(L1p2, L2p1, L2p2, rp, EPSILON)) && v01.Equals(v23))
            {
                return 2;  // overlap
            }

            double d = L1p2.X - L1p1.X;
            double d1 = L1p2.Y - L1p1.Y;
            double d2 = L2p2.X - L2p1.X;
            double d3 = L2p2.Y - L2p1.Y;
            double d4 = d * d3 - d1 * d2;
            if (Math.Abs(d4) < EPSILON)
            {
                if (rp != null)
                {
                    rp.X = NULL;
                    rp.Y = NULL;
                }
                return 0; // No intersection

            }
            double d5 = L2p1.X - L1p1.X;
            double d6 = L2p1.Y - L1p1.Y;
            double d7 = (d5 * d3 - d6 * d2) / d4;
            if (rp != null)
            {
                rp.X = L1p1.X + d7 * d;
                rp.Y = L1p1.Y + d7 * d1;
            }
            return 1; // have an intersection
        }

        public int lineLineIntersection_old(Point3D[] line1, Point3D[] line2, Point3D rp)
        {
            Point3D p1 = line1[0];
            Point3D p2 = line1[1];
            Point3D p3 = line2[0];
            Point3D p4 = line2[1];

            //if (p4 == null || p1 == null || p2 == null  || p3 == null )
            //{
            //    int a = 0; 
            //}


            int result = lineLineIntersection_old(p1, p2, p3, p4, rp);
            return result;
        }


        public int lineLineIntersection(Point3D[] L1, Point3D[] L2, Point3D ptIntersection)
        {

            int r = lineLineIntersection(L1[0], L1[1], L2[0], L2[1], ptIntersection);
            return r;
        }

        public bool isOverlap(Point3D p1, Point3D p2, Point3D p3, Point3D p4, Point3D ptIntersection)
        {
            Vector3D p12 = new Vector3D(p2.X - p1.X, p2.Y - p1.Y, 0);
            p12.Normalize();
            Vector3D p34 = new Vector3D(p4.X - p3.X, p4.Y - p3.Y, 0);
            p34.Normalize();
            Vector3D p13 = new Vector3D(p3.X - p1.X, p3.Y - p1.Y, 0);
            p13.Normalize();

            if (p12 == p34 || p12 == -p34)
            {
                if (p13 == p12 || p13 == -p12)
                {
                    if (isPointOnLine(p1, p3, p4, ptIntersection, EPSILON)) ptIntersection = p1;
                    if (isPointOnLine(p2, p3, p4, ptIntersection, EPSILON)) ptIntersection = p2;

                    return true;
                }
            }

            return false;

        }


        public int lineLineIntersection(Point3D p1, Point3D p2, Point3D p3, Point3D p4, Point3D ptIntersection)
        {

            if (isOverlap(p1, p2, p3, p4, ptIntersection))
                return 2;


            // Denominator for ua and ub are the same, so store this calculation
            double d =
               (p4.Y - p3.Y) * (p2.X - p1.X)
               -
               (p4.X - p3.X) * (p2.Y - p1.Y);

            //n_a and n_b are calculated as seperate values for readability
            double n_a =
               (p4.X - p3.X) * (p1.Y - p3.Y)
               -
               (p4.Y - p3.Y) * (p1.X - p3.X);

            double n_b =
               (p2.X - p1.X) * (p1.Y - p3.Y)
               -
               (p2.Y - p1.Y) * (p1.X - p3.X);

            // Make sure there is not a division by zero - this also indicates that
            // the lines are parallel.  
            // If n_a and n_b were both equal to zero the lines would be on top of each 
            // other (coincidental).  This check is not done because it is not 
            // necessary for this implementation (the parallel check accounts for this).
            if (d == 0)

                return 0;

            // Calculate the intermediate fractional point that the lines potentially intersect.
            double ua = n_a / d;
            double ub = n_b / d;

            // The fractional point will be between 0 and 1 inclusive if the lines
            // intersect.  If the fractional calculation is larger than 1 or smaller
            // than 0 the lines would need to be longer to intersect.
            if (ua >= 0d && ua <= 1d && ub >= 0d && ub <= 1d)
            {
                ptIntersection.X = p1.X + (ua * (p2.X - p1.X));
                ptIntersection.Y = p1.Y + (ua * (p2.Y - p1.Y));
                return 1;
            }
            return 0;
        }

        public bool isPointOnLine(Point3D pt, Point3D Lpt1, Point3D Lpt2, Point3D pointonline, double d)
        {
            bool flag = false;
            double d1 = projectPointOnLine(pt, Lpt1, Lpt2, pointonline);
            d *= d;
            if (d1 < 0.0D)
                flag = lengthSquared(Lpt1, pointonline) <= d;
            else
                if (d1 > 1.0D)
                    flag = lengthSquared(Lpt2, pointonline) <= d;
                else
                    flag = lengthSquared(pt, pointonline) <= d;
            return flag;
        }


        public double projectPointOnLine(Point3D pt, Point3D Lp1, Point3D Lp2, Point3D projectedPoint)
        {
            double d = lengthSquared(Lp1, Lp2);
            if (d < EPSILON)
                return 0.0D;
            double d1 = ((Lp2.X - Lp1.X) * (pt.X - Lp1.X) + (Lp2.Y - Lp1.Y) * (pt.Y - Lp1.Y)) / d;
            if (projectedPoint != null)
            {
                projectedPoint.X = Lp1.X + (Lp2.X - Lp1.X) * d1;
                projectedPoint.Y = Lp1.Y + (Lp2.Y - Lp1.Y) * d1;
            }
            return d1;
        }


        public double lengthSquared(Point3D point3d, Point3D point3d1)
        {
            return (point3d.X - point3d1.X) * (point3d.X - point3d1.X) + (point3d.Y - point3d1.Y) * (point3d.Y - point3d1.Y);
        }



        public double length(Point3D point3d, Point3D point3d1)
        {
            return Math.Sqrt(lengthSquared(point3d, point3d1));

        }
    }
}
