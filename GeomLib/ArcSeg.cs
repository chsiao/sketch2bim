﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Media.Media3D;


namespace GeomLib
{
    public struct ArcSeg
    {
        public PointF Center { get; set; }
        public double Radius { set; get; }
        public double Size { set; get; }
        public double StartAngle { set; get; }
    }
}
