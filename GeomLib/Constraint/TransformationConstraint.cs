﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeomLib.Constraint
{
    /// <summary>
    /// the transformation parameter(s) is depending on the geometric parameter(s)
    /// </summary>
    public class TransformationConstraint : Constraint
    {
        public event SolvingConstraint onSolvingConstraint;
        public event ConstraintSolved onSolvedConstraint;

        public TransformationConstraintEnum ConstraintName { set; get; }

        public TransformationConstraint()
        {
            this.ConstraintName = TransformationConstraintEnum.None;
            this.Type = ConstraintType.NotSet;
            this.ConstraintObjects = new List<GeometryProduct>();
            this.IsSolved = true;
        }

        public TransformationConstraint(GeometryProduct originate)
        {
            this.Subject = originate;
            this.ConstraintName = TransformationConstraintEnum.None;
            this.Type = ConstraintType.NotSet;
            this.ConstraintObjects = new List<GeometryProduct>();
            this.IsSolved = true;
        }

        public void SolveConstraint()
        {
            switch (this.Type)
            {
                case ConstraintType.Surface:
                    if (this.ConstraintObjects.Count > 0)
                    {
                        var cstobj = this.ConstraintObjects.First();
                        var subject = this.Subject;

                        if (onSolvingConstraint != null)
                        {
                            onSolvingConstraint(this, cstobj);
                        }
                    }
                    break;
            }
        }
    }
}
