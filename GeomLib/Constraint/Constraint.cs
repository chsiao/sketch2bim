﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeomLib.Constraint
{
    public delegate void ConstraintSolved(Constraint constrainObj);
    public delegate void SolvingConstraint(Constraint constrainObj, object curvalue);

    public abstract class Constraint
    {
        [XmlIgnore]
        public GeometryProduct Subject { get; set; }
        public ConstraintType Type { get; set; }
        public object ControllingObject { set; get; }
        [XmlIgnore]
        public List<GeometryProduct> ConstraintObjects { get; set; }
        public bool IsSolved { get; set; }
    }
}
