﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeomLib
{
    public enum GeometricConstraintEnum
    {
        SameLength,
        SameAngle,
        Rotation,
        Angle,
        Length,
        Array,
        Extrude,
        FacePolygon,
        None
    }

    public enum TransformationConstraintEnum
    {
        TopSurface,
        BottomSurface,
        EdgeSurface,
        None
    }

    public enum ConstraintType
    {
        NotSet,
        Angle,
        Position,
        Surface,
        Length,
        Area,
        ThreeDimension,
        Path
    }
}
