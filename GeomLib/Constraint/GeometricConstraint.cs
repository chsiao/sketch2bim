﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Media3D;


namespace GeomLib.Constraint
{
    public class GeometricConstraint : Constraint
    {
        public event SolvingConstraint onSolvingConstraint;
        public event ConstraintSolved onSolvedConstraint;

        public Vector3D ArrayOffset { set; get; }
        public double? Interval { set; get; }

        private System.Windows.Forms.Timer timer;
        private double timerCount;
        private double? orivalue;
        private double? tarvalue;
        public double? TarConstraintValue
        {
            get { return this.tarvalue; }
            set
            {
                this.tarvalue = value;
                this.IsSolved = this.tarvalue == this.orivalue;
            }
        }
        public double? OriConstraintValue
        {
            get { return this.orivalue; }
            set
            {
                this.orivalue = value;
                this.IsSolved = this.tarvalue == this.orivalue;
            }
        }

        public GeometricConstraintEnum ConstraintName { get; set; }

        public Point3D Location { get; set; }

        public GeometricConstraint(GeometryProduct originate, GeometricConstraintEnum constraint, ConstraintType type, double value)
        {
            this.Subject = originate;
            this.ConstraintName = constraint;
            this.Type = type;
            this.TarConstraintValue = value;
            this.ConstraintObjects = new List<GeometryProduct>();
            this.IsSolved = true;
        }
        public GeometricConstraint(GeometryProduct originate)
        {
            this.Subject = originate;
            this.ConstraintName = GeometricConstraintEnum.None;
            this.Type = ConstraintType.NotSet;
            this.ConstraintObjects = new List<GeometryProduct>();
            this.TarConstraintValue = null;
            this.IsSolved = true;
        }
        public GeometricConstraint()
        {
            //this.Subject = originate;
            this.ConstraintName = GeometricConstraintEnum.None;
            this.Type = ConstraintType.NotSet;
            this.ConstraintObjects = new List<GeometryProduct>();
            this.TarConstraintValue = null;
            this.IsSolved = true;
        }
        public GeometricConstraint Reset()
        {
            this.ConstraintName = GeometricConstraintEnum.None;
            this.Type = ConstraintType.NotSet;
            this.ConstraintObjects = new List<GeometryProduct>();
            this.TarConstraintValue = null;
            this.IsSolved = true;
            return this;
        }
        public void SolveConstraint()
        {
            
            if (!this.IsSolved)
            {
                timerCount = 0;
                // set timer and interval values for processing the constraint
                // Todo: need to change to the appropriate timer for WPF
                timer = new System.Windows.Forms.Timer();
                // in the timer, we call callback to change the parameters and render it on canvas
                timer.Interval = 30;
                switch (this.Type)
                {
                    case ConstraintType.Angle:
                        timer.Tick += solvingAngleConstraint;
                        break;
                    case ConstraintType.Length:
                        timer.Tick += solvingLengthConstraint;
                        break;
                }
                timer.Enabled = true;
                timer.Start();
            }
        }
        private void solvingAngleConstraint(object sender, EventArgs e)
        {
            if (onSolvingConstraint != null && timerCount < 31)
            {
                double curvalue = timerCount == 30 ?
                    this.tarvalue.Value : this.orivalue.Value + (this.tarvalue.Value - this.orivalue.Value) / 2;

                onSolvingConstraint(this, curvalue);
                // set the original value to the curvalue
                this.orivalue = curvalue;

                if (timerCount >= 30)
                {
                    timer.Tick -= solvingAngleConstraint;
                    timer.Stop();
                    if (onSolvedConstraint != null)
                        onSolvedConstraint(this);
                }
                timerCount++;
            }
            else
            {
                timer.Enabled = false;
                timer.Tick -= solvingAngleConstraint;
                this.timer.Stop();
                this.timer.Dispose();
                //timer = new System.Windows.Forms.Timer();
            }
        }
        private void solvingLengthConstraint(object sender, EventArgs e)
        {
            if (onSolvingConstraint != null && timerCount < 31)
            {
                double curvalue = timerCount == 30 ?
                    this.tarvalue.Value : this.orivalue.Value + (this.tarvalue.Value - this.orivalue.Value) / 2;

                onSolvingConstraint(this, curvalue);
                // set the original value to the curvalue
                this.orivalue = curvalue;

                if (timerCount >= 30)
                {
                    timer.Tick -= solvingLengthConstraint;
                    timer.Stop();
                    if (onSolvedConstraint != null)
                        onSolvedConstraint(this);
                }
                timerCount++;
            }
            else
            {
                timer.Enabled = false;
                timer.Tick -= solvingLengthConstraint;
                this.timer.Stop();
                this.timer.Dispose();
                //timer = new System.Windows.Forms.Timer();
            }
        }
        public GeometricConstraint Clone()
        {
            return (GeometricConstraint)this.MemberwiseClone();
        }
    }
}
