﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Controls;
using System.Windows.Input;
using System.Drawing;
using System.Drawing.Drawing2D;
using ClipperLib;
using DotNetMatrix;
using TriangleNet;
using TriangleNet.Geometry;

namespace GeomLib
{
    public static class BasicGeomLib
    {
        private const double EPSILON = 0.001D;

        public static Vector3D X = new Vector3D(1, 0, 0);
        public static Vector3D Y = new Vector3D(0, 1, 0);
        public static Vector3D Z = new Vector3D(0, 0, 1);

        #region Degree Radians Conversion
        public static double ToRadians(this double degree)
        {
            return degree * Math.PI / 180;
        }
        public static double ToDegree(this double radians)
        {
            return radians * 180 / Math.PI;
        }
        public static float ToRadians(this float degree)
        {
            return (float)degree * (float)Math.PI / 180.0f;
        }
        public static float ToDegree(this float radians)
        {
            return (float)radians * 180.0f / (float)Math.PI;
        }
        #endregion
        public static Vector Normalize(Vector v)
        {
            Vector result = v;
            result.Normalize();
            return result;
        }
        public static double TotalLength(this StylusPointCollection pts)
        {
            double distance = 0;
            StylusPoint? prept = null;
            foreach (var pt in pts)
            {
                if (prept != null)
                {
                    distance += prept.Value.distanceTo(pt);
                }
                prept = pt;
            }
            return distance;
        }
        #region Get Distance Between Two Points
        public static double distanceTo(this StylusPoint pt, StylusPoint toPt)
        {
            double diffX = pt.X - toPt.X;
            double diffY = pt.Y - toPt.Y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }
        public static double distanceTo(this StylusPoint pt, double x, double y)
        {
            double diffX = pt.X - x;
            double diffY = pt.Y - y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }
        public static double distanceTo(this System.Windows.Point pt1, System.Windows.Point pt2)
        {
            Vector vec = System.Windows.Point.Subtract(pt1, pt2);
            return vec.Length;
        }
        public static double distanceTo(this System.Windows.Point pt, double x, double y)
        {
            double diffX = pt.X - x;
            double diffY = pt.Y - y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }
        public static double distanceTo(this System.Drawing.Point pt, double x, double y)
        {
            double diffX = pt.X - x;
            double diffY = pt.Y - y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }
        public static double distanceTo(this System.Drawing.PointF pt1, System.Drawing.PointF pt2)
        {
            double diffX = pt1.X - pt2.X;
            double diffY = pt1.Y - pt2.Y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }
        public static double distanceTo(this Point3D oript, Point3D topt)
        {
            Vector3D vec = Point3D.Subtract(oript, topt);
            return vec.Length;
        }
        #endregion
        public static double GetShortestDistBtwLineAndPoint(System.Windows.Point A, System.Windows.Point B, System.Windows.Point P)
        {
            System.Windows.Point? projectedPt = GetProjectedPointNearLineSeg(A, B, P, double.MaxValue);
            if (!projectedPt.HasValue) return double.MaxValue;
            return P.distanceTo(projectedPt.Value);
        }
        public static double GetShortestDistBtwLineSegAndPoint(System.Windows.Point A, System.Windows.Point B, System.Windows.Point P)
        {
            System.Windows.Point? projectedPt = GetProjectedPointOnLineSeg(A, B, P);
            if (!projectedPt.HasValue) return double.MaxValue;
            return P.distanceTo(projectedPt.Value);
        }
        public static System.Windows.Point? GetProjectedPointOnLineSeg(System.Windows.Point A, System.Windows.Point B, System.Windows.Point P)
        {
            Vector v1 = new Vector(P.X - A.X, P.Y - A.Y);
            Vector v2 = new Vector(B.X - A.X, B.Y - A.Y);
            Vector projectionVector = v1.Project(v2);

            double newX = A.X + projectionVector.X;
            double newY = A.Y + projectionVector.Y;

            System.Windows.Point projectedPt = new System.Windows.Point((float)newX, (float)newY);

            double maxX = Math.Round(Math.Max(A.X, B.X), 3);
            double minX = Math.Round(Math.Min(A.X, B.X), 3);
            double maxY = Math.Round(Math.Max(A.Y, B.Y), 3);
            double minY = Math.Round(Math.Min(A.Y, B.Y), 3);
            double px = Math.Round(projectedPt.X, 3);
            double py = Math.Round(projectedPt.Y, 3);

            if (maxX >= px && px >= minX && maxY >= py && py >= minY)
                return projectedPt;
            return null;
        }
        public static System.Windows.Point? GetProjectedPointNearLineSeg(System.Windows.Point A, System.Windows.Point B, System.Windows.Point P, double distToEndPts = 2)
        {
            Vector v1 = new Vector(P.X - A.X, P.Y - A.Y);
            Vector v2 = new Vector(B.X - A.X, B.Y - A.Y);
            Vector projectionVector = v1.Project(v2);

            double newX = A.X + projectionVector.X;
            double newY = A.Y + projectionVector.Y;

            System.Windows.Point projectedPt = new System.Windows.Point((float)newX, (float)newY);

            double maxX = Math.Round(Math.Max(A.X, B.X), 3);
            double minX = Math.Round(Math.Min(A.X, B.X), 3);
            double maxY = Math.Round(Math.Max(A.Y, B.Y), 3);
            double minY = Math.Round(Math.Min(A.Y, B.Y), 3);
            double px = Math.Round(projectedPt.X, 3);
            double py = Math.Round(projectedPt.Y, 3);

            double distToA = A.distanceTo(projectedPt);
            double distToB = B.distanceTo(projectedPt);
            if ((maxX >= px && px >= minX && maxY >= py && py >= minY) || distToA < distToEndPts || distToB < distToEndPts)
                return projectedPt;
            return null;
        }
        public static Vector Project(this Vector a, Vector b)
        {
            double angle = Vector.AngleBetween(a, b).ToRadians();
            return Normalize(b) * a.Length * Math.Cos(angle);
        }
        #region Polygon Area
        public static double PolygonArea(this List<StylusPoint> pts)
        {
            if (pts.Count < 3) return 0; // not a polygon
            if (pts.First().distanceTo(pts.Last()) > 15) return 0; // not a closed polyline

            double dblArea = 0;
            int nNumOfPts = pts.Count;
            int j;

            for (int i = 0; i < nNumOfPts; i++)
            {
                j = (i + 1) % nNumOfPts;
                dblArea += pts[i].X * pts[j].Y;
                dblArea -= pts[i].Y * pts[j].X;
            }

            dblArea = dblArea / 2;

            return dblArea;
        }
        public static bool IsClosed(this List<Point3D> pts)
        {
            var fst = pts.First();
            var last = pts.Last();

            return fst.distanceTo(last) < 5 ? true : false;
        }
        public static bool IsClosed(this List<System.Windows.Point> pts)
        {
            var fst = pts.First();
            var last = pts.Last();

            return fst.distanceTo(last) < 5 ? true : false;
        }
        public static double PolygonArea(this List<System.Windows.Point> pts)
        {
            if (pts.Count < 3) return 0; // not a polygon
            if (pts.First().distanceTo(pts.Last()) > 8) return 0; // not a closed polyline

            double dblArea = 0;
            int nNumOfPts = pts.Count;
            int j;

            for (int i = 0; i < nNumOfPts; i++)
            {
                j = (i + 1) % nNumOfPts;
                dblArea += pts[i].X * pts[j].Y;
                dblArea -= pts[i].Y * pts[j].X;
            }

            dblArea = dblArea / 2;

            return dblArea;
        }
        /// <summary>
        /// To calculate the area of polygon made by given points 
        ///
        /// Good for polygon with holes, but the vertices make the 
        /// hole should be in different direction with bounding 
        /// polygon.
        ///
        /// Restriction: the polygon is not self intersecting 
        /// ref: www.swin.edu.au/astronomy/pbourke/geometry/polyarea/			
        /// </summary>
        /// <param name="vertices">the array of the polygon</param>
        /// <returns></returns>
        public static double PolygonArea(Point3D[] vertices)
        {
            if (vertices.Length < 3) return 0;

            Point3D[] traslatedPoints = vertices;

            // since we only have 3 points to compare, and have many cases in which the shapes are parellel to horizontal
            // we can take a faster route here
            if (!(vertices[0].Z == vertices[1].Z && vertices[2].Z == vertices[1].Z))
                traslatedPoints = PlaneToHorizontal(vertices);

            double dblArea = 0;
            int nNumOfPts = traslatedPoints.Length;

            int j;
            for (int i = 0; i < nNumOfPts; i++)
            {
                j = (i + 1) % nNumOfPts;
                dblArea += traslatedPoints[i].X * traslatedPoints[j].Y;
                dblArea -= traslatedPoints[i].Y * traslatedPoints[j].X;
            }

            dblArea = dblArea / 2;
            return dblArea;
        }
        #endregion
        public static Point3D[] PlaneToHorizontal(Point3D[] vertices)
        {
            Point3D a = new Point3D(vertices[0].X, vertices[0].Y, vertices[0].Z);
            Point3D b = new Point3D(vertices[1].X, vertices[1].Y, vertices[1].Z);
            Point3D c = new Point3D(vertices[2].X, vertices[2].Y, vertices[2].Z);

            Vector3D ab = new Vector3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);
            Vector3D ac = new Vector3D(c.X - a.X, c.Y - a.Y, c.Z - a.Z);

            Vector3D newZ = Vector3D.CrossProduct(ab, ac);
            newZ = Vector3D.CrossProduct(newZ, Z);
            /// in order to rotate the polygon to parellel to the xy plane, 
            /// we need to calculate the Z axis of the plan
            double angleZ = Vector3D.AngleBetween(newZ, Z); // assume it's in radians

            //angleZ = Math.PI*angleZ/180;
            //Matrix3D rotateZ = new Matrix3D(
            //    Math.Cos(angleZ), Math.Sin(angleZ), 0, 0,
            //   -Math.Sin(angleZ), Math.Cos(angleZ), 0, 0,
            //                                  0, 0, 1, 0,
            //                                  0, 0, 0, 1);            

            //MatrixTransform3D transform3D = new MatrixTransform3D(rotateZ);

            RotateTransform3D rotateTransform = new RotateTransform3D();
            AxisAngleRotation3D axisRotation = new AxisAngleRotation3D();
            axisRotation.Axis = newZ;
            axisRotation.Angle = angleZ;
            rotateTransform.Rotation = axisRotation;

            Point3D[] newPoints = new Point3D[vertices.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                Point3D oldPoint = new Point3D(vertices[i].X, vertices[i].Y, vertices[i].Z);
                Point3D newPoint = rotateTransform.Transform(oldPoint);
                newPoints[i] = new Point3D(newPoint.X, newPoint.Y, newPoint.Z);
            }

            return newPoints;
        }
        #region Two Segments Intersection
        public static bool IsTwoLineSegmentIntersected(System.Drawing.PointF[] line1, System.Drawing.PointF[] line2, out System.Drawing.PointF intersectedPt)
        {
            intersectedPt = new System.Drawing.PointF();
            double a1, a2, c1, c2;
            double b1 = -1; double b2 = -1;
            a1 = (line1[0].Y - line1[1].Y) / (line1[0].X - line1[1].X);
            a2 = (line2[0].Y - line2[1].Y) / (line2[0].X - line2[1].X);
            c1 = (line1[0].X * a1 - line1[0].Y);
            c2 = (line2[0].X * a2 - line2[0].Y);

            if (line1[0].X - line1[1].X == 0)
            {
                c1 = line1[0].X;
                a1 = 1;
                b1 = 0;
            }

            if (line2[0].X - line2[1].X == 0)
            {
                c2 = line2[0].X;
                a2 = 1;
                b2 = 0;
            }

            double delta = a1 * b2 - a2 * b1;

            // lines are parellel
            if (delta == 0)
                return false;

            // here is the intersection point!!
            double intersectX = (b2 * c1 - b1 * c2) / delta;
            double intersectY = (a1 * c2 - a2 * c1) / delta;

            //check if the intersect point is in the segment
            double line1Length = line1[0].distanceTo(line1[1]);
            double line2Length = line2[0].distanceTo(line2[1]);

            double dis1Line1 = Math.Sqrt(Math.Pow(intersectX - line1[0].X, 2) + Math.Pow(intersectY - line1[0].Y, 2));
            double dis2Line1 = Math.Sqrt(Math.Pow(intersectX - line1[1].X, 2) + Math.Pow(intersectY - line1[1].Y, 2));
            if (Math.Round(dis1Line1 + dis2Line1, 2) > Math.Round(line1Length, 2)) return false;

            double dis1Line2 = Math.Sqrt(Math.Pow(intersectX - line2[0].X, 2) + Math.Pow(intersectY - line2[0].Y, 2));
            double dis2Line2 = Math.Sqrt(Math.Pow(intersectX - line2[1].X, 2) + Math.Pow(intersectY - line2[1].Y, 2));
            if (Math.Round(dis1Line2 + dis2Line2, 2) > Math.Round(line2Length, 2)) return false;

            intersectedPt = new System.Drawing.PointF((float)intersectX, (float)intersectY);
            return true;
        }
        /// <summary>
        /// This is not working .... Need to Fix
        /// </summary>
        /// <param name="arc1"></param>
        /// <param name="arc2"></param>
        /// <param name="intersectedPts"></param>
        /// <returns></returns>
        public static bool IsTwoArcIntersected(ArcSeg arc1, ArcSeg arc2, out List<System.Drawing.PointF> intersectedPts)
        {
            intersectedPts = new List<PointF>();
            var ctr1 = arc1.Center;
            var ctr2 = arc2.Center;

            double x1, x2, y1, y2; //此為兩圓相交的坐標 
            if (ctr1.Y != ctr1.Y)//兩圓圓心Y值不同時 
            {
                //m= y=mx+k的x項系數、k= y=mx+k的k項常數、 a、b、c= x=(-b±√(b^2-4ac))/2a的係數 
                double m = (ctr1.X - ctr2.X) / (ctr2.Y - ctr1.Y),
                       k = (Math.Pow(arc1.Radius, 2) - Math.Pow(arc2.Radius, 2) + Math.Pow(ctr2.X, 2) - Math.Pow(ctr1.X, 2) + Math.Pow(ctr2.Y, 2) - Math.Pow(ctr1.Y, 2)) / (2 * (ctr2.Y - ctr1.Y)),
                       a = 1 + Math.Pow(m, 2),
                       b = 2 * (k * m - ctr2.X - m * ctr2.Y),
                       c = Math.Pow(ctr2.X, 2) + Math.Pow(ctr2.Y, 2) + Math.Pow(k, 2) - 2 * k * ctr2.Y - Math.Pow(arc2.Radius, 2);
                if (b * b - 4 * a * c >= 0)//有交點時 
                {
                    x1 = ((-b) + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);//x=(-b+√(b^2-4ac))/2a 
                    y1 = m * x1 + k;//y=mx+k
                    x2 = ((-b) - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);//x=(-b-√(b^2-4ac))/2a
                    y2 = m * x2 + k;//y=mx+k
                    if (b * b - 4 * a * c > 0)//兩交點
                    {
                        // this will need to decide which point is closer to the original arc
                        intersectedPts.Add(new PointF((float)x1, (float)y1));
                        intersectedPts.Add(new PointF((float)x2, (float)y2));
                    }
                    else//一交點 
                        intersectedPts.Add(new PointF((float)x1, (float)y1));
                    return true;
                }
                else//沒有交點時 
                    return false;
            }
            else
            {//x1= 兩交點的x值、 a、b、c= x=(-b±√(b^2-4ac))/2a的係數
                x1 = -(Math.Pow(ctr1.X, 2) - Math.Pow(ctr2.X, 2) - Math.Pow(arc1.Radius, 2) + Math.Pow(arc2.Radius, 2)) / (2 * ctr2.X - 2 * ctr1.X);
                double a = 1,
                       b = -2 * ctr1.Y,
                       c = Math.Pow(x1, 2) + Math.Pow(ctr1.X, 2) - 2 * ctr1.X * x1 + Math.Pow(ctr1.Y, 2) - Math.Pow(arc1.Radius, 2);
                if (b * b - 4 * a * c >= 0)
                {
                    y1 = ((-b) + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);//y=(-b+√(b^2-4ac))/2a
                    y2 = ((-b) - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);//y=(-b-√(b^2-4ac))/2a
                    if (b * b - 4 * a * c > 0)//兩交點
                    {
                        intersectedPts.Add(new PointF((float)x1, (float)y1));
                        intersectedPts.Add(new PointF((float)x1, (float)y2));
                    }
                    else//一交點
                        intersectedPts.Add(new PointF((float)x1, (float)y1));
                    return true;
                }
                else//沒有交點時
                    return false;
            }
            return false;
        }
        public static bool IsTwoLineIntersected(Point3D[] line1, Point3D[] line2, out System.Drawing.PointF intersectedPt)
        {
            intersectedPt = new System.Drawing.PointF();
            double a1, a2, c1, c2;
            double b1 = -1; double b2 = -1;
            a1 = (line1[0].Y - line1[1].Y) / (line1[0].X - line1[1].X);
            a2 = (line2[0].Y - line2[1].Y) / (line2[0].X - line2[1].X);
            c1 = (line1[0].X * a1 - line1[0].Y);
            c2 = (line2[0].X * a2 - line2[0].Y);

            if (line1[0].X - line1[1].X == 0)
            {
                c1 = line1[0].X;
                a1 = 1;
                b1 = 0;
            }

            if (line2[0].X - line2[1].X == 0)
            {
                c2 = line2[0].X;
                a2 = 1;
                b2 = 0;
            }

            double delta = a1 * b2 - a2 * b1;

            // lines are parellel
            if (delta == 0)
                return false;

            // here is the intersection point!!
            double intersectX = (b2 * c1 - b1 * c2) / delta;
            double intersectY = (a1 * c2 - a2 * c1) / delta;

            intersectedPt = new System.Drawing.PointF((float)intersectX, (float)intersectY);
            return true;
        }
        #endregion
        /// <summary>
        /// Calculates the centroid of a closed non-self intersecting polygon
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static Point3D Centroid(List<Point3D> points)
        {
            double area = PolygonArea(points.ToArray());
            double cx = 0, cy = 0;
            if (area > 0)
            {
                for (int i = 0; i < points.Count; i++)
                {
                    Point3D pnt1 = points[i];
                    Point3D pnt2 = points[(i + 1) % points.Count];
                    double dFactor = (pnt1.X * pnt2.Y) - (pnt2.X * pnt1.Y);
                    cx += (pnt1.X + pnt2.X) * dFactor;
                    cy += (pnt1.Y + pnt2.Y) * dFactor;
                }
                cx *= 1 / (area * 6);
                cy *= 1 / (area * 6);
            }
            else if(points.Count > 1)
            {
                cx = (points[0].X + points[1].X) / 2;
                cy = (points[0].Y + points[1].Y) / 2;
            }
            return new Point3D(cx, cy, 0);
        }
        public static Tuple<int, Point3D> FindClosetPointOnPolyline(this List<Point3D> polyline, Point3D testpt, Matrix3D? mtx = null)
        {
            mtx = mtx == null ? Matrix3D.Identity : mtx;
            double mindistance = double.MaxValue;
            int minIndex = -1;
            Point3D minPt = new Point3D();

            for (int i = 1; i < polyline.Count; i++)
            {
                var pt1 = polyline[i - 1];
                var pt2 = polyline[i];
                pt1 = mtx.Value.Transform(pt1);
                pt2 = mtx.Value.Transform(pt2);
                Vector3D vectorTotestPt = testpt - pt1;
                Vector3D vectorOnpolyline = pt2 - pt1;
                double segLength = vectorOnpolyline.Length;
                vectorOnpolyline.Normalize();
                double dotProduct = Vector3D.DotProduct(vectorTotestPt, vectorOnpolyline);
                dotProduct = dotProduct < 0 ? 0 : dotProduct > segLength ? segLength : dotProduct;

                Point3D hitpt = pt1 + dotProduct * vectorOnpolyline;
                var dist = testpt.distanceTo(hitpt);

                if (dist < mindistance)
                {
                    minIndex = i;
                    minPt = hitpt;
                    mindistance = dist;
                }
            }

            return new Tuple<int,Point3D>(minIndex, minPt);

        }
        public static List<IntPoint> ToIntPoint(this List<Point3D> pts, Matrix3D mtx, int scale = 1)
        {
            List<IntPoint> result = new List<IntPoint>();
            foreach (var pt in pts)
            {
                var newpt = mtx.Transform(pt);
                IntPoint intpt = new IntPoint(newpt.X * scale, newpt.Y * scale);
                result.Add(intpt);
            }
            return result;
        }
        public static List<IntPoint> ToIntPoint(this List<System.Windows.Point> pts, int scale = 1)
        {
            List<IntPoint> result = new List<IntPoint>();
            foreach (var pt in pts)
            {
                IntPoint intpt = new IntPoint(pt.X * scale, pt.Y * scale);
                result.Add(intpt);
            }
            return result;
        }
        public static List<Point3D> ToPoint3D(this List<IntPoint> pts, double scale = 1)
        {
            List<Point3D> result = new List<Point3D>();
            foreach (var pt in pts)
            {
                Point3D intpt = new Point3D(((double)pt.X) * scale, ((double)pt.Y) * scale, 0);
                result.Add(intpt);
            }
            return result;
        }
        public static List<Point3D> ToPoint3D(this List<System.Windows.Point> pts, double scale = 1)
        {
            List<Point3D> result = new List<Point3D>();
            foreach (var pt in pts)
            {
                Point3D intpt = new Point3D(((double)pt.X) * scale, ((double)pt.Y) * scale, 0);
                result.Add(intpt);
            }
            return result;
        }
        public static List<System.Windows.Point> ToPoint(this List<IntPoint> pts, double scale = 1)
        {
            List<System.Windows.Point> result = new List<System.Windows.Point>();
            foreach (var pt in pts)
            {
                System.Windows.Point intpt = new System.Windows.Point(((double)pt.X) * scale, ((double)pt.Y) * scale);
                result.Add(intpt);
            }
            return result;
        }
        public static PointF? FindPointInPolygon(List<Point3D> polygon, Point3D? translate = null)
        {
            translate = translate == null ? new Point3D() : translate;
            Matrix3D transform = Matrix3D.Identity;
            transform.OffsetX = translate.Value.X;
            transform.OffsetY = translate.Value.Y;
            transform.OffsetZ = translate.Value.Z;

            InputGeometry input = new InputGeometry();
            int curVertex = 0;
            ConstructInputProfile(input, ref curVertex, 1, polygon);

            Mesh mesh = new Mesh();
            mesh.Behavior.ConformingDelaunay = false;
            mesh.Behavior.Convex = false;
            mesh.Triangulate(input);

            if (mesh.Triangles.Count > 0)
            {
                var fstTriangle = mesh.Triangles.First();
                var fstVtx = fstTriangle.GetVertex(0);
                var sndVtx = fstTriangle.GetVertex(1);
                var trdVtx = fstTriangle.GetVertex(2);

                double x = (fstVtx.X + sndVtx.X + trdVtx.X) / 3;
                double y = (fstVtx.Y + sndVtx.Y + trdVtx.Y) / 3;
                return new PointF((float)x, (float)y);
            }
            return null;
        }
        public static PointF FindPointInPolygon(List<System.Drawing.PointF> polygon, Point3D? translate = null)
        {
            if (polygon.Count < 3) return PointF.Empty;
            translate = translate == null ? new Point3D() : translate;

            PointF pt1 = polygon.First();
            PointF pt2 = polygon[1];

            PointF ctr = new PointF((pt1.X + pt2.X) / 2, (pt1.Y + pt2.Y) / 2);

            System.Windows.Media.Matrix mtx = new System.Windows.Media.Matrix();
            mtx.Rotate(90);
            Vector vc = new Vector(pt2.X - pt1.X, pt2.Y - pt1.Y);
            vc = mtx.Transform(vc);
            vc.Normalize();

            PointF newpt1 = new PointF((float)(ctr.X + vc.X), (float)(ctr.Y + vc.Y));
            PointF newpt2 = new PointF((float)(ctr.X - vc.X), (float)(ctr.Y - vc.Y));

            GraphicsPath path = new GraphicsPath();
            path.AddPolygon(polygon.ToArray());

            Region r = new Region();
            r.MakeEmpty();
            r.Union(path);

            if (r.IsVisible(newpt1))
                return new PointF(newpt1.X - (float)translate.Value.X, newpt1.Y - (float)translate.Value.Y);
            else if (r.IsVisible(newpt2))
                return new PointF(newpt2.X - (float)translate.Value.X, newpt1.Y - (float)translate.Value.Y);

            return PointF.Empty;
        }
        public static void ConstructInputProfile(InputGeometry input, ref int curVertex, int pcount, List<Point3D> path)
        {
            int orij = curVertex;
            foreach (var pt in path)
            {
                //var transformedPt = inverseMtx.Transform(pt);
                input.AddPoint(Math.Round(pt.X, 4), Math.Round(pt.Y, 4), pcount);
                if (curVertex - orij - 1 == path.Count - 2)
                    input.AddSegment(curVertex, orij);
                else
                    input.AddSegment(curVertex, curVertex + 1);

                curVertex++;
            }
        }
        public static Matrix3D Get3DTransformationMatrix(Point3D location, Vector3D vectorX, Vector3D vectorY, Vector3D vectorZ)
        {
            try
            {
                double[][] A = { new double[] { 1, 0, 0 }, new double[] { 0, 1, 0 }, new double[] { 0, 0, 1 } };
                DotNetMatrix.GeneralMatrix mOriginal = new DotNetMatrix.GeneralMatrix(A);

                double[][] B = { new double[] { vectorX.X, vectorX.Y, vectorX.Z }, new double[] { vectorY.X, vectorY.Y, vectorY.Z }, new double[] { vectorZ.X, vectorZ.Y, vectorZ.Z } };
                DotNetMatrix.GeneralMatrix mNew = new DotNetMatrix.GeneralMatrix(B);

                DotNetMatrix.GeneralMatrix mSolve = new DotNetMatrix.GeneralMatrix(3, 3);

                //if (!isReverseTransform)
                //    mSolve = mOriginal.Solve(mNew);
                //else
                mSolve = mNew.Solve(mOriginal);


                Matrix3D mProduct = new Matrix3D();

                mProduct.M11 = mSolve.GetElement(0, 0);
                mProduct.M12 = mSolve.GetElement(1, 0);
                mProduct.M13 = mSolve.GetElement(2, 0);
                mProduct.M14 = 0;

                mProduct.M21 = mSolve.GetElement(0, 1);
                mProduct.M22 = mSolve.GetElement(1, 1);
                mProduct.M23 = mSolve.GetElement(2, 1);
                mProduct.M24 = 0;

                mProduct.M31 = mSolve.GetElement(0, 2);
                mProduct.M32 = mSolve.GetElement(1, 2);
                mProduct.M33 = mSolve.GetElement(2, 2);
                mProduct.M34 = 0;

                mProduct.OffsetX = location.X;
                mProduct.OffsetY = location.Y;
                mProduct.OffsetZ = location.Z;
                mProduct.M44 = 1;

                return mProduct;
            }
            catch (Exception e)
            {

                return new Matrix3D();
            }
        }
        public static AxisAngleRotation3D GetAxisRotation3D(Vector3D normal, Vector3D? axis = null)
        {
            Vector3D oriZ = new Vector3D(0, 0, 1);
            Vector3D rotAxis = Vector3D.CrossProduct(oriZ, normal);
            rotAxis.Normalize();
            double rotAngle = Math.Acos(Vector3D.DotProduct(oriZ, normal));
            if (rotAngle != 0 && !double.IsNaN(rotAngle))
            {
                if (axis != null)
                {
                    // use the normal as the pivot to rotate again for aligning the axis

                }
                else
                    return new AxisAngleRotation3D(rotAxis, rotAngle.ToDegree());
            }
            return new AxisAngleRotation3D();
        }
        public static Matrix3D Solve(this Matrix3D mtx1, Matrix3D mtx2)
        {
            double[][] A = new double[4][];
            A[0] = new double[] { mtx1.M11, mtx1.M12, mtx1.M13, mtx1.M14 };
            A[1] = new double[] { mtx1.M21, mtx1.M22, mtx1.M23, mtx1.M24 };
            A[2] = new double[] { mtx1.M31, mtx1.M32, mtx1.M33, mtx1.M34 };
            A[3] = new double[] { mtx1.OffsetX, mtx1.OffsetY, mtx1.OffsetZ, mtx1.M44 };
            double[][] B = new double[4][];
            B[0] = new double[] { mtx2.M11, mtx2.M12, mtx2.M13, mtx2.M14 };
            B[1] = new double[] { mtx2.M21, mtx2.M22, mtx2.M23, mtx2.M24 };
            B[2] = new double[] { mtx2.M31, mtx2.M32, mtx2.M33, mtx2.M34 };
            B[3] = new double[] { mtx2.OffsetX, mtx2.OffsetY, mtx2.OffsetZ, mtx2.M44 };

            GeneralMatrix mtxA = new GeneralMatrix(A, 4, 4);
            GeneralMatrix mtxB = new GeneralMatrix(B, 4, 4);

            GeneralMatrix mtxSolve = mtxA.Solve(mtxB);

            Matrix3D result = new Matrix3D(
                mtxSolve.GetElement(0, 0), mtxSolve.GetElement(0, 1), mtxSolve.GetElement(0, 2), mtxSolve.GetElement(0, 3),
                mtxSolve.GetElement(1, 0), mtxSolve.GetElement(1, 1), mtxSolve.GetElement(1, 2), mtxSolve.GetElement(1, 3),
                mtxSolve.GetElement(2, 0), mtxSolve.GetElement(2, 1), mtxSolve.GetElement(2, 2), mtxSolve.GetElement(2, 3),
                mtxSolve.GetElement(3, 0), mtxSolve.GetElement(3, 1), mtxSolve.GetElement(3, 2), mtxSolve.GetElement(3, 3));

            return result;
        }
        public static Matrix3D Clone(this Matrix3D mtx)
        {
            Matrix3D clone = new Matrix3D(
                mtx.M11, mtx.M12, mtx.M13, mtx.M14,
                mtx.M21, mtx.M22, mtx.M23, mtx.M34,
                mtx.M31, mtx.M32, mtx.M33, mtx.M24,
                mtx.OffsetX, mtx.OffsetY, mtx.OffsetZ, mtx.M44
                );
            return clone;
        }
        public static bool IsAboutIdentity(this Matrix3D mtx, int digit = 3)
        {
            var clonemtx = mtx.Clone();
            clonemtx.M11 = Math.Round(mtx.M11, digit);
            clonemtx.M12 = Math.Round(mtx.M12, digit);
            clonemtx.M13 = Math.Round(mtx.M13, digit);
            clonemtx.M14 = Math.Round(mtx.M14, digit);
            clonemtx.M21 = Math.Round(mtx.M21, digit);
            clonemtx.M22 = Math.Round(mtx.M22, digit);
            clonemtx.M23 = Math.Round(mtx.M23, digit);
            clonemtx.M24 = Math.Round(mtx.M24, digit);
            clonemtx.M31 = Math.Round(mtx.M31, digit);
            clonemtx.M32 = Math.Round(mtx.M32, digit);
            clonemtx.M33 = Math.Round(mtx.M33, digit);
            clonemtx.M34 = Math.Round(mtx.M34, digit);
            clonemtx.OffsetX = Math.Round(mtx.OffsetX, digit);
            clonemtx.OffsetY = Math.Round(mtx.OffsetY, digit);
            clonemtx.OffsetZ = Math.Round(mtx.OffsetZ, digit);
            clonemtx.M44 = Math.Round(mtx.M44, digit);

            return clonemtx.IsIdentity;
        }
    }
}
