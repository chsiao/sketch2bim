﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Runtime.InteropServices;

namespace GeomLib
{
    [StructLayout(LayoutKind.Sequential)]
    public class BasicPrimitive
    {
        public enum PrimitiveType
        {
            LINE = 0,
            ARC = 1,
            CLOTHOID = 2
        };
        public double length;
        public double startAngle;
        public double startCurvature;
        public double curvatureDerivative;
        public PrimitiveType type;
        public SPoint start;
    };

    public class Primitive : BasicPrimitive
    {
        private ControlPoint pnode;
        private ControlPoint nnode;
        private string guid = "";

        /// <summary>
        /// for topology
        /// and generate the segement when both previous node and next node are available
        /// </summary>
        public ControlPoint PreviousNode {
            get
            {
                return pnode;
            }
            set
            {
                pnode = value;
                this.generateSegment();
            }
        }
        public ControlPoint NextNode
        {
            get
            {
                return nnode;
            }
            set
            {
                nnode = value;
                this.generateSegment();
            }
        }

        public string GUID { get { return this.guid; } }
        public CustomStroke Host { get; set; }
        /// <summary>
        /// for drawing
        /// </summary>
        public PathSegment Segment { get; private set; }
        

        public Primitive()
        {
            this.pnode = null;
            this.nnode = null;
            this.guid = Guid.NewGuid().ToString();
            this.Host = null;
        }
        public Primitive(BasicPrimitive bpm, CustomStroke host = null) : this()
        {
            // these inherited properties are for the storage of
            // generated parameters from cornucopia engine
            this.length = bpm.length;
            this.startAngle = bpm.startAngle;
            this.start = bpm.start;
            this.startCurvature = bpm.startCurvature;
            this.type = bpm.type;
            this.curvatureDerivative = bpm.curvatureDerivative;
            this.Host = host;
        }
        /// <summary>
        /// generate segment when the PreviousNode and NextNode is defined
        /// </summary>
        /// <returns>return ture when successfully genrated</returns>
        public bool generateSegment()
        {
            if (this.pnode == null || this.nnode == null) return false;
            // determine if the previous node is the first node of the path
            // if yes, reset the start point of the whole path again
            if (this.PreviousNode.ConnPrimitives.Count == 1)
            {
                Point strpt = new Point(this.PreviousNode.X, this.PreviousNode.Y);
                // check whether we have created the presentation or not
                if (this.Host != null && this.Host.Presentation != null)
                {
                    // get the path geometry
                    var geometry = (PathGeometry)this.Host.Presentation.Data;
                    if (geometry.Figures.Count > 0)
                    {
                        // reset the start point here
                        geometry.Figures[0].StartPoint = strpt;
                    }
                }
            }
            
            Point endpt = new Point(this.NextNode.X, this.NextNode.Y);
            var angle = this.startAngle;
            var length = this.length;
            var strcurveture = this.startCurvature;
            var isgenerated = false;

            switch ((int)this.type)
            {
                case 0:
                    if (this.Segment == null)
                        this.Segment = new LineSegment(endpt, true);
                    else
                        ((LineSegment)this.Segment).Point = endpt;
                    isgenerated = true;
                    break;
                case 1:
                    //segment = new LineSegment(new Point(nextX, nextY), true);
                    double radius = 1 / strcurveture;
                    Size cruveSize = new Size(Math.Abs(radius), Math.Abs(radius));
                    var direction = radius > 0 ? SweepDirection.Clockwise : SweepDirection.Counterclockwise;
                    double a = (this.length / radius) * 180 / Math.PI;
                    if (this.Segment == null)
                        this.Segment = new ArcSegment(endpt, cruveSize, a, false, direction, true);
                    else
                        ((ArcSegment)this.Segment).Point = endpt;
                    isgenerated = true;
                    break;
                case 2:
                    break;
            }
            return isgenerated;
        }
    }
}
