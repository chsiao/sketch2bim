﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Media;
using System.Windows.Media.Media3D;
using Petzold.Media3D;

namespace GeomLib
{
    public static class Extensions
    {
        public static readonly DependencyProperty IDProperty =
            DependencyProperty.RegisterAttached("ID", typeof(string), typeof(Extensions), new PropertyMetadata(default(string)));

        public static void SetIDProperty(UIElement element, string value)
        {
            element.SetValue(IDProperty, value);
        }

        public static string GetIDProperty(UIElement element)
        {
            return (string)element.GetValue(IDProperty);
        }

        public static readonly DependencyProperty IsInHitTestProperty =
            DependencyProperty.RegisterAttached("IsInHitTest", typeof(bool), typeof(Extensions), new PropertyMetadata(default(bool)));

        public static void SetIsInHitTestProperty(UIElement element, bool value)
        {
            element.SetValue(IsInHitTestProperty, value);
        }

        public static bool GetIsInHitTestProperty(UIElement element)
        {
            return (bool)element.GetValue(IsInHitTestProperty);
        }

        public static readonly DependencyProperty MatrixProperty =
            DependencyProperty.RegisterAttached("Matrix", typeof(Matrix3D), typeof(Extensions), new PropertyMetadata(default(Matrix3D)));

        public static void SetTransformProperty(UIElement element, Matrix3D value)
        {
            element.SetValue(MatrixProperty, value);
        }

        public static Matrix3D GetTransformProperty(UIElement element)
        {
            return (Matrix3D)element.GetValue(MatrixProperty);
        }

        public static readonly DependencyProperty AxisProperty1 =
            DependencyProperty.RegisterAttached("Axis1", typeof(WireLine), typeof(Extensions), new PropertyMetadata(default(WireLine)));

        public static void SetAxis1Property(UIElement element, WireLine value)
        {
            element.SetValue(AxisProperty1, value);
        }

        public static WireLine GetAxis1Property(UIElement element)
        {
            return (WireLine)element.GetValue(AxisProperty1);
        }

        public static readonly DependencyProperty AxisProperty2 =
            DependencyProperty.RegisterAttached("Axis2", typeof(WireLine), typeof(Extensions), new PropertyMetadata(default(WireLine)));

        public static void SetAxis2Property(UIElement element, WireLine value)
        {
            element.SetValue(AxisProperty2, value);
        }

        public static WireLine GetAxis2Property(UIElement element)
        {
            return (WireLine)element.GetValue(AxisProperty2);
        }

        /// <summary>
        /// Rotates a Vector3 around the Z axis
        /// Change the roll of a Vector3
        /// </summary>
        /// <param name="v1">The Vector3 to be rotated</param>
        /// <param name="radians">The angle to rotate the Vector3 around in radians</param>
        /// <returns>Vector3 representing the rotation around the Z axis</returns>
        public static Vector3D Roll(this Vector3D v1, double degree)
        {
            double x = (v1.X * Math.Cos(degree)) - (v1.Y * Math.Sin(degree));
            double y = (v1.X * Math.Sin(degree)) + (v1.Y * Math.Cos(degree));
            double z = v1.Z;
            return new Vector3D(x, y, z);
        }

    }
}
