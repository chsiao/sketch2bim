﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Petzold.Media3D
{
    public enum WireTypeEnum
    {
        MainAxis,
        CursorAxis,
        HitAxis,
        ShapeEdge,
        Pivot
    }
}
