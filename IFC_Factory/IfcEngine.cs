////////////////////////////////////////////////////////////////////////
//  Author:  Pim van den Helm
//  Date:    11 July 2008
//  Project: IFC Engine Series (example using DLL in C#)
//
//  This code may be used and edited freely,
//  also for commercial projects in open and closed source software
//
//  In case of use of the DLL:
//  be aware of license fee for use of this DLL when used commercially
//  more info for commercial use:  peter.bonsma@tno.nl
//
//  more info using the IFC Engine DLL in other languages, 
//  see other examples or contact: pim.vandenhelm@tno.nl
////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace IFC_Factory
{
    public class IFCEngine
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static int sdaiADB = 1;
        public static int sdaiAGGR = 2;
        public static int sdaiBINARY = 3;
        public static int sdaiBOOLEAN = 4;
        public static int sdaiENUM = 5;
        public static int sdaiINSTANCE = 6;
        public static int sdaiINTEGER = 7;
        public static int sdaiLOGICAL = 8;
        public static int sdaiREAL = 9;
        public static int sdaiSTRING = 10;


        [DllImport("IFCEngine.dll", EntryPoint = "setStringUnicode")]
        public static extern int setStringUnicode(bool unicode);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiOpenModelBN")]
        public static extern int sdaiOpenModelBN(int repository, string fileName, string schemaName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateModelBN")]
        public static extern int sdaiCreateModelBN(int repository, string fileName, string schemaName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiSaveModelBN")]
        public static extern void sdaiSaveModelBN(int model, string fileName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiSaveModelAsXmlBN")]
        public static extern void sdaiSaveModelAsXmlBN(int model, string fileName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCloseModel")]
        public static extern void sdaiCloseModel(int model);

        [DllImport("IFCEngine.dll", EntryPoint = "engiGetAggrElement")]
        public static extern int engiGetAggrElement(int aggregate, int elementIndex, int valueType, ref int pValue);

        [DllImport("IFCEngine.dll", EntryPoint = "engiGetInstanceMetaInfo")]
        public static extern int engiGetInstanceMetaInfo(int instance, int localId, int className, int classNameUC);

        [DllImport("IFCEngine.dll", EntryPoint = "engiGetInstanceLocalId")]
        public static extern int engiGetInstanceLocalId(int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "engiGetInstanceClassInfo")]
        public static extern string engiGetInstanceClassInfo(int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "engiGetInstanceClassInfoUC")]
        public static extern string engiGetInstanceClassInfoUC(int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiAppend")]
        public static extern void sdaiAppend(int list, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiAppend")]
        public static extern void sdaiAppend(int list, int valueType, ref double value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiBeginning")]
        public static extern void sdaiBeginning(int iterator);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateADB")]
        public static extern int sdaiCreateADB(int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateAggr")]
        public static extern int sdaiCreateAggr(int instance, int attribute);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateAggrBN")]
        public static extern int sdaiCreateAggrBN(int instance, string attributeName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateInstance")]
        public static extern int sdaiCreateInstance(int model, int entity);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateInstanceBN")]
        public static extern int sdaiCreateInstanceBN(int model, string entityName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateInstanceBNEI")]
        public static extern int sdaiCreateInstanceBNEI(int model, string entityName, int express_id);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiCreateIterator")]
        public static extern int sdaiCreateIterator(int aggregate);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiDeleteInstance")]
        public static extern void sdaiDeleteInstance(int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiDeleteIterator")]
        public static extern void sdaiDeleteIterator(int iterator);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiEnd")]
        public static extern void sdaiEnd(int iterator);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiErrorQuery")]
        public static extern int sdaiErrorQuery();

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetAggrByIterator")]
        public static extern int sdaiGetAggrByIterator(int iterator, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetAttr")]
        public static extern int sdaiGetAttr(int instance, int attribute, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetAttrBN")]
        public static extern int sdaiGetAttrBN(int instance, string attributeName, int valueType, ref int value);
        
        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetAttrBN")]
        public static extern int sdaiGetAttrBN(int instance, string attributeName, int valueType, ref double value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetAttrDefinition")]
        public static extern int sdaiGetAttrDefinition(int entity, string attributeName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetEntity")]
        public static extern int sdaiGetEntity(int model, string entityName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetEntityExtent")]
        public static extern int sdaiGetEntityExtent(int model, int entity);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetEntityExtentBN")]
        public static extern int sdaiGetEntityExtentBN(int model, string entityName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetInstanceType")]
        public static extern int sdaiGetInstanceType(int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetMemberCount")]
        public static extern int sdaiGetMemberCount(int aggregate);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiIsInstanceOf")]
        public static extern bool sdaiIsInstanceOf(int instance, int entity);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiIsInstanceOfBN")]
        public static extern bool sdaiIsInstanceOfBN(int instance, string entityName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiNext")]
        public static extern bool sdaiNext(int iterator);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPrepend")]
        public static extern void sdaiPrepend(int list, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPrevious")]
        public static extern bool sdaiPrevious(int iterator);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPutADBTypePath")]
        public static extern void sdaiPutADBTypePath(int ADB, int pathCount, string path);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPutAggrByIterator")]
        public static extern void sdaiPutAggrByIterator(int iterator, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPutAttr")]
        public static extern void sdaiPutAttr(int instance, int attribute, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPutAttrBN")]
        public static extern void sdaiPutAttrBN(int instance, string attributeName, int valueType, int value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPutAttrBN")]
        public static extern void sdaiPutAttrBN(int instance, string attributeName, int valueType, string value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiPutAttrBN")]
        public static extern void sdaiPutAttrBN(int instance, string attributeName, int valueType, ref double value);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiTestAttr")]
        public static extern bool sdaiTestAttr(int instance, int attribute);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiTestAttrBN")]
        public static extern bool sdaiTestAttrBN(int instance, string attributeName);

        [DllImport("IFCEngine.dll", EntryPoint = "initializeModelling")]
        public static extern int initializeModelling(int model, ref int noVertices, ref int noIndices, double scale);

        [DllImport("IFCEngine.dll", EntryPoint = "initializeModellingInstance")]
        public static extern int initializeModellingInstance(int model, ref int noVertices, ref int noIndices, double scale, int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "initializeModellingInstanceEx")]
        public static extern int initializeModellingInstanceEx(int model, ref int noVertices, ref int noIndices, double scale, int instance, int instanceList);

        [DllImport("IFCEngine.dll", EntryPoint = "finalizeModelling")]
        public static extern int finalizeModelling(int model, ref float pVertices, ref int pIndices, int FVF);

        [DllImport("IFCEngine.dll", EntryPoint = "getInstanceInModelling")]
        public static extern int getInstanceInModelling(int model, int instance, int mode, ref int startVertex, ref int startIndex, ref int primitiveCount);

        [DllImport("IFCEngine.dll", EntryPoint = "getInstanceDerivedPropertiesInModelling")]
        public static extern int getInstanceDerivedPropertiesInModelling(int model, int instance, ref int height, ref int width, ref int thickness);

        [DllImport("IFCEngine.dll", EntryPoint = "getInstanceDerivedBoundingBox")]
        public static extern int getInstanceDerivedBoundingBox(int model, int instance, ref int Ox, ref int Oy, ref int Oz, ref int Vx, ref int Vy, ref int Vz);

        [DllImport("IFCEngine.dll", EntryPoint = "getInstanceDerivedTransformationMatrix")]
        public static extern int getInstanceDerivedTransformationMatrix(int model, int instance,
                                        ref int _11, ref int _12, ref int _13, ref int _14,
                                        ref int _21, ref int _22, ref int _23, ref int _24,
                                        ref int _31, ref int _32, ref int _33, ref int _34,
                                        ref int _41, ref int _42, ref int _43, ref int _44);

        [DllImport("IFCEngine.dll", EntryPoint = "circleSegments")]
        public static extern void circleSegments(int circles, int smallCircles);

        [DllImport("IFCEngine.dll", EntryPoint = "getTimeStamp")]
        public static extern int getTimeStamp(int model);

        [DllImport("IFCEngine.dll", EntryPoint = "getChangedData")]
        public static extern string getChangedData(int model, int timeStamp);

        [DllImport("IFCEngine.dll", EntryPoint = "setChangedData")]
        public static extern void setChangedData(int model, int timeStamp, string changedData);

        [DllImport("IFCEngine.dll", EntryPoint = "internalGetBoundingBox")]
        public static extern int internalGetBoundingBox(int model, int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "internalGetCenter")]
        public static extern int internalGetCenter(int model, int instance);

        [DllImport("IFCEngine.dll", EntryPoint = "internalSetLink")]
        public static extern void internalSetLink(int instance, string attributeName, int linked_id);

        [DllImport("IFCEngine.dll", EntryPoint = "internalAddAggrLink")]
        public static extern void internalAddAggrLink(int list, int linked_id);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetStringAttrBN")]
        public static extern string sdaiGetStringAttrBN(int instance, string attributeName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetInstanceAttrBN")]
        public static extern int sdaiGetInstanceAttrBN(int instance, string attributeName);

        [DllImport("IFCEngine.dll", EntryPoint = "sdaiGetAggregationAttrBN")]
        public static extern int sdaiGetAggregationAttrBN(int instance, string attributeName);

        //[STAThread]
        //static void Main()
        //{
        //    create ct = new create();
        //    ct.createWalls();
        //    //Application.EnableVisualStyles();
        //    //Application.SetCompatibleTextRenderingDefault(false);
        //    //Application.Run(new create());
        //}
    }
}