﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperLibrary.RDF;
using GeomLib;
using System.IO;

namespace IFC_Factory
{
    public class IfcGeneartor
    {
        private IFC ifcmodel;
        private HashSet<RDF_GeometricItem> ProcessedItem;
        double scale = 0.1;

        // Todo: three problems:
        // 1. header
        // 2. IFCRELAGGREGATES -- the aggregation should be a collection

        public IfcGeneartor()
        {
            this.ifcmodel = new IFC();
            var iscreated  = ifcmodel.createModel();
            ProcessedItem = new HashSet<RDF_GeometricItem>();
            Dictionary<int, List<int>> storyElementsMap = new Dictionary<int, List<int>>();
            foreach (var story in ifcmodel.buildingstorey)
                storyElementsMap.Add(story, new List<int>());
            
            foreach (var rdfobj in RDFWrapper.All_RDF_Objects.Values)
            {
                if (rdfobj is RDF_SweptAreaSolid)
                {
                    if (((RDF_SweptAreaSolid)rdfobj).ShapeSemantics == "wall")
                    {
                        var wallid = createIfcBuildingElement((RDF_SweptAreaSolid)rdfobj, "wall");
                        storyElementsMap[this.ifcmodel.buildingstorey.Last()].Add(wallid);
                    }
                    else if(((RDF_SweptAreaSolid)rdfobj).ShapeSemantics == "slab")
                    {
                        var newbuildingStory = ifcmodel.createBuildingStory("Floor " + (ifcmodel.buildingstorey.Count + 1));
                        storyElementsMap.Add(newbuildingStory, new List<int>());
                        ifcmodel.addRelAggregateElement(ifcmodel.building, newbuildingStory, "BuildingContainer");

                        var slabid = createIfcBuildingElement((RDF_SweptAreaSolid)rdfobj, "slab");
                        storyElementsMap[this.ifcmodel.buildingstorey.Last()].Add(slabid);
                    }
                    else if(((RDF_SweptAreaSolid)rdfobj).Direction != null)
                    {
                        var slabid = createIfcBuildingElement((RDF_SweptAreaSolid)rdfobj, "column");
                        storyElementsMap[this.ifcmodel.buildingstorey.Last()].Add(slabid);
                    }
                }
            }

            foreach (var story in ifcmodel.buildingstorey)
            {
                ifcmodel.createContainment(story, storyElementsMap[story]);
            }

        }

        private int createIfcBuildingElement(RDF_SweptAreaSolid solid, string type)
        {
            var profile = solid.SweptProfile;
            var direction = solid.Direction;

            double height = 0;
            if (direction is RDF_Line3D)
            {
                var dirLine = (RDF_Line3D)direction;
                height = dirLine.Pts[1].distanceTo(dirLine.Pts[0]);
            }

            listOf2DPoints points = new listOf2DPoints();
            if (profile is RDF_Polygon3D)
                createListOf2DPoints(profile, points);
            
            Matrix M = createMatrixForEngine(solid);

            int placement = 0;
            var fstStorey = ifcmodel.buildingstorey.Last();
            var reltoPlacement = IFCEngine.sdaiGetInstanceAttrBN(fstStorey, "ObjectPlacement");
            if (type == "wall")
            {
                listOf2DPoints pathpts = null;
                if (profile.DirectionFrom != null)
                {
                    pathpts = new listOf2DPoints();
                    createListOf2DPoints(profile.DirectionFrom, pathpts);
                }
                var wallInstance = ifcmodel.createWall(points, scale * height, M, out placement, reltoPlacement, pathpts);

                var br1 = solid.BackRelations.Find(br => br.Name == BackRelationName.Dim3_BooleanProduct1);
                if (br1 != null && br1.RelatedTo is RDF_Boolean3D)
                {
                    var rdfBool = (RDF_Boolean3D)br1.RelatedTo;
                    var rdfOpening = rdfBool.Second;
                    if (rdfOpening is RDF_SweptAreaSolid)
                    {
                        var rdfOpeningSweptArea = (RDF_SweptAreaSolid)rdfOpening;
                        var openingProfile = rdfOpeningSweptArea.SweptProfile;
                        listOf2DPoints openingPoints = new listOf2DPoints();
                        if (openingProfile is RDF_Polygon3D)
                            createListOf2DPoints(openingProfile, openingPoints);

                        if (openingPoints.Count > 2)
                        {
                            Matrix openingM = createMatrixForEngine(openingProfile);
                            var openingInstance = ifcmodel.addOpeningElement(openingPoints, 1000, openingM, placement);
                            ifcmodel.addRelVoidsElement(wallInstance, openingInstance);
                        }
                    }
                    else if(rdfOpening is RDF_ExtrudedPolygon)
                    {
                        listOf2DPoints openingPoints1 = new listOf2DPoints();
                        foreach (var pt in ((RDF_ExtrudedPolygon)rdfOpening).Profile)
                            openingPoints1.Add(new _2DPoint(scale * pt.X, scale * pt.Y));
                        if (openingPoints1.Count > 2)
                        {
                            Matrix openingM = createMatrixForEngine((RDF_ExtrudedPolygon)rdfOpening);
                            var openingInstance = ifcmodel.addOpeningElement(openingPoints1, 1000, openingM, placement);
                            ifcmodel.addRelVoidsElement(wallInstance, openingInstance);
                        }

                        foreach (var opening in rdfOpening.Constraint.ConstraintObjects)
                        {
                            if(opening is RDF_ExtrudedPolygon)
                            {
                                var openingProfile = ((RDF_ExtrudedPolygon)opening).Profile;
                                listOf2DPoints openingPoints = new listOf2DPoints();
                                foreach (var pt in openingProfile)
                                    openingPoints.Add(new _2DPoint(scale * pt.X, scale * pt.Y));
                                if (openingPoints.Count > 2)
                                {
                                    Matrix openingM = createMatrixForEngine((RDF_ExtrudedPolygon)opening);
                                    var openingInstance = ifcmodel.addOpeningElement(openingPoints, 1000, openingM, placement);
                                    ifcmodel.addRelVoidsElement(wallInstance, openingInstance);
                                }
                            }
                        }
                    }

                }

                return wallInstance;
            }
            else if (type == "slab")
                return ifcmodel.createSlab(points, scale * height, M, out placement, reltoPlacement);
            else if(type == "column" && direction != null)
                return ifcmodel.createColumn(points, scale * height, M, out placement, reltoPlacement);
            return 0;
        }

        private Matrix createMatrixForEngine(RDF_GeometricItem solid)
        {
            Matrix M = new Matrix();

            M._11 = solid.Transform.GetMatrix3D().M11;//1;
            M._12 = solid.Transform.GetMatrix3D().M12;//0;
            M._13 = solid.Transform.GetMatrix3D().M13;//0;
            M._14 = solid.Transform.GetMatrix3D().M14;//0;
            M._21 = solid.Transform.GetMatrix3D().M21;//0;
            M._22 = solid.Transform.GetMatrix3D().M22;//1;
            M._23 = solid.Transform.GetMatrix3D().M23;//0;
            M._24 = solid.Transform.GetMatrix3D().M24;//0;
            M._31 = solid.Transform.GetMatrix3D().M31;//0;
            M._32 = solid.Transform.GetMatrix3D().M32;//0;
            M._33 = solid.Transform.GetMatrix3D().M33;//1;
            M._34 = solid.Transform.GetMatrix3D().M34;//0;
            M._41 = scale * solid.Transform.GetMatrix3D().OffsetX;
            M._42 = scale * solid.Transform.GetMatrix3D().OffsetY;
            M._43 = scale * solid.Transform.GetMatrix3D().OffsetZ; // elevation
            M._44 = solid.Transform.GetMatrix3D().M44;
            return M;
        }

        private void createListOf2DPoints(RDF_Polygon3D profile, listOf2DPoints points)
        {
            var polygonProfile = (RDF_Polygon3D)profile;
            var pathGeom = polygonProfile.pathgeom;
            if (pathGeom != null && pathGeom.Paths != null)
            {
                var pts = pathGeom.Paths.First();

                foreach (var pt in pts)
                    points.Add(new _2DPoint(scale * pt.X, scale * pt.Y));
            }
        }

        public void Save(string filename)
        {
            this.ifcmodel.saveModel(filename);
            try
            {
                StringBuilder stbldg = new StringBuilder();
                using (StreamReader sr = new StreamReader(filename))
                {   
                    int i=0;
                    while (!sr.EndOfStream)
                    {
                        String line = sr.ReadLine();
                        if (i == 2)
                            line = "FILE_DESCRIPTION(('ViewDefinition [CoordinationView_V2.0]','ExchangeRequirement[Architecture]'),'2;1');";
                        else if (i == 4)
                            line = "FILE_SCHEMA(('IFC2X3'));";
                        line += "\n";
                        stbldg.Append(line);
                        i++;
                    }
                }

                using (StreamWriter sw = new StreamWriter(filename))
                {
                    sw.Write(stbldg);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

    }
}
