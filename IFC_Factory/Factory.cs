﻿using System;
using System.Collections.Generic;
using System.Text;
using GeomLib;
using System.Windows.Media;
using System.Windows.Media.Media3D;
//using GeomLib.Geometry;
using DotNetMatrix;
using ShortGuid;
//using System.Windows.Forms;

namespace IFC_Factory
{
    public class Matrix
    {
        public double _11;
        public double _12;
        public double _13;
        public double _14;
        public double _21;
        public double _22;
        public double _23;
        public double _24;
        public double _31;
        public double _32;
        public double _33;
        public double _34;
        public double _41;
        public double _42;
        public double _43;
        public double _44;

        public static Matrix Identity()
        {
            Matrix output = new Matrix();
            output._11 = 1;
            output._12 = 0;
            output._13 = 0;
            output._14 = 0;
            output._21 = 0;
            output._22 = 1;
            output._23 = 0;
            output._24 = 0;
            output._31 = 0;
            output._32 = 0;
            output._33 = 1;
            output._34 = 0;
            output._41 = 0;
            output._42 = 0;
            output._43 = 0;
            output._44 = 1;
            return output;
        }
    }

    public class _2DPoint
    {
        public double x;
        public double y;

        public _2DPoint(double iX, double iY)
        {
            x = iX;
            y = iY;
        }

        public _2DPoint Next;
    };

    public class listOf2DPoints
    {
        private int size;

        public listOf2DPoints()
        {
            size = 0;
            Head = null;
        }

        public int Count
        {
            get { return size; }
        }

        public _2DPoint Head;

        public int Add(_2DPoint newPoint)
        {
            _2DPoint point = new _2DPoint(newPoint.x, newPoint.y);

            point = newPoint;
            point.Next = Head;
            Head = point;
            return size++;
        }
    }

    public class _3DPoint
    {
        public double x;
        public double y;
        public double z;

        public _3DPoint(double iX, double iY, double iZ)
        {
            x = iX;
            y = iY;
            z = iZ;
        }

        public _3DPoint Next;
    };


    public class listOf3DPoints
    {
        private int size;

        public listOf3DPoints()
        {
            size = 0;
            Head = null;
        }

        public int Count
        {
            get { return size; }
        }

        public _3DPoint Head;

        public int Add(_3DPoint newPoint)
        {
            _3DPoint point = new _3DPoint(newPoint.x, newPoint.y, newPoint.z);

            point = newPoint;
            point.Next = Head;
            Head = point;
            return size++;
        }
    }

    public class Geometry
    {
        public Matrix IdentityMatrix(Matrix M)
        {
            M._11 = 1;
            M._12 = 0;
            M._13 = 0;
            M._14 = 0;
            M._21 = 0;
            M._22 = 1;
            M._23 = 0;
            M._24 = 0;
            M._31 = 0;
            M._32 = 0;
            M._33 = 1;
            M._34 = 0;
            M._41 = 0;
            M._42 = 0;
            M._43 = 0;
            M._44 = 1;

            return M;
        }
    };

    public class IFC
    {
        public int model;
        public int building;
        public List<int> buildingstorey;

        public IFC()
        {
            buildingstorey = new List<int>();
        }

        private int buildCartesianPointInstance(double x, double y)
        {
            int ifcCartesianPointInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCCARTESIANPOINT"),
                aggrCoordinates = IFCEngine.sdaiCreateAggrBN(ifcCartesianPointInstance, "Coordinates");

            IFCEngine.sdaiAppend(aggrCoordinates, IFCEngine.sdaiREAL, ref x);
            IFCEngine.sdaiAppend(aggrCoordinates, IFCEngine.sdaiREAL, ref y);

            return ifcCartesianPointInstance;
        }

        private int buildCartesianPointInstance(double x, double y, double z)
        {
            int ifcCartesianPointInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCCARTESIANPOINT"),
                aggrCoordinates = IFCEngine.sdaiCreateAggrBN(ifcCartesianPointInstance, "Coordinates");

            IFCEngine.sdaiAppend(aggrCoordinates, IFCEngine.sdaiREAL, ref x);
            IFCEngine.sdaiAppend(aggrCoordinates, IFCEngine.sdaiREAL, ref y);
            IFCEngine.sdaiAppend(aggrCoordinates, IFCEngine.sdaiREAL, ref z);

            return ifcCartesianPointInstance;
        }

        private int buildDirectionInstance(double x, double y)
        {
            int ifcDirectionInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCDIRECTION"),
                aggrDirectionRatios = IFCEngine.sdaiCreateAggrBN(ifcDirectionInstance, "DirectionRatios");

            IFCEngine.sdaiAppend(aggrDirectionRatios, IFCEngine.sdaiREAL, ref x);
            IFCEngine.sdaiAppend(aggrDirectionRatios, IFCEngine.sdaiREAL, ref y);

            return ifcDirectionInstance;
        }

        private int buildDirectionInstance(double x, double y, double z)
        {
            int ifcDirectionInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCDIRECTION"),
                aggrDirectionRatios = IFCEngine.sdaiCreateAggrBN(ifcDirectionInstance, "DirectionRatios");

            IFCEngine.sdaiAppend(aggrDirectionRatios, IFCEngine.sdaiREAL, ref x);
            IFCEngine.sdaiAppend(aggrDirectionRatios, IFCEngine.sdaiREAL, ref y);
            IFCEngine.sdaiAppend(aggrDirectionRatios, IFCEngine.sdaiREAL, ref z);

            return ifcDirectionInstance;
        }

        private int buildAxis2Placement3DInstance(Matrix M)
        {
            int ifcAxis2Placement3DInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCAXIS2PLACEMENT3D");

            IFCEngine.sdaiPutAttrBN(ifcAxis2Placement3DInstance, "Location", IFCEngine.sdaiINSTANCE, buildCartesianPointInstance(M._41, M._42, M._43));
            //IFCEngine.sdaiPutAttrBN(ifcAxis2Placement3DInstance, "Location", IFCEngine.sdaiINSTANCE, buildCartesianPointInstance(0, 0, 0));
            IFCEngine.sdaiPutAttrBN(ifcAxis2Placement3DInstance, "Axis", IFCEngine.sdaiINSTANCE, buildDirectionInstance(M._31, M._32, M._33));
            IFCEngine.sdaiPutAttrBN(ifcAxis2Placement3DInstance, "RefDirection", IFCEngine.sdaiINSTANCE, buildDirectionInstance(M._11, M._12, M._13));

            return ifcAxis2Placement3DInstance;
        }

        private int buildAxis2Placement2DInstance(Matrix M)
        {
            int ifcAxis2Placement2DInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCAXIS2PLACEMENT2D");

            IFCEngine.sdaiPutAttrBN(ifcAxis2Placement2DInstance, "Location", IFCEngine.sdaiINSTANCE, buildCartesianPointInstance(M._41, M._42));
            IFCEngine.sdaiPutAttrBN(ifcAxis2Placement2DInstance, "RefDirection", IFCEngine.sdaiINSTANCE, buildDirectionInstance(M._11, M._12));

            return ifcAxis2Placement2DInstance;
        }

        private int buildLocalPlacementInstance(Matrix M)
        {
            int ifcLocalPlacementInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCLOCALPLACEMENT");

            IFCEngine.sdaiPutAttrBN(ifcLocalPlacementInstance, "RelativePlacement", IFCEngine.sdaiINSTANCE, buildAxis2Placement3DInstance(M));

            return ifcLocalPlacementInstance;
        }

        private int buildLocalPlacementInstance(Matrix M, int reltoPlacement)
        {
            int ifcLocalPlacementInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCLOCALPLACEMENT");

            IFCEngine.sdaiPutAttrBN(ifcLocalPlacementInstance, "PlacementRelTo", IFCEngine.sdaiINSTANCE, reltoPlacement);
            IFCEngine.sdaiPutAttrBN(ifcLocalPlacementInstance, "RelativePlacement", IFCEngine.sdaiINSTANCE, buildAxis2Placement3DInstance(M));

            return ifcLocalPlacementInstance;
        }

        private int buildPolylineInstance(listOf2DPoints points)
        {
            int ifcPolylineInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCPOLYLINE"),
                aggrPoints = IFCEngine.sdaiCreateAggrBN(ifcPolylineInstance, "Points");


            _2DPoint point = points.Head;
            while (point != null)
            {
                IFCEngine.sdaiAppend(aggrPoints, IFCEngine.sdaiINSTANCE, buildCartesianPointInstance(point.x, point.y));
                point = point.Next;
            }
            //IFCEngine.sdaiAppend(aggrPoints, IFCEngine.sdaiINSTANCE, buildCartesianPointInstance(points.Head.x, points.Head.y));

            return ifcPolylineInstance;
        }

        private int buildArbitraryClosedProfileDefInstance(listOf2DPoints points)
        {
            int ifcArbitraryClosedProfileDefInstance;

            ifcArbitraryClosedProfileDefInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCARBITRARYCLOSEDPROFILEDEF");

            IFCEngine.sdaiPutAttrBN(ifcArbitraryClosedProfileDefInstance, "ProfileType", IFCEngine.sdaiENUM, "AREA");
            IFCEngine.sdaiPutAttrBN(ifcArbitraryClosedProfileDefInstance, "OuterCurve", IFCEngine.sdaiINSTANCE, buildPolylineInstance(points));

            return ifcArbitraryClosedProfileDefInstance;
        }

        private int buildRectangleProfileDefInstance(double[] dimensions, Matrix M)
        {
            int ifcRectangleProfileDefInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCRECTANGLEPROFILEDEF");

            IFCEngine.sdaiPutAttrBN(ifcRectangleProfileDefInstance, "Position", IFCEngine.sdaiINSTANCE, buildAxis2Placement2DInstance(M));

            IFCEngine.sdaiPutAttrBN(ifcRectangleProfileDefInstance, "XDim", IFCEngine.sdaiREAL, ref dimensions[0]);
            IFCEngine.sdaiPutAttrBN(ifcRectangleProfileDefInstance, "YDim", IFCEngine.sdaiREAL, ref dimensions[1]);

            return ifcRectangleProfileDefInstance;
        }

        private int buildExtrudedAreaSolidInstance(double[] dimensions, Matrix M1, Matrix M2)
        {
            int ifcExtrudedAreaSolidInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCEXTRUDEDAREASOLID");

            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "SweptArea", IFCEngine.sdaiINSTANCE, buildRectangleProfileDefInstance(dimensions, M1));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "Position", IFCEngine.sdaiINSTANCE, buildAxis2Placement3DInstance(M2));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "ExtrudedDirection", IFCEngine.sdaiINSTANCE, buildDirectionInstance(0, 0, 1));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "Depth", IFCEngine.sdaiREAL, ref dimensions[2]);

            return ifcExtrudedAreaSolidInstance;
        }

        private int buildExtrudedAreaSolidInstance(listOf2DPoints points, double extrusion)
        {
            int ifcExtrudedAreaSolidInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCEXTRUDEDAREASOLID");

            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "SweptArea", IFCEngine.sdaiINSTANCE, buildArbitraryClosedProfileDefInstance(points));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "Depth", IFCEngine.sdaiREAL, ref extrusion);

            return ifcExtrudedAreaSolidInstance;
        }

        private int buildExtrudedAreaSolidInstance(listOf2DPoints points, Matrix m2, double extrusion)
        {
            int ifcExtrudedAreaSolidInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCEXTRUDEDAREASOLID");

            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "SweptArea", IFCEngine.sdaiINSTANCE, buildArbitraryClosedProfileDefInstance(points));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "Position", IFCEngine.sdaiINSTANCE, buildAxis2Placement3DInstance(m2));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "ExtrudedDirection", IFCEngine.sdaiINSTANCE, buildDirectionInstance(0, 0, 1));
            IFCEngine.sdaiPutAttrBN(ifcExtrudedAreaSolidInstance, "Depth", IFCEngine.sdaiREAL, ref extrusion);

            return ifcExtrudedAreaSolidInstance;
        }

        private int buildBODYShapeRepresentationInstance(listOf2DPoints points, double extrusion, Matrix m1)
        {
            int ifcShapeRepresentationInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCSHAPEREPRESENTATION"),
                aggrItems = IFCEngine.sdaiCreateAggrBN(ifcShapeRepresentationInstance, "Items");

            IFCEngine.sdaiAppend(aggrItems, IFCEngine.sdaiINSTANCE, buildExtrudedAreaSolidInstance(points, m1, extrusion));
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationIdentifier", IFCEngine.sdaiSTRING, "Body");
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationType", IFCEngine.sdaiSTRING, "SweptSolid");

            return ifcShapeRepresentationInstance;
        }

        private int buildShapeRepresentationInstance(listOf2DPoints points, double extrusion)
        {
            int ifcShapeRepresentationInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCSHAPEREPRESENTATION"),
                aggrItems = IFCEngine.sdaiCreateAggrBN(ifcShapeRepresentationInstance, "Items");

            IFCEngine.sdaiAppend(aggrItems, IFCEngine.sdaiINSTANCE, buildExtrudedAreaSolidInstance(points, extrusion));
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationIdentifier", IFCEngine.sdaiSTRING, "Body");
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationType", IFCEngine.sdaiSTRING, "SweptSolid");

            return ifcShapeRepresentationInstance;
        }

        private int buildBODYShapeRepresentationInstance(double[] dimensions, Matrix M1, Matrix M2)
        {
            int ifcShapeRepresentationInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCSHAPEREPRESENTATION"),
                aggrItems = IFCEngine.sdaiCreateAggrBN(ifcShapeRepresentationInstance, "Items");

            IFCEngine.sdaiAppend(aggrItems, IFCEngine.sdaiINSTANCE, buildExtrudedAreaSolidInstance(dimensions, M1, M2));
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationIdentifier", IFCEngine.sdaiSTRING, "Body");
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationType", IFCEngine.sdaiSTRING, "SweptSolid");

            return ifcShapeRepresentationInstance;
        }

        private int buildAXISShapeRepresentationInstance(listOf2DPoints points)
        {
            int ifcShapeRepresentationInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCSHAPEREPRESENTATION"),
                aggrItems = IFCEngine.sdaiCreateAggrBN(ifcShapeRepresentationInstance, "Items");

            IFCEngine.sdaiAppend(aggrItems, IFCEngine.sdaiINSTANCE, buildPolylineInstance(points));
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationIdentifier", IFCEngine.sdaiSTRING, "Axis");
            IFCEngine.sdaiPutAttrBN(ifcShapeRepresentationInstance, "RepresentationType", IFCEngine.sdaiSTRING, "Curved2D");

            return ifcShapeRepresentationInstance;
        }

        private int buildProductDefinitionShapeInstance(listOf2DPoints points, double extrusion, Matrix m1)
        {
            int ifcProductDefinitionShapeInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCPRODUCTDEFINITIONSHAPE"),
                aggrRepresentations = IFCEngine.sdaiCreateAggrBN(ifcProductDefinitionShapeInstance, "Representations");

            IFCEngine.sdaiAppend(aggrRepresentations, IFCEngine.sdaiINSTANCE, buildBODYShapeRepresentationInstance(points, extrusion, m1));

            return ifcProductDefinitionShapeInstance;
        }

        private int buildProductDefinitionShapeInstance(listOf2DPoints points, double extrusion, Matrix m1, listOf2DPoints axisPath)
        {
            int ifcProductDefinitionShapeInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCPRODUCTDEFINITIONSHAPE"),
                aggrRepresentations = IFCEngine.sdaiCreateAggrBN(ifcProductDefinitionShapeInstance, "Representations");

            IFCEngine.sdaiAppend(aggrRepresentations, IFCEngine.sdaiINSTANCE, buildBODYShapeRepresentationInstance(points, extrusion, m1));
            if (axisPath != null)
                IFCEngine.sdaiAppend(aggrRepresentations, IFCEngine.sdaiINSTANCE, buildAXISShapeRepresentationInstance(axisPath));

            return ifcProductDefinitionShapeInstance;
        }

        private int buildProductDefinitionShapeInstance(listOf2DPoints points, double extrusion)
        {
            int ifcProductDefinitionShapeInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCPRODUCTDEFINITIONSHAPE"),
                aggrRepresentations = IFCEngine.sdaiCreateAggrBN(ifcProductDefinitionShapeInstance, "Representations");

            IFCEngine.sdaiAppend(aggrRepresentations, IFCEngine.sdaiINSTANCE, buildShapeRepresentationInstance(points, extrusion));

            return ifcProductDefinitionShapeInstance;
        }

        private int buildProductDefinitionShapeInstance(double[] dimensions, Matrix M1, Matrix M2)
        {
            int ifcProductDefinitionShapeInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCPRODUCTDEFINITIONSHAPE"),
                aggrRepresentations = IFCEngine.sdaiCreateAggrBN(ifcProductDefinitionShapeInstance, "Representations");

            IFCEngine.sdaiAppend(aggrRepresentations, IFCEngine.sdaiINSTANCE, buildBODYShapeRepresentationInstance(dimensions, M1, M2));

            return ifcProductDefinitionShapeInstance;
        }

        public int addOpeningElement(double[] dimension, Matrix M1, Matrix M2, Matrix M3, int reltoPlacement)
        {
            int ifcOpeningElement = IFCEngine.sdaiCreateInstanceBN(model, "IFCOPENINGELEMENT");

            IFCEngine.sdaiPutAttrBN(ifcOpeningElement, "ObjectPlacement", IFCEngine.sdaiINSTANCE, buildLocalPlacementInstance(M1, reltoPlacement));
            IFCEngine.sdaiPutAttrBN(ifcOpeningElement, "Representation", IFCEngine.sdaiINSTANCE, buildProductDefinitionShapeInstance(dimension, M2, M3));

            return ifcOpeningElement;
        }

        public int addRelVoidsElement(int attrRelatingBuildingElement, int attrRelatedOpeningElement)
        {
            int ifcRelVoidsElement = IFCEngine.sdaiCreateInstanceBN(model, "IFCRELVOIDSELEMENT");

            IFCEngine.sdaiPutAttrBN(ifcRelVoidsElement, "RelatingBuildingElement", IFCEngine.sdaiINSTANCE, attrRelatingBuildingElement);
            IFCEngine.sdaiPutAttrBN(ifcRelVoidsElement, "RelatedOpeningElement", IFCEngine.sdaiINSTANCE, attrRelatedOpeningElement);

            return ifcRelVoidsElement;
        }

        public int addRelAggregateElement(int attrRelatingSpatialElement, int attrRelatedBuildingElement, string name = "")
        {
            int ifcRelAggregateElement = IFCEngine.sdaiCreateInstanceBN(model, "IFCRELAGGREGATES");
            IFCEngine.sdaiPutAttrBN(ifcRelAggregateElement, "RelatingObject", IFCEngine.sdaiINSTANCE, attrRelatingSpatialElement);

            Int32 relatedObjs = IFCEngine.sdaiCreateAggrBN(ifcRelAggregateElement, "RelatedObjects");
            IFCEngine.sdaiAppend(relatedObjs, IFCEngine.sdaiINSTANCE, attrRelatedBuildingElement);
           
            string guid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(ifcRelAggregateElement, "GlobalId", IFCEngine.sdaiSTRING, guid);

            if (name != "")
            {
                IFCEngine.sdaiPutAttrBN(ifcRelAggregateElement, "Name", IFCEngine.sdaiSTRING, name);
            }

            return ifcRelAggregateElement;
        }

        public int addOpeningElement(listOf2DPoints points, double extrusion, Matrix M, int reltoPlacement = 0)
        {
            var offsetX = M._41;
            var offsetY = M._42;
            var offsetZ = M._43;
            //M._41 = 0;
            //M._42 = 0;
            //M._43 = 0;

            var geomMtx = Matrix.Identity();
            geomMtx._41 = 0;
            geomMtx._42 = 0;
            geomMtx._43 = -500;

            int placement = buildLocalPlacementInstance(M);
            int ifcOpeningElement = IFCEngine.sdaiCreateInstanceBN(model, "IFCOPENINGELEMENT");
            
            IFCEngine.sdaiPutAttrBN(ifcOpeningElement, "ObjectPlacement", IFCEngine.sdaiINSTANCE, placement);
            IFCEngine.sdaiPutAttrBN(ifcOpeningElement, "Representation", IFCEngine.sdaiINSTANCE,
                buildProductDefinitionShapeInstance(points, extrusion, geomMtx));

            return ifcOpeningElement;
        }

        public int addOpeningElement(listOf2DPoints points, double extrusion)
        {
            int reltoPlacement = 0;
            int ifcOpeningElement = IFCEngine.sdaiCreateInstanceBN(model, "IFCOPENINGELEMENT");

            IFCEngine.sdaiPutAttrBN(ifcOpeningElement, "ObjectPlacement", IFCEngine.sdaiINSTANCE, reltoPlacement);
            IFCEngine.sdaiPutAttrBN(ifcOpeningElement, "Representation", IFCEngine.sdaiINSTANCE, buildProductDefinitionShapeInstance(points, extrusion));

            return ifcOpeningElement;
        }

        
        public int createContainment(int attrRelatingStructure, List<int> attrRelatedElements)
        {
            int ifcRelContainedmentElement = IFCEngine.sdaiCreateInstanceBN(model, "IFCRELCONTAINEDINSPATIALSTRUCTURE");
            IFCEngine.sdaiPutAttrBN(ifcRelContainedmentElement, "RelatingStructure", IFCEngine.sdaiINSTANCE, attrRelatingStructure);

            Int32 relatedObjs = IFCEngine.sdaiCreateAggrBN(ifcRelContainedmentElement, "RelatedElements");
            foreach (var relement in attrRelatedElements)
                IFCEngine.sdaiAppend(relatedObjs, IFCEngine.sdaiINSTANCE, relement);

            string guid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(ifcRelContainedmentElement, "GlobalId", IFCEngine.sdaiSTRING, guid);

            return ifcRelContainedmentElement;
        }

        public int createWall(listOf2DPoints points, double extrusion, Matrix M, out int placement, int reltoPlacement = 0, listOf2DPoints axisPath = null)
        {
            int ifcWallInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCWALLSTANDARDCASE");

            if (reltoPlacement > 0)
                placement = buildLocalPlacementInstance(M, reltoPlacement);
            else
                placement = buildLocalPlacementInstance(M);

            IFCEngine.sdaiPutAttrBN(ifcWallInstance, "ObjectPlacement", IFCEngine.sdaiINSTANCE, placement);
            M._41 = 0;
            M._42 = 0;
            M._43 = 0;
            IFCEngine.sdaiPutAttrBN(ifcWallInstance, "Representation", IFCEngine.sdaiINSTANCE, buildProductDefinitionShapeInstance(points, extrusion, M, axisPath));
            string bldgGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(ifcWallInstance, "GlobalId", IFCEngine.sdaiSTRING, bldgGuid);

            return ifcWallInstance;
        }

        public int createColumn(listOf2DPoints points, double extrusion, Matrix M, out int placement, int reltoPlacement = 0)
        {
            int ifcColumn = IFCEngine.sdaiCreateInstanceBN(model, "IFCColumn");

            if(reltoPlacement > 0)
                placement = buildLocalPlacementInstance(M, reltoPlacement);
            else
                placement = buildLocalPlacementInstance(M);

            IFCEngine.sdaiPutAttrBN(ifcColumn, "ObjectPlacement", IFCEngine.sdaiINSTANCE, placement);
            M._41 = 0;
            M._42 = 0;
            M._43 = 0;
            IFCEngine.sdaiPutAttrBN(ifcColumn, "Representation", IFCEngine.sdaiINSTANCE, buildProductDefinitionShapeInstance(points, extrusion, M));
            string bldgGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(ifcColumn, "GlobalId", IFCEngine.sdaiSTRING, bldgGuid);

            return ifcColumn;
        }

        public int createSlab(listOf2DPoints points, double extrusion, Matrix M, out int placement, int reltoPlacement = 0)
        {
            int ifcSlab = IFCEngine.sdaiCreateInstanceBN(model, "IFCSlab");

            if (reltoPlacement > 0)
                placement = buildLocalPlacementInstance(M, reltoPlacement);
            else
                placement = buildLocalPlacementInstance(M);

            IFCEngine.sdaiPutAttrBN(ifcSlab, "ObjectPlacement", IFCEngine.sdaiINSTANCE, placement);
            M._41 = 0;
            M._42 = 0;
            M._43 = 0;
            IFCEngine.sdaiPutAttrBN(ifcSlab, "Representation", IFCEngine.sdaiINSTANCE, buildProductDefinitionShapeInstance(points, extrusion, M));
            string bldgGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(ifcSlab, "GlobalId", IFCEngine.sdaiSTRING, bldgGuid);

            return ifcSlab;
        }

        public int createSlab(listOf2DPoints points, double extrusion, Matrix M)
        {
            int ifcSlabInstance = IFCEngine.sdaiCreateInstanceBN(model, "IFCSLAB");

            int reltoPlacement = buildLocalPlacementInstance(M);
            IFCEngine.sdaiPutAttrBN(ifcSlabInstance, "ObjectPlacement", IFCEngine.sdaiINSTANCE, reltoPlacement);
            IFCEngine.sdaiPutAttrBN(ifcSlabInstance, "Representation", IFCEngine.sdaiINSTANCE, buildProductDefinitionShapeInstance(points, extrusion, M));

            return ifcSlabInstance;
        }

        public int createBuildingStory(string name = "", Matrix mtx = null)
        {
            mtx = mtx == null ? Matrix.Identity() : mtx;

            int firstfloor = IFCEngine.sdaiCreateInstanceBN(model, "IFCBUILDINGSTOREY");
            string storeyGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(firstfloor, "GlobalId", IFCEngine.sdaiSTRING, storeyGuid);
            IFCEngine.sdaiPutAttrBN(firstfloor, "Name", IFCEngine.sdaiSTRING, name);
            IFCEngine.sdaiPutAttrBN(firstfloor, "CompositionType", IFCEngine.sdaiENUM, "ELEMENT");

            int buildingPlacement = IFCEngine.sdaiGetInstanceAttrBN(building, "ObjectPlacement");

            int placement = buildLocalPlacementInstance(mtx, buildingPlacement); 
            IFCEngine.sdaiPutAttrBN(firstfloor, "ObjectPlacement", IFCEngine.sdaiINSTANCE, placement);

            buildingstorey.Add(firstfloor);
            return firstfloor;
        }

        private void createBuilding(Matrix mtx = null)
        {
            mtx = mtx == null ? Matrix.Identity() : mtx;

            building = IFCEngine.sdaiCreateInstanceBN(model, "IFCBUILDING");
            string bldgGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(building, "GlobalId", IFCEngine.sdaiSTRING, bldgGuid);
            IFCEngine.sdaiPutAttrBN(building, "Name", IFCEngine.sdaiSTRING, "Default Building");
            IFCEngine.sdaiPutAttrBN(building, "CompositionType", IFCEngine.sdaiENUM, "ELEMENT");

            int placement = buildLocalPlacementInstance(mtx);
            IFCEngine.sdaiPutAttrBN(building, "ObjectPlacement", IFCEngine.sdaiINSTANCE, placement);
        }

        private int createSite()
        {
            var site = IFCEngine.sdaiCreateInstanceBN(model, "IFCSITE");
            string siteGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(site, "GlobalId", IFCEngine.sdaiSTRING, siteGuid);
            IFCEngine.sdaiPutAttrBN(site, "Name", IFCEngine.sdaiSTRING, "Default Site");
            IFCEngine.sdaiPutAttrBN(site, "CompositionType", IFCEngine.sdaiENUM, "ELEMENT");
            return site;
        }

        private int createProject()
        {
            var unitAssignment = createUnitAssignment();

            var project = IFCEngine.sdaiCreateInstanceBN(model, "IFCPROJECT");
            string projectGuid = ShortGuid.ShortGuid.NewGuid();
            IFCEngine.sdaiPutAttrBN(project, "GlobalId", IFCEngine.sdaiSTRING, projectGuid);
            IFCEngine.sdaiPutAttrBN(project, "Name", IFCEngine.sdaiSTRING, "Default Project");
            IFCEngine.sdaiPutAttrBN(project, "UnitsInContext", IFCEngine.sdaiINSTANCE, unitAssignment);

            return project;
        }

        private int createUnitAssignment()
        {
            var unitAssignment = IFCEngine.sdaiCreateInstanceBN(model, "IFCUNITASSIGNMENT");

            var aggrUnits = IFCEngine.sdaiCreateAggrBN(unitAssignment, "Units");
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("AREAUNIT", "", "SQUARE_METRE"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("TIMEUNIT", "", "SECOND"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("SOLIDUNIT", "", "STERADIAN"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("LUMINOUSINTENSITYUNIT", "", "LUMEN"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("VOLUMEUNIT", "", "CUBIC_METRE"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("THERMODYNAMICTEMPERATUREUNIT", "", "DEGREE_CELSIUS"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("LENGTHUNIT", "MILLI", "METRE"));
            IFCEngine.sdaiAppend(aggrUnits, IFCEngine.sdaiINSTANCE, createUnit("MASSUNIT", "", "GRAM"));

            return unitAssignment;
        }

        private int createUnit(string unittype, string prefix, string name)
        {
            var unit = IFCEngine.sdaiCreateInstanceBN(model, "IFCSIUNIT");
            IFCEngine.sdaiPutAttrBN(unit, "UnitType", IFCEngine.sdaiENUM, unittype);
            IFCEngine.sdaiPutAttrBN(unit, "Prefix", IFCEngine.sdaiSTRING, prefix);
            IFCEngine.sdaiPutAttrBN(unit, "Name", IFCEngine.sdaiENUM, name);
            return unit;
        }

        public bool createModel()
        {
            model = IFCEngine.sdaiCreateModelBN(1, null, "IFC2X3_Final.exp");//"http://www.ifcbrowser.com/IFC2X3_TC1.exp");//

            if (model == 0)
            {
                return false;
            }
            else
            {
                var project = createProject();
                var site = createSite();
                createBuilding();

                int relAggregateElement = this.addRelAggregateElement(project, site, "ProjectContainer");
                relAggregateElement = this.addRelAggregateElement(site, building, "SiteContainer");

                int firstfloor = createBuildingStory("Floor 1");
                // put the building storey into the building
                relAggregateElement = this.addRelAggregateElement(building, firstfloor, "BuildingContainer");


                return true;
            }
        }

        public void closeModel()
        {
            IFCEngine.sdaiCloseModel(model);
        }

        public void saveModel(string fileName)
        {
            IFCEngine.sdaiSaveModelBN(model, fileName);
        }

        public Tuple<float[], int[]> getIndexAndVertexArrays(int ifcInstance)
        {
            int noVertices = 0, noIndices = 0,
                startVertex = 0, startIndex = 0, primitiveCount = 0;

            IFCEngine.initializeModellingInstance(model, ref noVertices, ref noIndices, 0, ifcInstance);

            //
            //      D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_NORMAL) =>  0x012 (0x002 | 0x010)
            //      
            //      This means a vertex of size of 6 floats, i.e. x, y, z, nx, ny, nz
            //
            float[] vertexArray = new float[noVertices * 6];
            int[] indexArray = new int[noIndices];

            IFCEngine.finalizeModelling(model, ref vertexArray[0], ref indexArray[0], 18);
            IFCEngine.getInstanceInModelling(model, ifcInstance, 1, ref startVertex, ref startIndex, ref primitiveCount);
            
            return new Tuple<float[],int[]>(vertexArray, indexArray);
        }
    }

    internal class IFCEngineCreateGeometry
    {
        internal static Tuple<float[],int[]> createWall(List<Point3D> pts, double height, IFC ifc, double elevation = 0)
        {

            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = elevation;
                M._44 = 1;

                listOf2DPoints points = new listOf2DPoints();
                points.Add(new _2DPoint(pts[0].X, pts[0].Y));
                points.Add(new _2DPoint(pts[1].X, pts[1].Y));
                points.Add(new _2DPoint(pts[2].X, pts[2].Y));
                points.Add(new _2DPoint(pts[3].X, pts[3].Y));
                int reltoPlacement;
                int ifcInstance = ifc.createWall(points, height, M, out reltoPlacement);

                Tuple<float[],int[]> pd = ifc.getIndexAndVertexArrays(ifcInstance);

                //ifc.closeModel();
                return pd;
            //}
            //else return null;
        }

        internal static Tuple<float[], int[]> createWall(List<Point3D> pts, IFC ifc, double height)
        {
            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = 0;
                M._44 = 1;

                listOf2DPoints points = new listOf2DPoints();
                points.Add(new _2DPoint(pts[0].X, pts[0].Y));
                points.Add(new _2DPoint(pts[1].X, pts[1].Y));
                points.Add(new _2DPoint(pts[2].X, pts[2].Y));
                points.Add(new _2DPoint(pts[3].X, pts[3].Y));
                int reltoPlacement;
                int ifcInstance = ifc.createWall(points, height, M, out reltoPlacement);

                Tuple<float[], int[]> pd = ifc.getIndexAndVertexArrays(ifcInstance);

                //ifc.closeModel();
                return pd;
            //}
            //else return null;
        }

        // Todo: set the windows height and width
        internal static Tuple<float[], int[]> createWallwithWindow(double minX, double maxX, double minY, double maxY, double elevation,
            List<Point3D> pts, double height, double wallWidth, double wallThickness, int type, IFC ifc)
        {
            //if (ifc.createModel() == true)
            //{
                //CalculateWindowSize(height, wallWidth, objWindow);

            double winH = 0;// objWindow.Height;
            double winLOffset = 0;// objWindow.LOffset;
            double winElev = 0;// objWindow.TopElevation - winH;
            double winROffset = 0;// objWindow.ROffset;

                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = minX;
                M._42 = minY;
                M._43 = 0;
                M._44 = 1;

                listOf2DPoints points = new listOf2DPoints();
                points.Add(new _2DPoint(pts[0].X - M._41, pts[0].Y - M._42));
                points.Add(new _2DPoint(pts[1].X - M._41, pts[1].Y - M._42));
                points.Add(new _2DPoint(pts[2].X - M._41, pts[2].Y - M._42));
                points.Add(new _2DPoint(pts[3].X - M._41, pts[3].Y - M._42));
                int reltoPlacement;
                int ifcWallInstance = ifc.createWall(points, height, M, out reltoPlacement);

                // For Window
                double winWidth = wallWidth - winLOffset - winROffset;
                double[] dimension = new double[3] { winWidth, winH, wallThickness };
                Matrix m1 = M;
                m1._41 = winROffset + winWidth / 2;
                m1._42 = 0;
                Matrix m2 = M;
                //m2._43 = winLOffset + winWidth / 2;
                m2._41 = 0;
                m2._42 = winElev + winH / 2;

                Matrix m3 = M;
                m3._11 = -1;
                m3._41 = 0;
                m3._42 = 0;
                m3._31 = 0;
                m3._32 = 1;
                m3._33 = 0;


                m3._22 = 0; // jaemin changed. 
                if (type == 1 || type == 2 || type == 3 || type == 4)
                {

                }
                else
                {
                    m1._41 = 0;
                    m1._42 = winROffset + winWidth / 2;
                    m1._11 = 0;
                    m1._12 = -1;
                }

                int ifcOpeningInstance = ifc.addOpeningElement(dimension, m1, m2, m3, reltoPlacement);

                int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcWallInstance, ifcOpeningInstance);

                ifc.getIndexAndVertexArrays(ifcWallInstance);

                Tuple<float[], int[]> pd = ifc.getIndexAndVertexArrays(ifcWallInstance);

                //ifc.closeModel();
                return pd;
            //}
            //else return null;
        }

        internal static Matrix Convert(GeneralMatrix m)
        {

            Matrix M = new Matrix();
            M._11 = m.A[0][0];
            M._12 = m.A[1][0];
            M._13 = m.A[2][0];
            M._14 = m.A[3][0];
            M._21 = m.A[0][1];
            M._22 = m.A[1][1];
            M._23 = m.A[2][1];
            M._24 = m.A[3][1];
            M._31 = m.A[0][2];
            M._32 = m.A[1][2];
            M._33 = m.A[2][2];
            M._34 = m.A[3][2];

            M._41 = m.A[0][3];
            M._42 = m.A[1][3];
            M._43 = m.A[2][3];
            M._44 = m.A[3][3];

            return M;

        }

        internal static Point3D Multiply(GeneralMatrix m, Point3D p)
        {
            Point3D pt = new Point3D();
            pt.X = m.A[0][0] * p.X + m.A[0][1] * p.Y + m.A[0][2] * p.Z + m.A[0][3];
            pt.Y = m.A[1][0] * p.X + m.A[1][1] * p.Y + m.A[1][2] * p.Z + m.A[1][3];
            pt.Z = m.A[2][0] * p.X + m.A[2][1] * p.Y + m.A[2][2] * p.Z + m.A[2][3];

            return pt;
        }

        internal static Tuple<float[], int[]> createWallwithWindow_freeform(Point3D p1, Point3D p2, Point3D p3, Point3D p4, double elevation,
            List<Point3D> pts, double height, double wallWidth, double wallThickness, out Tuple<float[], int[]> pdOpening, IFC ifc)
        {

            Math3D math = new Math3D();
            pdOpening = null;
            wallWidth = math.length(p3, p4);

            double winH = 0;//objWindow.Height;
            double winLOffset = 0;// objWindow.LOffset;
            double winElev = 0;// objWindow.TopElevation - objWindow.Height;
            double winROffset = 0;// objWindow.ROffset;
            double winWidth = 0;// wallWidth - objWindow.LOffset - objWindow.ROffset;

            //if (ifc.createModel() == true)
            //{

                ////////////////////// Old part
                Matrix M;

                // matrix calculation module /////////////////////////////////////////////
                GeneralMatrix M0 = math.TransformMatrix(p3, p4);

                GeneralMatrix M0_inverse = M0.Inverse();

                Point3D np1 = Multiply(M0_inverse, p1);
                Point3D np2 = Multiply(M0_inverse, p2);
                Point3D np3 = Multiply(M0_inverse, p3);
                Point3D np4 = Multiply(M0_inverse, p4);


                //np1.x = Math.Round(np1.x, 1, MidpointRounding.AwayFromZero);
                np1.X = Math.Round(np1.X);
                np1.Y = Math.Round(np1.Y);
                np2.X = Math.Round(np2.X);
                np2.Y = Math.Round(np2.Y);
                np3.X = Math.Round(np3.X);
                np3.Y = Math.Round(np3.Y);
                np4.X = Math.Round(np4.X);
                np4.Y = Math.Round(np4.Y);


                listOf2DPoints points = new listOf2DPoints();
                points.Add(new _2DPoint(np1.X, np1.Y));
                points.Add(new _2DPoint(np2.X, np2.Y));
                points.Add(new _2DPoint(np3.X, np3.Y));
                points.Add(new _2DPoint(np4.X, np4.Y));

                M = Convert(M0); // change matrix in semetrically. 
                int reltoPlacement;
                int ifcWallInstance = ifc.createWall(points, height, M, out reltoPlacement);

                ifc.getIndexAndVertexArrays(ifcWallInstance);
                //////////////////////////////////////////////////////////////////////////

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = 0;
                M._44 = 1;


                //////////////////////////////////////////////////

                // For Window

                //VtkLibrary.CheckWindowGlazingData(0,0,height, wallWidth, (heigth * wallWidth), //Need to add code here to handle the different parameters


                //if (true)
                //{
                //    dimension = new double[3] { winWidth, winH, wallThickness * 2 };
                //     m1 = M;
                //    m1._41 = 0;
                //    m1._42 = 0;
                //     m2 = M;
                //    //m2._43 = winLOffset + winWidth / 2;
                //    m2._41 = 0;
                //    m2._42 = 0;

                //     m3 = M;
                //    m3._11 = -1;
                //    m3._41 = 0;
                //    m3._42 = 0;
                //    m3._31 = 0;
                //    m3._32 = 1;
                //    m3._33 = 0;
                //}
                //else
                //{

                Matrix m1;
                Matrix m2;
                Matrix m3;
                double[] dimension;

                dimension = new double[3] { winWidth, winH, wallThickness * 2 };
                //double[] dimension = new double[3] { winWidth, winH, wallThickness * 2 };
                //Matrix m1 = M;
                m1 = M;
                m1._41 = winLOffset + winWidth / 2;
                m1._42 = 0;
                //Matrix m2 = M;
                m2 = M;
                m2._41 = 0;
                m2._42 = winElev + winH / 2;

                //Matrix m3 = M;
                m3 = M;
                m3._11 = -1;
                m3._41 = 0;
                m3._42 = 0;
                m3._31 = 0;
                m3._32 = 1;
                m3._33 = 0;
                //}



                int ifcOpeningInstance = ifc.addOpeningElement(dimension, m1, m2, m3, reltoPlacement);

                int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcWallInstance, ifcOpeningInstance);

                ifc.getIndexAndVertexArrays(ifcWallInstance);


                Tuple<float[], int[]> pd = ifc.getIndexAndVertexArrays(ifcWallInstance);
                pdOpening = ifc.getIndexAndVertexArrays(ifcOpeningInstance);

                //ifc.closeModel();
                return pd;
            //}
            //else return null;
        }


        /*
        internal static vtkPolyData createWallwithWindows_freeform(SimplePolygon wallPolygon, vtkPoints pts, 
            double height, double wallThickness, List<cAecObjectWindow> objWindows, List<vtkPolyData> pdOpenings, IFC ifc)
        {

            Math3D math = new Math3D();

            //if (ifc.createModel() == true)
            //{

                ////////////////////// Old part
                Matrix M;

                // matrix calculation module /////////////////////////////////////////////
                GeneralMatrix M0 = math.TransformMatrix(wallPolygon.Points3D[2], wallPolygon.Points3D[3]);

                GeneralMatrix M0_inverse = M0.Inverse();

                Point3D np1 = Multiply(M0_inverse, wallPolygon.Points3D[0]);
                Point3D np2 = Multiply(M0_inverse, wallPolygon.Points3D[1]);
                Point3D np3 = Multiply(M0_inverse, wallPolygon.Points3D[2]);
                Point3D np4 = Multiply(M0_inverse, wallPolygon.Points3D[3]);


                //np1.x = Math.Round(np1.x, 1, MidpointRounding.AwayFromZero);
                np1.X = Math.Round(np1.X);
                np1.Y = Math.Round(np1.Y);
                np2.X = Math.Round(np2.X);
                np2.Y = Math.Round(np2.Y);
                np3.X = Math.Round(np3.X);
                np3.Y = Math.Round(np3.Y);
                np4.X = Math.Round(np4.X);
                np4.Y = Math.Round(np4.Y);


                listOf2DPoints points = new listOf2DPoints();
                points.Add(new _2DPoint(np1.X, np1.Y));
                points.Add(new _2DPoint(np2.X, np2.Y));
                points.Add(new _2DPoint(np3.X, np3.Y));
                points.Add(new _2DPoint(np4.X, np4.Y));

                M = Convert(M0); // change matrix in semetrically. 
                int reltoPlacement;
                int ifcWallInstance = ifc.addExtrudedPolygonObject(points, height, M, out reltoPlacement);

                ifc.getIndexAndVertexArrays(ifcWallInstance);
                //////////////////////////////////////////////////////////////////////////

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = 0;
                M._44 = 1;

                // For Window
                foreach (cAecObjectWindow objWindow in objWindows)
                {
                    if (objWindow.Status != cAecWindowStatusEnum.Removed)
                    {
                        Matrix m1;
                        Matrix m2;
                        Matrix m3;
                        double[] dimension;

                        double winH = objWindow.Height;
                        double winLOffset = objWindows[0].LOffset;
                        double winElev = objWindow.TopElevation - objWindow.Height;
                        double winROffset = objWindow.ROffset;
                        double winWidth = objWindow.Width;
                        double winGap = objWindow.MinimumGap == null ? 0 : (double)objWindow.MinimumGap;

                        dimension = new double[3] { winWidth, winH, wallThickness * 2 };

                        m1 = M;
                        m1._41 = objWindow.FromWallLeft;
                        m1._42 = 0;

                        m2 = M;
                        m2._41 = 0;
                        m2._42 = winElev + winH / 2;

                        m3 = M;
                        m3._11 = -1;
                        m3._41 = 0;
                        m3._42 = 0;
                        m3._31 = 0;
                        m3._32 = 1;
                        m3._33 = 0;

                        int ifcOpeningInstance = ifc.addOpeningElement(dimension, m1, m2, m3, reltoPlacement);

                        int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcWallInstance, ifcOpeningInstance);

                        pdOpenings.Add(ifc.getIndexAndVertexArrays(ifcOpeningInstance));
                    }
                }

                ifc.getIndexAndVertexArrays(ifcWallInstance);

                vtkPolyData pd = ifc.getIndexAndVertexArrays(ifcWallInstance);
                //ifc.closeModel();
                return pd;
            //}
            //else return null;
        }
        */

        internal static Tuple<float[],int[]> createExtrudedGeometry(List<Point3D> profilePts, double slabThickness, IFC ifc)
        {
            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = profilePts[0].Z;
                M._44 = 1;

                try
                {
                    listOf2DPoints points = new listOf2DPoints();
                    for (int i = 0; i < profilePts.Count; i++)
                    {
                        points.Add(new _2DPoint(profilePts[i].X, profilePts[i].Y));
                    }

                    int ifcSlabInstance = ifc.createSlab(points, slabThickness, M);

                    Tuple<float[],int[]> pd = ifc.getIndexAndVertexArrays(ifcSlabInstance);

                    //ifc.closeModel();
                    return pd;
                }
                catch (Exception e)
                {
                    //ifc.closeModel();
                    return null;
                }
            //}
            //return null;
        }

        internal static Tuple<float[],int[]> createExtrudedGeometryWithOpening(List<Point3D> profilePts, List<Point3D> openingPts, double slabThickness, IFC ifc)
        {
            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = profilePts[0].Z;
                M._44 = 1;

                try
                {
                    listOf2DPoints points = new listOf2DPoints();
                    for (int i = 0; i < profilePts.Count; i++)
                    {
                        points.Add(new _2DPoint(profilePts[i].X, profilePts[i].Y));
                    }

                    int ifcSlabInstance = ifc.createSlab(points, slabThickness, M);

                    listOf2DPoints openingPts2D = new listOf2DPoints();
                    for (int i = 0; i < openingPts.Count; i++)
                    {
                        openingPts2D.Add(new _2DPoint(openingPts[i].X, openingPts[i].Y));
                    }

                    int ifcOpeningInstance = ifc.addOpeningElement(openingPts2D, 2 * slabThickness);

                    int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcSlabInstance, ifcOpeningInstance);

                    Tuple<float[],int[]> pd = ifc.getIndexAndVertexArrays(ifcSlabInstance);

                    //ifc.closeModel();
                    return pd;
                }
                catch (Exception e)
                {
                    //ifc.closeModel();
                    return null;
                }
            //}
            //return null;
        }

        internal static Tuple<float[],int[]> createExtrudedGeometryWithOpening_new(List<Point3D> profilePts, List<Point3D> openingPts,
            double slabThickness, double elevationofprofile, IFC ifc)
        {
            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = profilePts[0].Z;
                M._44 = 1;

                try
                {
                    listOf2DPoints points = new listOf2DPoints();
                    for (int i = 0; i < profilePts.Count; i++)
                    {
                        points.Add(new _2DPoint(profilePts[i].X, profilePts[i].Y));
                    }

                    int ifcSlabInstance = ifc.createSlab(points, slabThickness, M);

                    listOf2DPoints openingPts2D = new listOf2DPoints();
                    for (int i = 0; i < openingPts.Count; i++)
                    {
                        openingPts2D.Add(new _2DPoint(openingPts[i].X, openingPts[i].Y));
                    }

                    int ifcOpeningInstance = ifc.addOpeningElement(openingPts2D, 2 * slabThickness);

                    int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcSlabInstance, ifcOpeningInstance);

                    Tuple<float[],int[]> pd = ifc.getIndexAndVertexArrays(ifcSlabInstance);
                    //ifc.closeModel();
                    return pd;
                }
                catch (Exception e)
                {
                    //ifc.closeModel();
                    return null;
                }
            //}
            //return null;
        }

        internal static Tuple<float[],int[]> createSlabwithOpening(List<Point3D> slabPts, List<Point3D> openingPts, 
            List<Point3D> outSlabPts, double[] preLocation, double[] curLocation, 
            double slabThickness, out Tuple<float[], int[]> pdRoofWall, IFC ifc)
        {
            //pdRoofWall = new Tuple<float[],int[]>();
            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = preLocation[2];
                M._44 = 1;

                try
                {
                    listOf2DPoints points = new listOf2DPoints();
                    for (int i = 0; i < slabPts.Count; i++)
                    {
                        points.Add(new _2DPoint(slabPts[i].X, slabPts[i].Y));
                    }

                    Matrix M1 = M;
                    int ifcSlabInstance = ifc.createSlab(points, slabThickness, M1);

                    /////////////////////////////
                    listOf2DPoints openingPts2D = new listOf2DPoints();
                    for (int i = 0; i < openingPts.Count; i++)
                    {
                        openingPts2D.Add(new _2DPoint(openingPts[i].X + curLocation[0] - preLocation[0], openingPts[i].Y + curLocation[1] - preLocation[1]));
                    }

                    Matrix M2 = M;
                    int ifcOpeningInstance = ifc.addOpeningElement(openingPts2D, 300, M2);

                    int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcSlabInstance, ifcOpeningInstance);

                    Tuple<float[],int[]> pd = ifc.getIndexAndVertexArrays(ifcSlabInstance);

                    /////////////////
                    listOf2DPoints outSlabPts2D = new listOf2DPoints();
                    for (int i = 0; i < outSlabPts.Count; i++)
                    {
                        outSlabPts2D.Add(new _2DPoint(outSlabPts[i].X, outSlabPts[i].Y));
                    }
                    Matrix M3 = M;
                    int ifcOutSlabInstance = ifc.createSlab(outSlabPts2D, 40, M3);

                    //////////////////////////////
                    listOf2DPoints points2 = new listOf2DPoints();
                    for (int i = 0; i < slabPts.Count; i++)
                    {
                        points2.Add(new _2DPoint(slabPts[i].X, slabPts[i].Y));
                    }

                    Matrix M4 = M;
                    int ifcSlabInstance2 = ifc.addOpeningElement(points, 60, M4);
                    int ifcRelVoidsElementInstance2 = ifc.addRelVoidsElement(ifcOutSlabInstance, ifcOpeningInstance);
                    int ifcRelVoidsElementInstance3 = ifc.addRelVoidsElement(ifcOutSlabInstance, ifcSlabInstance2);



                    pdRoofWall = ifc.getIndexAndVertexArrays(ifcOutSlabInstance);
                    //ifc.closeModel();
                    return pd;
                }
                catch (Exception e)
                {
                    pdRoofWall = null;
                    //System.Windows.Forms.MessageBox.Show("Error message : " + e.ToString());
                    //ifc.closeModel();
                    return null;
                }
            //}
            //return null;
        }

        internal static Tuple<float[],int[]> createExtrudeSolidWithOpening(List<Point3D> boundPts, List<Point3D> openingPts,
            double extrusion_Length, IFC ifc)
        {
            //if (ifc.createModel() == true)
            //{
                Matrix M = new Matrix();

                M._11 = 1;
                M._12 = 0;
                M._13 = 0;
                M._14 = 0;
                M._21 = 0;
                M._22 = 1;
                M._23 = 0;
                M._24 = 0;
                M._31 = 0;
                M._32 = 0;
                M._33 = 1;
                M._34 = 0;
                M._41 = 0;
                M._42 = 0;
                M._43 = 0;
                M._44 = 1;

                try
                {
                    listOf2DPoints points = new listOf2DPoints();
                    for (int i = 0; i < boundPts.Count; i++)
                    {
                        points.Add(new _2DPoint(boundPts[i].X, boundPts[i].Y));
                    }

                    Matrix M1 = M;
                    int ifcSlabInstance = ifc.createSlab(points, extrusion_Length, M1);

                    /////////////////////////////
                    listOf2DPoints openingPts2D = new listOf2DPoints();
                    for (int i = 0; i < openingPts.Count; i++)
                    {
                        openingPts2D.Add(new _2DPoint(openingPts[i].X, openingPts[i].Y));
                    }

                    Matrix M2 = M;
                    int ifcOpeningInstance = ifc.addOpeningElement(openingPts2D, extrusion_Length, M2);

                    int ifcRelVoidsElementInstance = ifc.addRelVoidsElement(ifcSlabInstance, ifcOpeningInstance);

                    Tuple<float[],int[]> pd = ifc.getIndexAndVertexArrays(ifcSlabInstance);


                    //////////////////////////////
                    listOf2DPoints points2 = new listOf2DPoints();
                    for (int i = 0; i < boundPts.Count; i++)
                    {
                        points2.Add(new _2DPoint(boundPts[i].X, boundPts[i].Y));
                    }

                    //ifc.closeModel();
                    return pd;
                }
                catch (Exception e)
                {

                    //System.Windows.Forms.MessageBox.Show("Error message : " + e.ToString());
                    //ifc.closeModel();
                    return null;
                }
            //}
            //return null;
        }

    }
}
