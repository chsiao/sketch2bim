﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Petzold.Media3D;
using Utility;
using WrapperLibrary;
using WrapperLibrary.RDF;
using WrapperLibrary.Cornucopia;
using CanvasSketch;
using GeomLib;
using SolidSketch.Operation;
using HelixToolkit.Wpf;

namespace SolidSketch
{
    public static class Library
    {
        public static int TouchCount = 0;
        public static int MoveCount = 0;
        public static MovingAxis TouchOn = MovingAxis.none;
        public static int TouchOnID = -1;
        private static Point3D preSecTouch = new Point3D();
        public static SketchCanvas InkCanv = null;
        public static Viewport3D ViewPort3D = null;
        public static Cursor3D Cursor3D = null;
        public static Model3DGroup SolidModelGroup = null;
        public static Model3DGroup PlaneGroup = null;
        public static GridLinesVisual3D Gridlines = null;
        public static HitSketchHelper CurSketch;
        public static Prototype WorkingPrototype = null;
        public static List<Prototype> Prototypes;
        public static Prototype SelectedPrototype = null;
        public static RDF_GeometricItem ReplaceFrom = null;
        private static AxisAngleRotation3D curRotation = null;// Matrix3D.Identity;
        public static Canvas SketchOverlay = null;
        public static bool isSweptSolid = false;
        // temp hack
        public static bool isoffsetArray = false;
        public static double tmpInterval = 0;

        private static bool isMovingStroke = true;
        public static int? excludeDeviceID;
#if Debug
        private static List<WireLine> debugingLines = new List<WireLine>();
#endif
        //static WireLine wirePt1 = null;
        //static WireLine wirePt11 = null;
        //static WireLine wirePt2 = null;
        //static WireLine wirePt22 = null;

        public static void InitializeCanvasComponents(Viewport3D viewport, SketchCanvas inkcanvas, Cursor3D cursor, 
            Model3DGroup gp, Model3DGroup gp1, HitSketchHelper curSketch, GridLinesVisual3D gridlines)
        {
            InkCanv = inkcanvas;
            ViewPort3D = viewport;
            Cursor3D = cursor;
            SolidModelGroup = gp;
            PlaneGroup = gp1;
            CurSketch = curSketch;

            createXYZAxes();
            Gridlines = gridlines;

            isSweptSolid = false;
        }

        #region Create Utility Objects
        private static void createXYZAxes()
        {
            Color transparent = Colors.White;
            transparent.A = 0;

            //createContextAxis();

            #region main axis
            WireLine hxaxis = new WireLine();
            WireLine hyaxis = new WireLine();
            WireLine hzaxis = new WireLine();
            hxaxis.ID = "hxaxis";
            hxaxis.WireType = WireTypeEnum.HitAxis;
            hxaxis.Opacity = 0;
            hxaxis.Color = Colors.Red;
            hxaxis.Thickness = 15;
            hyaxis.ID = "hyaxis";
            hyaxis.WireType = WireTypeEnum.HitAxis;
            hyaxis.Opacity = 0;
            hyaxis.Color = Colors.Green;
            hyaxis.Thickness = 15;
            hzaxis.ID = "hzaxis";
            hzaxis.WireType = WireTypeEnum.HitAxis;
            hzaxis.Opacity = 0;
            hzaxis.Color = Colors.Blue;
            hzaxis.Thickness = 15;


            hxaxis.Point1 = new Point3D(-15, 0, 0);
            hxaxis.Point2 = new Point3D(15, 0, 0);
            hyaxis.Point1 = new Point3D(0, -15, 0);
            hyaxis.Point2 = new Point3D(0, 15, 0);
            hzaxis.Point1 = new Point3D(0, 0, -15);
            hzaxis.Point2 = new Point3D(0, 0, 15);

            ViewPort3D.Children.Add(hxaxis);
            ViewPort3D.Children.Add(hyaxis);
            ViewPort3D.Children.Add(hzaxis);

            WireLine xaxis = new WireLine();
            xaxis.ID = "xaxis";
            xaxis.Color = Colors.Red;
            xaxis.WireType = WireTypeEnum.CursorAxis;
            WireLine yaxis = new WireLine();
            yaxis.ID = "yaxis";
            yaxis.Color = Colors.Green;
            yaxis.WireType = WireTypeEnum.CursorAxis;
            WireLine zaxis = new WireLine();
            zaxis.ID = "zaxis";
            zaxis.Color = Colors.Blue;
            zaxis.WireType = WireTypeEnum.CursorAxis;

            xaxis.Point1 = new Point3D(-100, 0, 0);
            xaxis.Point2 = new Point3D(100, 0, 0);
            yaxis.Point1 = new Point3D(0, -100, 0);
            yaxis.Point2 = new Point3D(0, 100, 0);
            zaxis.Point1 = new Point3D(0, 0, -100);
            zaxis.Point2 = new Point3D(0, 0, 100);

            ViewPort3D.Children.Insert(3, xaxis);
            ViewPort3D.Children.Insert(3, yaxis);
            ViewPort3D.Children.Insert(3, zaxis);
            #endregion

            #region hit planee
            SolidColorBrush plancolor = new SolidColorBrush(transparent);

            MeshGeometry3D hitplane = new MeshGeometry3D();
            hitplane.Positions.Add(new Point3D(-10000, -10000, 0));
            hitplane.Positions.Add(new Point3D(10000, -10000, 0));
            hitplane.Positions.Add(new Point3D(10000, 10000, 0));
            hitplane.Positions.Add(new Point3D(-10000, 10000, 0));

            hitplane.TriangleIndices.Add(0);
            hitplane.TriangleIndices.Add(1);
            hitplane.TriangleIndices.Add(2);
            hitplane.TriangleIndices.Add(2);
            hitplane.TriangleIndices.Add(3);
            hitplane.TriangleIndices.Add(0);

            GeometryModel3D hitPlane = new GeometryModel3D(hitplane, new DiffuseMaterial(plancolor));
            hitPlane.BackMaterial = new DiffuseMaterial(plancolor);

            Transform3DGroup tgroup = new Transform3DGroup();
            RotateTransform3D rtransform = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), 0));
            TranslateTransform3D ttransform = new TranslateTransform3D(0, 0, 0);
            tgroup.Children.Add(rtransform);
            tgroup.Children.Add(ttransform);

            hitPlane.Transform = tgroup;
            hitPlane.SetValue(GeomLib.Extensions.IDProperty, "hitplan");
            hitPlane.SetValue(GeomLib.Extensions.IsInHitTestProperty, true);
            Library.PlaneGroup.Children.Add(hitPlane);

            Library.CurSketch.HitPlane = hitPlane;

            #endregion
        }
        #endregion
        /// <summary>
        /// Take the primitives and add the parts(segments) into the RDFPolygon
        /// </summary>
        /// <param name="primitives"></param>
        /// <param name="polygon"></param>
        /// <param name="mtx">for transforming the first point to the global locatoin</param>
        /// <returns>the offset of the polygon related to global locations</returns>
        public static Point3D CreateRDFSegments(this List<Point3D> pts, RDF_Polygon3D polygon)
        {
            Point3D prept = new Point3D();
            Point3D stpt = new Point3D();
            int i = 1;
            foreach (var curpt in pts)
            {
                if (i == 1)
                {
                    stpt = curpt;
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("Start Point " + i + ": " + pm.start);
                    var seg = new RDF_Line3D(prept, curpt);
                    polygon.AddPart(seg);
                }
                prept = curpt;
                i++;
            }
            return stpt;
        }


        /// <summary>
        /// Take the primitives and add the parts(segments) into the RDFPolygon
        /// </summary>
        /// <param name="primitives"></param>
        /// <param name="polygon"></param>
        /// <param name="mtx">for transforming the first point to the global locatoin</param>
        /// <returns>the offset of the polygon related to global locations</returns>
        internal static Point3D CreateRDFSegments(this List<Primitive> primitives, RDF_Polygon3D polygon, bool resetGUID = false)
        {
            Point3D stpt = new Point3D();
            if (polygon.Segments != null)
                polygon.CleanAllParts();
            
            int i = 1;
            foreach (Primitive pm in primitives)
            {
                if (resetGUID)
                    pm.GUID = Guid.NewGuid().ToString();

                //System.Diagnostics.Debug.WriteLine("Start Point " + i + ": " + pm.start);
                var seg = new RDF_Segment3D(pm.GUID);
                if (pm.type == WrapperLibrary.Cornucopia.BasicPrimitive.PrimitiveType.LINE)
                {
                    seg = new RDF_Line3D(new Point3D(pm.PreviousNode.X, pm.PreviousNode.Y, 0),
                        new Point3D(pm.NextNode.X, pm.NextNode.Y, 0), pm.GUID);
                }
                else if (pm.type == WrapperLibrary.Cornucopia.BasicPrimitive.PrimitiveType.ARC)
                {
                    seg = new RDF_Arc3D(pm.GUID);
                    double rotate = pm.startCurvature < 0 ? Math.PI / 2 : -Math.PI / 2;
                    ((RDF_Arc3D)seg).SetStartAngle(pm.startAngle + rotate);
                    ((RDF_Arc3D)seg).SetRadius(1 / pm.startCurvature);
                    ((RDF_Arc3D)seg).SetSize(pm.startCurvature * pm.length);
                    ((RDF_Arc3D)seg).StartNode = new RDF_Node3D(pm.PreviousNode.X, pm.PreviousNode.Y, 0);
                    ((RDF_Arc3D)seg).EndNode = new RDF_Node3D(pm.NextNode.X, pm.NextNode.Y, 0);
                    ((RDF_Arc3D)seg).StartNode.NextEdge = (RDF_Arc3D)seg;
                    ((RDF_Arc3D)seg).EndNode.PreEdge = (RDF_Arc3D)seg;
                }
                polygon.AddPart(seg, i == primitives.Count);
                if (i == 1)
                {
                    stpt = new Point3D(pm.PreviousNode.X, pm.PreviousNode.Y, 0);
                }

                i++;
            }

            return stpt;
        }
        internal static CustomStroke FindCustomStrokeByID(this StrokeCollection strokes, string GUID)
        {
            foreach (var st in strokes.ToList())
            {
                if (st is CustomStroke)
                {
                    CustomStroke ct = (CustomStroke)st;
                    if (ct.GUID == GUID)
                        return ct;
                }
            }
            return null;
        }
        internal static void TransformPolygon3D(RDF_GeometricItem geom, Matrix3D newTransform)
        {
            if (!(geom is RDF_Polygon3D)) return;
            RDF_Polygon3D outer = geom as RDF_Polygon3D;
            CustomStroke customStroke = InkCanv.Strokes.FindCustomStrokeByID(outer.GUID);
            if (customStroke == null && outer.DirectionFrom != null)
                customStroke = InkCanv.Strokes.FindCustomStrokeByID(outer.DirectionFrom.GUID);

            if (customStroke != null && GlobalVariables.CurrentMode != Mode.Array && isMovingStroke)
            {
                customStroke.matrix = newTransform.Clone();
                customStroke.matrix.OffsetX = -customStroke.matrix.OffsetX;
                customStroke.matrix.OffsetY = -customStroke.matrix.OffsetY;
            }

            //cursor.DestroyCursor();
            // set the cursor to the new location
            // not use for now -- util the object can retain selected when moving
            var oldMtx = outer.Transform.GetMatrix3D();
            #region translate all the wirepaths associated to this 2D shape
            // just translate outer.Transform.GeometryObject instead of rerender the whole geometry
            var line = outer.Transform.GeometryObject;
            if (line is WirePath && ((WirePath)line).Paths.Count > 0)
                TranslateWirePath(oldMtx, line);
            else
                line = RDF_Dim1.RenderConceptLine(outer.Transform, ViewPort3D);

            if (outer.DirectionFrom != null)
            {
                var directionfrom = outer.DirectionFrom;
                var directionline = directionfrom.PathGeometry;
                if (directionline != null)
                    TranslateWirePath(oldMtx, directionline);
            }
            #endregion
        }
        internal static void TransformFace2D(RDF_GeometricItem geom, Matrix3D newTransform)//, bool isFirst)
        {
            if (!(geom is RDF_Face2D)) return;
            RDF_Face2D rdfFace = geom as RDF_Face2D;
            RDF_Polygon3D outer = rdfFace.Outer;

            CustomStroke customStroke = InkCanv.Strokes.FindCustomStrokeByID(outer.GUID);
            if (customStroke == null && outer.DirectionFrom != null)
                customStroke = InkCanv.Strokes.FindCustomStrokeByID(outer.DirectionFrom.GUID);

            if (customStroke != null && GlobalVariables.CurrentMode != Mode.Array && isMovingStroke)
            {
                customStroke.matrix = newTransform.Clone();
                customStroke.matrix.OffsetX = -customStroke.matrix.OffsetX;
                customStroke.matrix.OffsetY = -customStroke.matrix.OffsetY;
                //customStroke.matrix.OffsetZ = -customStroke.matrix.OffsetY;
            }
            else
            {
                // break
            }


            //cursor.DestroyCursor();
            // set the cursor to the new location
            // not use for now -- util the object can retain selected when moving
            var oldMtx = outer.Transform.GetMatrix3D();
            MatrixTransform3D mtxTransform = new MatrixTransform3D(Matrix3D.Identity);
            var centroidmtx = oldMtx.Clone();
            var centroid = outer.Centroid;
            centroid = centroidmtx.Transform(centroid);
            centroidmtx.OffsetX = centroid.X;
            centroidmtx.OffsetY = centroid.Y;
            centroidmtx.OffsetZ = centroid.Z;
            mtxTransform.Matrix = centroidmtx;
            //if(GlobalVariables.CurrentMode != Mode.MoveShape)
            Cursor3D.SetTransform(mtxTransform);

            #region translate all the wirepaths associated to this 2D shape
            // just translate outer.Transform.GeometryObject instead of rerender the whole geometry
            var line = outer.Transform.GeometryObject;
            if (line is WirePath && ((WirePath)line).Paths.Count > 0)
                TranslateWirePath(oldMtx, line);
            else
                line = RDF_Dim1.RenderConceptLine(outer.Transform, ViewPort3D);

            foreach (var inner in rdfFace.Inners)
            {
                var innerline = inner.PathGeometry;
                if (innerline is WirePath && ((WirePath)innerline).Paths.Count > 0)
                    TranslateWirePath(oldMtx, innerline);
                else
                    innerline = RDF_Dim1.RenderConceptLine(inner.Transform, ViewPort3D);
            }

            if (outer.DirectionFrom != null)
            {
                var directionfrom = outer.DirectionFrom;
                var directionline = directionfrom.PathGeometry;
                if(directionline != null)
                    TranslateWirePath(oldMtx, directionline);
            }
            #endregion

            //PreviousStep pstep = addPstepLine(line);
            //pstep.ObjectType = pstep.ObjectType | PreStepObjectTypeEnum.MeshModel;
            //rdfFace.RenderShapeMesh(pstep, group1);
            if (rdfFace.Geometry != null)
                rdfFace.Geometry.Transform = new MatrixTransform3D(newTransform);
            //else
            //    System.Diagnostics.Debug.WriteLine("no rdfface geometry");
        }

        private static void TranslateWirePath(Matrix3D oldMtx, DependencyObject line)
        {
            // need to set a right matrix...
            WirePath translatingPath = (WirePath)line;
            var firstPt = translatingPath.Paths.First().First();
            Matrix3D newProfileMtx = oldMtx;

            AxisAngleRotation3D rot3D = new AxisAngleRotation3D();
            if (curRotation != null)
            {
                RotateTransform3D rotTransform = new RotateTransform3D(curRotation, firstPt);
                newProfileMtx = rotTransform.Value * newProfileMtx;
            }
            
            MatrixTransform3D newtransPathform = new MatrixTransform3D(newProfileMtx);
            translatingPath.Transform = newtransPathform;
        }

        public static void RotateShape(TouchPointCollection tppts)
        {
            var touchpt = tppts.Last();//.First(pt => pt.TouchDevice.Id != Library.TouchOnID);
            Vector3D pivot = new Vector3D(0, 0, 1);
            Vector3D oripivot = new Vector3D(0, 0, 1);
            bool isrotating = false;
            switch (TouchOn)
            {
                case MovingAxis.xaxis:
                    RotateHitPlanes(Cursor3D.CHZAxis, Cursor3D.CHYAxis, false);
                    CurSketch.Hover(touchpt.Position, false, true);
                    pivot = new Vector3D(Cursor3D.CHXAxis.Point2.X - Cursor3D.CHXAxis.Point1.X,
                        Cursor3D.CHXAxis.Point2.Y - Cursor3D.CHXAxis.Point1.Y, Cursor3D.CHXAxis.Point2.Z - Cursor3D.CHXAxis.Point1.Z);
                    oripivot = pivot;
                    pivot = Cursor3D.CHXAxis.Transform.Transform(pivot);
                    isrotating = true;
                    break;
                case MovingAxis.yaxis:
                    RotateHitPlanes(Cursor3D.CHXAxis, Cursor3D.CHZAxis, false);
                    CurSketch.Hover(touchpt.Position, false, true);
                    pivot = new Vector3D(Cursor3D.CHYAxis.Point2.X - Cursor3D.CHYAxis.Point1.X,
                        Cursor3D.CHYAxis.Point2.Y - Cursor3D.CHYAxis.Point1.Y, Cursor3D.CHYAxis.Point2.Z - Cursor3D.CHYAxis.Point1.Z);
                    oripivot = pivot;
                    pivot = Cursor3D.CHYAxis.Transform.Transform(pivot);
                    isrotating = true;
                    break;
                case MovingAxis.zaxis:
                    RotateHitPlanes(Cursor3D.CHXAxis, Cursor3D.CHYAxis, false);
                    CurSketch.Hover(touchpt.Position, false, true);
                    pivot = new Vector3D(Cursor3D.CHZAxis.Point2.X - Cursor3D.CHZAxis.Point1.X,
                        Cursor3D.CHZAxis.Point2.Y - Cursor3D.CHZAxis.Point1.Y, Cursor3D.CHZAxis.Point2.Z - Cursor3D.CHZAxis.Point1.Z);
                    oripivot = pivot;
                    pivot = Cursor3D.CHZAxis.Transform.Transform(pivot);    ////
                    isrotating = true;
                    break;
            }
            if (isrotating && CurSketch.HitPlanePoints3D.Count > 0)
            {
                Point3D curPoint = CurSketch.HitPlanePoints3D.Last();
                Matrix3D cursorMtx = Cursor3D.CHXAxis.Transform.Value;

                if (!Library.preSecTouch.Equals(new Point3D()))
                {
                    GlobalVariables.CurrentMode = Mode.RotateShape;
                    Point3D curCenter = new Point3D((float)cursorMtx.OffsetX, (float)cursorMtx.OffsetY, (float)cursorMtx.OffsetZ);
                    Vector3D preVector = Library.preSecTouch - curCenter;
                    Vector3D curVector = curPoint - curCenter;
                    double angle = Vector3D.AngleBetween(curVector, preVector);
                    var normal = Vector3D.CrossProduct(pivot, preVector);
                    //System.Diagnostics.Debug.WriteLine("pivot: " + pivot.ToString());
                    //System.Diagnostics.Debug.WriteLine("center: " + curCenter.ToString());
                    normal.Normalize();
                    double angleToNormal = Vector3D.AngleBetween(normal, curVector);
                    angle = angleToNormal > 90 ? -2 * angle : 2 * angle;
                    
                    AxisAngleRotation3D rot3D = new AxisAngleRotation3D(oripivot, angle);
                    var curLoc = new Point3D(Cursor3D.GetTransform().Value.OffsetX, Cursor3D.GetTransform().Value.OffsetY, Cursor3D.GetTransform().Value.OffsetZ);
                    RotateTransform3D cursorTransform = new RotateTransform3D(rot3D);
                    cursorMtx = cursorTransform.Value * cursorMtx;
                    Cursor3D.SetMatrix3D(cursorMtx);
                    
                    if (GlobalVariables.SelectedItem != null)
                    {
                        var selMatrix = GlobalVariables.SelectedItem.Transform.GetMatrix3D().Clone();
                        selMatrix.Invert();
                        curLoc = selMatrix.Transform(curLoc);
                        curRotation = new AxisAngleRotation3D(pivot, angle);
                        RotateTransform3D rotTransform = new RotateTransform3D(rot3D, curLoc);
                        if (!GlobalVariables.SelectedItem.IsInArray)
                        {
                            GlobalVariables.SelectedItem.Transform.Matrix.TransformRDFGeoms(rotTransform.Value, Library.TransformFace2D);
                            if (GlobalVariables.SelectedItem.Constraint.ConstraintName == GeometricConstraintEnum.Array)
                            {
                                foreach (var cstobj in GlobalVariables.SelectedItem.Constraint.ConstraintObjects)
                                {
                                    if (cstobj is RDF_Face2D)
                                    {
                                        ((RDF_Face2D)cstobj).Transform.Matrix.TransformRDFGeoms(rotTransform.Value, Library.TransformFace2D);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var constraint = GlobalVariables.SelectedItem.InstantiatedFrom.Constraint;
                            foreach (var cstobj in constraint.ConstraintObjects)
                            {
                                if (cstobj is RDF_Face2D)
                                {
                                    ((RDF_Face2D)cstobj).Transform.Matrix.TransformRDFGeoms(rotTransform.Value, Library.TransformFace2D);
                                    angle += angle;
                                    rot3D = new AxisAngleRotation3D(oripivot, angle);
                                    rotTransform = new RotateTransform3D(rot3D);
                                }
                            }
                        }
                            
                        // rotate the constraint objects
                        curRotation = null;
                    }
                }
                Library.preSecTouch = curPoint;
            }
        }

        public static void MoveShape(TouchEventArgs e)
        {
            if ((GlobalVariables.CurrentMode & Mode.MoveShape) != 0 && TouchCount <= 2 && 
                TouchCount > 0 && GlobalVariables.SelectedItem != null && GlobalVariables.CurrentMode != Mode.RotateShape)
            {
                TouchPoint mouseposition = e.GetTouchPoint(ViewPort3D);
                if (GlobalVariables.CurrentTouchObjects.Count > 0 && !GlobalVariables.CurrentTouchObjects.ContainsKey(e.TouchDevice.Id) && TouchCount == 2)//!GlobalVariables.CurrentTouchObjects.ContainsKey(e.TouchDevice.Id))
                {
                    var clonedObj = CloneRDF_Object(GlobalVariables.SelectedItem);
                    clonedObj.Select();
                    Tuple<Point, RDF_GeometricItem> touchedObj = new Tuple<Point, RDF_GeometricItem>(mouseposition.Position, clonedObj);
                    GlobalVariables.CurrentTouchObjects.Add(e.TouchDevice.Id, touchedObj);
                    return;
                }
                var touchobjects = GlobalVariables.CurrentTouchObjects;
                var touchindex = touchobjects.Keys.ToList().IndexOf(e.TouchDevice.Id);
                if (TouchCount == 2 && touchindex == 0)
                {
                    //System.Diagnostics.Debug.WriteLine("not reacting: " + e.TouchDevice.Id);
                    return;
                }

                if (touchobjects.ContainsKey(e.TouchDevice.Id) || TouchOn != MovingAxis.none)
                {
                    var touchedItem = TouchOn != MovingAxis.none ? GlobalVariables.SelectedItem : GlobalVariables.CurrentTouchObjects[e.TouchDevice.Id].Item2;
                    HitResultTypeEnum resulttype = CurSketch.Hover(mouseposition.Position);

                    // move the shape
                    //var hitpoints = CurSketch.GetHitPointOnGeom(touchedItem.Geometry);
                    //int ptcount = hitpoints.Count;//CurSketch.HitPlanePoints3D.Count;
                    int ptcount = CurSketch.HitPlanePoints3D.Count;

                    if (ptcount > 1)
                    {
                        if (touchedItem != null && touchedItem.HasTransform)
                        {
                            //Point3D? prept = null;
                            //Point3D? curpt = null;
                            //if (GlobalVariables.CurrentTouchObjects.Count > 1)
                            //{
                            //    prept = CurSketch.CurHitObjects.GetHitPointOnGeom(touchedItem.Geometry, 1);//.HitPlanePoints3D[ptcount - 2];
                            //    curpt = CurSketch.CurHitObjects.GetHitPointOnGeom(touchedItem.Geometry, 0);// HitPlanePoints3D[ptcount - 1];
                            //}
                            //else
                            //{
                                var prept = CurSketch.HitPlanePoints3D[ptcount - 2];
                                var curpt = CurSketch.HitPlanePoints3D[ptcount - 1];
                                Vector3D offset = curpt - prept;
                            //}
                            if (touchedItem.Geometry != null && prept != null && curpt != null)
                            {
                                Move2Dand3D(touchedItem, offset);
                            }
                            else if (touchedItem is RDF_Polygon3D)
                            {
                                var touchPl = (RDF_Polygon3D)touchedItem;
                                if (GlobalVariables.CurrentTouchObjects.Count > 1)
                                {
                                    OffsetRDFPolyLine(touchPl, curpt);
                                }
                                else
                                {
                                    // only move the polygon
                                    if (touchPl.IsClosed)
                                        Move2Dand3D(touchPl.Face, offset);
                                    else
                                        Move1D(touchedItem, offset);
                                }
                            }
                        }
                    }
                }
                else
                {

                }
            }
        }

        public static RDF_GeometricItem CloneRDF_Object(RDF_GeometricItem selObj)
        {
            // clone the original object
            var newObj = selObj.Clone();
            Step pstep = new Step();
            pstep.ObjectType = StepObjectTypeEnum.MeshModel;
            WirePath wire = null;
            if (newObj is RDF_Face2D)
                ((RDF_Face2D)newObj).RenderShapeMesh(pstep, Library.SolidModelGroup);
            else if (newObj is RDF_SweptAreaSolid)
                ((RDF_SweptAreaSolid)newObj).RenderConceptMesh(Library.SolidModelGroup);
            else if (newObj is RDF_Boolean3D)
                ((RDF_Boolean3D)newObj).RenderConceptMesh(Library.SolidModelGroup);
            else if (newObj is RDF_Polygon3D)
                wire = RDF_Dim1.RenderConceptLine(newObj.Transform, Library.ViewPort3D);
            
            //create a converted points
            if (wire != null)
            {
                // create a new stroke out of the new cloned polygon
                NewStrokeFromPolygon(selObj, newObj, wire);
            }
            return newObj;
        }

        public static CustomStroke NewStrokeFromPolygon(RDF_GeometricItem oldObj, RDF_GeometricItem newObj, WirePath wire, bool resetOffset = false)
        {
            var mtx3D = newObj.Transform.GetMatrix3D();
            var cvtpts = CustomStroke.CreateConvertedPoints(wire, newObj.Transform.GetMatrix3D());

            if (resetOffset)
            {
                var fstPt = cvtpts.First();
                List<Point> offCvtPts = new List<Point>();
                foreach (var pt in cvtpts)
                {
                    offCvtPts.Add(new Point(pt.X - fstPt.X, pt.Y - fstPt.Y ));
                }
                cvtpts = offCvtPts;
                mtx3D.OffsetX = fstPt.X;
                mtx3D.OffsetY = fstPt.Y;
            }

            mtx3D.Invert();
            var oldstroke = GlobalVariables.CanvasStrokes.FindCustomStrokeByID(oldObj.GUID);

            CustomStroke cst = new CustomStroke(oldstroke.StylusPoints, cvtpts, mtx3D);
            cst.GUID = newObj.GUID;
            GlobalVariables.CanvasStrokes.Add(cst);

            return cst;
        }
        /// <summary>
        /// offset the item and rerender it
        /// 1. take the points from the existing item
        ///     a. get the stroke that associate with this object
        ///     b. get the converted points in the stroke
        /// 2. offset the points
        /// 3. recreate the polygon from these new points
        /// </summary>
        /// <param name="curpt"></param>
        /// <param name="touchPl"></param>
        /// <returns></returns>
        private static void OffsetRDFPolyLine(RDF_Polygon3D touchPl, Point3D curpt)
        {
            CustomStroke st = GlobalVariables.CanvasStrokes.FindCustomStrokeByID(touchPl.GUID);
            if (st != null)
            {
                var tmpCurPt = st.matrix.Transform(curpt);
                var offsetPts = OffsetLinePoints(st.DrawnPts.ToPoint3D(), tmpCurPt);
                MatrixTransform3D transform = new MatrixTransform3D(touchPl.Transform.GetMatrix3D());
                if(offsetPts != null)
                    CommandLibrary.DrawSketchPath(transform, offsetPts, touchPl.GUID);
            }
        }

        public static List<Point3D> OffsetLinePoints(List<Point3D> point3D, Point3D tmpCurPt, bool flipping = false)
        {
            var closetPt = point3D.FindClosetPointOnPolyline(tmpCurPt).Item2;
            var offsetL = (closetPt - tmpCurPt).Length;
            if (offsetL > 3)
            {
                var itpts = point3D.ToIntPoint(Matrix3D.Identity, 5);
                List<List<ClipperLib.IntPoint>> result = new List<List<ClipperLib.IntPoint>>();
                ClipperLib.ClipperOffset coffset = new ClipperLib.ClipperOffset();
                coffset.AddPath(itpts, ClipperLib.JoinType.jtMiter, ClipperLib.EndType.etOpenButt);
                coffset.Execute(ref result, offsetL * 5, 0);

                if (result.Count > 0 && result.First() != null && result.First().Count > 3)
                {
                    //System.Diagnostics.Debug.WriteLine("result count: " + result.Count);
                    var offsetPts = result.First().ToPoint3D(0.2);
                    offsetPts.Add(offsetPts.First());
                    var ptfst = new Point(point3D.First().X, point3D.First().Y);
                    var ptlst = new Point(point3D.Last().X, point3D.Last().Y);
                    List<int> rmList = new List<int>();

                    for (int i = 1; i < offsetPts.Count; i++)
                    {
                        Point compPt1 = new Point(offsetPts[i].X, offsetPts[i].Y);
                        Point compPt2 = new Point(offsetPts[i - 1].X, offsetPts[i - 1].Y);
                        var dist1 = GeomLib.BasicGeomLib.GetShortestDistBtwLineSegAndPoint(compPt1, compPt2, ptfst);
                        var dist2 = GeomLib.BasicGeomLib.GetShortestDistBtwLineSegAndPoint(compPt1, compPt2, ptlst);

                        if (dist1 < 3)
                            rmList.Add(i);
                        else if (dist2 < 3)
                            rmList.Add(i);
                    }
#if Debug
                                                Vector3D ptofset = new Vector3D(1, 1, 0);
                                                int j = 0;
                                                foreach (var dbgLine in debugingLines)
                                                    ViewPort3D.Children.Remove(dbgLine);
                                                debugingLines.Clear();

                                                foreach (int rm in rmList)
                                                {
                                                    Point3D rmPt1 = touchPl.Transform.GetMatrix3D().Transform(offsetPts[rm]);
                                                    Point3D rmPt2 = touchPl.Transform.GetMatrix3D().Transform(offsetPts[rm - 1]);
                                                    WireLine wirePt2 = new WireLine();
                                                    wirePt2.Thickness = 5;
                                                    wirePt2.Color = Colors.Red;
                                                    WireLine wirePt11 = new WireLine();
                                                    wirePt11.Thickness = 5;
                                                    wirePt11.Color = Colors.Blue;

                                                    wirePt2.Point1 = rmPt1;
                                                    wirePt2.Point2 = rmPt1 + ptofset;
                                                    wirePt11.Point1 = rmPt2;
                                                    wirePt11.Point2 = rmPt2 + ptofset;

                                                    ViewPort3D.Children.Insert(0, wirePt2);
                                                    ViewPort3D.Children.Insert(0, wirePt11);

                                                    debugingLines.Add(wirePt2);
                                                    debugingLines.Add(wirePt11);
                                                    j++;
                                                }
#endif
                    if (rmList.Count > 1)
                    {
                        List<Point3D> side1 = new List<Point3D>(offsetPts);
                        side1.RemoveRange(rmList[1], offsetPts.Count - rmList[1]);
                        side1.RemoveRange(0, rmList.First());

                        Point3D[] otherside = new Point3D[offsetPts.Count];// = new List<Point3D>();
                        offsetPts.CopyTo(0, otherside, 0, rmList[0]);
                        Point3D[] reverseOtherside = new Point3D[offsetPts.Count - rmList.Last() - 1];
                        offsetPts.CopyTo(rmList[1], reverseOtherside, 0, offsetPts.Count - rmList.Last() - 1);
                        reverseOtherside.Reverse();
                        var side2 = otherside.ToList();
                        side2.InsertRange(0, reverseOtherside);
                        int elementCount = rmList[0] + (offsetPts.Count - rmList[1] - 1);
                        side2.RemoveRange(elementCount, side2.Count - elementCount);

                        var closest1 = side1.FindClosetPointOnPolyline(tmpCurPt).Item2;
                        var closest2 = side2.FindClosetPointOnPolyline(tmpCurPt).Item2;
                        if (!flipping && closest1 != null && closest2 != null && closest1.distanceTo(tmpCurPt) < closest2.distanceTo(tmpCurPt))
                            offsetPts = side1;
                        else
                            offsetPts = side2;

                        return offsetPts;
                    }
                }
            }
            return null;
        }

        private static void Move1D(RDF_GeometricItem touchedItem, Vector3D offset)
        {
            RDF_MatrixAbstract mtxabstract = touchedItem.Transform.Matrix;
            var mtx3D = mtxabstract.Matrix3D.Clone();
            mtx3D.Invert();
            offset = mtx3D.Transform(offset);

            if (touchedItem is RDF_Dim1 && ((RDF_Dim1)touchedItem).pathgeom != null)
            {
                if (((RDF_Dim1)touchedItem).pathgeom.WireType == WireTypeEnum.Pivot)
                    return;
            }
            mtxabstract.MoveRDFGeoms(offset, Library.TransformPolygon3D);
        }

        private static void Move2Dand3D(RDF_GeometricItem touchedItem, Vector3D offset)
        {
            //Vector3D offset = curpt.Value - prept.Value;
            

            RDF_MatrixAbstract mtxabstract = touchedItem.Transform.Matrix;
            var mtx3D = mtxabstract.Matrix3D.Clone();
            mtx3D.Invert();
            offset = mtx3D.Transform(offset);
            //if (mtxabstract is RDF_Matrix)
            {
                MoveCount++;
                switch (TouchOn)
                {
                    case MovingAxis.xaxis:
                        offset.Y = 0;
                        offset.Z = 0;
                        break;
                    case MovingAxis.yaxis:
                        offset.X = 0;
                        offset.Z = 0;
                        break;
                    case MovingAxis.zaxis:
                        offset.X = 0;
                        offset.Y = 0;
                        break;
                    case MovingAxis.xyplane:
                        //offset.Z = 0;
                        break;
                    case MovingAxis.xzplane:
                        //offset.Y = 0;
                        break;
                    case MovingAxis.yzplane:
                        //offset.Z = 0;
                        break;
                }
                mtx3D.Invert();
                offset = mtx3D.Transform(offset);
                List<Point3D> path = null;
                bool isInArray = touchedItem.IsInArray;
                var extendedItem = touchedItem;
                RDF_Polygon3D controlPolyline = null;
                GeomLib.Constraint.GeometricConstraint populatingCst = null;
                if (!isInArray && touchedItem is RDF_SweptAreaSolid && ((RDF_SweptAreaSolid)touchedItem).SweptProfile.Face!= null)
                {
                    RDF_Face2D selectedFace = ((RDF_SweptAreaSolid)touchedItem).SweptProfile.Face;
                    isInArray = selectedFace.IsInArray;
                    extendedItem = selectedFace;
                }

                if (isInArray)
                {
                    var masterItem = extendedItem.InstantiatedFrom;
                    if (masterItem != null)
                    {
                        var cstName = masterItem.Constraint.ConstraintName;
                        populatingCst = masterItem.Constraint;
                        if (cstName == GeometricConstraintEnum.None && masterItem is RDF_SweptAreaSolid)
                        {
                            masterItem = ((RDF_SweptAreaSolid)masterItem).SweptProfile.Face;
                            cstName = masterItem.Constraint.ConstraintName;
                            populatingCst = masterItem.Constraint;
                            foreach (var br in ((RDF_Face2D)masterItem).Outer.BackRelations)
                            {
                                if (br.Name == BackRelationName.Dim1_Face_Outer)
                                {
                                    bool isfound = false;
                                    foreach (var br2 in ((RDF_Face2D)br.RelatedTo).BackRelations)
                                    {
                                        if (br2.RelatedTo == touchedItem)
                                        {
                                            isfound = true;
                                            break;
                                        }
                                    }
                                    if (isfound)
                                    {
                                        extendedItem = (RDF_Face2D)br.RelatedTo;
                                        break;
                                    }
                                }
                            }
                        }

                        if (populatingCst.ConstraintName == GeometricConstraintEnum.Array && populatingCst.ControllingObject is RDF_Polygon3D)
                        {
                            controlPolyline = (RDF_Polygon3D)populatingCst.ControllingObject;
                            path = OffsetPathPoints(populatingCst, controlPolyline.pathgeom);
                        }
                    }
                }
                // transform the touched item
                isMovingStroke = !isInArray;
                var movingparam = mtxabstract.MoveRDFGeoms(offset, Library.TransformFace2D, false, path);
                if (extendedItem is RDF_Face2D && extendedItem.Constraint.Type == ConstraintType.Angle && controlPolyline != null)
                {
                    //((RDF_Face2D)extendedItem).RotateToConstraint();
                }
                isMovingStroke = true;

                // meet the criteria of resetting the array
                if (movingparam.Item1 > -1 && isInArray && path != null && populatingCst != null)
                    MoveConstraintObjects(path, populatingCst, movingparam.Item1, movingparam.Item2, extendedItem);
                else if (extendedItem.Constraint.ConstraintName == GeometricConstraintEnum.Array && extendedItem.Constraint.ControllingObject != null)
                {
                    var cst = extendedItem.Constraint;
                    var controlobj = (RDF_Polygon3D)cst.ControllingObject;
                    var ctlMtx = controlobj.Transform.Matrix;

                    ctlMtx.MoveRDFGeoms(offset, Library.TransformFace2D);
                    Library.TransformPolygon3D(controlobj, ctlMtx.Matrix3D);

                    foreach (RDF_GeometricItem obj in cst.ConstraintObjects)
                    {
                        var objMtx = obj.Transform.Matrix;
                        isMovingStroke = false;
                        objMtx.MoveRDFGeoms(offset, Library.TransformFace2D);
                        isMovingStroke = true;
                    }
                }
            }
        }

        internal static List<Point3D> OffsetPathPoints(GeomLib.Constraint.GeometricConstraint cst, WirePath controlPolyline)
        {
            var mtx = controlPolyline.Transform.Value.Clone();
            var path = new List<Point3D>(controlPolyline.Paths.First());

            // var cst = csts.First();

            for (int i = 0; i < path.Count; i++)
            {
                var pt = path[i];
                pt = mtx.Transform(pt);
                pt.X -= cst.ArrayOffset.X;
                pt.Y -= cst.ArrayOffset.Y;
                pt.Z -= cst.ArrayOffset.Z;
                path[i] = pt;
            }
            return path;
        }

        internal static void MoveConstraintObjects(List<Point3D> path, GeomLib.Constraint.GeometricConstraint cst, int stopIndex = -1, Point3D? stopPt = null, RDF_GeometricItem extendedItem = null, bool isOperating = false, bool isRecording = false)
        {
            double pathlength = 0;
            double stopDistance = 0;
            stopIndex = stopIndex == -1 ? path.Count : stopIndex;
            for (int i = 1; i < path.Count; i++)
            {
                var preptOnpath = path[i - 1];
                var curpt = path[i];
                if (i < stopIndex)
                {
                    var pt = i == stopIndex - 1 && stopPt != null ? stopPt.Value : path[i];
                    stopDistance += preptOnpath.distanceTo(pt);
                }
                pathlength += curpt.distanceTo(preptOnpath);
            }

            //var cst = extendedItem.InstantiatedFrom.Constraint;// selectedItem.InstantiatedFrom.Constraint;

            // 1. decide what index the object is in
            var changedIndex = extendedItem != null ? cst.ConstraintObjects.FindIndex(obj => obj == extendedItem) : cst.ConstraintObjects.Count;
            //changedIndex = changedIndex == -1 ? cst.ConstraintObjects.Count : changedIndex;

            // 2. decide what's the new intervals
            var interval = stopDistance / (changedIndex + 1);
            tmpInterval = interval;
            if (isRecording)
                cst.Interval = interval;

            int targetElem = (int)(pathlength / interval);
            if (GlobalVariables.CurrentTouchObjects.Count > 1)
            {
                if (targetElem > cst.ConstraintObjects.Count)
                {
                    CommandLibrary.InsertNewEllementInArray(cst);
                }
                else if (targetElem < cst.ConstraintObjects.Count - 1 && cst.ConstraintObjects.Count > 0)
                {
                    CommandLibrary.RemoveElementInArray(cst, (RDF_GeometricItem)cst.ConstraintObjects.Last());
                }
            }
            int pathIndex = 1;
            int objIndex = 0;
            stopDistance = interval;

            if (Library.isoffsetArray)
            {
                stopDistance += interval / 2 + 0.75;
                Library.isoffsetArray = false;
            }

            while (pathIndex < path.Count)
            {
                Point3D prept = path[pathIndex - 1];
                Point3D pt = path[pathIndex];
                Vector3D segVec = pt - prept;
                double segLength = segVec.Length;

                if (stopDistance > segLength)
                {
                    stopDistance = stopDistance - segLength;
                    pathIndex++;
                    continue;
                }
                
                segVec.Normalize();
                var newLoc = prept + stopDistance * segVec; //pathIndex == path.Count - 1 ? pt : 
                if (objIndex != changedIndex)
                {
                    // move the old object
                    if (objIndex < cst.ConstraintObjects.Count)
                    {
                        RDF_GeometricItem curobj = (RDF_GeometricItem)cst.ConstraintObjects[objIndex];
                        RDF_MatrixAbstract tmpmtx = curobj.Transform.Matrix;
                        isMovingStroke = false;
                        tmpmtx.MoveRDFGeoms(newLoc, Library.TransformFace2D);
                        if (curobj is RDF_Face2D)
                        {
                            //((RDF_Face2D)curobj).RotateToConstraint();
                        }
                        isMovingStroke = true;
                    }
                    else
                        break;
                }
                stopDistance += interval;
                objIndex++;
            }
        }
        
        internal static object RotateHitPlanes(WireLine caxis1, WireLine caxis2, bool rotateGrids = true)
        {
            Vector3D vec2 = new Vector3D(caxis2.Point2.X - caxis2.Point1.X, caxis2.Point2.Y - caxis2.Point1.Y, caxis2.Point2.Z - caxis2.Point1.Z);
            //vec2 = caxis2.Transform.Transform(vec2);
            return RotateHitPlanes(caxis1, vec2, rotateGrids);
        }

        internal static object RotateHitPlanes(WireLine caxis1, Vector3D vec2, bool rotateGrids = true, string planeName = "")
        {
            Vector3D vec1 = new Vector3D(caxis1.Point2.X - caxis1.Point1.X, caxis1.Point2.Y - caxis1.Point1.Y, caxis1.Point2.Z - caxis1.Point1.Z);
            Vector3D vec3 = Vector3D.CrossProduct(vec1, vec2);
            vec3 = caxis1.Transform.Transform(vec3);
            vec3.Normalize();
            RotateTransform3D rotate = new RotateTransform3D(GeomLib.BasicGeomLib.GetAxisRotation3D(vec3));

            TranslateTransform3D translate = new TranslateTransform3D();
            translate.OffsetX = caxis1.Transform.Value.OffsetX;
            translate.OffsetY = caxis1.Transform.Value.OffsetY;
            translate.OffsetZ = caxis1.Transform.Value.OffsetZ;

            Transform3DGroup transformgroup = new Transform3DGroup();
            transformgroup.Children.Add(rotate);
            transformgroup.Children.Add(translate);

            GeometryModel3D hitplane = CurSketch.HitPlane;
            if(rotateGrids)
                CurSketch.HitPlaneOnObject = Cursor3D.OnRDFObj;

            hitplane.Transform = transformgroup;

            if (rotateGrids)
            {
                if (Library.Cursor3D.IsOnCentroid && (planeName == "cxzplane" || planeName == "cyzplane"))
                    isSweptSolid = true;
                else
                    isSweptSolid = false;

                //Gridlines.Center = new Point3D(caxis1.Transform.Value.OffsetX, caxis1.Transform.Value.OffsetY, caxis1.Transform.Value.OffsetZ);
                //Gridlines.Normal = vec3;
                Transform3D oriTransoform = Library.Gridlines.Transform;
                Gridlines.Transform = transformgroup;

                if (UIUtility.PSteps.Count > 0 && UIUtility.PSteps.Last().PStep == StepTypeEnum.AxisTransform)
                {
                    oriTransoform = UIUtility.PSteps.Last().AxisTransform;
                    UIUtility.PSteps.Remove(UIUtility.PSteps.Last());
                }

                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.Transform;
                pstep.PStep = StepTypeEnum.AxisTransform;
                pstep.AxisTransform = oriTransoform;
                UIUtility.PSteps.Add(pstep);
                UIUtility.NSteps = new List<Step>();
            }
            return null;
        }

        internal static void SetFirstTouchTown(int id)
        {
            TouchOnID = id;
            preSecTouch = new Point3D();
        }

        internal static void rotateHitPlaneTowardCameraOnZ()
        {
            var camTransform = Library.ViewPort3D.Camera.Transform;
            var camMatrix = camTransform.Value;
            var vectorY = new Vector3D(1, 0, 0);
            vectorY = camMatrix.Transform(vectorY);
            vectorY.Z = 0;
            if (Math.Round(vectorY.Length, 2) == 0)
            {
                vectorY = new Vector3D(0, 1, 0);
            }
            else
                vectorY.Normalize();

            Library.RotateHitPlanes(Library.Cursor3D.CHZAxis, vectorY, false);
        }

        internal static void chkProfileOnSweptSolidSurface(RDF_SweptAreaSolid sweptSolid, RDF_Transformation constraintTransform)
        {
            // only deal with the RDF_Line3D at the moment
            if(sweptSolid.Direction is RDF_Line3D)
            {
                var line3D = (RDF_Line3D)sweptSolid.Direction;
                if (line3D.Pts.Count > 1)
                {
                    var pt1 = new Point3D(line3D.Pts[0].X, line3D.Pts[0].Y, line3D.Pts[0].Z);
                    var pt2 = new Point3D(line3D.Pts[1].X, line3D.Pts[1].Y, line3D.Pts[1].Z);
                    var mtx = sweptSolid.Transform.GetMatrix3D();
                    pt1 = mtx.Transform(pt1);
                    pt2 = mtx.Transform(pt2);

                    var testMtx = constraintTransform.GetMatrix3D();
                    testMtx.Invert();

                    pt1 = testMtx.Transform(pt1);
                    pt2 = testMtx.Transform(pt2);

                    // There is a problem when the swept solid is not on the XY plane
                    // the transformation is not quite right
                    if (Math.Abs(pt1.Z) < 0.001) // on the base surface
                    {
                        constraintTransform.Constraint.Type = ConstraintType.Surface;
                        constraintTransform.Constraint.ConstraintObjects.Add(line3D.StartNode);
                        constraintTransform.Constraint.ConstraintName = TransformationConstraintEnum.BottomSurface;
                        line3D.StartNode.Controlling.Add(constraintTransform.Constraint);
                    }
                    else if(Math.Abs(pt2.Z) < 0.001) // on the top surface
                    {
                        constraintTransform.Constraint.Type = ConstraintType.Surface;
                        constraintTransform.Constraint.ConstraintObjects.Add(line3D.EndNode);
                        constraintTransform.Constraint.ConstraintName = TransformationConstraintEnum.TopSurface;
                        line3D.EndNode.Controlling.Add(constraintTransform.Constraint);

                    }
                    else // decide which edge surface it is attaching to
                    {
                        var profile = sweptSolid.SweptProfile;
                        foreach (var seg in profile.Segments)
                        {
                            if (seg is RDF_Line3D)
                            {
                                RDF_Line3D lineseg = seg as RDF_Line3D;
                                var lineVector = lineseg.GetVector3D();
                                var zVector = new Vector3D(0, 0, 1);
                                var normalVector = Vector3D.CrossProduct(lineVector, zVector);

                                normalVector = testMtx.Transform(normalVector);

                                double angle = Vector3D.AngleBetween(zVector, normalVector);

                                if(angle >177)
                                {
                                    System.Diagnostics.Debug.WriteLine(seg.StartNode.GetPoint3D().ToString() + ", " + seg.EndNode.GetPoint3D().ToString());
                                }
                            }
                        }
                    }
                }
            }


            // need to recognize which face it is attching to here
            // there are only couple possibilities for a swept area solid
            // 1. on the top or bottom
            //      1. check the rotation of "mtx" and the rotation of the profile of the swept solid are align 
            //      2. check the translation of the the "mtx" is the same as the top face or not
            // 2. on the edge
            //      1. make the normal vector of the edge
            //      2. see the rotation of that vecor
            //      3. see if this rotation is aligned with the mtx rotation
        }

        /// <summary>
        /// set the mode back to standard and rotate the hitplane back with the gridlines
        ///  the command ends when user finish a stroke.
        /// </summary>
        internal static void setToDefault(bool resetSelection = true)
        {
            CommandLibrary.SetDefaultCommand();
            Events_SketchCanvas.isDiggedHole = false;
            Library.ReplaceFrom = null;
            GlobalVariables.IsPrototyping = false;
            if (GlobalVariables.CurrentMode != Mode.Standard)
            {
                Library.CurSketch.EndSketch();
                Library.CurSketch.HitPlane.Transform = Library.Gridlines.Transform;
                Library.TouchOn = MovingAxis.none;
                System.Diagnostics.Debug.WriteLine("set to default");
                GlobalVariables.CurrentMode = Mode.Standard;
                if (GlobalVariables.SelectedItem != null && resetSelection)
                    GlobalVariables.SelectedItem.UnSelect();
            }
            Events_SketchCanvas.prePtTick = long.MaxValue;
        }
    }
}
