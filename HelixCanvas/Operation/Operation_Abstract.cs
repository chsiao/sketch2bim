﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperLibrary.RDF;

namespace SolidSketch.Operation
{
    public abstract class Operation_Abstract
    {
        public OperationName OperationName;
        internal RDF_GeometricItem workingRDF = null;
        internal RDF_Abstract recordedOutput = null;
        public bool IsReadyToProcess { set; get; }
        public OperationName Name { get { return OperationName; } }
        public string GUID { set; get; }

        public void SetRecordedOutPut(RDF_Abstract output)
        {
            this.recordedOutput = output;
        }
        public RDF_Abstract RecordedOutputProduct{ get { return recordedOutput; } }
    }

    public enum OperationName
    {
        OffsetToFace2D,
        OffsetLine,
        Extrude,
        SwpetSolid,
        MakeArray,
        None
    }
}

