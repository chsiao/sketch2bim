﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using WrapperLibrary.RDF;
using Petzold.Media3D;
using CanvasSketch;
using System.Windows;
using System.Windows.Input;
using GeomLib;
using Utility;
using System.Xml;
using System.Xml.Serialization;

namespace SolidSketch.Operation
{
    public class Prototype
    {
        private RDF_Polygon3D principleLine;
        private List<Operation_Relation> operations;
        public bool isSpecialOperation = false;
        
        [XmlIgnoreAttribute]
        public System.Windows.Controls.RadioButton AssociateControl { set; get; }

        public string PrototypeSemantic { set; get; }

        public Prototype()
        {
            this.operations = new List<Operation_Relation>();

            // create a line and place it in the 3D viewport
            principleLine = new RDF_Polygon3D();
            Point3D pt3D_1 = new Point3D(0, 0, 0);
            Point3D pt3D_2 = new Point3D(50, 0, 0);
            RDF_Line3D protLine = new RDF_Line3D(pt3D_1, pt3D_2);
            principleLine.AddPart(protLine);

            Matrix3D mtx3D = new Matrix3D(1,0,0,0,0,1,0,0,0,0,1,0,-25,20,0,1);

            RDF_Matrix protMtx = new RDF_Matrix(mtx3D);
            RDF_Transformation protTransform = new RDF_Transformation();
            protTransform.SetTransformGeom(principleLine);
            protTransform.SetMatrix(protMtx);

            WirePath protPath = RDF_Dim1.RenderConceptLine(protTransform, Library.ViewPort3D);
            protPath.Thickness = 3;
            protPath.Color = Colors.OrangeRed;
            protPath.WireType = WireTypeEnum.Pivot;

            Point pt1 = new Point(0,0);
            Point pt2 = new Point(25,0);
            Point pt3 = new Point(50, 0);
            List<Point> ptcol = new List<Point>() { pt1, pt2, pt3};
            StylusPointCollection stylusPts = new StylusPointCollection(ptcol);
            Matrix3D inverse = mtx3D.Clone();
            inverse.Invert();

            CustomStroke cst = new CustomStroke(stylusPts, ptcol, inverse);
            cst.GUID = principleLine.GUID;
            cst.FitintoCurves();

            GlobalVariables.CanvasStrokes.Add(cst);
            //Library.ViewPort3D;
        }

        public void AddOperation(Operation_Relation operation)
        {
            // need to build the relationships between the existing operation and the new operation
            int count = operation.InputRelations.Count;
            List<BackRelation> oriBackRelations = new List<BackRelation>(operation.InputRelations);
            for (int i = 0; i < count; i++)
            {
                var br = oriBackRelations[i];
                foreach (var existOper in this.operations)
                {
                    
                    if (existOper.RecordedOutputProduct == br.Relating)
                        existOper.AddNextOperations(operation, br);
                    else if (br is ConstraintRelation)
                    {
                        var cbr = (ConstraintRelation)br;
                        if (existOper.RecordedOutputProduct == Library.ReplaceFrom)
                        {
                            existOper.AddNextOperations(operation, cbr);
                        }
                    }
                    else
                    {
                        // try to capture the next relation for the array objects
                        var cbr = existOper.InputRelations.Find(ir => ir is ConstraintRelation);
                        if (cbr != null)
                        {
                            if (((ConstraintRelation)cbr).Relating is RDF_Face2D)
                            {
                                var cstFace = (RDF_Face2D)((ConstraintRelation)cbr).Relating;
                                if (cstFace.Outer == br.Relating)
                                    existOper.AddNextOperations(operation, br);
                            }
                        }
                    }
                }
            }
            this.operations.Add(operation);
        }

        public RDF_Polygon3D PrincipleLine
        {
            get { return this.principleLine; }
        }

        public List<Operation_Relation> Operations { get { return this.operations; } }

        /// <summary>
        /// Clean the geometry (including the wire and meshes) when the operation is done
        /// </summary>
        public void CleanOnCanvasGeometry()
        {
            // clear the principle geometry
            Library.ViewPort3D.Children.Remove(this.principleLine.pathgeom);
            // remove the geom in the back relations

            foreach (var opr in this.operations)
            {
                foreach (var br in opr.InputRelations)
                {
                    if (br.Relating is RDF_GeometricItem)
                        ((RDF_GeometricItem)br.Relating).Remove();
                    if (br is ConstraintRelation)
                    {
                        var cst = ((ConstraintRelation)br).RelationConstraint;
                        ((RDF_Polygon3D)cst.ControllingObject).Remove();
                    }
                }
            }
        }
        
        // Todo: I should do a topological sorting here so that I know the order of process the operations
        internal void ProcessOperations()
        {
            // need to use Queue to store the operations
            Queue<Operation_Relation> tobeProcess = new Queue<Operation_Relation>(this.operations);
            Dictionary<BackRelation, object> results = new Dictionary<BackRelation, object>();

            if (Library.CurSketch.HitPlanePoints3D.Count > 1)
            {
                while (tobeProcess.Count > 0)
                {
                    var opr = tobeProcess.Dequeue();
                    opr.UpdateConnRelationResults(results);
                    // need to check if the prerequired item is done or not
                    if (opr.IsReadyToProcess)
                    {
                        if (opr.Name == OperationName.MakeArray && opr.workingRDF != null)
                            opr.workingRDF.Constraint.Interval = 20;

                        object result = null;
                        // need to give the input if there is any for the operation
                        if (opr.Name == OperationName.OffsetToFace2D)
                            result = opr.ProcessOffsetToFace2D(results);
                        else
                            result = opr.ProcessOperation(results);

                        if (result != null)
                            foreach (var nxtrel in opr.NextOperations.Keys)
                                if(!results.ContainsKey(nxtrel))
                                    results.Add(nxtrel, result);
                    }
                    else
                        tobeProcess.Enqueue(opr); // put into queue again and wait for it's ready
                }
            }
        }

        internal void ExecuteOperations(CustomStroke newStroke, Matrix3D mtx, RDF_Polygon3D polygon, RDF_Transformation rdfTran, RDF_MatrixAbstract rdfMatrix)
        {
            Queue<Operation_Relation> tobeProcess = new Queue<Operation_Relation>(this.operations);
            Dictionary<BackRelation, object> results = new Dictionary<BackRelation, object>();
            RDF_SweptAreaSolid oriSolid = null;
            int numOfObj = 1;

            while (tobeProcess.Count > 0)
            {
                var opr = tobeProcess.Dequeue();
                opr.UpdateConnRelationResults(results);
                if (opr.IsReadyToProcess)
                {
                    if (opr.Name == OperationName.MakeArray && opr.workingRDF != null)
                    {
                        numOfObj = opr.workingRDF.Constraint.ConstraintObjects.Count;
                    }
                    RDF_GeometricItem result = opr.Execute(results, newStroke, mtx, polygon, rdfTran, rdfMatrix);
                    if (result != null)
                    {
                        if (result is RDF_SweptAreaSolid)
                        {
                            ((RDF_SweptAreaSolid)result).ShapeSemantics = this.PrototypeSemantic;
                            if (this.PrototypeSemantic == "wall" && opr.Name != OperationName.MakeArray)
                                oriSolid = (RDF_SweptAreaSolid)result;
                        }
                        foreach (var nxtrel in opr.NextOperations.Keys)
                            results.Add(nxtrel, result);
                    }
                }
            }

            if (isSpecialOperation && oriSolid != null)
            {
                // 1. create the solid cube shape
                RDF_ExtrudedPolygon extrudedPl = new RDF_ExtrudedPolygon();
                RDF_Matrix mtxpl = new RDF_Matrix(Matrix3D.Identity);
                RDF_Transformation pltransform = new RDF_Transformation();
                pltransform.SetMatrix(mtxpl);
                pltransform.SetTransformGeom(extrudedPl);

                List<System.Windows.Point> profile = new List<System.Windows.Point>();
                profile.Add(new Point(-1.5, 100));
                profile.Add(new Point(1.5, 100));
                profile.Add(new Point(1.5, -100));
                profile.Add(new Point(-1.5, -100));
                profile.Add(new Point(-1.5, 100));

                extrudedPl.SetProfile(profile);
                extrudedPl.SetExtrusionLength(25);
                //extrudedPl.RenderConceptMesh();
                extrudedPl.RenderConceptMesh(oriSolid.group);

                for (int i = 0; i < numOfObj; i++)
                {
                    var newObj = (RDF_ExtrudedPolygon)extrudedPl.Clone();
                    newObj.RenderConceptMesh(oriSolid.group);
                    extrudedPl.Constraint.ConstraintObjects.Add(newObj);
                }

                Library.isoffsetArray = true;
                // 2. create an array of the same cube shape
                //RDF_Polygon3D pivotline = cstbr.IsConnectingRelation && cstbr.TemporyInput is RDF_Polygon3D ? (RDF_Polygon3D)cstbr.TemporyInput : polygon;
                CommandLibrary.ExecuteSketchLine(newStroke, mtx, polygon, rdfTran, overridingItem: extrudedPl);

                var translateMtx = polygon.Transform.GetMatrix3D();
                Point3D translateTo = new Point3D(translateMtx.OffsetX - (Library.tmpInterval / 2) - 0.75, translateMtx.OffsetY, translateMtx.OffsetZ);
                RDF_MatrixAbstract mtxabstract = extrudedPl.Transform.Matrix;
                mtxabstract.MoveRDFGeoms(translateTo, Library.TransformFace2D);

                Library.isoffsetArray = false;
                RDF_GeometricItem boolProduct = GestureCommand.ProcessBooleanCommand(oriSolid, extrudedPl); ;
                // 3. substract the geometries
                foreach (var cstobj in extrudedPl.Constraint.ConstraintObjects)
                {
                    if (cstobj is RDF_ExtrudedPolygon)
                    {
                        boolProduct = GestureCommand.ProcessBooleanCommand(boolProduct, (RDF_ExtrudedPolygon)cstobj);
                    }
                }

            }
        }
    }
}
