﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Xml.Serialization;
using WrapperLibrary.RDF;
using GeomLib;
using GeomLib.Constraint;
using Utility;
using CanvasSketch;

namespace SolidSketch.Operation
{
    public class Operation_Relation : Operation_Abstract
    {
        private List<BackRelation> brs;
        [XmlIgnore]
        private Dictionary<ClassRelation, Operation_Relation> nxtoprs;
        private int principleIndex = -1;

        public Operation_Relation()
        {
            this.IsReadyToProcess = true;
            this.OperationName = Operation.OperationName.None;
            this.brs = new List<BackRelation>();
            this.nxtoprs = new Dictionary<ClassRelation, Operation_Relation>();
            this.GUID = Guid.NewGuid().ToString();
        }

        public Operation_Relation(OperationName name)
        {
            this.IsReadyToProcess = true;
            this.OperationName = name;
            this.brs = new List<BackRelation>();
            this.nxtoprs = new Dictionary<ClassRelation, Operation_Relation>();
            this.GUID = Guid.NewGuid().ToString();
        }

        public void AddInputRelation(BackRelation br)
        {
            this.brs.Add(br);
            if(br is PrincipleRelation)
                this.principleIndex = this.brs.Count - 1;
            else if (br is ClassRelation && ((ClassRelation)br).IsConnectingRelation)
                IsReadyToProcess = false;
        }

        public int PrincipleIndex { get { return this.principleIndex; } }
        
        public List<BackRelation> InputRelations { get { return brs; } }
        [XmlIgnore]
        public Dictionary<ClassRelation, Operation_Relation> NextOperations
        {
            get { return this.nxtoprs; }
        }

        public void AddNextOperations(Operation_Relation nxtOpr, BackRelation connTo)
        {
            ClassRelation clr = null;
            if (connTo is ClassRelation)
                clr = (ClassRelation)connTo;
            else
                clr = new ClassRelation(connTo);
            clr.IsConnectingRelation = true;
            this.nxtoprs.Add(clr, nxtOpr);

            // switch the backreation to connectbr
            nxtOpr.InputRelations.Remove(connTo);
            nxtOpr.AddInputRelation(clr);
        }

        internal void UpdateConnRelationResults(Dictionary<BackRelation, object> results)
        {
            foreach (var connR in this.InputRelations)
            {
                if (results.ContainsKey(connR) && connR is ClassRelation && ((ClassRelation)connR).IsConnectingRelation)
                {
                    var tmpreuslt = results[connR];
                    ((ClassRelation)connR).TemporyInput = tmpreuslt;
                }
            }

            int notReadyCount = 0;
            foreach (var br in this.InputRelations)
            {
                if (br is ClassRelation && ((ClassRelation)br).IsConnectingRelation && ((ClassRelation)br).TemporyInput == null)
                    notReadyCount++;
            }

            this.IsReadyToProcess = notReadyCount == 0;
        }

        internal List<Point3D> ProcessOffsetToFace2D(Dictionary<BackRelation, object> results)
        {
            if (this.Name == OperationName.OffsetToFace2D && IsReadyToProcess)
            {
                var br1 = this.brs[0];
                var br2 = this.brs[1];
                RDF_GeometricItem widthPl = br1.Name == BackRelationName.Dim1_Polygon_Width ?
                    (RDF_GeometricItem)br1.Relating : br2.Name == BackRelationName.Dim1_Polygon_Width ?
                    (RDF_GeometricItem)br2.Relating : null;
                if (widthPl != null && widthPl is RDF_Polygon3D)
                {
                    GlobalVariables.CurrentMode = Mode.AddOneDimension;
                    List<Point3D> pivotLine = null;
                    var connBr = br1 is ClassRelation && ((ClassRelation)br1).IsConnectingRelation ? br1 : br2 is ClassRelation && ((ClassRelation)br2).IsConnectingRelation ? br2 : null;
                    if (connBr != null)
                    {
                        if (results.ContainsKey(connBr) && results[connBr] is List<Point3D>)
                        {
                            pivotLine = (List<Point3D>)results[connBr];
                        }
                    }

                    // coming from the drawing line, but it could be from other resources
                    var offsetPolygon = CommandLibrary.CreateOffsetShapeCommand((RDF_Polygon3D)widthPl, pivotLine);
                    //System.Diagnostics.Debug.WriteLine("offset pl count: " + offsetPolygon.Count);
                    foreach (var nextopr in this.NextOperations)
                        nextopr.Key.TemporyInput = offsetPolygon;

                    return offsetPolygon;
                    #region Slow!!
                    //if (workingRDF == null || !(workingRDF is RDF_Polygon3D))
                    //{
                    //    workingRDF = new RDF_Polygon3D();
                    //    RDF_Polygon3D newrdfpolygon = new RDF_Polygon3D();
                    //    newrdfpolygon.IsVirtual = true;
                    //
                    //    RDF_Matrix newplmtx = new RDF_Matrix(Matrix3D.Identity);
                    //    RDF_Transformation newpltransform = new RDF_Transformation();
                    //    newpltransform.SetMatrix(newplmtx);
                    //    newpltransform.SetTransformGeom(workingRDF);
                    //}
                    //else
                    //    ((RDF_Polygon3D)workingRDF).CleanAllParts();
                    //
                    //offsetPolygon.CreateRDFSegments((RDF_Polygon3D)workingRDF);
                    //
                    //return workingRDF;
                    #endregion
                }
            }

            return null;
        }
        int debugcount = 0;
        internal object ProcessOperation(Dictionary<BackRelation, object> results)
        {
            if (IsReadyToProcess)
            {
                switch (this.Name)
                {
                    case Operation.OperationName.MakeArray:
                        // clone the object from the array
                        var rel = this.InputRelations.First();
                        if (rel is ConstraintRelation)
                        {
                            var cstrel = (ConstraintRelation)rel;
                            if (this.workingRDF == null)
                            {
                                //var stpt = Library.CurSketch.HitPlanePoints3D.First();
                                //stpt = cstrel.IsConnectingRelation && results.ContainsKey(cstrel) && results[cstrel] is List<Point3D> ? 
                                //    ((List<Point3D>)results[cstrel]).First() : stpt;
                                //var newOrigin = cstrel.Relating.Clone();
                                //var curMtx = newOrigin.Transform.GetMatrix3D();
                                //curMtx.OffsetX = stpt.X - cstrel.Relating.Constraint.ArrayOffset.X;
                                //curMtx.OffsetY = stpt.Y - cstrel.Relating.Constraint.ArrayOffset.Y;
                                //curMtx.OffsetZ = stpt.Z - cstrel.Relating.Constraint.ArrayOffset.Z;
                                //RDF_Matrix rdfmtx = new RDF_Matrix(curMtx);
                                //newOrigin.Transform.SetMatrix(rdfmtx);
                                //if (newOrigin is RDF_Face2D)
                                //{
                                //    ((RDF_Face2D)newOrigin).RenderShapeMesh(pstep: null, group: Library.SolidModelGroup, usingItsTransform: true);
                                //}

                                //this.workingRDF = newOrigin;
                                this.workingRDF = cstrel.Relating;
                                if (this.workingRDF != null)
                                {
                                    this.workingRDF.Constraint.Interval = 20;
                                }
                            }
                            List<Point3D> controlPts = new List<Point3D>();
                            if (cstrel.IsConnectingRelation && cstrel.TemporyInput != null && cstrel.TemporyInput is List<Point3D>)
                            {
                                controlPts = (List<Point3D>)cstrel.TemporyInput;
                                // use the points in tempinput instead of the sketching lines
                            }

                            this.workingRDF = CommandLibrary.ProcessArrayCommand(this.workingRDF.Constraint, controlPts);
                            List<GeometryProduct> arrayProducts = new List<GeometryProduct>(this.workingRDF.Constraint.ConstraintObjects);
                            arrayProducts.Insert(0, this.workingRDF);
                            return arrayProducts;
                        }
                        break;
                    case OperationName.OffsetLine:
                        var br = this.InputRelations.First();
                        if (br is PrincipleRelation)
                        {
                            var principleSt = GlobalVariables.CanvasStrokes.FindCustomStrokeByID(((RDF_Polygon3D)br.RelatedTo).GUID);
                            var startPt = ((RDF_Polygon3D)br.Relating).StartPt.GetPoint3D();
                            var closetPt = principleSt.DrawnPts.ToPoint3D().FindClosetPointOnPolyline(startPt);

                            var offset = closetPt.Item2 - startPt;
                            var drawingPts = Library.CurSketch.HitPlanePoints3D;
                            var fstPt = drawingPts.First();
                            var stPt = new Point3D(fstPt.X + offset.X, fstPt.Y + offset.Y, fstPt.Z + offset.Z);

                            var offsetPts = Library.OffsetLinePoints(drawingPts, stPt, true);

                            if (offsetPts != null)
                            {
                                CommandLibrary.UpdateWorkingPath(Matrix3D.Identity, offsetPts, this.GUID);
                                return offsetPts;
                            }
                            //var oriPl = br.RelatedTo
                        }
                        break;
                    case OperationName.Extrude:
                        var br1 = this.InputRelations[0] is ClassRelation && ((ClassRelation)this.InputRelations[0]).IsConnectingRelation ?
                            this.InputRelations[0] : this.InputRelations[1];
                        var br2 = this.InputRelations[0] is ClassRelation && ((ClassRelation)this.InputRelations[0]).IsConnectingRelation ?
                            this.InputRelations[1] : this.InputRelations[0];
                        List<System.Windows.Point> profile = new List<System.Windows.Point>();
                        if (br1 is ClassRelation && ((ClassRelation)br1).IsConnectingRelation)
                        {
                            var tmpinput = ((ClassRelation)br1).TemporyInput;
                            if (tmpinput is List<Point3D>)
                            {
                                var pt1 = ((RDF_Line3D)br2.Relating).Pts[0];
                                var pt2 = ((RDF_Line3D)br2.Relating).Pts[1];
                                var exLength = GeomLib.BasicGeomLib.distanceTo(pt1, pt2);
                                var profile3D = (List<Point3D>)tmpinput;
                                var count = ((List<Point3D>)profile3D).Count;
                                for (int i = 0; i < count - 1; i++)
                                {
                                    var pt3d = ((List<Point3D>)profile3D)[i];
                                    profile.Add(new System.Windows.Point(pt3d.X, pt3d.Y));
                                }

                                if (workingRDF == null || !(workingRDF is RDF_ExtrudedPolygon))
                                {
                                    workingRDF = new RDF_ExtrudedPolygon();
                                }
                                RDF_Matrix wmtx = new RDF_Matrix(Matrix3D.Identity);
                                wmtx.SetTranslate(new Point3D(0, 0, ((List<Point3D>)tmpinput).First().Z));
                                RDF_Transformation wtransform = new RDF_Transformation();
                                wtransform.SetMatrix(wmtx);
                                wtransform.SetTransformGeom(workingRDF);
                                ((RDF_ExtrudedPolygon)workingRDF).SetProfile(profile);
                                ((RDF_ExtrudedPolygon)workingRDF).SetExtrusionLength(exLength);
                                ((RDF_ExtrudedPolygon)workingRDF).RenderConceptMesh(Library.SolidModelGroup);
                            }
                            else if (tmpinput is List<GeometryProduct>)
                            {
                                var arrayInputs = (List<GeometryProduct>)tmpinput;
                                foreach (var input in arrayInputs)
                                {
                                    if (input is RDF_Face2D)
                                    {
                                        var face = (RDF_Face2D)input;
                                        if (workingRDF == null)
                                        {
                                            var direction = (RDF_Line3D)br2.Relating;

                                            if (face.Constraint.ConstraintName == GeometricConstraintEnum.Array) // this is a subject
                                                ExecuteExtrusion(direction, face, true);
                                            workingRDF = face;
                                        }
                                        else if(workingRDF is RDF_Face2D)
                                        {
                                            if (workingRDF.BackRelations.Last().Name == BackRelationName.Dim2_RelatedSolid
                                                && face.BackRelations.Count == 0)
                                            {
                                                var solid = (RDF_SweptAreaSolid)workingRDF.BackRelations.Last().RelatedTo;
                                                var cloneSolid = solid.Clone();
                                                cloneSolid.IsInArray = true;
                                                CommandLibrary.AddRelatedSolid(face, (RDF_SweptAreaSolid)cloneSolid);
                                            }
                                        }
                                        var newtransform = face.Transform;
                                        var clonedMtx = newtransform.Matrix;
                                        RDF_Transformation clonedTranslate = new RDF_Transformation();
                                        clonedTranslate.SetMatrix(clonedMtx);
                                        var sweptSolid = (RDF_GeometricItem)face.BackRelations.Last().RelatedTo;
                                        clonedTranslate.SetTransformGeom(sweptSolid);
                                        if (sweptSolid is RDF_SweptAreaSolid)
                                        {
                                            ((RDF_SweptAreaSolid)sweptSolid).RenderConceptMesh(Library.SolidModelGroup);
                                        }
                                    }
                                    
                                    // transform to the 
                                }
                            }

                        }
                        //RDF_Matrix rdf_matrix = new RDF_Matrix(Matrix3D.Identity);
                        //RDF_Transformation rdf_transform = new RDF_Transformation();
                        //rdf_transform.SetMatrix(rdf_matrix);
                        //rdf_transform.SetTransformGeom(workingRDF);
                        //RDF_Dim3.RenderConceptMesh(workingRDF, Library.SolidModelGroup);

                        break;
                }
                //this.InputRelations
            }
            return null;
        }

        internal RDF_GeometricItem Execute(Dictionary<BackRelation, object> preResult, CanvasSketch.CustomStroke newStroke, 
            System.Windows.Media.Media3D.Matrix3D mtx, RDF_Polygon3D polygon, RDF_Transformation rdfTran, RDF_MatrixAbstract rdfMatrix)
        {
            switch (this.Name)
            {
                case OperationName.MakeArray:
                    var br = this.brs.First();

                    if (br is ConstraintRelation && this.workingRDF != null)
                    {
                        var cstbr = (ConstraintRelation)br;
                        RDF_Polygon3D pivotline = cstbr.IsConnectingRelation && cstbr.TemporyInput is RDF_Polygon3D ? (RDF_Polygon3D)cstbr.TemporyInput : polygon;
                        CommandLibrary.ExecuteSketchLine(newStroke, mtx, pivotline, rdfTran, overridingItem: this.workingRDF);

                        var translateMtx = pivotline.Transform.GetMatrix3D();
                        Point3D translateTo = new Point3D(translateMtx.OffsetX, translateMtx.OffsetY, translateMtx.OffsetZ);
                        RDF_MatrixAbstract mtxabstract = this.workingRDF.Transform.Matrix;
                        mtxabstract.MoveRDFGeoms(translateTo, Library.TransformFace2D);

                        this.workingRDF = null;
                    }                    
                    break;
                case OperationName.OffsetLine:
                    return ExecuteOffsetLine(newStroke, ref mtx, polygon, rdfTran);
                case OperationName.OffsetToFace2D:
                    var conRel = this.brs.Find(cbr => cbr is ClassRelation && ((ClassRelation)cbr).IsConnectingRelation);
                    if (conRel != null && ((ClassRelation)conRel).TemporyInput != null && ((ClassRelation)conRel).TemporyInput is RDF_Polygon3D)
                    {
                        polygon = (RDF_Polygon3D)((ClassRelation)conRel).TemporyInput;
                        rdfTran = polygon.Transform;
                    }
                    else
                    {

                    }

                    return executeOffsetToFace2D(newStroke, polygon, rdfTran);
                case OperationName.Extrude:
                    var product = ExecuteExtrusion();
                    if (this.workingRDF != null)
                        this.workingRDF = null;
                    return product;
            }

            return null;
        }

        private RDF_GeometricItem ExecuteOffsetLine(CanvasSketch.CustomStroke newStroke, ref System.Windows.Media.Media3D.Matrix3D mtx, RDF_Polygon3D polygon, RDF_Transformation rdfTran)
        {
            CommandLibrary.ExecuteSketchLine(newStroke, mtx, polygon, rdfTran);
            var br = this.InputRelations.First();
            RDF_Polygon3D usingPolygon = polygon;
            if (br is ClassRelation && ((ClassRelation)br).IsConnectingRelation)
            {
                // find the tmp result from connecting relations
            }
            if (CommandLibrary.Workingpaths.ContainsKey(this.GUID))
            {
                var clonedPl = usingPolygon.Clone();
                CustomStroke newstroke = Library.NewStrokeFromPolygon(polygon, clonedPl, CommandLibrary.Workingpaths[this.GUID], true);
                newstroke.FitintoCurves();
                newstroke.Primitives.CreateRDFSegments((RDF_Polygon3D)clonedPl);

                // the transformation is not right
                Matrix3D newmtx = newstroke.matrix.Clone();
                newmtx.Invert();
                RDF_Matrix rdfMtx = new RDF_Matrix(newmtx);
                RDF_Transformation rdftransform = new RDF_Transformation();
                rdftransform.SetMatrix(rdfMtx);
                rdftransform.SetTransformGeom(clonedPl);

                var newwire = RDF_Dim1.RenderConceptLine(clonedPl.Transform, Library.ViewPort3D);
                newStroke.UpdateCvtPoints(newwire, mtx, ((RDF_Polygon3D)clonedPl).IsClosed);
                return clonedPl;
            }
            return null;
        }

        private RDF_GeometricItem ExecuteExtrusion()
        {
            // there are some cases that's not always a connection relationship
            var br1 = this.InputRelations[0] is ClassRelation && ((ClassRelation)this.InputRelations[0]).IsConnectingRelation ?
                    this.InputRelations[0] : this.InputRelations[1];
            var br2 = this.InputRelations[0] is ClassRelation && ((ClassRelation)this.InputRelations[0]).IsConnectingRelation ?
                    this.InputRelations[1] : this.InputRelations[0];
            if (br2.Relating is RDF_Line3D)
            {
                RDF_Line3D dirPath = (RDF_Line3D)br2.Relating;
                if (((ClassRelation)br1).TemporyInput is RDF_Polygon3D)
                {
                    RDF_Polygon3D profile = (RDF_Polygon3D)((ClassRelation)br1).TemporyInput;
                    if (profile.Face != null)
                        return ExecuteExtrusion(dirPath, profile.Face);
                }
                else if (((ClassRelation)br1).TemporyInput is List<GeometryProduct>)
                {
                    foreach (var face in (List<GeometryProduct>)((ClassRelation)br1).TemporyInput)
                    {
                        if (((RDF_Face2D)face).Outer.Face != null)
                            ExecuteExtrusion(dirPath, ((RDF_Face2D)face).Outer.Face);
                    }
                }
            }
            return null;
        }

        private RDF_GeometricItem ExecuteExtrusion(RDF_Line3D dirPath, RDF_Face2D face, bool isInprocess = false)
        {
            RDF_SweptAreaSolid sweptSolid = new RDF_SweptAreaSolid();
            sweptSolid.SetDirection(dirPath);
            sweptSolid.SetSweptProfile(face.Outer);

                BackRelation br = new BackRelation(face);
                br.RelatedTo = sweptSolid;
                br.Name = BackRelationName.Dim2_RelatedSolid;
                face.BackRelations.Add(br);

            // add openings
            //foreach (var inner in area.Inners)
            //{
            //    //RDF_Polygon3D newInner = inner.CreateInnerPolygon();
            //    SweptSolid.AddOpening(inner);
            //}

            RDF_Matrix flippingMtx = new RDF_Matrix(Matrix3D.Identity);
            RDF_MatrixMultiplication sweptSolidMtx = new RDF_MatrixMultiplication(face.Transform.Matrix, flippingMtx);

            RDF_Transformation translate = new RDF_Transformation();
            translate.SetMatrix(sweptSolidMtx);
            translate.SetTransformGeom(sweptSolid);

            sweptSolid.RenderConceptMesh(Library.SolidModelGroup);

            if(!isInprocess)
                cleanWorkingPrototype();
            return sweptSolid;
        }

        private void cleanWorkingPrototype()
        {
            if(this.workingRDF != null)
                this.workingRDF.Remove();
        }

        private RDF_GeometricItem executeOffsetToFace2D(CanvasSketch.CustomStroke newStroke, RDF_Polygon3D polygon, RDF_Transformation rdfTran)
        {
            var br1 = this.brs[0];
            var br2 = this.brs[1];
            // need to take care of the connection relationship here

            RDF_GeometricItem widthPl = br1.Name == BackRelationName.Dim1_Polygon_Width ?
                (RDF_GeometricItem)br1.Relating : br2.Name == BackRelationName.Dim1_Polygon_Width ?
                (RDF_GeometricItem)br2.Relating : null;
            if (widthPl != null && widthPl is RDF_Polygon3D)
                return CommandLibrary.ExecuteAddDimension(newStroke, polygon, rdfTran, (RDF_Polygon3D)widthPl);
            return null;
        }
    }

}
