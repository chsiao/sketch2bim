﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperLibrary.RDF;
using GeomLib.Constraint;

namespace SolidSketch.Operation
{
    public class ClassRelation : BackRelation
    {
        public bool IsConnectingRelation { set; get; }
        public ClassRelation(BackRelation br)
            : base(br.Relating)
        {
            this.IsConnectingRelation = false;
            this.RelatedTo = br.RelatedTo;
            this.Name = br.Name;
        }
        public ClassRelation(RDF_GeometricItem relatingItem)
            : base(relatingItem)
        {
            this.IsConnectingRelation = false;
        }
        public object TemporyInput { get; set; }
    }

    public class PrincipleRelation : ClassRelation
    {
        public PrincipleRelation(BackRelation br)
            : base(br.Relating)
        {
            this.RelatedTo = br.RelatedTo;
            this.Name = br.Name;
        }
    }

    public class ConstraintRelation : ClassRelation
    {
        public Constraint RelationConstraint { private set; get; }
        public bool IsPivotPrinciple { set; get; }
        public bool IsOriginConnected { set; get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cst"></param>
        /// <param name="origin">origin = the originate obj that will be instantiaed for array</param>
        public ConstraintRelation(GeometricConstraint cst, RDF_GeometricItem origin)
            :base(origin)
        {
            this.RelationConstraint = cst;
            this.IsPivotPrinciple = false;
        }
    }
}
