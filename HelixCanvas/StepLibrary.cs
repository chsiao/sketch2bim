﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using WrapperLibrary;
using WrapperLibrary.RDF;
using Petzold.Media3D;
using Utility;

namespace SolidSketch
{
    public static class StepLibrary
    {
        internal static void moveToStep(Step laststep, bool forward = false)
        {
            // refresh the selected item to prevent some unexpect behavior
            if (GlobalVariables.SelectedItem != null)
            {
                GlobalVariables.SelectedItem.UnSelect();
            }
            GlobalVariables.PreSelectedItem = null;

            if (laststep.PStep == StepTypeEnum.ObjectTransform && laststep.PreModel != null)
            {
                RDF_MatrixAbstract mtxabstract = laststep.PreModel.Transform.Matrix;
                mtxabstract.MoveRDFGeoms(laststep.ObjectTranslation, Library.TransformFace2D);
            }
            else
            {
                RDF_GeometricItem rdfgeom = laststep.PreModel;

                if (rdfgeom != null && (laststep.PStep == StepTypeEnum.Add || laststep.PStep == StepTypeEnum.Delete))
                {
                    if ((laststep.PStep == StepTypeEnum.Add && !forward) || (laststep.PStep == StepTypeEnum.Delete && forward))
                        deleteRDFGeom(rdfgeom);
                    else if ((laststep.PStep == StepTypeEnum.Delete && !forward) || (laststep.PStep == StepTypeEnum.Add && forward))
                        addRDFGeom(rdfgeom);
                }
                else if (rdfgeom != null && (laststep.PStep == StepTypeEnum.modifypath || laststep.PStep == StepTypeEnum.modifysolid))
                {
                    if (!forward)
                    {
                        deleteRDFGeom(rdfgeom);
                        // also clear the related geometry
                        if (rdfgeom is RDF_Face2D)
                        {
                            var rdfface = (RDF_Face2D)rdfgeom;
                            foreach (var br in rdfface.Outer.BackRelations)
                            {
                                if (br.Name == BackRelationName.Dim1_SweptSolid_Profile_Outer)
                                {
                                    deleteRDFGeom((RDF_GeometricItem)br.RelatedTo);
                                }
                            }
                        }


                        if (laststep.ModifiedObjs.Count > 0 && laststep.ModifiedObjs.First() is RDF_GeometricItem)
                        {
                            foreach (RDF_GeometricItem modifiedgeom in laststep.ModifiedObjs)
                            {
                                if (modifiedgeom is RDF_Dim1 && modifiedgeom is RDF_Polygon3D &&
                                    ((RDF_Polygon3D)modifiedgeom).IsClosed)
                                {
                                    var modifiedPolygon = (RDF_Polygon3D)modifiedgeom;
                                    if (modifiedPolygon.HasTransform)
                                    {
                                        // find the transform and siwtch it back to original transform
                                        // also try to generate the rendering again
                                        var modifiedTransform = modifiedPolygon.Transform;
                                        modifiedTransform.SetTransformGeom(modifiedPolygon);
                                        //RDF_Matrix rdfMatrix = (RDF_Matrix)modifiedTransform.Matrix;
                                        //rdfMatrix.SetTranslate(modifiedPolygon.StartPt.GetPoint3D());
                                        //modifiedTransform.GeometryObject = modifiedPolygon.pathgeom;
                                        RDF_Dim1.RenderConceptLine(modifiedTransform, Library.ViewPort3D);
                                    }
                                    var modifiedFace = modifiedPolygon.Face;
                                    addRDFGeom(modifiedFace);

                                    //if(modifiedFace.Transform.Matrix.rel)
                                }
                                else
                                    addRDFGeom(modifiedgeom);
                            }
                        }
                    }
                    else
                    {
                        addRDFGeom(rdfgeom);
                        if (rdfgeom is RDF_Face2D)
                        {
                            RDF_Transformation facetransform = rdfgeom.Transform;
                            RDF_Face2D redoFace2D = (RDF_Face2D)rdfgeom;
                            // add the solid back
                            foreach (var br in redoFace2D.Outer.BackRelations)
                            {
                                if (br.Name == BackRelationName.Dim1_SweptSolid_Profile_Outer)
                                {
                                    addRDFGeom((RDF_GeometricItem)br.RelatedTo);
                                }
                            }
                            if(facetransform != null)
                                facetransform.Matrix.RelatedTransforms.Add(facetransform);
                        }
                        if (laststep.ModifiedObjs.Count > 0 && laststep.ModifiedObjs.First() is RDF_GeometricItem)
                            foreach (RDF_GeometricItem todelete in laststep.ModifiedObjs)
                            {
                                deleteRDFGeom(todelete);
                                // delete object in back relations
                                if (todelete is RDF_Polygon3D)
                                {
                                    var pltodelete = (RDF_Polygon3D)todelete;
                                    foreach (var brtodelete in pltodelete.BackRelations)
                                        deleteRDFGeom((RDF_GeometricItem)brtodelete.RelatedTo);
                                }
                            }
                    }
                }

                if (laststep.ObjectType.HasFlag(StepObjectTypeEnum.Transform))
                {
                    Transform3D transform = laststep.AxisTransform;
                    Library.CurSketch.HitPlane.Transform = transform;
                    Library.Gridlines.Transform = transform;
                    
                }
            }
        }

        internal static void addRDFGeom(RDF_GeometricItem rdfgeom)
        {
            if (rdfgeom is RDF_Dim1)
            {
                addDim1((RDF_Dim1)rdfgeom);
            }
            else
            {
                GeometryModel3D mesh = rdfgeom.Geometry;
                if (mesh != null)
                    Library.SolidModelGroup.Children.Add(mesh);

                if (rdfgeom is RDF_Face2D)
                {
                    RDF_Face2D face = (RDF_Face2D)rdfgeom;
                    addDim1((RDF_Polygon3D)face.Outer);
                }
                else if (rdfgeom is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid solid = (RDF_SweptAreaSolid)rdfgeom;
                    RDF_Polygon3D profile = solid.SweptProfile;
                    profile.Transform.Matrix.RelatedTransforms.Add(solid.Transform);
                }
                if (!RDFWrapper.All_RDF_Objects.ContainsKey(rdfgeom.GUID))
                    RDFWrapper.All_RDF_Objects.Add(rdfgeom.GUID, rdfgeom);
            }
        }

        internal static void deleteRDFGeom(RDF_GeometricItem rdfgeom)
        {
            if (rdfgeom is RDF_Dim1)
            {
                removeDim1((RDF_Dim1)rdfgeom);
            }
            else
            {
                GeometryModel3D mesh = rdfgeom.Geometry;
                if (mesh != null)
                {
                    var isremoved = Library.SolidModelGroup.Children.Remove(mesh);

                    // release from related transform
                    RDF_Transformation itsTransform = rdfgeom.Transform;
                    itsTransform.Matrix.RelatedTransforms.Remove(itsTransform);


                    //RDF_Abstract rdfobj = (RDF_Abstract)mesh.GetValue(RDFExtension.RDFObjectProperty);
                    //if (GlobalVariables.SelectedItem == rdfobj)
                    //    GlobalVariables.SelectedItem.UnSelect();
                }
                if (rdfgeom is RDF_Face2D)
                {
                    RDF_Face2D face = (RDF_Face2D)rdfgeom;
                    removeDim1((RDF_Polygon3D)face.Outer);
                }
                if (RDFWrapper.All_RDF_Objects.ContainsKey(rdfgeom.GUID))
                    RDFWrapper.All_RDF_Objects.Remove(rdfgeom.GUID);
            }
            if (GlobalVariables.SelectedItem == rdfgeom)
                GlobalVariables.SelectedItem = null;
        }

        internal static void addDim1(RDF_Dim1 rdfline)
        {
            // connect the transform back to the geometry
            if (rdfline.HasTransform)
            {
                rdfline.Transform.SetTransformGeom(rdfline);
                rdfline.Transform.GeometryObject = rdfline.pathgeom;
            }

            WireBase line = rdfline.PathGeometry;
            if (!Library.ViewPort3D.Children.Contains(line))
                Library.ViewPort3D.Children.Add(line);
            if (RDFWrapper.All_RDF_Objects.ContainsKey(rdfline.GUID))
                RDFWrapper.All_RDF_Objects[rdfline.GUID] = rdfline;
            else
                RDFWrapper.All_RDF_Objects.Add(rdfline.GUID, rdfline);

            rdfline.Select();
        }

        internal static void removeDim1(RDF_Dim1 rdfline)
        {
            WireBase line = rdfline.PathGeometry;
            Library.ViewPort3D.Children.Remove(line);
            if(RDFWrapper.All_RDF_Objects.ContainsKey(rdfline.GUID))
                RDFWrapper.All_RDF_Objects.Remove(rdfline.GUID);
        }

        internal static Step addPstepLine(RDF_GeometricItem line)
        {
            if (line != null)
            {
                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.Line;
                pstep.PreModel = line;
                pstep.PStep = StepTypeEnum.Add;
                UIUtility.PSteps.Add(pstep);
                UIUtility.NSteps = new List<Step>();
                return pstep;
            }
            return new Step();
        }

        internal static Step modifyPstepLine(RDF_GeometricItem oldline, RDF_GeometricItem newline)
        {
            if (newline != null && oldline != null)
            {
                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.Line;
                pstep.PreModel = newline;
                pstep.ModifiedObjs.Add(oldline);
                pstep.PStep = StepTypeEnum.modifypath;
                UIUtility.PSteps.Add(pstep);
                UIUtility.NSteps = new List<Step>();
                return pstep;
            }
            return new Step();
        }
        internal static void moveForward()
        {
            if (UIUtility.NSteps != null && UIUtility.NSteps.Count > 0)
            {
                Step laststep = UIUtility.NSteps.Last();

                StepLibrary.moveToStep(laststep, true);

                UIUtility.NSteps.Remove(laststep);
                UIUtility.PSteps.Add(laststep);
            }
        }
        internal static void moveBack()
        {
            if (UIUtility.PSteps.Count > 0)
            {
                Step laststep = UIUtility.PSteps.Last();

                StepLibrary.moveToStep(laststep);

                UIUtility.PSteps.Remove(laststep);
                UIUtility.NSteps.Add(laststep);
            }
        }

    }
}
