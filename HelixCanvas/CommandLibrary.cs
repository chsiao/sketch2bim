﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Utility;
using GeomLib;
using GeomLib.Constraint;
using WrapperLibrary;
using WrapperLibrary.RDF;
using Petzold.Media3D;
using SolidSketch.Operation;
using CanvasSketch;

namespace SolidSketch
{
    public static class CommandLibrary
    {
        private static Point3D Origin = new Point3D(0, 0, 0);
        private static Point3D preSWpt;
        private static double firstZ;
        private static double preZ;
        private static int curArrayInterval;
        public static RDF_SweptAreaSolid SweptSolid;
        public static Dictionary<string, WirePath> Workingpaths = new Dictionary<string,WirePath>();
        public static List<WirePath> SketchPaths = new List<WirePath>();
        public static void SetDefaultCommand(bool initializing = false)
        {
            firstZ = 0;
            preZ = 0;
            preSWpt = new Point3D();
            
            if (GlobalVariables.CurrentMode != Mode.Standard || initializing)
            {
                SweptSolid = new RDF_SweptAreaSolid();
            }
            foreach(var wpath in Workingpaths.Values)
                Library.ViewPort3D.Children.Remove(wpath);
            Workingpaths.Clear();
        }

        public static void ProcessSweptSolidCommand()
        {
            if (Library.CurSketch.HitPlanePoints3D.Count > 0)
            {
                System.Diagnostics.Debug.WriteLine("try extruding");
                Point3D hitloc = Library.CurSketch.HitPlanePoints3D.Last();// CurSketch.HitPoints.First();
                Matrix3D mtxextrusion = Library.Cursor3D.CHZAxis.Transform.Value.Clone();
                // get the local coordinate system
                mtxextrusion.Invert();
                mtxextrusion.OffsetX = 0;
                mtxextrusion.OffsetY = 0;
                mtxextrusion.OffsetZ = 0;
                Point3D curLoc = mtxextrusion.Transform(hitloc); 

                if (Library.CurSketch.Count > 1 && SweptSolid != null && SweptSolid.SweptProfile != null)
                {
                    //System.Diagnostics.Debug.WriteLine(hitloc);
                    RDF_Line3D newline = new RDF_Line3D(preSWpt, curLoc);// hitloc);
                    var workingRDF = SweptSolid.Direction;
                    ((RDF_Polygon3D)workingRDF).AddPart(newline);
                    //System.Diagnostics.Debug.WriteLine("extruding.....");
                    SweptSolid.SetDirection((RDF_Polygon3D)workingRDF);
                    SweptSolid.RenderConceptMesh(Library.SolidModelGroup);
                    //System.Diagnostics.Debug.WriteLine("extrud success");
                }
                else
                {
                    CreateSweptSolidCommand(hitloc, false);
                }
                preSWpt = curLoc;//hitloc;
            }
        }

        public static List<Point3D> CreateOffsetShapeCommand(RDF_Polygon3D widthPl = null, List<Point3D> pivotPts = null)
        {
            widthPl = widthPl == null ? (GlobalVariables.SelectedItem is RDF_Polygon3D ? (RDF_Polygon3D)GlobalVariables.SelectedItem : null) 
                : widthPl;
            if (Library.CurSketch.HitPlanePoints3D.Count > 0 && widthPl != null)
            {
                if (widthPl.IsClosed || widthPl.Segments.Count == 0) return null;
                var brDirection = widthPl.BackRelations.Find(br => br.Name == BackRelationName.Dim1_Polygon_Direction);
                if (brDirection != null)
                {
                    var oldOffsetPl = (RDF_Polygon3D)brDirection.RelatedTo;
                    widthPl = (RDF_Polygon3D)oldOffsetPl.WidthFrom;
                }

                var startPoint = widthPl.Segments.First().StartNode.GetPoint3D();
                var endPoint = widthPl.Segments.Last().EndNode.GetPoint3D();
                var matrix = widthPl.Transform.GetMatrix3D();
                startPoint = matrix.Transform(startPoint);
                endPoint = matrix.Transform(endPoint);
                //var rotation = GeomLib.BasicGeomLib.GetAxisRotation3D(Library.CurSketch.HitNormal);
                //RotateTransform3D transform3D = new RotateTransform3D(rotation);
                Transform3D transform3D = Library.CurSketch.HitPlane.Transform;
                Matrix3D mtx = transform3D.Value;
                var stoffset = new Vector3D();//startPoint - Library.CurSketch.HitPlanePoints3D.First();
                mtx.Invert();
                double width = startPoint.distanceTo(endPoint) / 2;

                ClipperLib.ClipperOffset coffset = new ClipperLib.ClipperOffset();
                var itpts = pivotPts == null ? Library.CurSketch.HitPlanePoints3D.ToIntPoint(mtx, 1000) :
                    pivotPts.ToIntPoint(Matrix3D.Identity, 1000);
                List<ClipperLib.IntPoint> reverse = new List<ClipperLib.IntPoint>(itpts);
                reverse.Reverse();
                itpts.InsertRange(0, reverse);


                List<List<ClipperLib.IntPoint>> result = new List<List<ClipperLib.IntPoint>>();
                coffset.AddPath(itpts, ClipperLib.JoinType.jtMiter, ClipperLib.EndType.etClosedLine);
                coffset.Execute(ref result, width * 1000);
                //System.Diagnostics.Debug.WriteLine("offset pts count: " + result.Count);
                if (result.First() != null && result.First().Count > 3)
                {
                    var offsetPt3D = result.First().ToPoint3D(0.001);

                    //System.Diagnostics.Debug.WriteLine(offsetPt3D.First().X + ", " + offsetPt3D.First().Y + ", " + offsetPt3D.First().Z);

                    offsetPt3D.Add(offsetPt3D.First());

                    DrawSketchPath(transform3D, offsetPt3D, widthPl.GUID);

                    for (int i=0;i<offsetPt3D.Count; i++)//var pt in offsetPt3D)
                    {
                        Point3D newpt = transform3D.Transform(offsetPt3D[i]);
                        offsetPt3D[i] = newpt;
                    }

                    return offsetPt3D;
                    #region render the lines through traditional way
                    //RDF_Polygon3D rdfPL = new RDF_Polygon3D();
                    //var stpt = offsetPt3D.CreateRDFSegments(rdfPL);

                    //Matrix3D mtx3D = Library.Cursor3D.CHZAxis.Transform.Value.Clone();
                    //mtx3D.OffsetX = stpt.X;
                    //mtx3D.OffsetY = stpt.Y;
                    //mtx3D.OffsetZ = stpt.Z;

                    //RDF_Matrix mtx = new RDF_Matrix(mtx3D);

                    //RDF_Transformation rdftrans = new RDF_Transformation();
                    //rdftrans.SetTransformGeom(rdfPL);
                    //rdftrans.SetMatrix(mtx);

                    //RDF_Dim1.RenderConceptLine(rdftrans, Library.ViewPort3D);

                    //rdfPL.Select();
                    #endregion
                }
            }
            return null;
        }

        public static void UpdateWorkingPath(Matrix3D mtx, List<Point3D> offsetPt3D, string guid)
        {
            MatrixTransform3D transform = new MatrixTransform3D(mtx);
            DrawSketchPath(transform, offsetPt3D, guid);
        }

        public static void DrawSketchPath(Transform3D transform, List<Point3D> offsetPt3D, string working_guid = "")
        {
            PathSegment3DCollection pscollection = new PathSegment3DCollection();
            PathFigure3D pf = new PathFigure3D();
            pf.Segments = pscollection;
            pf.StartPoint = offsetPt3D.First();// +stoffset;
            foreach (var pt in offsetPt3D)
            {
                LineSegment3D segment = new LineSegment3D();
                segment.Point = pt;// +stoffset;
                pscollection.Add(segment);
            }

            PathFigure3DCollection pfcollection = new PathFigure3DCollection();
            pfcollection.Add(pf);

            PathGeometry3D pathdata = new PathGeometry3D();
            pathdata.Figures = pfcollection;

            if (Workingpaths.ContainsKey(working_guid))
            {
                Library.ViewPort3D.Children.Remove(Workingpaths[working_guid]);
                Workingpaths.Remove(working_guid);
            }

            WirePath Workingpath = new WirePath();
            Workingpath.Rounding = 0;
            Workingpath.Color = Colors.Gray;// isVirtual ? Color.FromArgb(50, 200, 200, 200) : Colors.Gray;
            Workingpath.Thickness = 2;
            Workingpath.Opacity = working_guid == "" ? (byte)200 : (byte)255;
            Workingpath.Data = pathdata;
            Workingpath.Transform = transform;
            if (working_guid != "")
                Workingpaths.Add(working_guid, Workingpath);
            else
                SketchPaths.Add(Workingpath);
            Library.ViewPort3D.Children.Insert(0, Workingpath);
        }

        public static void CreateSweptSolidCommand(Point3D? origin = null, bool isStraight = true)
        {
            Point3D hitloc = Library.CurSketch.HitPoints.First();
            Matrix3D mtxextrusion = Library.Cursor3D.CHZAxis.Transform.Value;
            // get the local coordinate system
            mtxextrusion.Invert();
            Point3D curLoc = mtxextrusion.Transform(hitloc);
            origin = origin == null ? Origin : origin.Value;
            mtxextrusion.Invert();

            firstZ = Math.Round(curLoc.Z, 4);
            curLoc = new Point3D(origin.Value.X, origin.Value.Y, Math.Round(curLoc.Z, 4) - firstZ);

            RDF_Face2D area = (RDF_Face2D)GlobalVariables.SelectedItem;
            RDF_Polygon3D outer = area.Outer;
            Point3D startPt = outer.StartPt.GetPoint3D();
            RDF_Line3D dirPath = new RDF_Line3D(origin.Value, curLoc);
            if (isStraight)
            {
                // need to transform the points in order to fix 
                // the flipping problem when the direction is negative
                SweptSolid.SetDirection(dirPath);
            }
            else
            {
                RDF_Polygon3D dirPolygon = new RDF_Polygon3D();
                SweptSolid.SetDirection(dirPolygon);
            }
            SweptSolid.SetSweptProfile(outer);
            AddRelatedSolid(area, SweptSolid);

            if (Library.CurSketch.HitPlaneOnObject != null && Library.CurSketch.HitPlaneOnObject.ShapeSemantics == "wall")
            {
                SweptSolid.ShapeSemantics = "slab";
            }

            foreach (var inner in area.Inners)
            {
                //RDF_Polygon3D newInner = inner.CreateInnerPolygon();
                SweptSolid.AddOpening(inner);
            }
            RDF_Matrix flippingMtx = new RDF_Matrix(Matrix3D.Identity);
            RDF_MatrixMultiplication sweptSolidMtx = new RDF_MatrixMultiplication(area.Transform.Matrix, flippingMtx);
            
            RDF_Transformation translate = new RDF_Transformation();
            translate.SetMatrix(sweptSolidMtx);
            translate.SetTransformGeom(SweptSolid);

            GeometricConstraint cst = area.Constraint;
            if (cst.ConstraintName != GeometricConstraintEnum.Array && area.InstantiatedFrom != null)
            {
                if (area.InstantiatedFrom.Constraint.ConstraintName == GeometricConstraintEnum.Array)
                {
                    cst = area.InstantiatedFrom.Constraint;
                }
            }
            #region in array
            if (cst.ConstraintName == GeometricConstraintEnum.Array)
            {
                // 1. get all the constraint objects
                foreach (var clonedArea in cst.ConstraintObjects)
                {
                    // 2. get the transform from the constraint object
                    var clonedTransform = ((RDF_Face2D)clonedArea).Transform;
                    var clonedMtx = clonedTransform.Matrix;

                    // 3. create the cloned swept solid
                    RDF_SweptAreaSolid clonedSweptSolid = (RDF_SweptAreaSolid)SweptSolid.Clone();
                    clonedSweptSolid.IsInArray = true;
                    // 4. apply the transform to the cloned swept solid
                    RDF_Transformation clonedTranslate = new RDF_Transformation();
                    clonedTranslate.SetMatrix(clonedMtx);
                    clonedTranslate.SetTransformGeom(clonedSweptSolid);

                    AddRelatedSolid((RDF_Face2D)clonedArea, clonedSweptSolid);
                }
            }
            #endregion
        }

        public static void AddRelatedSolid(RDF_Face2D area, RDF_SweptAreaSolid solid)
        {
            BackRelation br = new BackRelation(area);
            br.RelatedTo = solid;
            br.Name = BackRelationName.Dim2_RelatedSolid;
            area.BackRelations.Add(br);
        }

        public static void ProcessExtrusionCommand()
        {
            Point3D hitloc = Library.CurSketch.HitPoints.First();
            Matrix3D mtxextrusion = Library.Cursor3D.CHZAxis.Transform.Value;
            // get the local coordinate system
            mtxextrusion.Invert();
            Point3D curLoc = mtxextrusion.Transform(hitloc);
            mtxextrusion.Invert();

            double curZ = Math.Round(curLoc.Z, 4) - firstZ;
            curLoc = new Point3D(Origin.X, Origin.Y, Math.Abs(curZ));
            if (Math.Round(curLoc.Z, 4) - firstZ < 0)
            {
                //System.Diagnostics.Debug.WriteLine(curLoc.Z);
            }
            //System.Diagnostics.Debug.WriteLine("extrude!!");
            var dir = SweptSolid.Direction;
            RDF_Line3D dirPath;
            if (dir is RDF_Line3D)
                dirPath = (RDF_Line3D)dir;
            else
                dirPath = (RDF_Line3D)((RDF_Transformation)dir).TransformGeom;
            dirPath.ChangePoint(curLoc, 1);

            // flip => not working good
            if (curZ < 0)
            {
                //flipSweptSolid(SweptSolid, curZ);
                var sweptProfile = SweptSolid.SweptProfile;
                foreach (var bkSweptSolid in sweptProfile.BackRelations)
                {
                    if (bkSweptSolid.RelatedTo is RDF_SweptAreaSolid)
                    {
                        flipSweptSolid((RDF_SweptAreaSolid)bkSweptSolid.RelatedTo, curZ);
                    }
                }
            }
            preZ = curZ;
        }

        private static void flipSweptSolid(RDF_SweptAreaSolid swptSolid, double curZ)
        {
            var swMtxAbstract = swptSolid.Transform.Matrix;
            if (swMtxAbstract is RDF_MatrixMultiplication)
            {
                var swMtx = (RDF_MatrixMultiplication)swMtxAbstract;
                var fstMtx3D = swMtx.FirstMatrix.Matrix3D.Clone();
                fstMtx3D.OffsetX = 0;
                fstMtx3D.OffsetY = 0;
                fstMtx3D.OffsetZ = 0;

                var newTraslate = new Point3D(0, 0, curZ);
                newTraslate = fstMtx3D.Transform(newTraslate);

                ((RDF_Matrix)swMtx.SecondMatrix).SetTranslate(newTraslate);
            }
            else if (swMtxAbstract is RDF_Matrix)
            {
                var swMtx = (RDF_Matrix)swMtxAbstract;
                var fstMtx3D = swMtx.Matrix3D.Clone();
                fstMtx3D.OffsetX = 0;
                fstMtx3D.OffsetY = 0;
                fstMtx3D.OffsetZ = 0;
                var newTraslate = new Point3D(0, 0, curZ - preZ);
                newTraslate = fstMtx3D.Transform(newTraslate);
                newTraslate = new Point3D(swMtx.Matrix3D.OffsetX + newTraslate.X, swMtx.Matrix3D.OffsetY + newTraslate.Y, swMtx.Matrix3D.OffsetZ + newTraslate.Z);
                ((RDF_Matrix)swMtx).SetTranslate(newTraslate);
            }
        }

        public static RDF_GeometricItem ProcessArrayCommand(GeometricConstraint cst, List<Point3D> overwritingPts = null)
        {
            List<Point3D> usingPts = overwritingPts != null && overwritingPts.Count > 1 ?
                overwritingPts : Library.CurSketch.HitPlanePoints3D;
            if (cst.Subject != null && usingPts.Count > 0)
            {
                if (cst.ConstraintName == GeometricConstraintEnum.None)
                {
                    curArrayInterval = GlobalConstant.ArrayInterval;
                    if (cst.Subject is RDF_GeometricItem)
                    {
                        var oldsubject = (RDF_GeometricItem)cst.Subject;
                        var newsubject = oldsubject.Clone(true);
                        double? interval = cst.Interval;
                        cst = newsubject.Constraint;
                        cst.Interval = interval;
                        curArrayInterval = interval == null ? curArrayInterval : (int)interval.Value;
                        var translateTo = usingPts[0] - cst.ArrayOffset;
                        RDF_MatrixAbstract mtxabstract = ((RDF_GeometricItem)newsubject).Transform.Matrix;
                        mtxabstract.MoveRDFGeoms(translateTo, Library.TransformFace2D);

                        if (newsubject is RDF_Face2D)
                        {
                            CustomStroke newstroke = Library.NewStrokeFromPolygon(((RDF_Face2D)oldsubject).Outer,
                                ((RDF_Face2D)newsubject).Outer, ((RDF_Face2D)cst.Subject).Outer.pathgeom, false);
                            newstroke.FitintoCurves();
                            newstroke.Primitives.CreateRDFSegments(((RDF_Face2D)newsubject).Outer);
                            //GlobalVariables.CanvasStrokes.Add(newstroke);
                        }

                        renderClonedRDFGeom(newsubject);
                        newsubject.Select();
                    }
                    cst.ConstraintName = GeometricConstraintEnum.Array;
                    //InsertNewEllementInArray(cst);
                    //cst.ArrayOffset = usingPts.Last() - ((RDF_GeometricItem)cst.Subject).Transform.Matrix.Translate;
                }
                else if (cst.ConstraintName != GeometricConstraintEnum.Array)
                {
                    cst.Reset();
                    //cst.ArrayOffset = usingPts.Last() - ((RDF_GeometricItem)cst.Subject).Transform.Matrix.Translate;
                }
                int numInArray = cst.ConstraintObjects.Count;
                double distance = Library.CurSketch.CurrentDrawingDistance;
                int tarNumInArray = (int)distance / curArrayInterval + 2;

                System.Diagnostics.Debug.WriteLine(tarNumInArray + ", " + numInArray + ", " + distance);

                // insert a new element in array if necessary
                if (tarNumInArray > numInArray)
                {
                    System.Diagnostics.Debug.WriteLine("insert!!");
                    InsertNewEllementInArray(cst);
                }

                ReallocateConstraintObjects(cst, distance, tarNumInArray, usingPts);

                if (cst.Subject is RDF_GeometricItem)
                    return (RDF_GeometricItem)cst.Subject;
            }
            return null;
        }

        public static void InsertNewEllementInArray(GeometricConstraint cst)
        {
            var newObj = ((RDF_GeometricItem)cst.Subject).Clone();
            newObj.IsInArray = true;
            cst.ConstraintObjects.Insert(0, newObj);
            renderClonedRDFGeom(newObj);
        }

        private static void renderClonedRDFGeom(RDF_GeometricItem newObj)
        {
            Step pstep = new Step();
            pstep.ObjectType = StepObjectTypeEnum.MeshModel;
            if (newObj is RDF_Face2D)
                ((RDF_Face2D)newObj).RenderShapeMesh(pstep, Library.SolidModelGroup);
            else if (newObj is RDF_SweptAreaSolid)
                ((RDF_SweptAreaSolid)newObj).RenderConceptMesh(Library.SolidModelGroup);
            else if (newObj is RDF_Boolean3D)
                ((RDF_Boolean3D)newObj).RenderConceptMesh(Library.SolidModelGroup);
        }

        public static void RemoveElementInArray(GeometricConstraint cst, RDF_GeometricItem removingItem)
        {
            cst.ConstraintObjects.Remove(removingItem);
            removingItem.Remove(false, false);
        }

        private static void ReallocateConstraintObjects(GeometricConstraint cst, double distance, int tarNumInArray, List<Point3D> pathpts)
        {
            var ptI = 1;
            double segDist = distance / tarNumInArray;
            double accuDist = 0;
            int j = 0;
            foreach (var cloneElem in cst.ConstraintObjects)
            {
                //if (j == 0)
                //{
                //    var translateTo = pathpts[0] - cst.ArrayOffset;
                //    RDF_MatrixAbstract mtxabstract = ((RDF_GeometricItem)cloneElem).Transform.Matrix;
                //    mtxabstract.MoveRDFGeoms(translateTo, Library.TransformFace2D);
                //    j++;
                //    continue;
                //}
                for (; ptI < pathpts.Count; ptI++)
                {
                    int prePtI = ptI - 1;
                    var prePt = pathpts[prePtI];
                    var curPt = pathpts[ptI];
                    accuDist += curPt.distanceTo(prePt);

                    if (accuDist >= segDist || ptI == pathpts.Count - 1)
                    {
                        accuDist = 0;
                        // move the elements to the right location
                        var translateTo = pathpts[ptI] - cst.ArrayOffset;
                        RDF_MatrixAbstract mtxabstract = ((RDF_GeometricItem)cloneElem).Transform.Matrix;
                        mtxabstract.MoveRDFGeoms(translateTo, Library.TransformFace2D);
                        break;
                    }
                }

                j++;
            }

        }
        /// <summary>
        /// extrude all the items that related to it, such as the array of profile shape
        /// extruding all the profile shapes
        /// </summary>
        /// <param name="originSolid"></param>
        /// <param name="group"></param>
        public static void RenderConnectedSweptSolid(this RDF_SweptAreaSolid originSolid, Model3DGroup group)
        {
            // find the face2D
            RDF_Polygon3D profile = originSolid.SweptProfile;
            RDF_Face2D face = profile.Face;

            // two routes to get to the connected swept solid
            // 1. find the swept solid in the back relation list
            // 2. find the constraint object, and its swept solid from the matrix

            #region Method 1
            foreach (var brObj in profile.BackRelations)
            {
                if (brObj.RelatedTo is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid clonedSolid = (RDF_SweptAreaSolid)brObj.RelatedTo;
                    clonedSolid.RenderConceptMesh(group);
                }
            }
            #endregion


            #region Method 2

            //foreach (var clonedArea in face.Constraint.ConstraintObjects)
            //{
            //    if (clonedArea is RDF_Face2D)
            //    {
            //        var relatedTransforms = ((RDF_Face2D)clonedArea).Transform.Matrix.RelatedTransforms;


            //    }
            //}
            #endregion
        }

        private static void ProcessClipperPolygon(List<Point3D> newpolygon)
        {
            // decide which point is closet to stpt, and reorder it for geometry generation
            double distance = double.MaxValue;
            int j = 0;
            for (int i = 0; i < newpolygon.Count; i++)
            {
                int prei = i == 0 ? newpolygon.Count - 1 : i - 1;
                var curpt = newpolygon[i];
                var prept = newpolygon[prei];
                var mid = new Point3D((curpt.X + prept.X) / 2, (curpt.Y + prept.Y) / 2, (curpt.Z + prept.Z) / 2);
                var curdist = mid.distanceTo(new Point3D());
                if (curdist < distance)
                {
                    distance = curdist;
                    j = i;
                }
            }
            var pushtoend = newpolygon.GetRange(0, j);
            newpolygon.RemoveRange(0, j);
            if (pushtoend.Count > 0)
            {
                newpolygon.AddRange(pushtoend);
            }
            newpolygon.Insert(0, new Point3D());
            newpolygon.Add(new Point3D());
        }

        private static RDF_Polygon3D CreateInnerProfile(CustomStroke newStroke, ref Matrix3D oriMtx, RDF_GeometricItem parentObj)
        {
            RDF_Polygon3D innerPL = new RDF_Polygon3D();

            Matrix3D solvedMtx = parentObj.Transform.GetMatrix3D().Solve(oriMtx);
            var strokeMtx3D = newStroke.matrix.Clone();
            strokeMtx3D.OffsetX = 0;
            strokeMtx3D.OffsetY = 0;
            strokeMtx3D.OffsetZ = 0;
            Point3D translateStroke = strokeMtx3D.Transform(new Point3D(solvedMtx.OffsetX, solvedMtx.OffsetY, solvedMtx.OffsetZ));

            newStroke.TranslateStroke(translateStroke);
            newStroke.Primitives.CreateRDFSegments(innerPL, true);
            newStroke.GUID = innerPL.GUID;

            RDF_Transformation innerTransform = new RDF_Transformation();
            innerTransform.SetTransformGeom(innerPL);
            innerTransform.SetMatrix(parentObj.Transform.Matrix);

            RDF_Dim1.RenderConceptLine(innerPL.Transform, Library.ViewPort3D, true);
            return innerPL;
        }

        internal static List<RDF_Polygon3D> CreateOffsetFromDirection(Matrix3D matrix, RDF_Polygon3D dirpolygon, RDF_Polygon3D widthPl)
        {
            var stpt = widthPl.pathgeom.Paths.First().First();// selpl.Segments.First().StartNode.GetPoint3D();
            var endpt = widthPl.pathgeom.Paths.First().Last();// selpl.Segments.Last().EndNode.GetPoint3D();
            double width = stpt.distanceTo(endpt) / 2;

            List<RDF_Polygon3D> returnedPls = new List<RDF_Polygon3D>();

            if (dirpolygon.pathgeom != null)
            {
                var points = dirpolygon.pathgeom.Paths.First();
                Matrix3D mtx = matrix.Clone();//Matrix3D.Identity;
                mtx.OffsetX = points[0].X;
                mtx.OffsetY = points[0].Y;
                mtx.OffsetZ = points[0].Z;
                mtx.Invert();
                //var stoffset = new Vector3D(-points[0].X, -points[0].Y, -points[0].Z);
                ClipperLib.ClipperOffset coffset = new ClipperLib.ClipperOffset();
                var itpts = points.ToIntPoint(Matrix3D.Identity, 1000);

                List<List<ClipperLib.IntPoint>> result = new List<List<ClipperLib.IntPoint>>();
                ClipperLib.EndType endtype = dirpolygon.IsClosed ? ClipperLib.EndType.etOpenSquare : ClipperLib.EndType.etOpenButt;
                coffset.AddPath(itpts, ClipperLib.JoinType.jtMiter, endtype);
                coffset.Execute(ref result, width * 1000);
                if (result.Count > 0 && result.First().Count > 3)
                {
                    var newpolygon = result.First().ToPoint3D(0.001);

                    ProcessClipperPolygon(newpolygon);

                    RDF_Polygon3D newrdfpolygon = new RDF_Polygon3D();
                    newrdfpolygon.IsVirtual = true;
                    newrdfpolygon.SetDirectionPolygon(dirpolygon);
                    if (GlobalVariables.SelectedItem is RDF_Dim1)
                        newrdfpolygon.SetWidthLine(widthPl);
                    newpolygon.CreateRDFSegments(newrdfpolygon);

                    RDF_Matrix newplmtx = new RDF_Matrix(matrix);
                    RDF_Transformation newpltransform = new RDF_Transformation();
                    newpltransform.SetMatrix(newplmtx);
                    newpltransform.SetTransformGeom(newrdfpolygon);

                    RDF_Dim1.RenderConceptLine(newpltransform, Library.ViewPort3D);

                    returnedPls.Add(newrdfpolygon);
                }
            }
            return returnedPls;
        }
       
        internal static RDF_Polygon3D ExecuteAddDimension(CustomStroke newStroke, RDF_Polygon3D polygon, RDF_Transformation rdfTran, RDF_Polygon3D widthPl = null)
        {
            foreach(var wpath in Workingpaths.Values)
                Library.ViewPort3D.Children.Remove(wpath);
            Workingpaths.Clear();
            if (GlobalVariables.SelectedItem is RDF_Polygon3D || widthPl != null)
            {
                bool isWidthPlNull = widthPl == null;
                widthPl = widthPl == null ? (RDF_Polygon3D)GlobalVariables.SelectedItem : widthPl;
                if (widthPl.pathgeom != null)
                {
                    var startPoint = widthPl.pathgeom.Paths.First().First();
                    var endPoint = widthPl.pathgeom.Paths.First().Last();
                    var newMtx3D = widthPl.Transform.GetMatrix3D().Clone();
                    //if (isWidthPlNull)
                    //{
                    //    newMtx3D.OffsetX += (startPoint.X + endPoint.X) / 2;
                    //    newMtx3D.OffsetY += (startPoint.Y + endPoint.Y) / 2;
                    //    newMtx3D.OffsetZ += (startPoint.Z + endPoint.Z) / 2;
                    //}
                    //else
                    //{
                        var tmpmtx = rdfTran.GetMatrix3D();
                        newMtx3D.OffsetX = tmpmtx.OffsetX;
                        newMtx3D.OffsetY = tmpmtx.OffsetY;
                        newMtx3D.OffsetZ = tmpmtx.OffsetZ;
                    //}

                    RDF_Matrix newmtx = new RDF_Matrix(newMtx3D);
                    // move the drawing polygon location to the selected polygon location
                    // and use the selected polygon matrix for drawing polygon
                    rdfTran.SetMatrix(newmtx);
                    ExecuteSketchLine(newStroke, newMtx3D, polygon, rdfTran, false);
                    var newOffsetPls = CreateOffsetFromDirection(newMtx3D, polygon, widthPl);

                    if (newOffsetPls.Count > 0)
                    {
                        var newoffsetPl = newOffsetPls.First();
                        Step pstep = StepLibrary.addPstepLine(newoffsetPl.Transform.TransformGeom);
                        RDF_Face2D face = new RDF_Face2D(newoffsetPl);

                        if (newOffsetPls.Count > 1)
                        {
                            var newinnerPl = newOffsetPls[1];
                            face.AddInnerPolygon(newinnerPl);
                        }

                        pstep.ObjectType = pstep.ObjectType | StepObjectTypeEnum.MeshModel;
                        face.RenderShapeMesh(pstep, Library.SolidModelGroup);
                        //face.Select();
                        polygon.Select();

                        if (Library.ReplaceFrom != null && Library.WorkingPrototype != null)
                        {
                            Operation_Relation protOper = new Operation_Relation(OperationName.OffsetToFace2D);
                            protOper.AddInputRelation(widthPl.BackRelations.Last());
                            bool hasPivotLine = false;

                            if (Library.ReplaceFrom == Library.WorkingPrototype.PrincipleLine)
                            {
                                PrincipleRelation pr = new PrincipleRelation(polygon.BackRelations.Last());
                                protOper.AddInputRelation(pr);
                                hasPivotLine = true;
                            }
                            else
                            {
                                foreach (var relation in Library.WorkingPrototype.Operations)
                                {
                                    if (Library.ReplaceFrom == relation.RecordedOutputProduct)
                                    {
                                        ClassRelation cr = new ClassRelation(relation.InputRelations.Last());
                                        cr.IsConnectingRelation = true;
                                        protOper.AddInputRelation(cr);
                                        hasPivotLine = true;
                                    }
                                }
                            }

                            if (hasPivotLine)
                            {
                                protOper.SetRecordedOutPut(polygon.BackRelations.Last().RelatedTo);
                                Library.WorkingPrototype.AddOperation(protOper);
                            }
                        }

                        return newoffsetPl;
                    }
                }
            }
            return null;
        }

        internal static void ExecuteAddHole(CustomStroke newStroke, object hitobj, Matrix3D oriMtx)
        {
            RDF_Face2D hitface = (RDF_Face2D)hitobj;
            if (hitface.Geometry != null)
                Library.SolidModelGroup.Children.Remove(((RDF_Face2D)hitobj).Geometry);
            RDF_Polygon3D innerPL = CreateInnerProfile(newStroke, ref oriMtx, hitface);
            Step pstep = StepLibrary.addPstepLine(innerPL);

            hitface.AddInnerPolygon(innerPL);
            hitface.RenderShapeMesh(pstep, Library.SolidModelGroup);
        }

        internal static void ExecuteSketchLine(CustomStroke newStroke, Matrix3D mtx, RDF_Polygon3D polygon,
            RDF_Transformation rdfTran, bool creatingFace = true, RDF_GeometricItem overridingItem = null)
        {
            WirePath line = null;
            bool isInArray = false;
            if (GlobalVariables.SelectedItem != null || overridingItem != null)
            {
                if (GlobalVariables.CurrentMode == Mode.Array || overridingItem != null)
                {
                    var origin = overridingItem == null ? GlobalVariables.SelectedItem : overridingItem;

                    System.Diagnostics.Debug.WriteLine("Create relationship for the array");
                    var constraint = origin.Constraint;
                    constraint.ControllingObject = polygon;
                    polygon.Controlling.Add(constraint);

                    // move the polygon to the shape center of the constraint objects
                    var oritranslate = rdfTran.Matrix.Translate;
                    Point3D translation = origin.Transform.Matrix.Translate;
                    //if (origin is RDF_Face2D)
                    //{
                    //    translation = ((RDF_Face2D)origin).Outer.Centroid;
                    //    translation = ((RDF_Face2D)origin).Outer.Transform.GetMatrix3D().Transform(translation);
                    //}
                    //if (overridingItem == null)
                    //{   
                    //    rdfTran.Matrix.translateRDFMatrix(translation);
                    //    constraint.ArrayOffset = constraint.ArrayOffset - (oritranslate - translation);
                    //}

                    line = RDF_Dim1.RenderConceptLine(rdfTran, Library.ViewPort3D);
                    
                    // reallocate the shapes
                    if (polygon.pathgeom != null)
                    {
                        var path = Library.OffsetPathPoints(constraint, polygon.pathgeom);
                        Library.MoveConstraintObjects(path, constraint, isRecording: GlobalVariables.IsRecordingPrototype);
                        isInArray = true;

                        if (GlobalVariables.IsRecordingPrototype)
                        {
                            Library.ViewPort3D.Children.Remove(line);
                            ConstraintRelation cstRel = new ConstraintRelation(constraint, (RDF_GeometricItem)constraint.Subject);
                            var distance = polygon.StartPt.GetPoint3D().distanceTo(polygon.EndPt.GetPoint3D());
                            if (Math.Round(distance, 0) == 50) // check if the controlling line is the pivot
                            {
                                cstRel.IsPivotPrinciple = true;
                                var ori = ((RDF_GeometricItem)((GeometricConstraint)cstRel.RelationConstraint).Subject).Transform.GetMatrix3D();
                                //var x = ((GeometricConstraint)cstRel.RelationConstraint).ArrayOffset.X + oritranslate.X
                                // = 
                            }
                            Operation_Relation opr = new Operation_Relation(OperationName.MakeArray);
                            opr.AddInputRelation(cstRel);
                            Library.WorkingPrototype.AddOperation(opr);
                        }
                    }
                }

                GlobalVariables.SelectedItem.UnSelect();
            }
            if (!isInArray)
                line = RDF_Dim1.RenderConceptLine(rdfTran, Library.ViewPort3D);

            if (line != null)
            {
                // trasform the points to XY plane so that we can set the 
                // new points in the stroke
                newStroke.UpdateCvtPoints(line, mtx, polygon.IsClosed);
            }
            // Todo: Check whether this is the right implementation!!
            Step pstep = StepLibrary.addPstepLine(rdfTran.TransformGeom);
            if (creatingFace && polygon.IsClosed && line != null && !(GlobalVariables.CurrentMode == Mode.Array || GlobalVariables.CurrentMode == Mode.Extrusion || GlobalVariables.CurrentMode == Mode.SweptSolid))
            {
                RDF_Face2D face = new RDF_Face2D(polygon);
                pstep.ObjectType = pstep.ObjectType | StepObjectTypeEnum.MeshModel;
                face.RenderShapeMesh(pstep, Library.SolidModelGroup);
                face.Select();
                Events_Gesture.gesture_OnObjectSelected(face);
            }
            else
            {
                polygon.Select();
            }
        }

        internal static void ExecuteSweptSolid(CustomStroke newStroke, Matrix3D mtx, RDF_Polygon3D polygon, RDF_Transformation rdfTran, RDF_MatrixAbstract rdfMatrix)
        {
            // move the swepting line to the origin
            rdfMatrix.translateRDFMatrix(CommandLibrary.SweptSolid.SweptProfile.Transform.Matrix.Translate);

            var sweptSolidMtx3D = mtx.Clone();
            //sweptSolidMtx3D.Invert();
            sweptSolidMtx3D.OffsetX = 0;
            sweptSolidMtx3D.OffsetY = 0;
            sweptSolidMtx3D.OffsetZ = 0;
            RDF_Matrix sweptSolidMtx = new RDF_Matrix(sweptSolidMtx3D);
            RDF_Transformation sweptSolidTransformation = new RDF_Transformation();
            sweptSolidTransformation.SetMatrix(sweptSolidMtx);
            sweptSolidTransformation.SetTransformGeom(polygon);

            CommandLibrary.SweptSolid.SetDirection(sweptSolidTransformation);
            CommandLibrary.SweptSolid.RenderConceptMesh(Library.SolidModelGroup);
            CommandLibrary.ExecuteSketchLine(newStroke, mtx, polygon, rdfTran);

            //RDF_Dim1.RenderConceptLine(rdfTran, viewport);

            Step pstep = new Step();
            pstep.ObjectType = StepObjectTypeEnum.MeshModel;
            pstep.PreModel = CommandLibrary.SweptSolid;
            pstep.PStep = StepTypeEnum.Add;
            Utility.UIUtility.PSteps.Add(pstep);
        }

    }
}
