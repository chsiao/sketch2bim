﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidSketch
{
    public enum ButtonEnum
    {
        Extrusion,
        Array,
        SweptSolid,
        Prototyping
    }
    public enum MovingAxis
    {
        xaxis,
        yaxis,
        zaxis,
        xyplane,
        yzplane,
        xzplane,
        none
    }
}
