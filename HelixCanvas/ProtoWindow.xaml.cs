﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Ink;

namespace SolidSketch
{
    /// <summary>
    /// Interaction logic for ProtoWindow.xaml
    /// </summary>
    public partial class ProtoWindow
    {
        public string ProtoSemantic { get; private set; }
        public ProtoWindow()
        {
            InitializeComponent();
        }

        public InkCanvas SketchCanvas { get { return this.inkCanv; } }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtProtoName_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.ProtoSemantic = ((TextBox)sender).Text;
        }
    }
}
