﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Ink;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using System.Xml.Serialization;
using Microsoft.Win32;

using Utility;
using CanvasSketch;
using WrapperLibrary;
using WrapperLibrary.RDF;
using Petzold.Media3D;
using GeomLib;
using GeomLib.Constraint;
using HelixToolkit.Wpf;
using SolidSketch.Operation;
using IFC_Factory;

using Newtonsoft.Json;

namespace SolidSketch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private delegate void executeParametricCommand(Point newpt);

        public MainWindow()
        {
            GlobalConstant.SetUpGlobalConstant();
            GlobalVariables.InitializeGlobalVariables();
			
            InitializeComponent();
            UIUtility.PSteps = new List<Step>();
            UIUtility.NSteps = new List<Step>();
            Library.Prototypes = new List<Prototype>();
            this.SizeChanged += MainWindow_SizeChanged;
            Stylus.SetIsPressAndHoldEnabled(this.maingrid, false);
            
            //this.Loaded += MainWindow_Loaded;
            this.viewport.Loaded += viewport_Loaded;
            this.Closing += MainWindow_Closing;
            this.KeyDown += MainWindow_KeyDown;
            this.inkCanv.BeforeStrokeGenerated += Events_SketchCanvas.inkCanv_BeforeStrokeGenerated;
            this.inkCanv.OnStrokeGenerated += Events_SketchCanvas.inkCanv_OnStrokeGenerated;
            this.inkCanv.OnStrokeModified += Events_SketchCanvas.inkCanv_OnStrokeModified;
            this.inkCanv.customRenderer.AfterDrawn += customRenderer_AfterDrawn;
            this.inkCanv.OnFinalGestureGenerated += Events_SketchCanvas.inkCanv_OnGestureGenerated;

            Touch.FrameReported += Touch_FrameReported;

            RDFWrapper.FilePtr = RDFWrapper.LoadFile(@"base.ttl");
            this.viewport.ViewCubeWidth = 150;
            this.viewport.ViewCubeHeight = 150;

            file = new StreamWriter(@"test_SS.csv");
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(file != null)
                file.Close();
        }

        #region window event
        void viewport_Loaded(object sender, RoutedEventArgs e)
        {
            this.curSketch = new HitSketchHelper(viewport, this.inkCanv);
            this.cursor = new Cursor3D(this.viewport.Viewport, this.gplans);

            CommandLibrary.SetDefaultCommand(true);
            Library.InitializeCanvasComponents(this.viewport.Viewport, this.inkCanv, this.cursor, 
                this.group1, this.gplans, this.curSketch, this.gridplane);

            //RDF_ExtrudedPolygon testpl = new RDF_ExtrudedPolygon();
            //List<Point> profile = new List<Point>(){new Point(0,0), new Point(1,1.45269), new Point(1,2), new Point(0,0.54731)};
            //testpl.SetProfile(profile);
            //testpl.SetExtrusionLength(5);
            //testpl.RenderConceptMesh(Library.SolidModelGroup);
        }
        void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.viewport.Height = this.ActualHeight-40;
        }
        #endregion
        
        #region Touch Event
        private void stopViewNavigation()
        {
            this.viewport.IsRotationEnabled = false;
            this.viewport.IsPanEnabled = false;
            this.viewport.IsMoveEnabled = false;
            this.viewport.IsZoomEnabled = false;
        }
        private void startViewNavigation()
        {
            this.viewport.IsRotationEnabled = true;
            this.viewport.IsPanEnabled = true;
            this.viewport.IsMoveEnabled = true;
            this.viewport.IsZoomEnabled = true;
        }
        protected override void OnTouchDown(TouchEventArgs e)
        {
            //this.gridplane.Visible = true;
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetTouchPoint(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", TouchDown, {0}, {1:0000}, {2:0000}", e.TouchDevice.Id, toFormPos.Position.X, toFormPos.Position.Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            base.OnTouchDown(e);
            TouchPoint touchposition = e.GetTouchPoint(viewport);

            HitResultTypeEnum resulttype = this.curSketch.Touch(touchposition.Position);
            //this.touchcount++;
            if (this.curSketch.HitUtilities.Count > 0)
            {
                #region hit Utility
                var hitutil = this.curSketch.HitUtilities.First();
                if (hitutil.ID == "cxaxis" || hitutil.ID == "cyaxis" || hitutil.ID == "czaxis")
                {
                    //System.Diagnostics.Debug.WriteLine("hit utility");
                    GlobalVariables.CurrentMode = Mode.MoveShape | Mode.MoveShapeAxis;
                    Library.SetFirstTouchTown(touchposition.TouchDevice.Id);
                    if (hitutil.ID == "czaxis")
                        Library.rotateHitPlaneTowardCameraOnZ();
                    switch (hitutil.ID)
                    {
                        case "cxaxis":
                            cursor.CXAxis.Color = Colors.Pink;
                            cursor.CXAxis.Thickness = 6;
                            cursor.CXAxis.Point2 = new Point3D(30, 0, 0);
                            Library.TouchOn = MovingAxis.xaxis;
                            break;
                        case "cyaxis":
                            cursor.CYAxis.Color = Colors.LightGreen;
                            cursor.CYAxis.Thickness = 6;
                            cursor.CYAxis.Point2 = new Point3D(0, 30, 0);
                            Library.TouchOn = MovingAxis.yaxis;
                            break;
                        case "czaxis":
                            cursor.CZAxis.Color = Colors.LightBlue;
                            cursor.CZAxis.Thickness = 6;
                            cursor.CZAxis.Point2 = new Point3D(0, 0, 30);
                            Library.TouchOn = MovingAxis.zaxis;
                            break;
                    }
                    // try to hit again to hit on the plane
                    this.curSketch.Touch(touchposition.Position);
                    this.stopViewNavigation();

                    if (this.curSketch.CurHitGeoms.Count > 0)
                    {
                        var rdfobj = this.curSketch.CurHitGeoms.First().GetValue(RDFExtension.RDFObjectProperty);
                        if (rdfobj is RDF_GeometricItem)
                        {
                            var rdfGeom = (RDF_GeometricItem)rdfobj;
                            if (!GlobalVariables.CurrentTouchObjects.ContainsValue(rdfGeom))
                                GlobalVariables.CurrentTouchObjects.Add(e.TouchDevice.Id, 
                                    new Tuple<Point, RDF_GeometricItem>(touchposition.Position, rdfGeom));
                        }
                    }
                }
                #endregion
            }
            else if (resulttype == HitResultTypeEnum.HitGeometry)
            {
                GlobalVariables.CurrentMode = Mode.MoveShape;
                Library.TouchOn = MovingAxis.xyplane;
                var rdfobj = this.curSketch.CurHitGeoms.First().GetValue(RDFExtension.RDFObjectProperty);
                if (rdfobj is RDF_GeometricItem)
                {
                    var rdfGeom = (RDF_GeometricItem)rdfobj;
                    if (!GlobalVariables.CurrentTouchObjects.ContainsValue(rdfGeom))
                    {
                        System.Diagnostics.Debug.WriteLine("1: " + e.TouchDevice.Id);
                        GlobalVariables.CurrentTouchObjects.Add(e.TouchDevice.Id, 
                            new Tuple<Point, RDF_GeometricItem>(touchposition.Position, rdfGeom));
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("2: " + e.TouchDevice.Id);
                    }


                    if (GlobalVariables.SelectedItem != rdfGeom && rdfGeom.HasTransform)
                    {
                        cursor.CreateCursor();
                        rdfGeom.Select();
                        var mtx3D = rdfGeom.Transform.GetMatrix3D();
                        MatrixTransform3D mtxtransform3D = new MatrixTransform3D(mtx3D);
                        cursor.SetTransform(mtxtransform3D);
                    }
                    if (GlobalVariables.SelectedItem!= null && GlobalVariables.SelectedItem.Transform != null)
                    {
                        RDF_MatrixAbstract mtxabstract = GlobalVariables.SelectedItem.Transform.Matrix;
                        this.oriShapeOffset = new Point3D(mtxabstract.Matrix3D.OffsetX, mtxabstract.Matrix3D.OffsetY, mtxabstract.Matrix3D.OffsetZ);
                    }
                    else
                        this.oriShapeOffset = new Point3D();
                }
                this.stopViewNavigation();
            }
            else if(Library.TouchCount < 2)
            {
                GlobalVariables.SelectedItem.UnSelect();
                cursor.DestroyCursor();
                var hitedges = curSketch.FindAreaHitWires(touchposition.Position);
                if (hitedges.Item1.Count > 0)
                {
                    var fstEdge = hitedges.Item1.First();
                    if (fstEdge.ID != null && RDFWrapper.All_RDF_Objects.ContainsKey(fstEdge.ID))
                    {
                        RDF_Abstract touchObj = RDFWrapper.All_RDF_Objects[fstEdge.ID];
                        if (touchObj is RDF_Transformation)
                        {
                            GlobalVariables.CurrentMode = Mode.MoveShape;
                            this.stopViewNavigation();
                            RDF_Dim1 rdf_path = (RDF_Dim1)((RDF_Transformation)touchObj).TransformGeom;
                            rdf_path.Select();

                            if (!GlobalVariables.CurrentTouchObjects.ContainsValue(rdf_path))
                                GlobalVariables.CurrentTouchObjects.Add(e.TouchDevice.Id,
                                    new Tuple<Point, RDF_GeometricItem>(touchposition.Position, rdf_path));
                        }
                    }
                }
                else
                    this.startViewNavigation();
            }
        }
        protected override void OnTouchMove(TouchEventArgs e)
        {
            base.OnTouchMove(e);
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetTouchPoint(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", TouchMove, {0}, {1:0000}, {2:0000}", e.TouchDevice.Id, toFormPos.Position.X, toFormPos.Position.Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            // move the objects that has been selected
            // by offseting the matrix
            //System.Diagnostics.Debug.WriteLine("move: " + e.TouchDevice.Id);
            Library.MoveShape(e);
        }
        protected override void OnTouchUp(TouchEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetTouchPoint(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", TouchUp, {0}, {1:0000}, {2:0000}", e.TouchDevice.Id, toFormPos.Position.X, toFormPos.Position.Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            base.OnTouchUp(e);
            preTouchUp = DateTime.Now.Ticks;
            GlobalVariables.CurrentTouchObjects.Remove(e.TouchDevice.Id);
            Library.excludeDeviceID = null;
            cursor.setDefaultContextAxis();
            //this.touchcount--;
            if ((GlobalVariables.CurrentMode & Mode.MoveShape) != 0)
            {
                // update the path
                foreach(var path in CommandLibrary.Workingpaths.Values)
                    UpdatePath(path);
                CommandLibrary.Workingpaths.Clear();

                Library.setToDefault(false);
                Library.MoveShape(e);

                if (this.oriShapeOffset.X != double.MaxValue)
                {
                    Step pstep = new Step();
                    pstep.ObjectType = StepObjectTypeEnum.Line | StepObjectTypeEnum.MeshModel;
                    pstep.PStep = StepTypeEnum.ObjectTransform;
                    pstep.ObjectTranslation = this.oriShapeOffset;
                    pstep.PreModel = GlobalVariables.SelectedItem;
                    UIUtility.PSteps.Add(pstep);

                    //System.Diagnostics.Debug.WriteLine("object transform");
                    UIUtility.NSteps = new List<Step>();
                }

                //if (GlobalVariables.SelectedItem != null)
                //{
                //    GlobalVariables.PreSelectedItem = GlobalVariables.SelectedItem; 
                //    GlobalVariables.SelectedItem.UnSelect();
                //}
            }
            hasSwitched = false;
            this.startViewNavigation();
        }
        bool hasSwitched = false;
        private void UpdatePath(WirePath wpath)
        {
            var curStroke = (CustomStroke)GlobalVariables.CanvasStrokes.Last();
            curStroke.UpdateCvtPoints(line: wpath, isInserting: true);
            if (RDFWrapper.All_RDF_Objects.ContainsKey(curStroke.GUID))
            {
                var modifyingObj = RDFWrapper.All_RDF_Objects[curStroke.GUID];
                if (modifyingObj is RDF_Polygon3D)
                {
                    curStroke.FitintoCurves();
                    curStroke.Primitives.CreateRDFSegments((RDF_Polygon3D)modifyingObj);

                    var modifyingPolygon = (RDF_Polygon3D)modifyingObj;
                    if (modifyingPolygon.pathgeom != null)
                        viewport.Children.Remove(modifyingPolygon.pathgeom);
                    RDF_Dim1.ConnectWirePathAndRDFobj(modifyingPolygon.Transform, wpath);

                    if (GlobalVariables.IsRecordingPrototype)
                    {
                        Operation_Relation protOper = new Operation_Relation(OperationName.OffsetLine);
                        BackRelation br = modifyingPolygon.BackRelations.Last();
                        if (Library.WorkingPrototype.PrincipleLine == br.RelatedTo)
                        {
                            PrincipleRelation prinR = new PrincipleRelation(br);
                            protOper.AddInputRelation(prinR);
                            protOper.SetRecordedOutPut(br.Relating);
                        }
                        else
                        {
                            ClassRelation conR = new ClassRelation(br);
                            conR.IsConnectingRelation = true;
                            protOper.AddInputRelation(conR);
                            //protOper.SetRecordedOutPut();
                        }
                        Library.WorkingPrototype.AddOperation(protOper);
                    }
                    modifyingPolygon.Select();
                }
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            TouchPointCollection tpts = e.GetTouchPoints(this);
            Library.TouchCount = tpts.Count;
            if (tpts.Count == 2)
            {
                Library.RotateShape(tpts);
                var touchedObjs = GlobalVariables.CurrentTouchObjects;
                if (touchedObjs.Count == 2 && !hasSwitched)
                {
                    var newfstPos = tpts.First().Position;
                    var lstfstPos = touchedObjs.First().Value.Item1;
                    var offset = viewport.TranslatePoint(new Point(0,0), this);
                    lstfstPos = new Point(lstfstPos.X + offset.X, lstfstPos.Y + offset.Y);
                    var dist = lstfstPos.distanceTo(newfstPos);
                    if (dist > 5)
                    {
                        var kvp1 = GlobalVariables.CurrentTouchObjects.First();
                        var kvp2 = GlobalVariables.CurrentTouchObjects.Last();
                        GlobalVariables.CurrentTouchObjects.Clear();

                        GlobalVariables.CurrentTouchObjects.Add(kvp2.Key, new Tuple<Point, RDF_GeometricItem>(kvp2.Value.Item1, kvp1.Value.Item2));
                        GlobalVariables.CurrentTouchObjects.Add(kvp1.Key, new Tuple<Point, RDF_GeometricItem>(kvp1.Value.Item1, kvp2.Value.Item2));
                        hasSwitched = true;
                        //System.Diagnostics.Debug.WriteLine("has switched: " + dist);
                    }
                }
            }
        }
        #endregion

        #region Stylus Event
        protected override void OnStylusButtonDown(StylusButtonEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetStylusPoints(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", StylusButtonDown, {0:0000}, {1:0000}, " + e.StylusButton.Name, toFormPos.Last().X, toFormPos.Last().Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception)
            {

            }
            if (e.StylusButton.Name.ToLower().Contains("barrel"))
            {
                this.isBarrelBtDown = true;
            }
            else
            {
                if (e.StylusButton.Name.ToLower().Contains("tip") && this.isBarrelBtDown
                    && (GlobalVariables.SelectedItem != null || btstates == ButtonEnum.Prototyping))
                {
                    //GlobalVariables.PreSelectedItem.Select();
                    switch (btstates)
                    {
                        case ButtonEnum.Extrusion:
                            if (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_Polygon3D || GlobalVariables.SelectedItem is RDF_SweptAreaSolid)
                                GlobalVariables.CurrentMode = Mode.Extrusion;
                            break;
                        case ButtonEnum.Array:
                            GlobalVariables.CurrentMode = Mode.Array;
                            break;
                        case ButtonEnum.Prototyping:
                            GlobalVariables.IsPrototyping = true;
                            break;
                    }

                    if (GlobalVariables.CurrentMode == Mode.Array || GlobalVariables.CurrentMode == Mode.Extrusion)
                    {
                        Step pstep = new Step();
                        pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                        pstep.PStep = StepTypeEnum.Add;
                        UIUtility.PSteps.Add(pstep);
                        UIUtility.NSteps = new List<Step>();
                    }
                }
            }
            base.OnStylusButtonDown(e);
        }
        protected override void OnStylusButtonUp(StylusButtonEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetStylusPoints(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", StylusButtonUp, {0:0000}, {1:0000}, " + e.StylusButton.Name, toFormPos.Last().X, toFormPos.Last().Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (e.StylusButton.Name.ToLower().Contains("barrel"))
            {
                //GlobalVariables.CurrentMode = Mode.Standard;
            }
            this.isBarrelBtDown = false;
            base.OnStylusButtonUp(e);
        }
        protected override void OnStylusInRange(StylusEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetStylusPoints(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", StylusInRange, {0:0000}, {1:0000}", toFormPos.Last().X, toFormPos.Last().Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (e.StylusDevice.TabletDevice.Name == "ISD-V4" || (e.StylusDevice.TabletDevice.Name == "HIDI2C Device" && e.StylusDevice.TabletDevice.Type != TabletDeviceType.Touch))
            {
                //System.Diagnostics.Debug.WriteLine("tip switch" + e.StylusDevice.StylusButtons[0].StylusButtonState);
                //System.Diagnostics.Debug.WriteLine("barrel" + e.StylusDevice.StylusButtons[1].StylusButtonState);

                // when stylus is in range, it will cause a touch up event fired
                long diffTime = DateTime.Now.Ticks - preTouchUp;
                if (diffTime < 500000)
                {
                    if (GlobalVariables.PreSelectedItem is RDF_Face2D || GlobalVariables.PreSelectedItem is RDF_Polygon3D)
                    {
                        GlobalVariables.PreSelectedItem.Select();
                        GlobalVariables.CurrentMode = Mode.Extrusion;

                        Step pstep = new Step();
                        pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                        pstep.PStep = StepTypeEnum.Add;
                        UIUtility.PSteps.Add(pstep);
                        UIUtility.NSteps = new List<Step>();
                    }
                    else if (GlobalVariables.PreSelectedItem == null)
                    {
                        //GlobalVariables.CurrentMode = Mode.UndoRedo;

                    }
                }

                base.OnStylusInRange(e);
               // this.chkAllowSketch.IsChecked = true;
                this.inkCanv.IsHitTestVisible = true;
                Utility.UIUtility.IsStylusInRange = true;
            }
        }
        protected override void OnStylusOutOfRange(StylusEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetStylusPoints(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", StylusOutRange, {0:0000}, {1:0000}", toFormPos.Last().X, toFormPos.Last().Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (e.StylusDevice.TabletDevice.Name == "ISD-V4" || (e.StylusDevice.TabletDevice.Name == "HIDI2C Device" && e.StylusDevice.TabletDevice.Type != TabletDeviceType.Touch))
            {
                base.OnStylusOutOfRange(e);
                //this.chkAllowSketch.IsChecked = false;
                this.inkCanv.IsHitTestVisible = false;
                Utility.UIUtility.IsStylusInRange = false;
            }
        }
        protected override void OnStylusInAirMove(StylusEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetStylusPoints(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", StylusInAirMove, {0:0000}, {1:0000}", toFormPos.Last().X, toFormPos.Last().Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            base.OnStylusInAirMove(e);

            if (stylusInAirCount % Utility.GlobalConstant.DrawFrequency == 0)
            {
                if (Utility.UIUtility.IsStylusInRange && Utility.GlobalVariables.CurrentMode == Mode.Standard)
                {
                    Point pos = e.GetPosition(viewport);
                    //Dispatcher.BeginInvoke(new executeParametricCommand(moveCursor), new object[] { pos });
                    Events_SketchCanvas.moveCursor(pos);
                }
                stylusInAirCount = 0;
            }

            stylusInAirCount++;
        }
        protected override void OnStylusMove(StylusEventArgs e)
        {
            try
            {
                if (file != null)
                {
                    lock (file)
                    {
                        var toFormPos = e.GetStylusPoints(this);
                        string newline = string.Format(System.DateTime.Now.Ticks + ", StylusMove, {0:0000}, {1:0000}", toFormPos.Last().X, toFormPos.Last().Y);
                        file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            base.OnStylusMove(e);
        }
        #endregion

        void customRenderer_AfterDrawn(Point newpt)
        {
            Dispatcher.BeginInvoke(new executeParametricCommand(Events_SketchCanvas.newSketchPtReceived), 
                new object[] { newpt });
        }
        
        #region general UI buttons
        private void CursorMove(object sender, MouseEventArgs e)
        {
            if (Utility.GlobalVariables.CurrentMode == Mode.Standard)
            {
                Point mouseposition = e.GetPosition(viewport);
                // cause some jaggy problem in profile
                Events_SketchCanvas.moveCursor(mouseposition);
            }
        }
        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rdoMode = (RadioButton)sender;
            Library.SelectedPrototype = null;
            if (GlobalVariables.IsPrototyping)
            {
                btstates = ButtonEnum.Prototyping;
            }
            else
            {
                bool isFound = false;
                if(sender == rbtExtrusion)
                    btstates = ButtonEnum.Extrusion;
                else if(sender == rbtArray)
                    btstates = ButtonEnum.Array;
                else
                {
                    foreach (var proto in Library.Prototypes)
                    {
                        if (proto.AssociateControl == sender)
                        {
                            Library.SelectedPrototype = proto;
                            GlobalVariables.IsPrototyping = true;
                            btstates = ButtonEnum.Prototyping;
                        }
                    }
                }
            }
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsChecked.HasValue && ((CheckBox)sender).IsChecked.Value)
            {
                btstates = ButtonEnum.Extrusion;
                rbtExtrusion.IsChecked = true;
                canvasBorder.BorderBrush = Brushes.Red;
                GlobalVariables.IsRecordingPrototype = true;
                Library.WorkingPrototype = new Prototype();
                Library.Prototypes.Add(Library.WorkingPrototype);
            }
            else
            {
                GlobalVariables.IsRecordingPrototype = false;
                canvasBorder.BorderBrush = null;
                ((CheckBox)sender).IsChecked = false;

                // popup a sketch panel for creating the new button
                ProtoWindow winProto = new ProtoWindow();
                winProto.Owner = this;
                winProto.ShowDialog();

                RenderTargetBitmap rtb = new RenderTargetBitmap((int)winProto.SketchCanvas.ActualWidth,
                                   (int)winProto.SketchCanvas.ActualHeight, 0, 0, PixelFormats.Default);
                rtb.Render(winProto.SketchCanvas);

                Image im = new Image();
                im.Source = rtb;
                im.Margin = new Thickness(0, 0, 0, 0);
                im.Height = rbtExtrusion.Height;
                im.Width = rbtExtrusion.Width;

                StackPanel st = new StackPanel();
                st.Children.Add(im);

                RadioButton newbt = new RadioButton();
                newbt.Style = rbtExtrusion.Style;
                newbt.Height = rbtExtrusion.Height;
                newbt.Width = rbtExtrusion.Width;
                newbt.HorizontalAlignment = rbtExtrusion.HorizontalAlignment;
                newbt.Margin = rbtExtrusion.Margin;
                newbt.VerticalAlignment = rbtExtrusion.VerticalAlignment;
                newbt.Click += RadioButton_Click;
                newbt.Content = st;
                newbt.ContextMenuOpening += newbt_ContextMenuOpening;
                // need to link the button with the prototype

                btPanel.Children.Insert(2, newbt);

                Library.WorkingPrototype.AssociateControl = newbt;
                Library.WorkingPrototype.PrototypeSemantic = winProto.ProtoSemantic;
                Library.WorkingPrototype.CleanOnCanvasGeometry();
                Library.WorkingPrototype = null;
            }
        }

        void newbt_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ProtoTree treeview = new ProtoTree(Library.Prototypes.Last());
            treeview.Show();
            
            //throw new NotImplementedException();
        }
        private void Undo_MouseDown(object sender, TouchEventArgs e)
        {
            StepLibrary.moveBack();
            isUndoing = true;
            GlobalVariables.CurrentMode = Mode.UndoRedo;
        }
        private void Undo_MouseUp(object sender, TouchEventArgs e)
        {
            isUndoing = false;
            GlobalVariables.CurrentMode = Mode.Standard;
            Library.setToDefault();
        }
        private void Redo_MouseDown(object sender, TouchEventArgs e)
        {
            StepLibrary.moveForward();
            isRedoing = true;
            GlobalVariables.CurrentMode = Mode.UndoRedo;
        }
        private void Redo_MouseUp(object sender, TouchEventArgs e)
        {
            isRedoing = false;
            GlobalVariables.CurrentMode = Mode.Standard;
            Library.setToDefault();
        }
        private void Clean_TouchUp(object sender, TouchEventArgs e)
        {
            Library.setToDefault();
            if (WrapperLibrary.RDF.RDFWrapper.All_RDF_Objects.Count > 0)
            {
                var allrdfobjs = WrapperLibrary.RDF.RDFWrapper.All_RDF_Objects.Values.ToList<RDF_Abstract>();
                foreach (var rdf in allrdfobjs)
                {
                    if (rdf is RDF_Dim1)
                    {
                        this.viewport.Viewport.Children.Remove(((RDF_Dim1)rdf).PathGeometry);
                    }
                }
            }
            while (this.group1.Children.Count > 2)
            {
                this.group1.Children.RemoveAt(2);
            }
            foreach (var path in CommandLibrary.SketchPaths)
            {
                this.viewport.Children.Remove(path);
            }
            CommandLibrary.SketchPaths.Clear();
            WrapperLibrary.RDF.RDFWrapper.All_RDF_Objects.Clear();            
        }
        private void Export_Click(object sender, RoutedEventArgs e)
        {
            var d = new SaveFileDialog();
            d.Filter = Exporters.Filter;
            d.DefaultExt = ".obj";
            if (d.ShowDialog().Value)
            {
                string ext = System.IO.Path.GetExtension(d.FileName).ToLower();
                
                //viewport.Export(d.FileName);
                //Process.Start(System.IO.Path.GetDirectoryName(d.FileName));
                var exporter = new ObjExporter();// { TextureFolder = System.IO.Path.GetDirectoryName(d.FileName) };
                exporter.SwitchYZ = false;
                using (var stream = File.Create(d.FileName))
                {
                    //var allrdfobjs = RDFWrapper.All_RDF_Objects.Values;
                    //foreach (var rdfobj in allrdfobjs)
                    //{
                    //    if (rdfobj is RDF_SweptAreaSolid && ((RDF_GeometricItem)rdfobj).Geometry != null)// || rdfobj is RDF_Boolean3D)
                    //    {
                    //        exporter.Export(((RDF_GeometricItem)rdfobj).Geometry, stream);
                    //    }
                    //    else if (rdfobj is RDF_Boolean3D && ((RDF_Boolean3D)rdfobj).Geometry != null)
                    //    {
                    //        exporter.Export(((RDF_Boolean3D)rdfobj).Geometry, stream);
                    //    }
                    //}
                    exporter.Export(this.group1, stream);
                }
            }
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var d = new SaveFileDialog();
            d.Filter = "IFC Files | *.ifc";
            d.DefaultExt = ".ifc";
            if (d.ShowDialog().Value)
            {
                string ext = System.IO.Path.GetExtension(d.FileName).ToLower();
                if (ext == ".ifc")
                {
                    string name = System.IO.Path.GetFileName(d.FileName);
                    IfcGeneartor gen = new IfcGeneartor();
                    gen.Save(d.FileName);
                }
            }
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }    
        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            TranslateTransform3D translate = new TranslateTransform3D(0, 0, 0);
            Transform3DGroup transform = new Transform3DGroup();
            switch (e.Key)
            {
                case Key.S:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        var d = new SaveFileDialog();
                        d.Filter = Exporters.Filter;
                        d.DefaultExt = ".obj";
                        if (d.ShowDialog().Value)
                        {
                            string ext = System.IO.Path.GetExtension(d.FileName).ToLower();
                            //viewport.Export(d.FileName);
                            //Process.Start(System.IO.Path.GetDirectoryName(d.FileName));
                            var exporter = new ObjExporter();// { TextureFolder = System.IO.Path.GetDirectoryName(d.FileName) };
                            using (var stream = File.Create(d.FileName))
                            {
                                //var allrdfobjs = RDFWrapper.All_RDF_Objects.Values;
                                //foreach (var rdfobj in allrdfobjs)
                                //{
                                //    if (rdfobj is RDF_SweptAreaSolid && ((RDF_GeometricItem)rdfobj).Geometry != null)// || rdfobj is RDF_Boolean3D)
                                //    {
                                //        exporter.Export(((RDF_GeometricItem)rdfobj).Geometry, stream);
                                //    }
                                //    else if (rdfobj is RDF_Boolean3D && ((RDF_Boolean3D)rdfobj).Geometry != null)
                                //    {
                                //        exporter.Export(((RDF_Boolean3D)rdfobj).Geometry, stream);
                                //    }
                                //}
                                exporter.Export(this.group1, stream);
                            }
                            string ttlname = d.FileName.TrimEnd(ext.ToCharArray());
                            ttlname += ".ttl";
                            RDFWrapper.SaveAsFile(ttlname);
                            //var json = JsonConvert.SerializeObject(RDFWrapper.All_RDF_Objects.Values.First());
                        }
                    }
                    break;
                case Key.O:
                    break;
                case Key.X:
                    RotateTransform3D transformXY = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), 0));
                    transform.Children.Add(transformXY);
                    transform.Children.Add(translate);
                    this.curSketch.HitPlane.Transform = transform;
                    break;
                case Key.Y:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        StepLibrary.moveForward();
                    }
                    else
                    {
                        RotateTransform3D transformYZ = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), 90));
                        transform.Children.Add(transformYZ);
                        transform.Children.Add(translate);
                        this.curSketch.HitPlane.Transform = transform;
                    }
                    break;
                case Key.Z:
                    // Undo!!
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        StepLibrary.moveBack();
                    }
                    else
                    {
                        RotateTransform3D transformXZ = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), 90));
                        transform.Children.Add(transformXZ);
                        transform.Children.Add(translate);
                        this.curSketch.HitPlane.Transform = transform;
                    }
                    break;
                case Key.H:

                    break;
                case Key.G:
                    inkCanv.SaveLastStrokeAsGesture();
                    break;
                case Key.P:
                    //XmlAttributeOverrides attOverride = new XmlAttributeOverrides();
                    //XmlAttributes attrs = new XmlAttributes();
                    //attrs.XmlIgnore = true;


                    System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Operation_Relation));
                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//wall.xml";
                    System.IO.FileStream file = System.IO.File.Create(path);

                    if (Library.Prototypes.Count > 0)
                        writer.Serialize(file, Library.Prototypes.First().Operations.First());
                    break;
                case Key.E:

                    Library.SelectedPrototype.isSpecialOperation = true;

                    break;
            }
        }

        private void Sketch_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var path in CommandLibrary.SketchPaths)
            {
                this.viewport.Children.Insert(0, path);
            }
            GlobalVariables.IsSketchingOverlay = true;
        }

        private void Sketch_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var path in CommandLibrary.SketchPaths)
            {
                this.viewport.Children.Remove(path);
            }
            GlobalVariables.IsSketchingOverlay = false;
        }

        private void ShowSketch_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var path in CommandLibrary.SketchPaths)
            {
                this.viewport.Children.Insert(0, path);
            }
        }

        private void ShowSketch_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var path in CommandLibrary.SketchPaths)
            {
                this.viewport.Children.Remove(path);
            }
        }
        #endregion

        private HitSketchHelper curSketch;
        private Cursor3D cursor;
        private ButtonEnum btstates = ButtonEnum.Extrusion;
        
        private bool isBarrelBtDown;
        private long preTouchUp;
        private int stylusInAirCount;
        private bool isUndoing;
        private bool isRedoing;

        public Point3D oriShapeOffset { get; set; }
        public static System.IO.StreamWriter file = null;
    }
}