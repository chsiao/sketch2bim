﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Chipin Studio">
//   Copyright (c) 2014 Chih-Pin Hsiao
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Reflection;
using System.Windows;

[assembly: AssemblyTitle("Solid Sketch")]
[assembly: AssemblyDescription("")]

[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]