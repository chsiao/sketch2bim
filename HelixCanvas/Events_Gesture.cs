﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using WrapperLibrary;
using WrapperLibrary.RDF;
using CanvasSketch;
using GeomLib;
using Utility;

namespace SolidSketch
{
    public static class Events_Gesture
    {
        internal static void gesture_OnObjectUnSelected(RDF_Abstract selObj)
        {
            Library.Cursor3D.DestroyCursor();
            Library.isSweptSolid = false;
        }
        internal static void gesture_OnObjectSelected(RDF_Abstract selObj)
        {
            if (selObj != null)
            {
                Library.Cursor3D.DestroyCursor();
                Library.Cursor3D.CreateCursor();

                // set up the transformation
                MatrixTransform3D mtxTransform = new MatrixTransform3D(Matrix3D.Identity);
                // only deal with the face2D for now
                if (selObj is RDF_Face2D)
                {
                    RDF_Face2D selFace = (RDF_Face2D)selObj;
                    Point3D centroid = selFace.Outer.Centroid;
                    Matrix3D mtx = Matrix3D.Identity;
                    if (selFace.HasTransform)
                    {
                        Vector3D offsetCentroid = centroid - selFace.Outer.StartPt.GetPoint3D();
                        Point3D tmpcentroid = new Point3D(offsetCentroid.X, offsetCentroid.Y, offsetCentroid.Z);
                        mtx = selFace.Transform.GetMatrix3D();
                        centroid = mtx.Transform(tmpcentroid);
                    }
                    mtx.OffsetX = centroid.X;
                    mtx.OffsetY = centroid.Y;
                    mtx.OffsetZ = centroid.Z;
                    mtxTransform.Matrix = mtx;
                }

                Library.Cursor3D.SetTransform(mtxTransform);
                Library.Cursor3D.IsOnCentroid = true;
            }
        }
        internal static void gesture_CommandExecuted(object unsolvedObj, object solvedObj)
        {
            if (solvedObj is CustomStroke && unsolvedObj == null)
            {
                Matrix3D mtx = ((CustomStroke)solvedObj).matrix;

                Events_SketchCanvas.inkCanv_OnStrokeModified((CustomStroke)solvedObj, ref mtx, null);
            }
            else
            {
                if (unsolvedObj is CustomStroke && solvedObj is CustomStroke)
                {
                    //GlobalVariables.CanvasStrokes.Remove((CustomStroke)unsolvedObj);
                    //GlobalVariables.CanvasStrokes.Add((CustomStroke)solvedObj);
                    var updatingPl = RDFWrapper.All_RDF_Objects[((CustomStroke)solvedObj).GUID];
                    if (updatingPl is RDF_Polygon3D)
                    {
                        ((CustomStroke)solvedObj).UpdateCvtPoints((updatingPl as RDF_Polygon3D).pathgeom, ((CustomStroke)solvedObj).matrix);
                        ((CustomStroke)solvedObj).matrix.Invert();
                    }
                }

                Step newstep = new Step();
                newstep.PStep = StepTypeEnum.modifypath;
                newstep.ObjectType = StepObjectTypeEnum.Line;
                newstep.ModifiedObjs.Add(unsolvedObj);

                UIUtility.PSteps.Add(newstep);
                //if (unsolvedObj is RDF_Abstract)
                gesture_OnObjectUnSelected(null);

                if (solvedObj is RDF_Abstract)
                    gesture_OnObjectSelected((RDF_Abstract)solvedObj);
            }
        }

    }
}
