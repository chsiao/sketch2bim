﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SolidSketch.Operation;
using WrapperLibrary.RDF;

namespace SolidSketch
{
    /// <summary>
    /// Interaction logic for ProtoTree.xaml
    /// </summary>
    public partial class ProtoTree
    {
        private Prototype proto;

        public ProtoTree(Prototype proto)
        {
            this.proto = proto;
            InitializeComponent();
            this.Loaded += ProtoTree_Loaded;
            
        }

        void ProtoTree_Loaded(object sender, RoutedEventArgs e)
        {
            // find all the root operation relations
            var operations = this.proto.Operations.FindAll(opr => opr.PrincipleIndex >= 0);
            var cstOprs = this.proto.Operations.FindAll(opr => opr.Name == OperationName.MakeArray);
            foreach (var cstopr in cstOprs)
            {
                foreach (var br in cstopr.InputRelations)
                {
                    if (br is ConstraintRelation && ((ConstraintRelation)br).IsPivotPrinciple)
                        operations.Add(cstopr);
                }
            }
            //operations.AddRange();
            Stack<Operation_Relation> qOpr = new Stack<Operation_Relation>(operations);
            Dictionary<Operation_Relation, TreeViewItem> toBeAdded = new Dictionary<Operation_Relation, TreeViewItem>();
            while (qOpr.Count > 0) 
            {
                var curopr = qOpr.Pop();

                // North America 
                TreeViewItem treeItem = new TreeViewItem();
                treeItem.Header = "Operation: " + curopr.Name;
                treeItem.IsExpanded = true;

                treeItem.Items.Add(new TreeViewItem() { Header = "GUID: " + curopr.GUID });
                
                TreeViewItem inputItems = new TreeViewItem() { Header = "Inputs"};
                bool constraintFromPrinciple = false;
                foreach (var br in curopr.InputRelations)
                {
                    TreeViewItem subitem = new TreeViewItem();
                    subitem.Header = br.Name;
                    if(br is PrincipleRelation)
                        subitem.Items.Add(new TreeViewItem() { Header = "Relating: Sketch Line" });
                    else if (br is ClassRelation && ((ClassRelation)br).IsConnectingRelation)
                        subitem.Items.Add(new TreeViewItem() { Header = "Relating: Connected from Parent" });
                    else if (br is ConstraintRelation)
                    {
                        var cbr = (ConstraintRelation)br;
                        subitem.Header = "Array";

                        if (cbr.IsPivotPrinciple)
                            subitem.Items.Add(new TreeViewItem() { Header = "Controlling From: Sketch Line" });
                        else if(cbr.Relating != null)
                            subitem.Items.Add(new TreeViewItem() { Header = "Controlling From: " + cbr.Relating.GUID});
                        
                        if(cbr.RelationConstraint.Subject != null && cbr.RelationConstraint.Subject is WrapperLibrary.RDF.RDF_GeometricItem)
                        {
                            var subject = (WrapperLibrary.RDF.RDF_GeometricItem)cbr.RelationConstraint.Subject;
                            subitem.Items.Add(new TreeViewItem() { Header = "Array of: " + subject.GUID });
                        }
                    }
                    else
                        subitem.Items.Add(CreateRelatingTreeViewItem(br.Relating, "Relating"));
                    inputItems.Items.Add(subitem);
                    if (br is ConstraintRelation && ((ConstraintRelation)br).IsPivotPrinciple)
                        constraintFromPrinciple = true;
                }
                treeItem.Items.Add(inputItems);

                TreeViewItem nextItems = new TreeViewItem() { Header = "Connecting To" };
                foreach (var key in curopr.NextOperations.Keys)
                {
                    TreeViewItem subItem = new TreeViewItem() { Header = key.Name};
                    toBeAdded.Add(curopr.NextOperations[key], subItem);
                    nextItems.Items.Add(subItem);
                    qOpr.Push(curopr.NextOperations[key]);
                }
                treeItem.Items.Add(nextItems);

                if (curopr.PrincipleIndex >= 0 || constraintFromPrinciple) // one of the root node that connects to the pivot sketch line
                    this.treeView.Items.Add(treeItem);
                else if (toBeAdded.ContainsKey(curopr))
                {
                    // find a parent and add into the next items
                    var parentItem = toBeAdded[curopr];
                    parentItem.Items.Add(treeItem);
                }

                if (curopr.recordedOutput != null)
                {
                    TreeViewItem newtreeItem = new TreeViewItem();
                    treeItem.Items.Add(newtreeItem);
                    newtreeItem.Header = "Recorded Output: " + curopr.recordedOutput.ToString();
                    newtreeItem.Items.Add(new TreeViewItem() { Header = curopr.recordedOutput.GUID });
                    if (curopr.recordedOutput is RDF_Polygon3D)
                    {
                        newtreeItem.Items.Add(CreatePolygon3DTreeViewItem((RDF_GeometricItem)curopr.recordedOutput));
                    }
                }
            }
   
        }

        private TreeViewItem CreateRelatingTreeViewItem(RDF_GeometricItem relRDFGeom, string relName)
        {
            TreeViewItem newtreeItem = new TreeViewItem();
            newtreeItem.Header = relName + ": " + relRDFGeom.ToString();
            newtreeItem.Items.Add(new TreeViewItem() { Header = relRDFGeom.GUID });
            
            if (relRDFGeom is RDF_Polygon3D)
                newtreeItem.Items.Add(CreatePolygon3DTreeViewItem(relRDFGeom));
            else if (relRDFGeom is RDF_Line3D)
            {
                var relLine3D = (RDF_Line3D)relRDFGeom;
                newtreeItem.Items.Add(CreateLine3DTreeViewItem(relLine3D));
            }

            return newtreeItem;
        }

        private TreeViewItem CreatePolygon3DTreeViewItem(RDF_GeometricItem relRDFGeom)
        {
            var segments = ((RDF_Polygon3D)relRDFGeom).Segments;
            TreeViewItem segsitem = new TreeViewItem();
            segsitem.Header = "Segments";
            foreach (var seg in segments)
            {
                segsitem.Items.Add(CreateLine3DTreeViewItem(seg));
            }

            return segsitem;
        }

        private static TreeViewItem CreateLine3DTreeViewItem(RDF_Segment3D seg)
        {
            TreeViewItem segitem = new TreeViewItem();
            string stpt = seg.StartNode.GetPoint3D().ToString();
            string endpt = seg.EndNode.GetPoint3D().ToString();
            stpt = "(" + stpt + ")";
            endpt = "(" + endpt + ")";
            segitem.Header = stpt + " - " + endpt;
            return segitem;
        }
    }
}
