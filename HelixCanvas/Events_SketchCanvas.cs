﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Input;
using GeomLib;
using Utility;
using CanvasSketch;
using Petzold;
using Petzold.Media3D;
using WrapperLibrary.RDF;
using WrapperLibrary;
using SolidSketch.Operation;
using System.Windows.Shapes;

namespace SolidSketch
{
    public static class Events_SketchCanvas
    {
        private static int prestepIndex;
        public static bool isDiggedHole;
        public static long prePtTick = long.MaxValue;
        /// <summary>
        /// move the axes cursor to the location where we need
        /// Todo: may need to create a cursor class to do it
        /// </summary>
        /// <param name="pos"></param>
        internal static void moveCursor(Point pos)
        {
            Library.Cursor3D.UnHighlightAllElements();
            //this.Library.Cursor3D.DestroyCursor();

            // create hit point
            HitResultTypeEnum resulttype = Library.CurSketch.Hover(pos);
            if (resulttype != HitResultTypeEnum.None && (GlobalVariables.CurrentMode == Mode.Standard || GlobalVariables.CurrentMode == Mode.Extrusion))
            {
                if (Library.CurSketch.HitPoints.Count > 0)
                {
                    //tartranslate.OffsetX = Library.CurSketch.HitPoints.First().X;
                    //tartranslate.OffsetY = Library.CurSketch.HitPoints.First().Y;
                    //tartranslate.OffsetZ = Library.CurSketch.HitPoints.First().Z;
                    GlobalVariables.CursorPosition = new Point3D(Library.CurSketch.HitPoints.First().X,
                        Library.CurSketch.HitPoints.First().Y, Library.CurSketch.HitPoints.First().Z);
                }

                // I can filter out the hitgeoms here to match the one I want
                // System.Diagnostics.Debug.WriteLine("move cursor");
                if (!GlobalVariables.isSketching)
                    Library.CurSketch.EndSketch();
            }
        }
        internal static void DynamicGestureRecognition()
        {
            GestureCommand gm = new GestureCommand(Library.CurSketch.SketchPoints, Library.InkCanv.GestureRecognizer);

            // validate the erase gesture and also mark the deletion object if found
            // Erase is a strong type gesture. Once it is detected as erase gesture, it will be erase gesture until
            // pen up
            if (gm.Gesture == GestureEnums.Erase || GlobalVariables.IsErasing)
            {
                GlobalVariables.IsErasing = true;
                HashSet<WireBase> hitwires = Library.CurSketch.FindAreaHitWires().Item1;
                GlobalVariables.IsErasing = Library.CurSketch.FirstHitGeom != null || hitwires.Count > 0;
                GestureEnums curgesture = gm.Gesture;
                
                if (Library.CurSketch.FirstHitGeom != null && !isDiggedHole)  // mark the geometry for deletion if validated
                    curgesture = gm.processGesture(Library.CurSketch.FirstHitGeom, Library.ViewPort3D, Library.CurSketch.CurHitObjects);
                else if (hitwires.Count > 0)         // mark the wires for deletion if validated
                    gm.processGesture(Library.CurSketch.FirstHitGeom, Library.ViewPort3D, Library.CurSketch.CurHitObjects, hitwires);

                isDiggedHole = curgesture == GestureEnums.DigHole;
            }
        }
        internal static void inkCanv_OnGestureGenerated(GestureCommand gesture, GestureEnums gestureName)
        {
            try
            {
                if (MainWindow.file != null)
                {
                    lock (MainWindow.file)
                    {
                        string newline = string.Format(System.DateTime.Now.Ticks + ", GestureGenerated," + gesture.Gesture.ToString() + ", {0:0000}, {1:0000}", gesture.GestureLocation.X, gesture.GestureLocation.Y);
                        MainWindow.file.WriteLine(newline);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            var oriGestureName = gesture.Gesture;
            gesture.CommandExecuted += Events_Gesture.gesture_CommandExecuted;
            gesture.OnObjectSelected += Events_Gesture.gesture_OnObjectSelected;
            gesture.OnObjectUnSelected += Events_Gesture.gesture_OnObjectUnSelected;

            HashSet<WireBase> hitwires = Library.CurSketch.AreaHitWires;// Library.CurSketch.FindAreaHitWires().Item1;

            if (GlobalVariables.CurrentMode == Mode.Sketch)
                GlobalVariables.CurrentMode = Mode.Standard;
            // process gesture
            if (!(gesture.Gesture == GestureEnums.Erase && isDiggedHole) && (Library.CurSketch.FirstHitGeom != null || 
                Library.CurSketch.AreaHitWires.Count > 0 || Library.CurSketch.HitUtilPlanes.Count > 0))
            {
                gestureName = gesture.processGesture(Library.CurSketch.FirstHitGeom, Library.ViewPort3D, 
                    Library.CurSketch.CurHitObjects, hitwires, Library.CurSketch.HitUtilPlanes, 
                    Library.RotateHitPlanes, true, Library.CurSketch.HitPlanePoints3D);
            }
            else if (gestureName == GestureEnums.Point)
            {
                Library.Cursor3D.DestroyCursor();
                if (GlobalVariables.SelectedItem != null)
                    GlobalVariables.SelectedItem.UnSelect();
            }
            else
                gestureName = GestureEnums.None;
            // create the cursor at the gesture place
            if (gestureName == GestureEnums.None && oriGestureName == GestureEnums.Glue &&
                Library.CurSketch.HitPoints.Count > 0)// && Library.CurSketch.FirstHitGeom != null)
            {
                var rotation = GeomLib.BasicGeomLib.GetAxisRotation3D(Library.CurSketch.HitNormal);
                RDF_GeometricItem hitobj = null;
                if (Library.CurSketch.FirstHitGeom != null)
                {
                    var tmpHitObj = Library.CurSketch.FirstHitGeom.GetValue(RDFExtension.RDFObjectProperty);
                    hitobj = tmpHitObj is RDF_GeometricItem ? (RDF_GeometricItem)tmpHitObj: null;
                }
                RotateTransform3D rot3D = new RotateTransform3D(rotation);
                var mtx = rot3D.Value;
                mtx.OffsetX = Library.CurSketch.HitPoints.First().X;
                mtx.OffsetY = Library.CurSketch.HitPoints.First().Y;
                mtx.OffsetZ = Library.CurSketch.HitPoints.First().Z;
                Library.Cursor3D.CreateCursor(hitobj);
                Library.Cursor3D.SetMatrix3D(mtx);
            }

            // if it is in erasing mode, erase the item here
            if (!isDiggedHole && GlobalVariables.SelectedItem != null && (gestureName == GestureEnums.Erase || GlobalVariables.IsErasing) && gestureName != GestureEnums.Glue)
            {
                Step newstep = new Step();
                newstep.PStep = StepTypeEnum.Delete;
                newstep.PreModel = GlobalVariables.SelectedItem;
                UIUtility.PSteps.Add(newstep);
                // erasing here
                var selItem = GlobalVariables.SelectedItem;
                GlobalVariables.SelectedItem.Remove();
                selItem.UnSelect();
                Library.Cursor3D.DestroyCursor();
                GlobalVariables.IsErasing = false;
                // make sure that everything is erased
                CommandLibrary.SetDefaultCommand(true);
            }
            else if (gestureName == GestureEnums.Glue && hitwires.Count > 0 && GlobalVariables.SelectedItem is RDF_Polygon3D)
            {
                var gestureBound = Library.CurSketch.BoundingBox3D;
                List<RDF_Polygon3D> hitPolygons = new List<RDF_Polygon3D>();
                foreach (var wire in hitwires)
                {
                    if (RDFWrapper.All_RDF_Objects.ContainsKey(wire.ID))
                    {
                        var hitRdf = RDFWrapper.All_RDF_Objects[wire.ID];
                        if (hitRdf is RDF_Polygon3D)
                        {
                            if (!hitPolygons.Contains(hitRdf))
                                hitPolygons.Add((RDF_Polygon3D)hitRdf);
                        }
                        else if (hitRdf is RDF_Transformation)
                        {
                            var hitTransform = (RDF_Transformation)hitRdf;
                            if (hitTransform.TransformGeom is RDF_Polygon3D)
                                if (!hitPolygons.Contains(hitTransform.TransformGeom))
                                    hitPolygons.Add((RDF_Polygon3D)hitTransform.TransformGeom);
                        }
                    }
                }

                List<RDF_Node3D> mergineNodes = new List<RDF_Node3D>();

                foreach (var hitpl in hitPolygons) // usually only one or two polygons
                {
                    var plmtx = hitpl.Transform.GetMatrix3D();
                    foreach (var seg in hitpl.Segments)
                    {
                        var stpt = seg.StartNode.GetPoint3D();
                        stpt = plmtx.Transform(stpt);
                        if (gestureBound.Contains(stpt))
                        {
                            mergineNodes.Add(seg.StartNode);
                        }
                    }
                }
                //gestureBound.Contains()

            }

            Library.CurSketch.EndSketch();
        }
        
        /// <summary>
        /// Deal with the realtime rendering
        /// </summary>
        /// <param name="newpt"></param>
        internal static void newSketchPtReceived(Point newpt)
        {
            long curtick = DateTime.Now.Ticks;
            Point3D prept = Library.CurSketch.SketchPoints3D.Count > 0 ? Library.CurSketch.SketchPoints3D.Last() : new Point3D();

            // rotate the hit plane to the best orientation for extrusion
            if (GlobalVariables.CurrentMode == Mode.Extrusion)
            {
                if (!Library.isSweptSolid && (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_SweptAreaSolid))
                    Library.rotateHitPlaneTowardCameraOnZ();
            }
            else if (GlobalVariables.CurrentMode == Mode.UndoRedo && Library.CurSketch.SketchPoints.Count > 1)
            {
                var oriPoint = Library.CurSketch.SketchPoints.First();
                var curPoint = Library.CurSketch.SketchPoints.Last();
                int stepIndex = (int)((oriPoint.X - curPoint.X) / 20);
                //System.Diagnostics.Debug.WriteLine(oriPoint.X + "-" + curPoint.X);

                if (stepIndex > prestepIndex)
                    StepLibrary.moveBack();
                else if (stepIndex < prestepIndex)
                    StepLibrary.moveForward();

                prestepIndex = stepIndex;
            }
            else if (GlobalVariables.CurrentMode == Mode.Standard && GlobalVariables.CurrentMode != Mode.UndoRedo)
            {
                // the default mode when creating the new sketch
                GlobalVariables.CurrentMode = Mode.Sketch;
                System.Diagnostics.Debug.WriteLine("sketch mode");
            }
            else if (GlobalVariables.SelectedItem is RDF_Polygon3D)
            {
                bool modifyAddOneDimension = RecognizeModifyAddOneDimension();
            }

            HitResultTypeEnum hittype = Library.CurSketch.AddSketchPoint(newpt);
            //var hitwires = Library.CurSketch.FindAreaHitWires();

            //System.Diagnostics.Debug.WriteLine("before: " + hitwires.Item1.Count);
            //System.Diagnostics.Debug.WriteLine(curtick - prePtTick);
            if (Library.CurSketch.SketchPoints.Count % 5 == 0 && GlobalVariables.CurrentMode != Mode.Array && GlobalVariables.IsPrototyping
                && Library.CurSketch.SketchPoints.Count < 100 && GlobalVariables.CurrentMode != Mode.Extrusion
                && GlobalVariables.CurrentMode != Mode.SweptSolid && GlobalVariables.CurrentMode != Mode.AddOneDimension
                && GlobalVariables.CurrentMode != Mode.UndoRedo && curtick - prePtTick < GlobalConstant.DynamicGestureThreshold)
                DynamicGestureRecognition();


            if (!GlobalVariables.IsErasing)
            {
                if (GlobalVariables.IsPrototyping && Library.SelectedPrototype != null)
                    Library.SelectedPrototype.ProcessOperations();
                else if (GlobalVariables.CurrentMode == Mode.Extrusion && !Library.isSweptSolid 
                    && Library.CurSketch.HitPoints.Count > 0)
                    InitializeExtrusion(newpt);
                else if (GlobalVariables.CurrentMode == Mode.AddOneDimension 
                    || GlobalVariables.CurrentMode == Mode.ModifyAddOneDimension)
                    CommandLibrary.CreateOffsetShapeCommand();
                else if ((GlobalVariables.CurrentMode == Mode.SweptSolid || GlobalVariables.CurrentMode == Mode.Extrusion) 
                    && Library.isSweptSolid && (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_SweptAreaSolid))
                {
                    GlobalVariables.CurrentMode = Mode.SweptSolid;
                    CommandLibrary.ProcessSweptSolidCommand();
                }
                else if (GlobalVariables.CurrentMode == Mode.Array && Library.CurSketch.HitPoints.Count > 0
                    && (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_Boolean3D || GlobalVariables.SelectedItem is RDF_SweptAreaSolid))
                {
                    GeomLib.Constraint.GeometricConstraint cst = GlobalVariables.SelectedItem.Constraint;
                    CommandLibrary.ProcessArrayCommand(cst);
                }
            }
            prePtTick = curtick;
        }

        private static bool RecognizeModifyAddOneDimension()
        {
            var selPolygon = (RDF_Polygon3D)GlobalVariables.SelectedItem;
            var brDirection = selPolygon.BackRelations.Find(br => br.Name == BackRelationName.Dim1_Polygon_Direction);
            if (brDirection != null && brDirection.Relating is RDF_Polygon3D)
            {
                var stpt = ((RDF_Polygon3D)brDirection.Relating).StartPt.GetPoint3D();
                var endpt = ((RDF_Polygon3D)brDirection.Relating).EndPt.GetPoint3D();
                var tmpmtx = ((RDF_Polygon3D)brDirection.Relating).Transform.GetMatrix3D();
                stpt = tmpmtx.Transform(stpt);
                endpt = tmpmtx.Transform(endpt);
                if (Library.CurSketch.SketchPoints3D.Count > 0)
                {
                    var dist1 = Library.CurSketch.SketchPoints3D.First().distanceTo(stpt);
                    var dist2 = Library.CurSketch.SketchPoints3D.First().distanceTo(endpt);
                    if (dist1 < 3 || dist2 < 3)
                    {
                        GlobalVariables.CurrentMode = Mode.ModifyAddOneDimension;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// prepare the command executing process
        /// convert the hit point 3D to the 2D locat coordinate system so that the curve recognizer can process
        /// </summary>
        /// <param name="inputPts"></param>
        /// <param name="convertedPts"></param>
        /// <param name="mtx">the matrix for sketch plane</param>
        /// <param name="hitobj"></param>
        internal static void inkCanv_BeforeStrokeGenerated(List<StylusPoint> inputPts, List<Point> convertedPts, ref Matrix3D mtx, ref object hitobj, ref bool isOnSamePlane)
        {
            if (Library.CurSketch.LastResultType != HitResultTypeEnum.HitTwoAxes && Library.CurSketch.SketchPoints3D.Count > 0 && GlobalVariables.CurrentMode != Mode.UndoRedo)
            {
                System.Diagnostics.Debug.WriteLine("before stroke");
                // find first hit object
                if (Library.CurSketch.CurHitGeoms.Count > 0)
                {
                    var firsthitobj = Library.CurSketch.CurHitGeoms.First().GetValue(RDFExtension.RDFObjectProperty);
                    if (firsthitobj is RDF_GeometricItem)
                        hitobj = firsthitobj;
                }

                // reverse the order for its needs
                // if the drawing is inside a polygon, we assume it's trying to make a hole (inner polygon)
                double area = inputPts.PolygonArea();

                if (area > 0) inputPts.Reverse();
                // Decide what points to use for projecting the points onto the surface
                List<Point3D> usingPts = new List<Point3D>();
                // || will cause the shape editing on other shape doesn't work, 
                // && will cause the selected profile not able to be modified through transparent geometry
                if (GlobalVariables.CurrentMode == Mode.Array || GlobalVariables.IsPrototyping ||
                    ((GlobalVariables.SelectedItem is RDF_SweptAreaSolid || GlobalVariables.SelectedItem is RDF_Face2D) && area == 0))
                {
                    usingPts = new List<Point3D>(Library.CurSketch.HitPlanePoints3D);
                    mtx = Library.CurSketch.HitPlane.Transform.Value;
                }
                else
                {
                    usingPts = new List<Point3D>(Library.CurSketch.SketchPoints3D);

                    var rotation = GeomLib.BasicGeomLib.GetAxisRotation3D(Library.CurSketch.HitNormal);
                    RotateTransform3D rot3D = new RotateTransform3D(rotation);
                    mtx = rot3D.Value;
                }

                // Find out whether the sketch lines are hitting the existing wire many times
                var hitobject = Library.CurSketch.CurHitObjects.FindHitCounts();
                bool isreplaced = RecognizeReplacingWire(convertedPts, ref mtx, usingPts, hitobject);

                if (usingPts.Count > 0 && !isreplaced)
                {
                    Point3D first = usingPts[0];
                    // Get the orientation of the sketch lines based on its contexts
                    // Todo: it is not able to create a transformation from a single sketch line
                    // I will need to take the meshes orientation to get the orientation of sketch plane
                    bool isMerging = false;
                    if (area == 0 || usingPts.Count > Library.CurSketch.HitGeoms.Count)
                    {
                        if (GlobalVariables.SelectedItem != null && GlobalVariables.SelectedItem.Transform != null)
                        {
                            var testMtx = GlobalVariables.SelectedItem.Transform.Matrix.Matrix3D * mtx;
                            testMtx.OffsetX = 0;
                            testMtx.OffsetY = 0;
                            testMtx.OffsetZ = 0;
                            if (testMtx.IsAboutIdentity(1) && (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_Polygon3D))
                                isMerging = true;
                            else // test again with reversed mtx
                            {
                                var mtxclone = mtx.Clone();
                                mtxclone.Invert();
                                testMtx = GlobalVariables.SelectedItem.Transform.Matrix.Matrix3D * mtx;
                                testMtx.OffsetX = 0;
                                testMtx.OffsetY = 0;
                                testMtx.OffsetZ = 0;
                                testMtx.Invert();
                                if (testMtx.IsAboutIdentity(1) && (GlobalVariables.SelectedItem is RDF_Face2D || GlobalVariables.SelectedItem is RDF_Polygon3D))
                                    isMerging = true;
                            }
                        }
                    }
                    // not able to make it perfectly good. temporary do this first.
                    isOnSamePlane = true; // isMerging;

                    if (/*!isMerging*/true)
                    {
                        mtx.OffsetX = first.X;
                        mtx.OffsetY = first.Y;
                        mtx.OffsetZ = first.Z;
                    }
                    if (mtx.HasInverse)
                        mtx.Invert();

                    // can't translate the points to oring here since cornucopia will change the first point
                    // in the segmentation process
                    int i = 0;
                    foreach (Point3D pt in usingPts)
                    {
                        Point3D cvtPt = mtx.Transform(pt);
                        //if (i == 0) stpt = cvtPt;
                        //convertedPts.Add(new Point(cvtPt.X - stpt.X, cvtPt.Y - stpt.Y));
                        convertedPts.Add(new Point(cvtPt.X, cvtPt.Y));
                        i++;
                    }

                    double cvtArea = convertedPts.PolygonArea();
                    if (cvtArea < 0) convertedPts.Reverse();


                    if (GlobalVariables.IsSketchingOverlay)
                    {
                        MatrixTransform3D pathTransform = new MatrixTransform3D(Matrix3D.Identity);
                        CommandLibrary.DrawSketchPath(pathTransform, usingPts);
                    }
                }
                // ending the sketch and clear the variables
                Library.CurSketch.EndSketch();
            }
            else
            {
                Library.CurSketch.EndSketch();
                GlobalVariables.CurrentMode = Mode.Standard;
            }
            GlobalVariables.IsErasing = false;
            Petzold.Media3D.Utility.IsRenderingWireBase = true;
        }

        private static bool RecognizeReplacingWire(List<Point> convertedPts, ref Matrix3D mtx, List<Point3D> usingPts, Dictionary<DependencyObject, int> hitobject)
        {
            WirePath replacingWire = null;
            double percentage = 0;
            foreach (var keyvalue in hitobject)
            {
                var tmpreplacingObj = keyvalue.Key;
                if (tmpreplacingObj is WirePath && ((WirePath)tmpreplacingObj).ID != null)
                {
                    var tmpreplaceWire = (WirePath)tmpreplacingObj;
                    //var collideObj = RDFWrapper.All_RDF_Objects[tmpreplaceWire.ID];
                    double percent = (double)keyvalue.Value / (double)Library.CurSketch.CurHitObjects.HitCount;
                    if (percent > 0.7)
                    {
                        percentage = percent;
                        replacingWire = tmpreplaceWire;
                    }
                }
            }

            bool isreplaced = false;
            if (replacingWire != null && replacingWire.ID != null)
            {
                if (RDFWrapper.All_RDF_Objects.ContainsKey(replacingWire.ID))
                {
                    var collideObj = RDFWrapper.All_RDF_Objects[replacingWire.ID];
                    if (collideObj is RDF_Transformation)
                    {
                        var pl = (RDF_Polygon3D)((RDF_Transformation)collideObj).TransformGeom;
                        var selitem = GlobalVariables.SelectedItem;
                        if (!pl.IsClosedProfile || (pl.IsClosedProfile && GlobalVariables.SelectedItem != pl.Face))
                        {
                            var stroke = (CustomStroke)GlobalVariables.CanvasStrokes.FindCustomStrokeByID(pl.GUID);
                            if (stroke != null)
                            {
                                stroke.DrawnPts.ForEach(pt => convertedPts.Add(pt));
                                if (convertedPts.Count == 2)
                                {
                                    Point newpt = new Point((convertedPts.First().X + convertedPts.Last().X) / 2, (convertedPts.First().Y + convertedPts.Last().Y) / 2);
                                    convertedPts.Insert(1, newpt);
                                }
                                var tmpMtx = stroke.matrix.Clone();
                                mtx = tmpMtx;
                                tmpMtx.Invert();

                                var testFstPt = tmpMtx.Transform(new Point3D(convertedPts.First().X, convertedPts.First().Y, 0));
                                var testLstPt = tmpMtx.Transform(new Point3D(convertedPts.Last().X, convertedPts.Last().Y, 0));
                                var distFst = usingPts.First().distanceTo(testFstPt);
                                var distLst = usingPts.First().distanceTo(testLstPt);
                                if (distLst < distFst)
                                    convertedPts.Reverse();

                                isreplaced = true;
                                Library.ReplaceFrom = pl;
                            }
                        }
                    }
                }
            }
            return isreplaced;
        }
        /// <summary>
        /// Executing the commnad based on the sketch behavior we managed in the previous actions
        /// </summary>
        /// <param name="newStroke"></param>
        /// <param name="mtx"></param>
        /// <param name="hitobj"></param>
        internal static void inkCanv_OnStrokeGenerated(CustomStroke newStroke, ref Matrix3D mtx, object hitobj)
        {
            RDF_Polygon3D polygon = new RDF_Polygon3D(newStroke.GUID);
            if (GlobalVariables.CurrentMode == Mode.Extrusion) // || GlobalVariables.CurrentMode == Mode.SweptSolid GlobalVariables.CurrentMode == Mode.Array ||)
                polygon.IsVirtual = true;

            if (mtx.HasInverse)
                mtx.Invert();

            Matrix3D oriMtx = mtx.Clone();
            Point3D stpt = newStroke.Primitives.CreateRDFSegments(polygon);

            //if (GlobalVariables.CurrentMode == Mode.SweptSolid)
            //    stpt = new Point3D(0, 0, 0);

            #region Transformation matrix for moving the polygon back to the global location
            Point3D transformedStPt = mtx.Transform(stpt);
            mtx.OffsetX = transformedStPt.X;// stpt.X;
            mtx.OffsetY = transformedStPt.Y;
            mtx.OffsetZ = transformedStPt.Z;

            RDF_Transformation rdfTran = new RDF_Transformation();
            rdfTran.SetTransformGeom(polygon);
            RDF_MatrixAbstract rdfMatrix = null;

            if (Library.CurSketch.CurHitGeoms.Count > 0 && GlobalVariables.CurrentMode != Mode.SweptSolid)
            {
                RDF_Abstract hitrdf = (RDF_Abstract)Library.CurSketch.CurHitGeoms[0].GetValue(RDFExtension.RDFObjectProperty);
                if (hitrdf is RDF_SweptAreaSolid)
                {
                    RDF_SweptAreaSolid hitswept = (RDF_SweptAreaSolid)hitrdf;
                    if (hitswept.HasTransform)
                    {
                        RDF_Transformation hittransform = hitswept.Transform;
                        RDF_MatrixAbstract rdf_hitmtx = (RDF_MatrixAbstract)hittransform.Matrix;
                        Matrix3D hitmtx3D = rdf_hitmtx.Matrix3D;
                        hitmtx3D.Invert();
                        mtx.Invert();
                        Matrix3D relationmtx = hitmtx3D.Solve(mtx);// mtx.Solve(hitmtx3D);//hitmtx3D.Solve(mtx);
                        relationmtx.Invert();
                        rdfMatrix = new RDF_Matrix(relationmtx);

                        RDF_MatrixMultiplication mtxMultiply = new RDF_MatrixMultiplication(rdfMatrix, rdf_hitmtx);
                        rdfTran.SetMatrix(mtxMultiply);
                        mtx.Invert();

                        Library.chkProfileOnSweptSolidSurface(hitswept, rdfTran);
                    }
                }
            }
            if (rdfMatrix == null)
            {
                rdfMatrix = new RDF_Matrix(mtx);
                rdfTran.SetMatrix(rdfMatrix);
            }

            #endregion

            if (GlobalVariables.IsPrototyping && Library.SelectedPrototype != null)
                Library.SelectedPrototype.ExecuteOperations(newStroke, mtx, polygon, rdfTran, rdfMatrix);
            else if (GlobalVariables.CurrentMode == Mode.SweptSolid && Library.isSweptSolid)
                CommandLibrary.ExecuteSweptSolid(newStroke, mtx, polygon, rdfTran, rdfMatrix);
            else if (hitobj is RDF_Face2D && polygon.IsClosed && GlobalVariables.CurrentMode != Mode.Array)
                CommandLibrary.ExecuteAddHole(newStroke, hitobj, oriMtx);
            else if (GlobalVariables.CurrentMode == Mode.AddOneDimension) // add one dimension
                CommandLibrary.ExecuteAddDimension(newStroke, polygon, rdfTran);
            else if (GlobalVariables.CurrentMode == Mode.Extrusion && GlobalVariables.IsRecordingPrototype)
            {
                if (CommandLibrary.SweptSolid.SweptProfile != null && CommandLibrary.SweptSolid.Direction != null)
                {
                    Operation_Relation protOper = new Operation_Relation(OperationName.Extrude);
                    protOper.AddInputRelation(CommandLibrary.SweptSolid.SweptProfile.BackRelations.Last()); // the last one must be swept area profile outer
                    if(CommandLibrary.SweptSolid.Direction is RDF_Dim1)
                        protOper.AddInputRelation(((RDF_Dim1)CommandLibrary.SweptSolid.Direction).BackRelations.Last());
                    else if(CommandLibrary.SweptSolid.Direction is RDF_Transformation)
                    {

                    }
                    protOper.SetRecordedOutPut(CommandLibrary.SweptSolid);
                    Library.Prototypes.Last().AddOperation(protOper);
                }
            }
            else // add a line (and a surface)
                CommandLibrary.ExecuteSketchLine(newStroke, mtx, polygon, rdfTran);

            Library.setToDefault(false);
        }

        /// <summary>
        /// Call back after the stroke is modified
        /// </summary>
        /// <param name="oriStroke"></param>
        /// <param name="mtx">the new stroke matrix</param>
        /// <param name="hitobj"></param>
        internal static void inkCanv_OnStrokeModified(CustomStroke oriStroke, ref Matrix3D mtx, object hitobj)
        {
            // find the original RDF object
            if (RDFWrapper.All_RDF_Objects.ContainsKey(oriStroke.GUID))
            {
                RDF_Abstract existingObject = RDFWrapper.All_RDF_Objects[oriStroke.GUID];

                if (existingObject != null && existingObject is RDF_Polygon3D)
                {
                    RDF_Polygon3D oriPolygon = (RDF_Polygon3D)existingObject;
                    // clean the old relations of the polygon
                    //rdfPolygon.CleanAllParts();

                    RDF_Polygon3D newPolygon = new RDF_Polygon3D();
                    oriStroke.GUID = newPolygon.GUID;

                    if (mtx.HasInverse)
                        mtx.Invert();

                    // add the new parts into the polygon
                    Point3D stpt = oriStroke.Primitives.CreateRDFSegments(newPolygon);
                    newPolygon.IsCircle = oriStroke.isCircle;

                    Point3D transformedStPt = mtx.Transform(stpt);
                    // update the matrix due to the shifts made by cornucopia
                    mtx.OffsetX = transformedStPt.X;
                    mtx.OffsetY = transformedStPt.Y;
                    mtx.OffsetZ = transformedStPt.Z;

                    #region Transformation matrix for moving the polygon back to the global location
                    //      Since the location will be moved to the origin
                    if (oriPolygon.HasTransform)
                    {
                        //stpt = rdfPolygon.Transform.Matrix.Matrix3D.Transform(stpt);
                        RDF_Transformation rdftransform = oriPolygon.Transform;
                        RDF_Transformation newTransform = new RDF_Transformation();

                        newTransform.SetTransformGeom(newPolygon);
                        if (rdftransform.Matrix is RDF_Matrix)
                        {
                            RDF_Matrix rdfMatrix = (RDF_Matrix)rdftransform.Matrix.Clone();
                            rdfMatrix.SetTranslate(new Point3D(mtx.OffsetX, mtx.OffsetY, mtx.OffsetZ));
                            newTransform.SetMatrix(rdfMatrix);
                        }
                        else if (rdftransform.Matrix is RDF_MatrixInverse)
                        {
                            RDF_MatrixInverse rdfinverseMatrix = (RDF_MatrixInverse)rdftransform.Matrix;
                        }
                        else if (rdftransform.Matrix is RDF_MatrixMultiplication)
                        {
                            RDF_MatrixMultiplication rdfMtxMulti = (RDF_MatrixMultiplication)rdftransform.Matrix;

                            if (rdfMtxMulti.FirstMatrix is RDF_Matrix)
                            {
                                Matrix3D hitmtx3D = rdfMtxMulti.SecondMatrix.Matrix3D;
                                hitmtx3D.Invert();
                                mtx.Invert();
                                Matrix3D relationmtx = hitmtx3D.Solve(mtx);// mtx.Solve(hitmtx3D);//hitmtx3D.Solve(mtx);
                                relationmtx.Invert();
                                RDF_Matrix rdfMatrix = new RDF_Matrix(relationmtx);

                                RDF_MatrixMultiplication mtxMultiply = new RDF_MatrixMultiplication(rdfMatrix, rdfMtxMulti.SecondMatrix);
                                newTransform.SetMatrix(mtxMultiply);
                                mtx.Invert();
                            }
                        }
                        // remove the original polygon from the matrix
                        //rdfMatrix.RelatedTransforms.Remove(oriPolygon.Transform);
                    }
                    #endregion

                    // render the polygon.. all the polygons will associate to a transform at this point
                    if (newPolygon.HasTransform)
                    {
                        //WirePath line = rdfPolygon.RenderConceptLine(viewport);//RDF_Dim1.RenderConceptLine(rdfPolygon.Transform, viewport);
                        WirePath line = RDF_Dim1.RenderConceptLine(newPolygon.Transform, Library.ViewPort3D);
                        if (line != null)
                            oriStroke.UpdateCvtPoints(line, mtx, newPolygon.IsClosed);

                        Step pstep = StepLibrary.modifyPstepLine(oriPolygon, newPolygon);

                        bool hasface = false;

                        newPolygon.BackRelations.Clear();
                        // render the models depends on what kind of geometry connecting to the polygon
                        foreach (var br in oriPolygon.BackRelations)
                        {
                            var cbr = br.Clone();
                            switch (br.Name)
                            {
                                case BackRelationName.Dim1_Polygon_Direction:
                                    if (br.RelatedTo is RDF_Polygon3D)
                                    {
                                        var oldoffsetPl = (RDF_Polygon3D)br.RelatedTo;
                                        var widthPl = (RDF_Polygon3D)((RDF_Polygon3D)oldoffsetPl).WidthFrom;
                                        if (widthPl != null)
                                        {
                                            var offsetMtx3D = oldoffsetPl.Transform.GetMatrix3D();
                                            var newoffsetPls = CommandLibrary.CreateOffsetFromDirection(offsetMtx3D, newPolygon, widthPl);
                                            if (newoffsetPls.Count > 0)
                                            {
                                                var newoffsetpl = newoffsetPls.First();
                                                var brFaceOuter = oldoffsetPl.BackRelations.Find(tbr => tbr.Name == BackRelationName.Dim1_Face_Outer);
                                                var cbrFaceOuter = brFaceOuter.Clone();
                                                ModifyFaceOuter(oldoffsetPl, newoffsetpl, pstep, brFaceOuter, cbrFaceOuter);
                                                oldoffsetPl.Remove(true);
                                            }

                                        }
                                    }
                                    break;
                                case BackRelationName.Dim1_Face_Outer:
                                    hasface = ModifyFaceOuter(oriPolygon, newPolygon, pstep, br, cbr);

                                    break;
                                case BackRelationName.Dim1_SweptSolid_Profile_Outer:
                                    RDF_SweptAreaSolid oriSolid = (RDF_SweptAreaSolid)br.RelatedTo;
                                    //oriSolid.Remove();    
                                    bool hasexistingGeom = Library.SolidModelGroup.Children.Remove(oriSolid.Geometry);

                                    RDF_SweptAreaSolid sweptsolid = new RDF_SweptAreaSolid();
                                    //if(RDFWrapper.All_RDF_Objects.ContainsKey(sweptsolid.GUID))
                                    {
                                        sweptsolid.SetSweptProfile(newPolygon);
                                        sweptsolid.SetDirection(oriSolid.Direction);
                                        sweptsolid.BackRelations = oriSolid.BackRelations;

                                        RDF_Transformation solidTransform = new RDF_Transformation();
                                        if (oriSolid.IsInArray)
                                        {
                                            solidTransform.SetMatrix(oriSolid.Transform.Matrix);
                                            sweptsolid.IsInArray = true;
                                        }
                                        else
                                        {
                                            solidTransform.SetMatrix(newPolygon.Transform.Matrix);
                                        }
                                        solidTransform.SetTransformGeom(sweptsolid);

                                        if (!sweptsolid.BackRelations.Exists(tbr => tbr.Name == BackRelationName.Dim3_BooleanProduct2))
                                            sweptsolid.RenderConceptMesh(Library.SolidModelGroup);
                                        else
                                        {
                                            // rerender the boolean object
                                            var boolrelation = sweptsolid.BackRelations.Find(tbr => tbr.Name == BackRelationName.Dim3_BooleanProduct2);
                                            var boolObj = (RDF_Boolean3D)boolrelation.RelatedTo;
                                            boolObj.SetSecondObject(sweptsolid);
                                            boolObj.RenderConceptMesh();
                                        }
                                        //pstep.PreModel = sweptsolid;
                                        pstep.ModifiedObjs.Add(oriSolid);
                                    }
                                    //newPolygon.BackRelations.Add(cbr);
                                    break;
                            }
                        }

                        // try to make a face if it's not closed originally
                        newPolygon.CheckIfClosed();
                        if (newPolygon.IsClosed && line != null && !hasface)
                        {
                            RDF_Face2D face = new RDF_Face2D(newPolygon);
                            pstep.ObjectType = pstep.ObjectType | StepObjectTypeEnum.MeshModel;
                            face.RenderShapeMesh(pstep, Library.SolidModelGroup);
                            face.Select();
                        }
                        else if (!hasface)
                        {
                            newPolygon.Select();
                            System.Diagnostics.Debug.WriteLine("polygon 2");
                        }

                        if (oriPolygon.Controlling.Count > 0)
                        {
                            // Todo: need to find the right constraint instead of use the first one!!
                            var constraint = oriPolygon.Controlling.First();
                            constraint.ControllingObject = newPolygon;
                            newPolygon.Controlling = new List<GeomLib.Constraint.Constraint>();
                            newPolygon.Controlling.Add(constraint);
                            if (constraint is GeomLib.Constraint.GeometricConstraint)
                            {
                                var path = Library.OffsetPathPoints((GeomLib.Constraint.GeometricConstraint)constraint, newPolygon.pathgeom);
                                Library.MoveConstraintObjects(path, (GeomLib.Constraint.GeometricConstraint)constraint);
                                oriPolygon.Remove(isShallowRemove: true, resettingControllingObject: false);
                            }
                        }
                        else
                            oriPolygon.Remove(isShallowRemove: true);
                        //UIUtility.PSteps.Add(pstep);
                        //UIUtility.NSteps = new List<Step>();
                    }

                    Library.setToDefault(false);
                    CommandLibrary.SetDefaultCommand();
                }
            }
        }

        private static void InitializeExtrusion(Point newpt)
        {
            if (GlobalVariables.SelectedItem is RDF_Face2D)
            {
                bool isStarting = false;
                if (CommandLibrary.SweptSolid.Direction == null)
                {
                    moveCursor(newpt);
                    CommandLibrary.CreateSweptSolidCommand();
                    isStarting = true;
                }
                else
                    CommandLibrary.ProcessExtrusionCommand();

                CommandLibrary.SweptSolid.RenderConceptMesh(Library.SolidModelGroup);
                CommandLibrary.SweptSolid.RenderConnectedSweptSolid(Library.SolidModelGroup);
                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                pstep.PreModel = CommandLibrary.SweptSolid;
                pstep.PStep = StepTypeEnum.Add;
                UIUtility.PSteps[UIUtility.PSteps.Count - 1] = pstep;
            }
            else if (GlobalVariables.SelectedItem is RDF_Polygon3D)
            {
                GlobalVariables.CurrentMode = Mode.AddOneDimension;
                CommandLibrary.CreateOffsetShapeCommand();
            }
            else if (GlobalVariables.SelectedItem is RDF_SweptAreaSolid)
            {
                // modify the current swept area solid
                CommandLibrary.SweptSolid = (RDF_SweptAreaSolid)GlobalVariables.SelectedItem;
                CommandLibrary.ProcessExtrusionCommand();
                CommandLibrary.SweptSolid.RenderConceptMesh(Library.SolidModelGroup);
                CommandLibrary.SweptSolid.RenderConnectedSweptSolid(Library.SolidModelGroup);
                Step pstep = new Step();
                pstep.ObjectType = StepObjectTypeEnum.MeshModel;
                pstep.PreModel = CommandLibrary.SweptSolid;
                pstep.PStep = StepTypeEnum.Add;
                UIUtility.PSteps[UIUtility.PSteps.Count - 1] = pstep;
            }
        }

        private static bool ModifyFaceOuter(RDF_Polygon3D oriPolygon, RDF_Polygon3D newPolygon, Step pstep, BackRelation br, BackRelation cbr)
        {
            RDF_Face2D oriface = (RDF_Face2D)br.RelatedTo;
            bool hasExistingGeom = Library.SolidModelGroup.Children.Remove(oriface.Geometry);

            RDF_Face2D newface = new RDF_Face2D(newPolygon, "", !oriface.IsInArray);

            if (oriface.IsInArray)
            {
                var oritransform = oriface.Transform;
                oritransform.SetTransformGeom(newface);
                newface.IsInArray = true;
                newface.InstantiatedFrom = newPolygon.Face;
                newPolygon.Face.Constraint.ConstraintObjects.Add(newface);
            }
            else
            {
                newface.Constraint = oriPolygon.Face.Constraint.Clone();
                newface.Constraint.ConstraintObjects.Clear();
                newface.Constraint.Subject = newface;
            }
            // don't need to render if this face is used for boolean substration
            if (hasExistingGeom)
                newface.RenderShapeMesh(pstep, Library.SolidModelGroup, oriface.IsInArray);
            newface.Select();

            cbr.RelatedTo = newface;
            return true;
        }

    }
}
